{* Delivery informations *}
{block name='frontend_widgets_delivery_infos'}
	<div class="delivery_container">
	{if $sArticle.shippingfree}
	    <p class="red">
	    	<strong>{se name="DetailDataInfoShippingfree"}{/se}</strong>
	    </p>
	{/if}
	<div class="delivery_container">
            <div class="status2"></div>
            <p class="deliverable1">
                {s name="DetailDataInfoInstock"}Sofort versandfertig, Lieferzeit ca. 1-3 Werktage{/s}
            </p>
        </div>


	</div>
{/block}
