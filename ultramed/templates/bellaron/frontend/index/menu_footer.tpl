{* Footer menu *}

<ul class="footer_menu">
	{foreach from=$sMenu.gBottom item=item  key=key name="counter"}
	<li>
		<a href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description}" {if $item.target}target="{$item.target}"{/if}>
			{$item.description}
		</a>
		</li>

	{/foreach}
</ul>

