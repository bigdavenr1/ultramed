{**
 * Shopware 3.5 Template
 *
 * @category   Shopware
 * @package    Shopware_Template
 * @subpackage Shopware_Template_Frontend
 * @copyright  Copyright (c) 2010 shopware AG (http://www.shopware.de)
 * @author     hl/shopware AG
 * @author     stp/shopware AG
 *}
{block name="frontend_index_start"}{/block}
<?xml version="1.0" ?>
{block name="frontend_index_doctype"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
{/block}
{block name='frontend_index_html'}
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{s name='IndexXmlLang'}de{/s}">
{/block}
{block name='frontend_index_header'}
{include file='./frontend/index/header.tpl'}
{/block}
<body {if $Controller}class="ctl_{$Controller}"{/if}>

{* Message if javascript is disabled *}
{block name="frontend_index_no_script_message"}
<noscript>
	<div class="notice bold center noscript_notice">
		{s name="IndexNoscriptNotice"}{/s}
	</div>
</noscript>
{/block}

{block name='frontend_index_before_page'}{/block}

<div id="top"></div>
<div class="container_20">

	{* Shop header *}
	{block name='frontend_index_navigation'}
	<div id="header">

		{* Language and Currency bar *}
		{block name='frontend_index_actions'}
			{include file='frontend/index/actions.tpl'}
		{/block}

		{* Shop logo *}
		{block name='frontend_index_logo'}
		<div id="logo" class="grid_5">
			<a href="{url controller='index'}" title="{$sShopname} - {s name='IndexLinkDefault'}{/s}">{$sShopname}</a>
		</div>
		{/block}


		{block name='frontend_index_navigation_inline'}
		    {include file='frontend/index/categories_top.tpl'}
		{/block}



	</div>
</div>
	<div class="blue_line"></div>

	{/block}






	{* Content section *}
	<div id="content">
		<div class="inner">

			{* Content top container *}
			{block name="frontend_index_content_top"}{/block}



			{* Main content *}
			{block name='frontend_index_content'}{/block}



			<div class="clear">&nbsp;</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>



{* Footer *}
    {block name="frontend_index_footer"}
        {include file='frontend/index/footer.tpl'}
    {/block}

</body>
</html>