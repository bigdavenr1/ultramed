<div id="footer" class="first last">

<div class="bottom">
<div style="float:none !important;clear:both: !important;display:block;border:0">&nbsp;</div>
{block name='frontend_index_footer_menu'}
        {include file='frontend/index/menu_footer.tpl'}
    {/block}

	<div style="width:26%;">
	<b>Bellaron wird in Deutschland
produziert - Made in Germany</b>
<br/><br/>
UltraMed Cosmetics GmbH<br/>
D-97422 Schweinfurt
	</div>
	<div style="width:20%">
    <b>Bellaron ist ein Naturprodukt
und dermatologisch geprüft:</b>
<br/>
<img style="background:none;margin-top:5px;width:164px;" src="/media/image/siegel_invers.png"/>
    </div>
    <div style="border:0;padding-right:0 !important;width:auto">
        <b>Zahlungsarten</b><br/>
        <img src="/templates/bellaron/frontend/_resources/images/Lastschrift.jpg" style="float:right;margin-left:3px;margin-top:3px;"/>
        <img src="/templates/bellaron/frontend/_resources/images/secupay.jpg" style="margin-top:3px;"/><br/>
        <img src="/templates/bellaron/frontend/_resources/images/PayPal.jpg" style="float:left;margin-right:3px;margin-top:4px;"/>
        <img src="/templates/bellaron/frontend/_resources/images/Kreditkarte.jpg" style="margin-top:3px;"/><br style="float:none;"/><br/><br/>

    © 2015 UltraMed Cosmetics GmbH

    </div>
<div style="float:none !important;clear:both: !important;display:block;border:0">&nbsp;</div>
	</div>
</div>