{extends file='frontend/index/index.tpl'}

{* Empty sidebar left *}
{block name='frontend_index_content_left'}{/block}

{* Main content *}
{block name='frontend_index_content'}
<div class="container_20" style="width:1000px;float:none;margin:auto;overflow: visible">
<div class="grid_20 password" style="margin-top: 20px;">

	{* Error messages *}
	{block name='frontend_account_error_messages'}
		{include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
	{/block}
	
	{* Success message *}
	{if $sSuccess}
		{block name='frontend_account_password_success'}
	    <div class="success">
	    	<strong>{se name="PasswordInfoSuccess"}{/se}</strong>	
	    </div>
	    <p>
	   		<a href="javascript:history.back();" class="button-left large"><span>{se name="LoginBack"}{/se}</span></a>
	    </p>
	    {/block}
	{else}
	
	{* Recover password form *}
	{block name='frontend_account_password_form'}
	<form name="frmRegister" method="post" action="{url action=password}">	    
		<h2 class="headingbox_dark largesize" style="color: #2BADC9;">{se name="PasswordHeader"}{/se}</h2>
	    <div class="outer">
	        <fieldset>
	            <p>
	                <label>{se name="PasswordLabelMail"}{/se}</label>
	                <input name="email" type="text" id="txtmail" class="text" /><br />
	            </p>
	            <p class="description">{se name="PasswordText"}{/se}</p>
	        </fieldset>
	        
	        <p class="buttons">
	            <a href="javascript:history.back();" class="button-left large">
	               <style type="text/css">
                        .buttons a span{
                            background: none !important; 
                        }
                    </style>
	               {se name="PasswordLinkBack"}{/se}
               </a>
	            <input type="submit" class="button-right large" value="{s name="PasswordSendAction"}Passwort anfordern{/s}" style="background: #2BADC9;" />
	            <div class="clear">&nbsp;</div>
	        </p>
	    </form>
	    </div>
	{/block}
	{/if}
</div>
</div>
<div class="doublespace">&nbsp;</div>
{/block}