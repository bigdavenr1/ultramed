{extends file='frontend/account/index.tpl'}

{* Breadcrumb *}
{block name='frontend_index_start' append}
	{$sBreadcrumb[] = ['name'=>"{s name='ChangePaymentTitle'}{/s}", 'link'=>{url}]}
{/block}

{* Main content *}
{block name='frontend_index_content'}
<div class="" style="width:1000px;float:none;margin:auto;overflow: visible"> 
<div id="center" class="grid_16 first register" style="margin-top: 20px">
	
	{* Error messages *}
	{block name='frontend_account_payment_error_messages'}
		{include file="frontend/register/error_message.tpl" error_messages=$sErrorMessages}
	{/block}
	
	{* Payment form *}
	<form name="frmRegister" method="post" action="{url action=savePayment sTarget=$sTarget}" class="payment">
	
		{include file='frontend/register/payment_fieldset.tpl' form_data=$sFormData error_flags=$sErrorFlag payment_means=$sPaymentMeans}
		
		{block name="frontend_account_payment_action_buttons"}
		<div class="actions">
			{if $sTarget}
			<a class="button-left large left" href="{url controller=$sTarget}" title="{s name='PaymentLinkBack'}{/s}">
    			 <style type="text/css">
                    a span{
                        background: none !important; 
                    }
                </style>
				{se name="PaymentLinkBack"}{/se}
			</a>
			{/if}
			<input type="submit" value="{s name='PaymentLinkSend'}{/s}" class="button-right large right" style="background: #2BADC9;" />
		</div>
		{/block}
	</form>
	<div class="space">&nbsp;</div>
</div>
</div>
{/block}