{extends file='frontend/index/index.tpl'}

{block name='frontend_index_header'}
	{include file="frontend/detail/header.tpl"}
{/block}

{* Main content *}
{block name='frontend_index_content'}
	<div id="detail" class="grid_16 first last" style="width:1000px;margin:auto;float:none;display:block;padding-top:15px;padding-bottom:30px;">


		{* General detailbox *}
		<div id="detailbox"><!-- detailbox -->

			{* Detailbox left *}
			<div id="img" class="grid_6 first">

				<div class="wrapper">

					{* Images *}
					{include file="frontend/detail/image.tpl"}

				</div>
			</div>

			{* Detailbox middle *}
			<div id="detailbox_middle" class="grid_4">
				{* Article name *}
				{block name='frontend_detail_index_name'}
					<h1>{$sArticle.articleName}</h1>
				{/block}

				{* Supplier name *}

		    	 {* Article description *}
                            {block name="frontend_detail_index_tabs_description"}
                                {include file="frontend/detail/description.tpl"}
                            {/block}

				{* Caching article details for future use *}
				{if $sArticle.sBundles || $sArticle.sRelatedArticles && $sArticle.crossbundlelook || $sArticle.sVariants}
					<div id="{$sArticle.ordernumber}" class="displaynone">
				        {include file="frontend/detail/data.tpl" sArticle=$sArticle}
				    </div>
				{/if}



				{* Article data *}
				<div id="article_details">
					{block name='frontend_detail_index_data'}
						{include file="frontend/detail/data.tpl" sArticle=$sArticle sView=1}
					{/block}
				</div>
				{block name='frontend_detail_index_after_data'}{/block}
			</div>

			{* Configurator table *}
			{if $sArticle.sConfigurator && $sArticle.sConfiguratorSettings.type==2}<div class="clear">&nbsp;</div>{/if}

			{* Detailbox right *}

			{* Configurator table // div buybox *}
			{if $sArticle.sConfigurator && $sArticle.sConfiguratorSettings.type==2}<div class="grid_16 first last" id="buybox">{else}<div class="right" id="buybox">{/if}
				<div id="detail_more"></div>

				{* Article notification *}




				{* Include buy button and quantity box *}
				{block name="frontend_detail_index_buybox"}
					{include file="frontend/detail/buy.tpl"}
				{/block}



			</div><!-- //buybox -->
			<div class="space">&nbsp;</div>
		</div> <!-- //detailbox -->


		<div class="clear">&nbsp;</div>


	</div>
{/block}