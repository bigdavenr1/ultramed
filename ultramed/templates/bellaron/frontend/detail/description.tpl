{block name="frontend_detail_description"}
<div id="description">

	{* Headline *}


	{* Properties *}
	{if $sArticle.sProperties}
		{block name='frontend_detail_description_properties'}
		<table cellspacing="0">
			{foreach from=$sArticle.sProperties item=sProperty}
				<tr>
					<td>
						{$sProperty.name}
					</td>
					<td>
						{$sProperty.value}
					</td>
				</tr>
			{/foreach}
		</table>
		{/block}
	{/if}

	{* Article description *}
	{block name='frontend_detail_description_text'}
	{$sArticle.description_long|replace:"<table":"<table id=\"zebra\""}
	{/block}


	{* Links *}
	{block name='frontend_detail_description_links'}
	{if $sArticle.sLinks}
		<div class="space">&nbsp;</div>



	{/if}
	{/block}

    {* Supplier *}



</div>
{/block}