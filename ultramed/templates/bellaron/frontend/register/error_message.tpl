{block name='frontend_register_error_messages'}
{if $error_messages}
	<div class="error" style="width:1000px;float:none;margin:auto;"><strong>{s name='RegisterErrorHeadline'}{/s}</strong><br />
		{foreach from=$error_messages item=errorItem}{$errorItem}<br />{/foreach}
	</div>
{/if}
{/block}