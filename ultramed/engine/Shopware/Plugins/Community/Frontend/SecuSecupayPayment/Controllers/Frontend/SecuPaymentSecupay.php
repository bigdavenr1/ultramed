<?php
/**
 * Secupay Payment Plugin Class
 *
 */

require dirname(__FILE__) . '/../../secupay_api.php';

/**
 * Secupay Payment Plugin Class
 *
 * @package Secupay
 * @copyright 2013 Secucard Projekt KG
 */
class Shopware_Controllers_Frontend_SecuPaymentSecupay extends Shopware_Controllers_Frontend_Payment {

    /**
     * indexAction is called first
     */
    public function indexAction() {
        switch ($this->getPaymentShortName()) {
            case 'secupay_debit':
                return $this->redirect(array('action' => 'gateway', 'forceSecure' => true));
            case 'secupay_creditcard':
                return $this->redirect(array('action' => 'gateway', 'forceSecure' => true));
            case 'secupay_invoice':
                return $this->redirect(array('action' => 'gateway', 'forceSecure' => true));
            default:
                return $this->redirect(array('controller' => 'checkout'));
        }
    }

    /**
     * Method used to create payment request to Secupay API
     * Forwards user to error page or displays IFrame
     */
    public function gatewayAction() {
        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        $log = $pluginConfig->sSECUPAY_ALL_SWITCH_DEBUGMODE;

        $payment_type = $this->getPaymentType();

        if (empty($payment_type)) {
            return $this->forward('error', null, null, array('secupay_error_msg' => 'payment type not available'));
        }

        $router = $this->Front()->Router();
        $userData = $this->getUser();

        $data = array();
        $data['apikey'] = $pluginConfig->sSECUPAY_APIKEY;
        $data['payment_type'] = $payment_type;
        $amount = intval(strval($this->getAmount() * 100));
        $data['amount'] = $amount;

        // uniqueId will be used by us to find the created secupay_transactions record
        $uniqueId = $this->createPaymentUniqueId();
        $data['shopware_unique_id'] = $uniqueId;
        $data['purpose'] = $this->getPurpose();
        $data['order_number'] = $this->getOrderNumber();
        
        $locale = Shopware()->Shop()->getLocale()->getLocale();
        
        if(isset($locale)) {
            if($locale == 'en_GB') {
               $locale = 'en_US'; 
            }
            $data['language'] = $locale;
        }    
        
        $data['title'] = $userData["billingaddress"]["salutation"];

        $data['firstname'] = $userData["billingaddress"]["firstname"];
        $data['lastname'] = $userData["billingaddress"]["lastname"];
        $data['street'] = $userData["billingaddress"]["street"];
        $data['housenumber'] = $userData["billingaddress"]["streetnumber"];
        $data['zip'] = $userData["billingaddress"]["zipcode"];
        $data['city'] = $userData["billingaddress"]["city"];

        $data['email'] = $userData["additional"]["user"]["email"];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];

        $data['apiversion'] = secupay_api::get_api_version();
        $info = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->getInfo();
        $data['modulversion'] = $info['version'];

        $data['shop'] = "Shopware";
        $data['shopversion'] = "Shopware" . Shopware()->Config()->Version;

        if ($pluginConfig->sSECUPAY_ALL_SWITCH_TESTMODE) {
            $data['demo'] = 1;
        }

        $data['company'] = $userData["billingaddress"]["company"];
        $data['dob'] = $userData["billingaddress"]["birthday"];
        $data['telephone'] = $userData["billingaddress"]["phone"];
        $data['fax'] = $userData["billingaddress"]["fax"];
        $data['country'] = $userData['additional']['countryBilling']['countryiso'];
        if(empty($data['country'])) {
            $data['country'] = $userData['additional']['country']['countryiso'];
        }

        
        $delivery_address = new stdClass();
        $delivery_address->firstname = $userData["shippingaddress"]["firstname"];
        $delivery_address->lastname = $userData["shippingaddress"]["lastname"];
        $delivery_address->street = $userData["shippingaddress"]["street"];
        $delivery_address->housenumber = $userData["shippingaddress"]["streetnumber"];
        $delivery_address->zip = $userData["shippingaddress"]["zipcode"];
        $delivery_address->city = $userData["shippingaddress"]["city"];
        $delivery_address->country = $userData['additional']['countryShipping']['countryiso'];
        $delivery_address->company = $userData["shippingaddress"]["company"];
        
        if ($pluginConfig->sSECUPAY_ALL_SWITCH_SEND_LAST_HASH) {
            $last_hash = $this->getLastTransactionHash();
            if(!empty($last_hash)) {
                $data['hash_ref_payment_data'] = $last_hash;
            }
        }        
        
        $data['delivery_address'] = $delivery_address;

        $data['basket'] = json_encode($this->getBasketInformation());

        $data['url_push'] = $router->assemble(array('action' => 'notify', 'forceSecure' => true));
        $data['url_push'] .= '?id=' . $uniqueId;
        $data['url_success'] = $router->assemble(array('action' => 'return', 'forceSecure' => true));
        $data['url_success'] .= '?id=' . $uniqueId;
        $data['url_failure'] = $router->assemble(array('action' => 'failure', 'forceSecure' => true));
        $data['url_failure'] .= '?id=' . $uniqueId;

        $sp_api = new secupay_api($data);
        $api_return = $sp_api->request();

        $_req_data = addslashes(json_encode($data));
        $_ret_data = addslashes(json_encode($api_return));

        $status = $api_return->get_status();
        $api_hash = $api_return->get_hash();

        $msg = utf8_decode(addslashes($api_return->get_error_message()));
        if (empty($api_return)) {
            $msg = "Verbindungs-Fehler";
        }

        $sql = "INSERT INTO secupay_transactions (
            req_data,
            ret_data,
            payment_method,
            `hash`,
            unique_id,
            ordernr,
            status,
            amount,
            msg,
            rank,
            created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 99, NOW() )";

        Shopware()->Db()->query($sql, array(
            $_req_data,
            $_ret_data,
            $payment_type,
            $api_hash,
            $uniqueId,
            $this->getOrderNumber(),
            $status,
            $amount,
            $msg
        ));

        if (isset($api_return) && $api_return instanceof secupay_api_response && $api_return->check_response()) {
            $this->View()->gatewayUrl = $api_return->get_iframe_url();
        } else {
            // if there was an error
            secupay_log::log($log, $api_return->get_error_message());
            return $this->forward('error', null, null, array('secupay_error_msg' => $api_return->get_error_message_user()));
        }
    }

    /**
     * Method that is called after iFrame is completed
     * If the payment is accepted, then the order is created in shopware
     */
    public function returnAction() {
        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        $log = $pluginConfig->sSECUPAY_ALL_SWITCH_DEBUGMODE;
        
        $basket = $this->getBasket();
        if (empty($basket)) {
            //Bestellung bereits abgeschlossen            
            return $this->redirect(array('controller' => 'index'));
        }

        //$apikey = $pluginConfig->sSECUPAY_APIKEY;

        $payment_type = $this->getPaymentType();
        if (empty($payment_type)) {
            secupay_log::log($log, 'returnAction - error: payment type is not set');
            return $this->forward('error', null, null, array('secupay_error_msg' => 'payment type is not set'));
        }

        $amount_sent = intval(strval($this->getAmount() * 100));
        $uniqueId = $this->Request()->getParam('id');

        $sql = 'SELECT hash FROM secupay_transactions WHERE unique_id = ? AND status!=-1 ORDER BY created DESC LIMIT 1';
        $hash = Shopware()->Db()->fetchOne($sql, array($uniqueId));

        // check if request is valid
        if (empty($hash)) {
            return $this->forward('error', null, null, array('secupay_error_msg' => 'Zahlung derzeit nicht möglich, bitte wenden Sie sich an den Shopbetreiber (Validierungs-Fehler)'));
        }

        $response = $this->getTransactionStatusResponse($hash);
        if (!$response || empty($response->status) || ( $response->status != 'accepted' && $response->status != 'authorized' )) {
            secupay_log::log($log, 'returnAction - error: hash status not accepted');
            return $this->forward('error', null, null, array('secupay_error_msg' => 'Zahlung derzeit nicht möglich, bitte wenden Sie sich an den Shopbetreiber (Status Validierung-Fehler)'));
        }
        $response_amount = (int)$response->amount;

        // check if amount matches
        if ($amount_sent != $response_amount) {
            secupay_log::log($log, 'returnAction - error: amount does not match');
            secupay_log::log($log, 'returnAction - amount sent: '.$amount_sent);
            secupay_log::log($log, 'returnAction - amount received: '.$response_amount);
            return $this->forward('error', null, null, array('secupay_error_msg' => 'Zahlung derzeit nicht möglich, bitte wenden Sie sich an den Shopbetreiber (Betrag Validierung-Fehler)'));
        }

        // everything is ok
        // set payment status to waiting (waiting for push action)
        $paymentStatus = $pluginConfig->sSECUPAY_STATUS_WAITING;
        // hash will be used as transactionId and uniqueId will be used as paymentUniqueId
        $order_number = $this->getOrderNumber();
        if (empty($order_number)) {
            $order_number = $this->saveOrder($hash, $uniqueId);
        }
        $this->savePaymentStatus($hash, $uniqueId, $paymentStatus);
        
        if(!empty($response->trans_id)) {
            $secupay_transaction_id = $response->trans_id;
        } else {
            $secupay_transaction_id = 0;
        }
        $sql_update = "UPDATE secupay_transactions SET status = ?, ordernr = ?, trans_id = ? WHERE unique_id = ? AND hash = ?";
        Shopware()->Db()->query($sql_update, array($paymentStatus, $order_number, $secupay_transaction_id, $uniqueId, $hash));

        return $this->forward('finish', 'checkout', null, array('sUniqueID' => $uniqueId));
    }

    /**
     * This function is called when secupay denies the transaction
     */
    public function failureAction() {
        return $this->forward('error', null, null, array('secupay_error_msg' => 'Zahlung abgelehnt oder abgebrochen'));
    }

    /**
     * This action displays error template - template that shows user-friendly error message
     */
    public function errorAction() {
        $view = $this->View();
        $view->errorMsg = $this->Request()->getParam('secupay_error_msg', 'Zahlung derzeit nicht möglich, bitte wenden Sie sich an den Shopbetreiber (Verbindungs-Fehler)');
    }

    /**
     * Action to change payment status from push API
     *
     */
    public function notifyAction() {
        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        $log = $pluginConfig->sSECUPAY_ALL_SWITCH_DEBUGMODE;
        
        $low_ip = '91.195.150.1';
        $high_ip = '91.195.150.255';        
        // debug log
        $post_data = http_build_query($this->Request()->getPost());
        secupay_log::log($log, 'Notify action with parameters: '. http_build_query($post_data));
        secupay_log::log($log, 'Client IP : '. print_r($this->Request()->getClientIp(false), true));
        // accept notifyAction requests only from secupay
        /*
        //getClientIp does not return expected value since Shopware 4.2 
        $client_ip = ip2long($this->Request()->getClientIp(false));
        if($client_ip > ip2long($high_ip) || ip2long($low_ip) > $client_ip) {
        //if (!in_array($this->Request()->getClientIp(false), array($ip, $ip_2))) {
            die('ack=Disapproved&error=request+invalid&' . $post_data);
        }*/

        $apikey = $pluginConfig->sSECUPAY_APIKEY;

        $uniqueId = $this->Request()->getParam('id');
        if (empty($uniqueId)) {
            die('ack=Disapproved&error=no+matching+order+found+for+id&' . $post_data);
        }
        
        $hash_sent = $this->Request()->getParam('hash');
        $apikey_sent = $this->Request()->getParam('apikey');
        $payment_status = $this->Request()->getParam('payment_status');
        $status_desc = $this->Request()->getParam('status_description');
        
        if (empty($apikey_sent) || $apikey_sent !== $apikey) {
            die('ack=Disapproved&error=apikey+invalid&' . $post_data);
        }

        $row = Shopware()->Db()->fetchAll("SELECT status, ordernr, payment_method FROM secupay_transactions WHERE unique_id = ? AND hash = ? ORDER BY created DESC LIMIT 1", array($uniqueId, $hash_sent));
        if (empty($row) || !isset($row[0]['ordernr'])) {
            //matching transaction, but without order
            //save push to DB for transaction list
            if(!empty($row) && !empty($payment_status) && !empty($status_desc)) {
                $sql = "UPDATE secupay_transactions SET payment_status = ?, msg = ?, updated = NOW() WHERE unique_id = ? AND hash = ?";
                Shopware()->Db()->query($sql, array($payment_status, $status_desc, $uniqueId, $hash_sent));            
            }            
            
            die('ack=Disapproved&error=no+matching+order+found+for+hash&' . $post_data);
        }
        //$row_status = $row[0]['status'];
        //check if order still exists
        $sql = 'SELECT ordernumber FROM s_order WHERE ordernumber=?';
        $orderNumber = Shopware()->Db()->fetchOne($sql, array(
                $row[0]['ordernr']
        ));

        if(!isset($orderNumber) || empty($orderNumber)) {
            secupay_log::log($log, "Notify action - order {$row[0]['ordernr']} not found");
            die('ack=Disapproved&error=no+matching+order+found+for+transaction&' . $post_data);
        }        

        // if the status is other, than save it
        $new_status = $this->getShopwareStatus($payment_status, $row[0]['payment_method']);
        if (!$new_status) {
            die('ack=Disapproved&error=payment_status+not+supported&' . $post_data);
        }
        secupay_log::log($log, "Notify action successful for {$hash_sent} with status: {$payment_status} and shopware status: {$new_status}");
        // everything ok
        // update status in DB
        $sql = "UPDATE secupay_transactions SET status = ? WHERE unique_id = ? AND hash = ?";
        Shopware()->Db()->query($sql, array($new_status, $uniqueId, $hash_sent));
        $this->saveOrder($hash_sent, $uniqueId);
        $this->savePaymentStatus($hash_sent, $uniqueId, $new_status);

        if(!empty($payment_status) && !empty($status_desc)) {
            $sql = "UPDATE secupay_transactions SET payment_status = ?, msg = ?, updated = NOW() WHERE unique_id = ? AND hash = ?";
            Shopware()->Db()->query($sql, array($payment_status, $status_desc, $uniqueId, $hash_sent));            
        }
        
        die('ack=Approved&' . $post_data);
    }

    /**
     * Function to get transaction status from Secupay
     *
     * @return status response array or false as failed
     */
    private function getTransactionStatusResponse($hash) {

        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        if (empty($hash)) {
            return false;
        }
        $data = array();
        $data['apikey'] = $pluginConfig->sSECUPAY_APIKEY;
        $data['hash'] = $hash;
        $sp_api = new secupay_api($data, 'status');
        $api_return = $sp_api->request();

        if (isset($api_return) && $api_return instanceof secupay_api_response && $api_return->check_response()) {
            return $api_return->data;
        } else {
            return false;
        }
    }

    /**
     * Returns shopware_status_id for status identified by string
     *
     * @param string status
     * @param string payment_method
     * @return int status_id or 0
     */
    private function getShopwareStatus($status, $payment_method) {

        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        switch ($status) {
            case 'denied':
                return $pluginConfig->sSECUPAY_STATUS_DENIED;
            case 'accepted':
                switch ($payment_method) {
                    case 'creditcard':
                        return $pluginConfig->sSECUPAY_STATUS_ACCEPTED_CREDITCARD;
                    case 'debit':
                        return $pluginConfig->sSECUPAY_STATUS_ACCEPTED_DEBIT;
                    case 'invoice':
                        return $pluginConfig->sSECUPAY_STATUS_ACCEPTED_INVOICE;                      
                }
                //return $pluginConfig->sSECUPAY_STATUS_ACCEPTED;
            case 'authorized':
                return $pluginConfig->sSECUPAY_STATUS_AUTHORIZED;
            case 'void':
                return $pluginConfig->sSECUPAY_STATUS_VOID;
            case 'issue':
                return $pluginConfig->sSECUPAY_STATUS_ISSUE;                
        }
        
        return 0;
    }

    /**
     * Function that returns purpose of the payment
     *
     * @return string
     */
    private function getPurpose() {

        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        $_shop = $pluginConfig->sSECUPAY_ALL_PURPOSE_SHOPNAME;
        if (empty($_shop)) {
            $_shop = Shopware()->Config()->Shopname;
        }
        return $_shop . "|Bestellung vom " . date("d.m.Y") . "|Bei Fragen TEL 035955755055";
    }

    /**
     * Function that returns payment type for Secupay
     *
     * @return string paymentType
     */
    private function getPaymentType() {
        switch ($this->getPaymentShortName()) {
            case 'secupay_debit':
                return 'debit';
            case 'secupay_creditcard':
                return 'creditcard';
            case 'secupay_invoice':
                return 'invoice';
        }
        return '';
    }

    /**
     * Function that returns array of available paymentTypes
     *
     * @return array of strings
     */
    private function checkPaymentTypeAvailability($payment_type) {
        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
        $data = array();
        $data['apikey'] = $pluginConfig->sSECUPAY_APIKEY;
        $sp_api = new secupay_api($data, 'gettypes');
        $api_return = $sp_api->request();
        if (isset($api_return) && $api_return instanceof secupay_api_response && $api_return->check_response()) {
            return in_array($payment_type, $api_return->data);
        } else {
            return false;
        }    
    }

    /**
     * Function that returns basket information for the API
     *
     * @return array
     */
    private function getBasketInformation() {

        $basketcontents = $this->getBasket();
        
        foreach ($basketcontents['content'] as $item) {
            $product = new stdClass();
            $article = Shopware()->Modules()->Articles()->sGetArticleById($item['articleID']);
            $product->article_number = $item['ordernumber'];
            $product->name = $item['articlename'];
            $product->model = '';
            if(isset($article) && isset($article['ean'])) {
                $product->ean = $article['ean'];
            } else {
                $product->ean = '';
            }
            $product->quantity = $item['quantity'];
            $product->price = $item['priceNumeric'] * 100;
            $product->total = $item['priceNumeric'] * $item['quantity'] * 100;
            $product->tax = $item['tax_rate'];
            $products[] = $product;
        }
        return $products;
    }

    /**
     * Function that returns the last hash for the user with the same payment_type
     *
     * @return array
     */
    private function getLastTransactionHash() {
        $payment_name = $this->getPaymentShortName();
        $userData = $this->getUser();
        if (!empty($userData) && !empty($payment_name)) {
            $userID = $userData["billingaddress"]["userID"];
            if (!empty($userID)) {
                $sql_get_hash = "SELECT secupay_transactions.`hash` 
                        FROM secupay_transactions
                        INNER JOIN s_order ON s_order.ordernumber = secupay_transactions.ordernr AND s_order.transactionID = secupay_transactions.`hash`
                        INNER JOIN s_core_paymentmeans ON s_core_paymentmeans.id = s_order.paymentID
                        WHERE s_order.userID = ? AND s_core_paymentmeans.`name` = ? AND secupay_transactions.trans_id > 0
                        ORDER BY secupay_transactions.created DESC
                        LIMIT 1";
                $hash = Shopware()->Db()->fetchOne($sql_get_hash, array($userID, $payment_name));
                return $hash;
            }
        }

        return false;
    }

}