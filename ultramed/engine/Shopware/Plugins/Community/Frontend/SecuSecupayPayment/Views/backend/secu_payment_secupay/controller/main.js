Ext.define('Shopware.apps.SecuPaymentSecupay.controller.Main', {
 
    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
    extend: 'Ext.app.Controller',
 
    mainWindow: null,
 
    /**
     * Creates the necessary event listener for this
     * specific controller and opens a new Ext.window.Window
     * to display the subapplication
     *
     * @return void
     */
    init: function() {
        var me = this;
 
        me.mainWindow = me.getView('main.Window').create({
            listStore: me.getStore('List').load()
        });
 
        me.callParent(arguments);
        
        return me.mainWindow;
    }
});