Ext.define('Shopware.apps.SecuPaymentSecupay.view.iframe.Window', {
    extend: 'Enlight.app.Window',
    title: 'secupay iFrame',
    alias: 'widget.payment_secupay-iframe-window',
    border: false,
    autoShow: false,
    layout: 'border',
    height: 650,
    iframe: null, 
    width: 650,
 
    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    
    initComponent: function() {
        var me = this;
        
         me.iframe = Ext.create('Ext.container.Container', {
 
           title: 'iFrame',
 
           html: ''
 
        });

        me.items = [
            me.iframe
        ];
        
        me.callParent(arguments);        

    }, 
    
    SetUrl: function(url) {
        var me = this;

         me.iframe = Ext.create('Ext.container.Container', {
 
           title: 'iFrame',
 
           html: url
 
        });

        me.add(me.iframe);

    }
    
});