Ext.define('Shopware.apps.SecuPaymentSecupay.model.Config', {
 
    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend : 'Ext.data.Model',
 
    /**
     * The fields used for this model
     * @array
     */
    fields : [
        { name : 'apikey', type : 'string' },
        { name : 'secupay_url', type : 'string' },
    ],
  
    /**
     * Configure the data communication
     * @object
     */
    proxy : {
        type : 'ajax',
 
        api:{
            read:   '{url action=getSecuPaymentSecupayConfig}'
        },
 
        reader : {
            type : 'json',
            root : 'data'
        }
    }
});