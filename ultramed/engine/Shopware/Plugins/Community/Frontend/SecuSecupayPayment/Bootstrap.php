<?php
/**
 * Class that creates Payment Plugin for Shopware 4.0
 * Secu
 */

require dirname(__FILE__) . '/secupay_api.php';

/**
 * Secupay Payment Plugin Bootstrap
 *
 * @package Secupay
 * @copyright 2013 Secucard Projekt KG
 */
class Shopware_Plugins_Frontend_SecuSecupayPayment_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    /**
     * constant for logging to sp_log file
     */
    const log = true;

    /**
     * Payment Types array
     * @var Array
     */
    private $payment_types = array(
        'secupay_creditcard' => array(
                                    'name' => 'secupay_creditcard',
                                    'description' => 'Secupay Kreditkarte',
                                    'action' => 'secu_payment_secupay',
                                    'active' => 0,
                                    'position' => 1,
                                    'additionalDescription' => '<!-- paymentLogo -->
<img src="engine/Shopware/Plugins/Community/Frontend/SecuSecupayPayment/images/secupay-Kreditkarte.png"/>
<br/><br/>
<div id="payment_desc">
    Sie zahlen einfach und sicher mit Ihrer Kreditkarte.
</div>'
),
        'secupay_debit' => array(
                                'name' => 'secupay_debit',
                                'description' => 'Secupay Lastschrift',
                                'action' => 'secu_payment_secupay',
                                'active' => 0,
                                'position' => 2,
                                'template' => 'secupay_payment.tpl',            
                                'additionalDescription' =>  '<!-- paymentLogo -->
<img src="engine/Shopware/Plugins/Community/Frontend/SecuSecupayPayment/images/secupay-Lastschrift.png"/>
<br/><br/>
<div id="payment_desc">
    Der Rechnungsbetrag wird von Ihrem Konto abgebucht.
</div>'
),
        'secupay_invoice' => array(
                                'name' => 'secupay_invoice',
                                'description' => 'Secupay Rechnung',
                                'action' => 'secu_payment_secupay',
                                'active' => 0,
                                'position' => 3,
                                'template' => 'secupay_payment.tpl',            
                                'additionalDescription' =>  '<!-- paymentLogo -->
<img src="engine/Shopware/Plugins/Community/Frontend/SecuSecupayPayment/images/secupay-Rechnungskauf.png"/>
<br/><br/>
<div id="payment_desc">
    Sie erhalten eine Rechnung über den Rechnungsbetrag.
</div>'
)
        );

    /**
     * Method that returns plugin version
     *
     * @return string current version of plugin
     */
    public function getVersion() {
        return '2.0.5.1';
    }

    /**
     * Method that returns plugin label
     *
     * @return string current Label of plugin
     */
    public function getLabel() {
        return "Secupay Payment for Shopware";
    }

    /**
     * Method that returns information array about plugin
     *
     * @return associative array
     */
    public function getInfo() {
        return array(
            'version' => $this->getVersion(),
            'label' => $this->getLabel(),
            'autor' => 'secupay AG',
            'copyright' => 'Copyright © 2013, Secupay AG',
            // 'license' => '',
            'description' => 'Secupay - einfach.sicher.zahlen',
            // 'description_long' => 'Secupay - payments online and secure..',
            'support' => 'info@secupay.ag',
            'link' => 'http://www.secupay.ag/'
        );
    }
    
    /**
     * Method that returns capabilities array for plugin
     *
     * @return associative array
     */    
    public function getCapabilities() {
        return array(
            'install' => true,
            'update' => true,
            'enable' => true
        );
    }    

    /**
     * Function that installs the plugin
     *
     * @return array with success field
     */
    public function install() {
        try {
            $this->createEvents();
            $this->createDatabase();
            $this->createForm();
            //$this->createTranslations();
            $availablePaymentTypes = $this->getAvailablePaymentTypes();
            $this->createPaymentTypes($availablePaymentTypes);
            $this->createMenu();

            return array('success' => true, 'invalidateCache' => array('config', 'templates'));
            
        } catch (Exception $e) {
            secupay_log::log(self::log, 'install - Exception '.$e->getMessage());
            secupay_log::log(self::log, 'install - StackTrace '.$e->getTraceAsString());            
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    /**
     * Function that uninstalls the plugin
     *
     * @return array with success field
     */
    public function uninstall() {
        try {
            $this->disable();
            $this->removePaymentTypes();
            $this->removeDatabase();

            //return array('success' => true, 'invalidateCache' => array('config'));
            return true;
        } catch (Exception $e) {
            secupay_log::log(self::log, 'uninstall - Exception '.$e->getMessage());
            secupay_log::log(self::log, 'uninstall - StackTrace '.$e->getTraceAsString());            
            return array('success' => false, 'message' => $e->getMessage());
        }            
    }
    
    /**
     * Function that updates the plugin
     *
     * @return array with success field
     */
    public function update($version) {
        //TODO check behaviour with new plugin Name
        switch($version) {
            case '2.0.2':
            case '2.0.3':
            case '2.0.4':
                try {
                    secupay_log::log(self::log, 'update from Version '.$version);
                    $this->createEvents();
                    $this->createForm();
                    $this->createMenu();
                    $sql = "ALTER TABLE secupay_transactions ADD COLUMN `payment_status` varchar(255) collate latin1_general_ci default NULL AFTER `trans_id`;";
                    Shopware()->Db()->query($sql);                    
                } catch (Exception $e) {
                    secupay_log::log(self::log, 'update 2.0.4 - Exception '.$e->getMessage());
                    secupay_log::log(self::log, 'update 2.0.4 - StackTrace '.$e->getTraceAsString());
                    return false;
                }
            case '2.0.4.2':
                try {
                    //remove old hooks and events
                    $sql = "DELETE FROM `s_core_subscribes` WHERE `listener` LIKE '%SecupayPayment%'";
                    Shopware()->Db()->query($sql);
                
                    $this->createEvents();
                    //remove old configuration setting
                    $repository = Shopware()->Models()->getRepository('Shopware\Models\Config\Element');
                    $status_goods_sent = $repository->findOneBy(array('name' => 'sSECUPAY_STATUS_GOODS_SENT'));
                    if(!empty($status_goods_sent)) {
                        Shopware()->Models()->remove($status_goods_sent);
                        Shopware()->Models()->flush();
                    }
                    
                    $this->createForm();
                } catch (Exception $e) {
                    secupay_log::log(self::log, 'update 2.0.4.2 - Exception '.$e->getMessage());
                    secupay_log::log(self::log, 'update 2.0.4.2 - StackTrace '.$e->getTraceAsString());
                    return false;
                }
            case '2.0.4.3':
                try {
                    //add template for payment types
                    foreach ($this->payment_types as $payment_name => $payment_type_def) {
                        $payment = $this->Payments()->findOneBy(array('name' => $payment_name));
                        if(isset($payment_type_def['template'])) {
                            secupay_log::log(self::log, 'update 2.0.4.3: ' . $payment_name . ' - ' . $payment_type_def['template']);
                            $payment->setTemplate($payment_type_def['template']);
                        }
                    }                    
                    
                    
                } catch (Exception $e) {
                    secupay_log::log(self::log, 'update 2.0.4.3 - Exception '.$e->getMessage());
                    secupay_log::log(self::log, 'update 2.0.4.3 - StackTrace '.$e->getTraceAsString());
                    return false;
                }
            default:
        }
        return true;
    }    
    

    /**
     * Function that enables the plugin
     * This function checks if paymentType is available for apikey
     * If new payment Type is available, then it is created and set to active,
     * If payment type is no longer available, then it is deactivated
     *
     * @return array with success field
     */
    public function enable() {

        $availablePaymentTypes = $this->getAvailablePaymentTypes();
        // this should create new payment type if it was added
        $this->createPaymentTypes($availablePaymentTypes);
        $success = false;

        secupay_log::log(self::log, 'Activating available payment types');
        foreach (array_keys($this->payment_types) as $payment_name) {
            $payment = $this->Payments()->findOneBy(array('name' => $payment_name));
            if ($payment !== null) {
                $active = in_array(substr($payment_name, 8), $availablePaymentTypes); // removes 'secupay_' part of payment name and checks if it is in array
                $payment->setActive($active);
                $success = $success || $active;
            }
        }
        $this->createInvoiceDocumentBox();
        return array('success' => $success, 'invalidateCache' => array('config'));
    }

    /**
     * Function that disables the plugin
     *
     * @return array with success field
     */
    public function disable() {

        foreach (array_keys($this->payment_types) as $payment_name) {
            $payment = $this->Payments()->findOneBy(array('name' => $payment_name));
            if ($payment !== null) {
                $payment->setActive(false);
            }
        }
        $this->removeInvoiceDocumentBox();        
        return array('success' => true, 'invalidateCache' => array('config'));
    }

    /**
     * Function that creates Payment types for Payment module
     * PaymentTypes are created only if they are available
     * if they have already been created and they are not available anymore, then payment type is deactivated
     *
     * @param array names of available payment types
     */
    public function createPaymentTypes($availablePaymentTypes) {

        // if there are not any available , then disable all
        if (empty($availablePaymentTypes)) {
            secupay_log::log(self::log, 'Disabling all Secupay payment types - apikey not valid');
            return $this->disable();
        }

        foreach ($this->payment_types as $payment_name => $payment_type_def) {
            $payment = $this->Payments()->findOneBy(array('name' => $payment_name));
            $name = substr($payment_name, 8); // remove 'secupay_' part of payment name
            if ($payment == null && in_array($name, $availablePaymentTypes)) {
                secupay_log::log(self::log, 'creating payment type: ' . $payment_name);
                $this->createPayment($payment_type_def + array('pluginID' => $this->getId()));
            }
        }
    }

    public static function onBeforeRenderDocument(Enlight_Hook_HookArgs $args) {
        $document = $args->getSubject();
		
        if ($document->_order->payment['name'] != 'secupay_invoice') {
            return;
        }

        $view = $document->_view;
        $orderData = array();
        $orderData = $view->getTemplateVars('Order');
        $hash = $orderData['_order']['transactionID'];

        $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
/*
        $sql = "SELECT  trans.trans_id, DATE_FORMAT(trans.created, '%Y%m%d') AS date
FROM    secupay_transactions AS trans
WHERE   trans.hash = ? AND trans.payment_method = 'invoice'";
        $transaction_data = Shopware()->Db()->fetchRow($sql, array($hash));        
*/
        $invoice_data = self::secupay_get_invoice_data($hash);
        
        secupay_log::log(self::log, 'invoice PDF');
        secupay_log::log(self::log, $hash);
        secupay_log::log(self::log, $invoice_data);
        secupay_log::log(self::log, print_r($invoice_data, true));        
        
        //$trans_id = $transaction_data['trans_id'];
        //$ta_date = $transaction_data['date'];
        
	secupay_log::log(self::log, 'hash: ' . $hash);
        #$iframe_url = SECUPAY_URL . $hash;

        $document->_template->addTemplateDir(dirname(__FILE__) . '/Views/');
        $document->_template->assign('secupay_payment_link', $invoice_data->payment_link);
        $document->_template->assign('secupay_payment_qr_image_url', $invoice_data->payment_qr_image_url);
        
        $document->_template->assign('secupay_recipient_legal', $invoice_data->recipient_legal);
        $document->_template->assign('secupay_accountowner', $invoice_data->transfer_payment_data->accountowner);
        $document->_template->assign('secupay_invoice_account', $invoice_data->transfer_payment_data->accountnumber);
        $document->_template->assign('secupay_invoice_bankcode', $invoice_data->transfer_payment_data->bankcode);
        $document->_template->assign('secupay_invoice_bank', $invoice_data->transfer_payment_data->bankname);
        $document->_template->assign('secupay_invoice_iban', $invoice_data->transfer_payment_data->iban);
        $document->_template->assign('secupay_invoice_bic', $invoice_data->transfer_payment_data->bic);
        $document->_template->assign('secupay_purpose', $invoice_data->transfer_payment_data->purpose);
        
        if($pluginConfig->sSECUPAY_INVOICE_SWITCH_DUE_DATE && is_numeric($pluginConfig->sSECUPAY_INVOICE_DAYS_TO_DUE_DATE) && $pluginConfig->sSECUPAY_INVOICE_DAYS_TO_DUE_DATE > 0) {
            $days = $pluginConfig->sSECUPAY_INVOICE_DAYS_TO_DUE_DATE;
            $date = new DateTime();
            $date->modify('+'.$days.' day');
            $due_date = date_format($date, "d.m.Y");

            $document->_template->assign('secupay_due_date_text', 'F&auml;lligkeitsdatum: '.$due_date);
        } else {
            $document->_template->assign('secupay_due_date_text', '');
        }
        
        $containerData = $view->getTemplateVars('Containers');
        $containerData['Content_Info'] = $containerData['Secupay_Invoice_Content_Info'];
        $containerData['Content_Info']['value'] = $document->_template->fetch('string:' . $containerData['Content_Info']['value']);
        $containerData['Content_Info']['style'] = '}' . $containerData['Content_Info']['style'] . ' #info {';
        $view->assign('Containers', $containerData);
    }

    /**
     * Function that creates events
     */
    protected function createEvents() {
        $this->subscribeEvent(
                'Enlight_Controller_Dispatcher_ControllerPath_Frontend_SecuPaymentSecupay', 'onGetControllerPathFrontend');
        $this->subscribeEvent(
                'Enlight_Controller_Action_PostDispatch_Frontend_Register', 'onPostDispatchAccount');
        $this->subscribeEvent(
                'Enlight_Controller_Action_PostDispatch_Frontend_Account', 'onPostDispatchAccount');
        $this->subscribeEvent(
                'Enlight_Controller_Action_PostDispatch_Frontend_Checkout', 'onPostDispatchAccount');
        // event that allows to check if invoice goods are sent to customer
        /*
        $this->subscribeEvent(
                'Shopware_Controllers_Backend_Order::saveAction::after', 'onSaveOrderStatus');
        */
        $this->subscribeEvent(
                'Shopware_Components_Document::assignValues::after', 'onBeforeRenderDocument');
        $this->subscribeEvent(
                'Enlight_Controller_Dispatcher_ControllerPath_Backend_SecuPaymentSecupay', 'onGetControllerPathBackend');        
    }

    /**
     * Function that creates database table
     */
    private function createDatabase() {
        $sql = "
            CREATE TABLE IF NOT EXISTS `secupay_transactions` (
              `id` int(10) unsigned NOT NULL auto_increment,
              `req_data` text collate latin1_general_ci,
              `ret_data` text collate latin1_general_ci,
              `payment_method` varchar(255) collate latin1_general_ci default NULL,
              `hash` varchar(255) collate latin1_general_ci default NULL,
              `unique_id` varchar(255) collate latin1_general_ci default NULL,
              `ordernr` int(11) default NULL,
              `trans_id` int(11) default NULL,
              `payment_status` varchar(255) collate latin1_general_ci default NULL,
              `msg` varchar(255) collate latin1_general_ci default NULL,
              `rank` int(10) default NULL,
              `status` int(11) default NULL,
              `invoice_status_change_informed` tinyint(3) default '0',
              `amount` varchar(255) collate latin1_general_ci default NULL,
              `action` text collate latin1_general_ci,
              `updated` datetime default NULL,
              `created` datetime default NULL,
              `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) ;";
        $this->Application()->Db()->query($sql);
    }

    /**
     * Function that removes created paymentTypes
     */
    private function removePaymentTypes() {
        $sql = "DELETE FROM s_core_paymentmeans WHERE name = ?";
        foreach (array_keys($this->payment_types) as $payment_type) {
            $this->Application()->Db()->query($sql, array($payment_type));
        }
    }

    /**
     * Function that removes database tables
     */
    private function removeDatabase() {
        $sql = "DROP TABLE IF EXISTS `secupay_transactions`";
        $this->Application()->Db()->query($sql);
    }

    /**
     * Function that creates configuration form for Plugin
     */
    protected function createForm() {

        include('defaults.php');

        $form = $this->Form();

        $form->setElement('boolean', 'sSECUPAY_ALL_SWITCH_TESTMODE', array(
            'label' => 'Testmodus',
            'value' => 1
        ));
        
        $form->setElement('boolean', 'sSECUPAY_ALL_SWITCH_DEBUGMODE', array(
            'label' => 'Debugmodus',
            'description' => 'Bitte aktivieren Sie diese Einstellung nur nach R&uuml;cksprache mit dem secupay Kundendienst. Es werden zus&auml;tzliche Log Eintr&auml;ge zur Problemanalsyse erzeugt.',            
            'value' => 0
        ));        

        $form->setElement('text', 'sSECUPAY_ALL_PURPOSE_SHOPNAME', array(
            'label' => 'Shopname im Verwendungszweck',
            'value' => $this->Application()->Config()->Shopname
        ));

        $form->setElement('boolean', 'sSECUPAY_ALL_SWITCH_SHOW_ALT_DELIVERY_WARNING', array(
            'label' => 'Warnung bei abweichender Lieferanschrift',
            'value' => 1
        ));

        $form->setElement('text', 'sSECUPAY_APIKEY', array(
            'label' => 'API Key',
            'value' => DEFAULT_APIKEY,
            'description' => 'Ihr APIKey für Secupay Zahlungen.',
            'required' => true
        ));

        $form->setElement('select', 'sSECUPAY_STATUS_WAITING', array(
            'label' => 'Zahlungsstatus nach Daten&uuml;bermittlung',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der nach der Daten&uuml;bermittlung f&uuml;r die Bestellung hinterlegt wird.',
            'required' => true
        ));
        /*
        $form->setElement('select', 'sSECUPAY_STATUS_GOODS_SENT', array(
            'label' => 'Bestellstatus f&uuml;r Versand',
            'store' => $this->getPaymentStatus('state'),
            'description' => 'Der Bestellstatus bei dem der Beginn des F&auml;lligkeitzeitraums für Rechnungskauf an Secupay gemeldet wird.',
            'required' => true
        ));
        */
        $form->setElement('select', 'sSECUPAY_STATUS_ACCEPTED_DEBIT', array(
            'label' => 'Zahlungsstatus bei erfolgreichen Transaktionen (Lastschrift)',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei erfolgreichem Abschluss der Lastschrift Transaktion f&uuml;r die Bestellung hinterlegt wird.',
            'required' => true
        ));
        
        $form->setElement('select', 'sSECUPAY_STATUS_ACCEPTED_CREDITCARD', array(
            'label' => 'Zahlungsstatus bei erfolgreichen Transaktionen (Kreditkarte)',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei erfolgreichem Abschluss der Kreditkarten Transaktion f&uuml;r die Bestellung hinterlegt wird.',
            'required' => true
        ));
        
        $form->setElement('select', 'sSECUPAY_STATUS_ACCEPTED_INVOICE', array(
            'label' => 'Zahlungsstatus bei erfolgreichen Transaktionen (Rechnungskauf)',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei erfolgreichem Abschluss der Rechnungskauf Transaktion f&uuml;r die Bestellung hinterlegt wird.',
            'required' => true
        ));        

        $form->setElement('select', 'sSECUPAY_STATUS_DENIED', array(
            'label' => 'Zahlungsstatus bei abgelehnten Transaktionen',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei Ablehnung der Transaktion f&uuml;r die Bestellung hinterlegt wird.',
            'required' => true
        ));

        $form->setElement('select', 'sSECUPAY_STATUS_ISSUE', array(
            'label' => 'Zahlungsstatus bei Zahlungsproblemen',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei Problemen mit der Zahlung f&uuml;r die Bestellung hinterlegt wird.'
        ));

        $form->setElement('select', 'sSECUPAY_STATUS_VOID', array(
            'label' => 'Zahlungsstatus bei stornierten Transaktionen',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei Stornierung oder Gutschrift der Transaktion f&uuml;r die Bestellung hinterlegt wird.'
        ));

        $form->setElement('select', 'sSECUPAY_STATUS_AUTHORIZED', array(
            'label' => 'Zahlungsstatus bei vorautorisierten Transaktionen',
            'store' => $this->getPaymentStatus(),
            'description' => 'Der Zahlungsstatus der bei Vorautorisierung der Transaktion f&uuml;r die Bestellung hinterlegt wird.'
        ));
/*        
        $form->setElement('text', 'sSECUPAY_INVOICE_ACCOUNT', array(
            'label' => 'Kontonummer',
            'description' => 'Rechnungskauf: Kontonummer für Rechnungsdruck'
        ));        

        $form->setElement('text', 'sSECUPAY_INVOICE_BANKCODE', array(
            'label' => 'BLZ',
            'description' => 'Rechnungskauf: BLZ für Rechnungsdruck'
        ));        

        $form->setElement('text', 'sSECUPAY_INVOICE_BANK', array(
            'label' => 'Bank',
            'description' => 'Rechnungskauf: Bank für Rechnungsdruck'
        ));        
        
        $form->setElement('text', 'sSECUPAY_INVOICE_IBAN', array(
            'label' => 'IBAN',
            'description' => 'Rechnungskauf: IBAN für Rechnungsdruck'
        ));
        
        $form->setElement('text', 'sSECUPAY_INVOICE_BIC', array(
            'label' => 'BIC',
            'description' => 'Rechnungskauf: BIC für Rechnungsdruck'
        ));
*/
        $form->setElement('boolean', 'sSECUPAY_INVOICE_SWITCH_DUE_DATE', array(
            'label' => 'Zahlungsfristhinweis bei Rechnungsdruck anzeigen',
            'value' => 0
        ));
        
        $form->setElement('number', 'sSECUPAY_INVOICE_DAYS_TO_DUE_DATE', array(
            'label' => 'Zahlungsfrist in Tagen für Rechnungsdruck',
            'value' => 14
        ));        
        
        $form->setElement('boolean', 'sSECUPAY_ALL_SWITCH_SEND_LAST_HASH', array(
            'label' => 'Letzte Zahlungsmitteldaten vorschlagen',
            'value' => 0
        ));        
        
        $repository = Shopware()->Models()->getRepository('Shopware\Models\Config\Form');

        $form->setParent(
                $repository->findOneBy(array('name' => 'Interface'))
        );
    }

    /**
     * Function that creates translations
     * Not implemented correctly
     */
    /*
    public function createTranslations() {
        $form = $this->Form();
        $translations = array(
            'en_GB' => array(
                'sSECUPAY_APIKEY' => 'API Key',
            )
        );

        $shopRepository = Shopware()->Models()->getRepository('\Shopware\Models\Shop\Locale');
        foreach ($translations as $locale => $snippets) {
            $localeModel = $shopRepository->findOneBy(array(
                'locale' => $locale
                    ));
            foreach ($snippets as $element => $snippet) {
                if ($localeModel === null) {
                    continue;
                }
                $elementModel = $form->getElement($element);
                if ($elementModel === null) {
                    continue;
                }
                $translationModel = new \Shopware\Models\Config\ElementTranslation();
                $translationModel->setLabel($snippet);
                $translationModel->setLocale($localeModel);
                $elementModel->addTranslation($translationModel);
            }
        }
    }*/

    /**
     * Function that registers our class to path
     *
     * @return string
     */
    public function onGetControllerPathFrontend(Enlight_Event_EventArgs $args) {
        secupay_log::log(self::log, 'onGetControllerPathFrontend');
        Shopware()->Template()->addTemplateDir(dirname(__FILE__) . '/Views/');
        return dirname(__FILE__) . '/Controllers/Frontend/SecuPaymentSecupay.php';
    }

    /**
     * just points to the path of this controller
     *
     * @static
     * @param Enlight_Event_EventArgs $args
     * @return string
     */
    public static function onGetControllerPathBackend(Enlight_Event_EventArgs $args) {
        Shopware()->Template()->addTemplateDir(dirname(__FILE__) . '/Views/');
        return dirname(__FILE__) . '/Controllers/Backend/SecuPaymentSecupay.php';
    }
    
    /**
     *
     */
    public function onPostDispatchAccount(Enlight_Event_EventArgs $args) {

        $view = $args->getSubject()->View();
        $request = $args->getSubject()->Request();
        $response = $args->getSubject()->Response();
        
        //check if dispatch is valid
        if(empty($view) || empty($request) || empty($response) || !$request->isDispatched() 
                || $response->isException() || $request->getModuleName() != 'frontend' || !$view->hasTemplate()) {
            
             return;
        }

        if ($request->getControllerName() == 'account' && ($request->getActionName() == 'payment' || $request->getActionName() == 'savePayment' )
                || $request->getControllerName() == 'register'
                || $request->getControllerName() == 'checkout') {

            $view->addTemplateDir($this->Path() . '/Views/');
            $view->assign($this->getTemplateVars());
        }
    }
    
    /**
    * Creates the secupay backend menu item.
    *
    * The secupay menu item opens the listing of secupay transactions.
    */
   public function createMenu()
   {
      $this->createMenuItem(array(
         'label' => 'secupay Transaktionsliste',
         'controller' => 'SecuPaymentSecupay',
         'class' => 'sprite-credit-cards',
         'action' => 'Index',
         'active' => 1,
         'parent' => $this->Menu()->findOneBy('label', 'Kunden')
      ));
   }    

    /**
     * Function returns all available transaction statuses for shopware
     *
     * @param string groupType - Default payment or state
     * @return array
     */
    private function getPaymentStatus($groupType = 'payment') {
        $states = $this->Application()->Db()->fetchAll("
SELECT `id`, `description`
FROM `s_core_states`
WHERE `group` = ?
ORDER BY `position`", array($groupType));

        foreach ($states as $row) {
            $paymentState[] = array(
                $row['id'],
                $row['description']
            );
        }
        return $paymentState;
    }

    /**
     * @return array
     */
    private function getTemplateVars() {
        $pluginConfig = $this->Application()->Plugins()->Frontend()->SecuSecupayPayment()->Config();

        $template_vars = array(
            'secupay_show_alt_delivery_warning' => $pluginConfig->sSECUPAY_ALL_SWITCH_SHOW_ALT_DELIVERY_WARNING,
            'secupay_delivery_address_differs' => $this->checkDeliveryDifference()
        );
        return $template_vars;
    }

    /**
     * @return boolean
     */
    private function checkDeliveryDifference() {
        $template_vars = $this->Application()->Template()->getTemplateVars();

        $shipping_address = $template_vars['sUserData']['shippingaddress'];
        $billing_address = $template_vars['sUserData']['billingaddress'];

        $address_diff = array_diff($shipping_address, $billing_address);

        return !empty($address_diff);
    }

    /**
     * Function that returns array of available paymentTypes for the apikey
     *
     * @return array of strings or null
     */
    private function getAvailablePaymentTypes() {
        $apikey = $this->Application()->Plugins()->Frontend()->SecuSecupayPayment()->Config()->sSECUPAY_APIKEY;
        secupay_log::log(self::log, 'Apikey for getpaymentTypes: '. $apikey);
        if (empty($apikey)) {
            return null;
        }
        $data = array();
        $data['apikey'] = $apikey;
        $sp_api = new secupay_api($data, 'gettypes', 'application/json', true);
        $api_return = $sp_api->request();
        
        if (isset($api_return) && $api_return instanceof secupay_api_response && $api_return->check_response()) {
            return $api_return->data;
        } else {
            return false;
        }    
    }
    
    private function createInvoiceDocumentBox() {
        $sql = "
INSERT INTO `s_core_documents_box` (`documentID`, `name`, `style`, `value`) 
SELECT 
1, 'Secupay_Invoice_Content_Info', '', CONCAT_WS(' ', `s_core_documents_box`.`value`, ?)
FROM `s_core_documents_box`
WHERE `s_core_documents_box`.documentID = 1 AND `s_core_documents_box`.`name` LIKE 'Content_Info';";

        $this->Application()->Db()->query($sql, array(
            '<table>
<tr>
<td>
{$secupay_due_date_text}<br />
<p>
Der Rechnungsbetrag wurde an die {$secupay_recipient_legal}, abgetreten. <br />
                <b>Eine Zahlung mit schuldbefreiender Wirkung ist nur auf folgendes Konto m&ouml;glich:</b><br /><br />

                Empf&auml;nger: {$secupay_accountowner}<br />
                Kontonummer: {$secupay_invoice_account}, BLZ: {$secupay_invoice_bankcode}, Bank: {$secupay_invoice_bank}<br />
                IBAN: {$secupay_invoice_iban}, BIC: {$secupay_invoice_bic}<br />
                <b>Verwendungszweck: {$secupay_purpose}</b><br /><br />
                Um diese Rechnung bequem online zu zahlen, k&ouml;nnen Sie den QR-Code mit einem internetf&auml;higen Telefon einscannen <br />oder Sie nutzen diese
URL: {$secupay_payment_link}<br />
</p>
</td>
<td>
<img src="{$secupay_payment_qr_image_url}" width="100" height="100" alt="" />
</td>
</tr>
</table>'
        ));
    }
    
    private function removeInvoiceDocumentBox() {
        $sql = "DELETE FROM s_core_documents_box WHERE s_core_documents_box.documentID = 1 AND s_core_documents_box.name LIKE 'Secupay_Invoice_Content_Info';";
        $this->Application()->Db()->query($sql);
    }
    
    static function secupay_get_invoice_data($hash) {

        if(!empty($hash)) {
            $request = array();
            $request['hash'] = $hash;
            $pluginConfig = Shopware()->Plugins()->Frontend()->SecuSecupayPayment()->Config();
            $request['apikey'] = $pluginConfig->sSECUPAY_APIKEY;

            $sp_api = new secupay_api($request, 'status');
            $response = $sp_api->request();

            if($response->check_response() && isset($response->data->opt)) {
                return $response->data->opt;
            }            
        }
        return false;
    }    
}