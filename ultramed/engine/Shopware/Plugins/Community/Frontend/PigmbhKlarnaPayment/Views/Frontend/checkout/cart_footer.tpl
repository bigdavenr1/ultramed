{block name='frontend_checkout_cart_footer_tax_rates' append}
</div>
{if $pi_klarna_rate_active && $pi_Klarna_lang['iso'] != "at"}
<div class="KlarnaShowRateDiv KlarnaFloatRight">
    <span class="Klarna_rate_span">{$pi_Klarna_lang['rate']['from_amount']} {$pi_klarna_rateAmount|currency}{$pi_Klarna_lang['rate']['value_month']}</span><br />
    <span class="Klarna_rate_span_link">(<a class="Klarnacolor klarna_partpayment" href="#" title="{$pi_Klarna_lang['rate']['href']}">{$pi_Klarna_lang['rate']['read_more']}</a>)</span>
    <a href="https://klarna.com/de/privatpersonen/unsere-services/klarna-ratenkauf" title="{$pi_Klarna_lang['rate']['href']}" target="_blank">
        <img src="{$piKlarnaImgDir|cat:'KlarnaRatepayLogoProduct.png'}" class="KlarnaRateSmallImg"/>
    </a>
{/if}
{/block}