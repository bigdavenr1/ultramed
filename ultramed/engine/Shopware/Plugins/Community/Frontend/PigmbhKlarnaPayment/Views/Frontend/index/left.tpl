{block name="frontend_index_left_menu" prepend}
	{if $piKlarnaConfig.showLogos=="links"}
		<ul id="servicenav">
	        <li class="heading">
	            <span>{$pi_Klarna_lang['LeftSidebarHeader']}</span>
	        </li>
	    {if $pi_klarna_active}
	        <li>
                <script defer async="true" data-vendor="klarna" data-eid="X" data-locale="{$piKlarnaShopLocale}" data-product="invoice" data-width="155" type="text/javascript" src="https://cdn.klarna.com/1.0/code/tooltip/2.0/js/all.js"></script>
	        </li>
	    {/if}
	    {if $pi_klarna_rate_active && $piKlarnaShopLocale != 'de_at'}
	        <li>
                <script defer async="true" data-vendor="klarna" data-eid="X" data-locale="{$piKlarnaShopLocale}" data-product="account" data-width="155" type="text/javascript" src="https://cdn.klarna.com/1.0/code/tooltip/2.0/js/all.js"></script>
	        </li>
	    {/if}
		</ul><br/>
	{/if}
	{if ($piKlarnaConfig.RatepayBanner == "links" || $piKlarnaConfig.InvoiceBanner == "links") && $piKlarnaShopLang == "de" && $piKlarnaConfig.piKlarnaShowOneBanner}
		<a href="https://{$pi_Klarna_lang['both']['ahref']}" title="{$pi_Klarna_lang['klarna_href']}" target="_blank">
		    <img src="https://cdn.klarna.com/public/images/DE/badges/v1/unified/DE_unified_badge_banner_blue.png?width=179&eid=sw" class="KlarnaBannerBothLeft" />
		</a>
	{else}
		{if $piKlarnaConfig.RatepayBanner=="links"}
            <script defer async="true" data-vendor="klarna" data-eid="X" data-locale="{$piKlarnaShopLocale}" data-product="invoice" data-width="179" type="text/javascript" src="https://cdn.klarna.com/1.0/code/tooltip/2.0/js/all.js"></script>
            <br><br>
		{/if}
		{if $piKlarnaConfig.InvoiceBanner=="links" && $piKlarnaShopLocale != 'de_at'}
            <script defer async="true" data-vendor="klarna" data-eid="X" data-locale="{$piKlarnaShopLocale}" data-product="account" data-width="179" type="text/javascript" src="https://cdn.klarna.com/1.0/code/tooltip/2.0/js/all.js"></script>
            <br><br>
		{/if}
	{/if}
{/block}