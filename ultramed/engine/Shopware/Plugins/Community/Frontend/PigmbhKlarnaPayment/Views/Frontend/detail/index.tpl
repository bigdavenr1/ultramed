{block name='frontend_detail_data_price_info' prepend}
{if $pi_klarna_rate_active && $pi_klarna_rate}
	<div class="KlarnaShowRateDiv">
	    <span class="Klarna_rate_span">{$pi_Klarna_lang['rate']['from_amount']} {$pi_klarna_rate|currency}{$pi_Klarna_lang['rate']['value_month']}</span><br />
            <span class="Klarna_rate_span_link">(<a class="Klarnacolor klarna_partpayment" href="#" title="{$pi_Klarna_lang['rate']['href']}">{$pi_Klarna_lang['rate']['read_more']}</a>)</span>
	    <a class="Klarnacolor" href="https://klarna.com/de/privatpersonen/unsere-services/klarna-ratenkauf" title="{$pi_Klarna_lang['rate']['href']}" target="_blank">
	        <img src="{$piKlarnaImgDir|cat:'KlarnaRatepayLogoProduct.png'}" class="KlarnaRateSmallImg"/>
	    </a>
	    <br /><br /><br />
    </div>
{/if}
{/block}