{block name="frontend_index_header_css_print" append}
<link type="text/css" media="screen, projection" rel="stylesheet" href="{link file='engine/Shopware/Plugins/Default/Frontend/PigmbhKlarnaPayment/css/klarnastyles.css' fullPath}" />
{/block}

{block name="frontend_index_header_javascript" append}
{if $pi_klarna_active || $pi_klarna_rate_active}
    <script src="https://cdn.klarna.com/public/kitt/core/v1.0/js/klarna.min.js" ></script>
    <script src="https://cdn.klarna.com/public/kitt/toc/v1.1/js/klarna.terms.min.js" ></script>
{/if}
    {if $pi_klarna_active}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            if($('#klarna_invoice').length > 0) {
                new Klarna.Terms.Invoice({
                    placeholder: 'klarna_invoice',
                    eid: '{$pi_klarna_shopid}',
                    country: '{$piKlarnaShopLang}',
                    charge: '{$pi_klarna_surcharge}'
                });
            }
        });
    </script>
    {/if}
    {if $pi_klarna_rate_active}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('a.klarna_partpayment').each(function(i, el) {
                new Klarna.Terms.Account({
                    placeholder: el,
                    eid: '{$pi_klarna_shopid}',
                    country: '{$piKlarnaShopLang}'
                });
            });
        });
    </script>
    {/if}
{/block}
