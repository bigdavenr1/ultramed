{block name='frontend_listing_box_article_name' append}
{if $pi_klarna_rate_active && $piKlarnaArticles}
    {$currentRate = $pi_klarna_rate[array_search($sArticle, $sArticles)]}
{elseif $pi_klarna_rate_active && $piKlarnaOffers}
    {$currentRate = $pi_klarna_rate[array_search($sArticle, $sOffers)]}
{else}
    {$currentRate = 0}
{/if}
{if $currentRate}
    <div class="KlarnaRateListingPrice">{$pi_Klarna_lang['rate']['from']} {$currentRate|currency}{$pi_Klarna_lang['rate']['value_month']}
        (<a href="#" class="klarna_partpayment"></a>)
    </div>
{/if}
{/block}