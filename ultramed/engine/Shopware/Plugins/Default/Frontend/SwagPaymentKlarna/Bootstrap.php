<?php
/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Plugins
 * @subpackage Plugin
 * @copyright  Copyright (c) 2014, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Heiner Lohaus
 * @author     $Author$
 */

/**
 * Shopware Klarna Plugin
 */
class Shopware_Plugins_Frontend_SwagPaymentKlarna_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    /**
     * Installs the plugin
     *
     * Creates and subscribe the events and hooks
     * Creates and save the payment row
     * Creates the payment table
     * Creates payment menu item
     *
     * @return bool
     */
    public function install()
    {
        $this->createMyEvents();
        $this->createMyPayment();
        $this->createMyForm();
        $this->createMyTranslations();
        $this->createMyAttributes();

        return array(
            'success' => true,
            'invalidateCache' => array('backend')
        );
    }

    /**
     * @param string $version
     * @return bool
     */
    public function update($version)
    {
        if($version == '0.0.1') {
            return false;
        }
        return $this->install();
    }

    /**
     * @return bool
     */
    public function uninstall()
    {
        try {
            $this->Application()->Models()->removeAttribute(
                's_order_attributes',
                'swag_klarna', 'status'
            );
            $this->Application()->Models()->removeAttribute(
                's_order_attributes',
                'swag_klarna', 'invoice_number'
            );
            $this->Application()->Models()->removeAttribute(
                's_order_details_attributes',
                'swag_klarna', 'invoice_number'
            );
            $this->Application()->Models()->generateAttributeModels(array(
                's_order_attributes', 's_order_details_attributes'
            ));
        } catch(Exception $e) { }

        return true;
    }

    /**
     * Fetches and returns klarna payment row instance.
     *
     * @return \Shopware\Models\Payment\Payment
     */
    public function Payment()
    {
        return $this->Payments()->findOneBy(
            array('name' => 'klarna_checkout')
        );
    }

    /**
     * @return \Klarna_Checkout_Connector
     */
    public function Connector()
    {
        return $this->Application()->KlarnaCheckoutConnector();
    }

    /**
     * @return \Klarna
     */
    public function getService()
    {
        return $this->Application()->KlarnaService();
    }

    /**
     * Activate the plugin klarna plugin.
     * Sets the active flag in the payment row.
     *
     * @return bool
     */
    public function enable()
    {
        $payment = $this->Payment();
        if ($payment !== null) {
            $payment->setActive(true);
        }
        return true;
    }

    /**
     * Disable plugin method and sets the active flag in the payment row
     *
     * @return bool
     */
    public function disable()
    {
        $payment = $this->Payment();
        if ($payment !== null) {
            $payment->setActive(false);
        }
        return true;
    }

    /**
     * Creates and subscribe the events and hooks.
     */
    protected function createMyEvents()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Frontend_PaymentKlarna',
            'onGetControllerPathFrontend'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_PaymentKlarna',
            'onGetControllerPathBackend'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch',
            'onPostDispatch',
            110
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout',
            'onPostDispatchCheckout'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PreDispatch_Frontend_Checkout',
            'onPreDispatchCheckout'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_KlarnaCheckoutConnector',
            'onInitResourceKlarnaCheckoutConnector'
        );

        $this->subscribeEvent(
            'Enlight_Bootstrap_InitResource_KlarnaService',
            'onInitResourceKlarnaService'
        );

        $this->subscribeEvent(
            'Enlight_Controller_Action_PostDispatch_Backend_Order',
            'onLoadBackendModule'
        );
    }

    /**
     * Creates and save the payment row.
     */
    protected function createMyPayment()
    {
        /** @var \Shopware\Components\Model\ModelManager $manager */
        $manager = $this->get('models');
        $repository = 'Shopware\Models\Country\Country';
        $repository = $manager->getRepository($repository);
        $countries = array(
            $repository->findOneBy(array('iso' => 'SE')),
            $repository->findOneBy(array('iso' => 'DE')),
            $repository->findOneBy(array('iso' => 'NO')),
            $repository->findOneBy(array('iso' => 'FI')),
            //$repository->findOneBy(array('iso' => 'AT')),
            //$repository->findOneBy(array('iso' => 'NL')),
            //$repository->findOneBy(array('iso' => 'DK'))
        );
        $this->createPayment(array(
            'name' => 'klarna_checkout',
            'description' => $this->getLabel(),
            'action' => 'payment_klarna',
            'active' => 0,
            'position' => 0,
            'additionalDescription' => '',
            'countries' => $countries
        ));
    }

    /**
     * Creates and stores the payment config form.
     */
    protected function createMyForm()
    {
        $form = $this->Form();

        // API settings
        $form->setElement('boolean', 'testDrive', array(
            'label' => 'Testmodus aktvieren',
            'value' => false,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('text', 'merchantId', array(
            'label' => 'API-HändlerID (EID)',
            'required' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('text', 'sharedSecret', array(
            'label' => 'API-Secret (sharedsecret)',
            'required' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));

        $form->setElement('boolean', 'hideCheckoutSteps', array(
            'label' => 'Box mit den Bestellschritten ausblenden',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'expressButton', array(
            'label' => 'Express-Kauf-Button im Warenkorb anzeigen',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'expressButtonLayer', array(
            'label' => 'Express-Kauf-Button in der Modal-Box anzeigen',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'noAccountCheckout', array(
            'label' => '"Kein Kundenkonto"-Anmeldung auf Express-Kauf umleiten',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'partPaymentWidget', array(
            'label' => 'Teilzahlungs-Widget auf der Detailseite anzeigen',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));

        $form->setElement('boolean', 'convertShippingFee', array(
            'label' => 'Versandkosten in eine Bestellposition umwandeln',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));

        $form->setElement('number', 'termSiteId', array(
            'label' => 'Shopseite mit den Geschäftsbedingungen',
            'value' => 4,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'allowSeparateShippingAddress', array(
            'label' => 'Abweichende Lieferadresse erlauben',
            'value' => true,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'supportPackstation', array(
            'label' => 'DHL Packstation unterstützen',
            'value' => false,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('text', 'postnumberField', array(
            'label' => 'Alternatives Feld für die DHL-Postnummer',
            'value' => 'attribute1',
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'mandatoryPhone', array(
            'label' => 'Telefonummer als Pflichtfeld',
            'value' => false,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('boolean', 'disableAutofocus', array(
            'label' => 'Autofokus deaktivieren',
            'value' => false,
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));

        $form->setElement('select', 'statusId', array(
            'label' => 'Zahlstatus nach der Bestellung',
            'value' => 18,
            'store' => 'base.PaymentStatus',
            'displayField' => 'description',
            'valueField' => 'id',
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('select', 'activateStatusId', array(
            'label' => 'Zahlstatus nach der Aktivierung',
            'value' => 12,
            'store' => 'base.PaymentStatus',
            'displayField' => 'description',
            'valueField' => 'id',
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
        $form->setElement('select', 'cancelStatusId', array(
            'label' => 'Zahlstatus nach der Stornierung',
            'value' => 17,
            'store' => 'base.PaymentStatus',
            'displayField' => 'description',
            'valueField' => 'id',
            'scope' => \Shopware\Models\Config\Element::SCOPE_SHOP
        ));
    }

    /**
     *
     */
    public function createMyTranslations()
    {
        $form = $this->Form();
        $translations = array(
            'en_GB' => array(
                //'klarnaUsername' => 'API username',
                //'klarnaPassword' => 'API password',
            )
        );
        $shopRepository = Shopware()->Models()->getRepository('\Shopware\Models\Shop\Locale');
        foreach($translations as $locale => $snippets) {
            $localeModel = $shopRepository->findOneBy(array(
                'locale' => $locale
            ));
            foreach($snippets as $element => $snippet) {
                if($localeModel === null){
                    continue;
                }
                $elementModel = $form->getElement($element);
                if($elementModel === null) {
                    continue;
                }
                $translationModel = new \Shopware\Models\Config\ElementTranslation();
                $translationModel->setLabel($snippet);
                $translationModel->setLocale($localeModel);
                $elementModel->addTranslation($translationModel);
            }
        }
    }

    /**
     *
     */
    public function createMyAttributes()
    {
        try {
            $this->Application()->Models()->addAttribute(
                's_order_details_attributes', 'swag_klarna',
                'invoice_number', 'VARCHAR(255)'
            );
        } catch(Exception $e) { }

        $this->Application()->Models()->generateAttributeModels(array(
            's_order_details_attributes'
        ));

        try {
            $this->Application()->Models()->addAttribute(
                's_order_attributes', 'swag_klarna',
                'status', 'VARCHAR(255)'
            );
            $this->Application()->Models()->addAttribute(
                's_order_attributes', 'swag_klarna',
                'invoice_number', 'VARCHAR(255)'
            );
        } catch(Exception $e) { }

        $this->Application()->Models()->generateAttributeModels(array(
            's_order_attributes'
        ));
    }

    /**
     *
     */
    protected function registerMyTemplateDir()
    {
        $this->Application()->Template()->addTemplateDir(
            $this->Path() . 'Views/', 'klarna'
        );
    }

    /**
     * Returns the path to a frontend controller for an event.
     *
     * @param Enlight_Event_EventArgs $args
     * @return string
     */
    public function onGetControllerPathFrontend(Enlight_Event_EventArgs $args)
    {
        $this->registerMyTemplateDir();
        return $this->Path() . 'Controllers/Frontend/PaymentKlarna.php';
    }

    /**
     * Returns the path to a backend controller for an event.
     *
     * @param Enlight_Event_EventArgs $args
     * @return string
     */
    public function onGetControllerPathBackend(Enlight_Event_EventArgs $args)
    {
        $this->registerMyTemplateDir();
        $this->Application()->Snippets()->addConfigDir(
            $this->Path() . 'Snippets/'
        );
        return $this->Path() . 'Controllers/Backend/PaymentKlarna.php';
    }

    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onPostDispatch(Enlight_Event_EventArgs $args)
    {
        /** @var $action Enlight_Controller_Action */
        $action = $args->getSubject();
        $request = $action->Request();
        $response = $action->Response();
        $view = $action->View();

        if (!$request->isDispatched()
            || $response->isException()
            || $request->getModuleName() != 'frontend'
            || !$view->hasTemplate()
        ) {
            return;
        }

        //Logout klarna guest account
        if(($user = $view->sUserData) !== null
          && $user['billingaddress']['zipcode'] == '00000'
          && ($user['additional']['payment']['name'] != 'klarna_checkout' || $request->getControllerName() != 'checkout')) {
            $session = Shopware()->Session();
            $manager = Shopware()->Models();
            $model = $manager->find('Shopware\Models\Customer\Customer', $session->sUserId);
            //$skipLogin = $session->KlarnaSkipLogin;
            $session->unsetAll();
            $session['sPaymentID'] = Shopware()->Config()->get('paymentDefault');
            $session['sCountry'] = $user['additional']['country']['id'];
            if(!empty($user['additional']['state'])) {
                $session['sState'] = $user['additional']['state']['id'];
            }
            if ($model !== null) {
                try {
                    $manager->remove($model);
                    $manager->flush($model);
                } catch (Exception $e) { }
            }
            if($request->getControllerName() == 'account') {
                $action->redirect(array('controller' => 'register', 'skipLogin' => 1));
            }
            return;
        }

        if ($request->getControllerName() == 'checkout' || $request->getControllerName() == 'account') {
            $this->registerMyTemplateDir();
        }

        $config = $this->Config();

        if (!empty($config->expressButtonLayer)
          && $request->getControllerName() == 'checkout' && $request->getActionName() == 'ajax_add_article') {
            $view->KlarnaShowButton = true;
            $view->extendsBlock(
                'frontend_checkout_ajax_add_article_action_buttons',
                '{include file="frontend/payment_klarna_checkout/layer.tpl"}' . "\n",
                'prepend'
            );
        }

        if (!empty($config->expressButton)
          && $request->getControllerName() == 'checkout' && $request->getActionName() == 'cart') {
            $view->KlarnaShowButton = true;
            $view->extendsTemplate('frontend/payment_klarna_checkout/cart.tpl');
        }

        if(isset($view->KlarnaShowButton)) {
            $showButton = false;
            $admin = Shopware()->Modules()->Admin();
            $payments = isset($view->sPayments) ? $view->sPayments : $admin->sGetPaymentMeans();
            foreach($payments as $payment) {
                if($payment['name'] == 'klarna_checkout') {
                    $showButton = true;
                    break;
                }
            }
            $view->KlarnaPayment = $payment;
            $view->KlarnaShowButton = $showButton;
        }

        if (!empty($config->partPaymentWidget)
          && $request->getControllerName() == 'detail' && $request->getActionName() == 'index') {
            $this->registerMyTemplateDir();
            $view->KlarnaLocale = $this->getLocale();
            $view->KlarnaMerchantId = $config->get('merchantId');
            $view->extendsTemplate(
                'frontend/payment_klarna_part/detail.tpl'
            );
        }

        if (!empty($config->noAccountCheckout)
          && $request->getControllerName() == 'register' && $request->getActionName() == 'index'
          && $request->getParam('skipLogin') !== null) {
            $session = Shopware()->Session();
            $session->KlarnaSkipLogin = true;
            $payments = $view->register->payment->payment_means;
            foreach($payments as $payment) {
                if($payment['name'] == 'klarna_checkout' && $payment['id'] == $session['sPaymentID']) {
                    $action->redirect(array('controller' => 'payment_klarna', 'action' => 'express'));
                    return;
                }
            }
        }
        if ($request->getControllerName() == 'register' && $request->getActionName() == 'index') {
            $this->registerMyTemplateDir();
            $view->KlarnaLocale = $this->getLocale();
            $view->KlarnaMerchantId = $config->get('merchantId');
            $view->KlarnaHideCheckoutSteps = (bool)$config->get('hideCheckoutSteps');
            $view->extendsTemplate('frontend/payment_klarna_checkout/register.tpl');
        }
        if ($request->getControllerName() == 'account' && $request->getActionName() == 'ajax_login') {
            $this->registerMyTemplateDir();
            $view->KlarnaLocale = $this->getLocale();
            $view->KlarnaMerchantId = $config->get('merchantId');
            $view->extendsTemplate('frontend/payment_klarna_checkout/ajax_login.tpl');
        }
    }

    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onPostDispatchCheckout(Enlight_Event_EventArgs $args)
    {
        /** @var $action Enlight_Controller_Action */
        $action = $args->getSubject();
        $request = $action->Request();
        $response = $action->Response();
        $view = $action->View();

        if (!$request->isDispatched()
            || $response->isException()
            || $response->isRedirect()
            || $request->getActionName() != 'confirm'
        ) {
            return;
        }

        $connector = $this->Connector();
        $session = $this->Application()->Session();
        $user = $view->sUserData;
        $basket = $view->sBasket;
        /** @var \Shopware\Models\Shop\Shop $shop */
        $shop = $this->Application()->Shop();
        $config = $this->Config();

        $this->registerMyTemplateDir();
        $view->extendsTemplate('frontend/payment_klarna_checkout/confirm.tpl');

        if(empty($user['additional']['payment'])
            || $user['additional']['payment']['name'] != 'klarna_checkout') {
            return;
        }

        // SwagDhl fix
        $allowPackstation = true;
        if(!empty($view->dhlDispatchIds)) {
            $allowPackstation = false;
            if(in_array($session->sDispatch, $view->dhlDispatchIds)) {
                $allowPackstation = true;
            }
            unset($view->dhlDispatchIds);
        }

        try {
            if(isset($session['KlarnaOrder'])) {
                $order = new Klarna_Checkout_Order($connector, $session['KlarnaOrder']);
                $order->fetch();

                if ($order['status'] == "checkout_incomplete") {
                    $update = array();
                    $update['cart']['items'] = $this->getCheckoutCart($basket);
                    $update['options'] = $this->getCheckoutOptions($allowPackstation);
                    $order->update($update);
                } else {
                    unset($session['KlarnaOrder']);
                }
            }
            if(!isset($session['KlarnaOrder'])) {
                $create = array();
                $create['cart']['items'] = $this->getCheckoutCart($basket);
                $create['merchant'] = $this->getCheckoutMerchant();
                $create['purchase_country'] = $user['additional']['countryShipping']['countryiso'];
                $create['purchase_currency'] = $shop->getCurrency()->toString();
                $create['locale'] = $this->getLocale();

                $create['options'] = $this->getCheckoutOptions($allowPackstation);

                if($config->get('disableAutofocus')) {
                    $create['gui']['options'] = array('disable_autofocus');
                }

                if($user['additional']['countryShipping']['id'] == $this->getCountryByShop($shop)
                  && $user['billingaddress']['zipcode'] != '00000') {
                    $create['customer'] = $this->getCheckoutCustomer($user);
                    if($user['additional']['countryShipping']['id'] == $user['additional']['country']['id']) {
                        $create['shipping_address'] = $this->getCheckoutAddress($user, 'billing');
                    } else {
                        $create['shipping_address'] = $this->getCheckoutAddress($user, 'shipping');
                    }
                }

                $order = new Klarna_Checkout_Order($connector);
                $order->create($create);
                $order->fetch();

                $session['KlarnaOrder'] = $order->getLocation();
            }
        } catch(Klarna_Checkout_ConnectorException $e) {
            $view->extendsTemplate('frontend/payment_klarna_checkout/error.tpl');
            $view->assign('KlarnaError', json_decode($e->getMessage(), true));
            unset($order, $session['KlarnaOrder']);
        }

        $view->assign('KlarnaHideCheckoutSteps', (bool)$config->get('hideCheckoutSteps'));

        if(isset($order)) {
            $view->assign('KlarnaOrder', $order->marshal());
        }
    }

    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onPreDispatchCheckout(Enlight_Event_EventArgs $args)
    {
        /** @var $action Enlight_Controller_Action */
        $action = $args->getSubject();
        $request = $action->Request();
        $response = $action->Response();
        $view = $action->View();
        $session = $this->Application()->Session();

        if (!$request->isDispatched()
            || $response->isException()
            || $request->getModuleName() != 'frontend'
        ) {
            return;
        }

        if(!isset($session['sCountry'])) {
            $shop = $this->Application()->Shop();
            $session['sCountry'] = $this->getCountryByShop($shop);
        }

        if($request->getActionName() != 'finish' || !isset($session['KlarnaOrder'])) {
            return;
        }

        $connector = $this->Connector();
        $order = new Klarna_Checkout_Order($connector, $session['KlarnaOrder']);
        $order->fetch();

        if($order['status'] != 'created' && $order['status'] != 'checkout_complete') {
            return;
        }

        $this->registerMyTemplateDir();
        $view->extendsTemplate('frontend/payment_klarna_checkout/finish.tpl');
        $view->assign('KlarnaOrder', $order->marshal());
    }

    /**
     * @param Enlight_Event_EventArgs $args
     */
    public function onLoadBackendModule(Enlight_Event_EventArgs $args)
    {
        /** @var $subject Enlight_Controller_Action */
        $subject = $args->getSubject();
        $request = $subject->Request();
        $view = $subject->View();

        switch($request->getActionName()) {
            case 'load':
                $this->registerMyTemplateDir();
                $view->extendsTemplate(
                    'backend/order/payment_klarna.js'
                );
                break;
            default:
                break;
        }
    }

    /**
     * @param Klarna_Checkout_Order $order
     */
    public function createAccount($order = null)
    {
        $module = Shopware()->Modules()->Admin();
        $session = Shopware()->Session();

        $version = Shopware()->Config()->version;
        if ($version == '___VERSION___' || version_compare($version, '4.1.0', '>=')) {
            $encoder =  Shopware()->PasswordEncoder()->getDefaultPasswordEncoderName();
        }

        $data = array();

        if($order !== null && !empty($order['billing_address']['email'])) {
            $data['auth']['email'] = $order['billing_address']['email'];
            $data['auth']['password'] = $order['reference'];
        } else {
            $sessionId = Shopware()->SessionID();
            $data['auth']['email'] = md5($sessionId) . '@klarna.com';
            $data['auth']['password'] = $sessionId;
        }
        $data['auth']['accountmode'] = '1';

        $data['billing']['phone'] = !empty($order['billing_address']['phone']) ? $order['billing_address']['phone'] : ' ';
        if(!empty($order['customer']['date_of_birth'])) {
            list($data['billing']['birthyear'], $data['billing']['birthmonth'], $data['billing']['birthday']) = explode(
                '-', $order['customer']['date_of_birth']
            );
        }

        foreach(array('billing', 'shipping') as $type) {
            if(isset($order[$type . '_address'])) {
                $orderAddress = $order[$type . '_address'];
                if(isset($orderAddress['title'])) {
                    $data[$type]['salutation'] = $orderAddress['title'] == 'Frau' ? 'ms' : 'mr';
                } else {
                    $data[$type]['salutation'] = 'mr';
                }
                $data[$type]['firstname'] = $orderAddress['given_name'];
                $data[$type]['lastname'] = $orderAddress['family_name'];
                if(isset($orderAddress['street_name']) && $orderAddress['street_number']) {
                    $data[$type]['street'] = $orderAddress['street_name'];
                    $data[$type]['streetnumber'] = $orderAddress['street_number'];
                } else {
                    $data[$type]['street'] = $orderAddress['street_address'];
                    $data[$type]['streetnumber'] = ' ';
                }
                $data[$type]['zipcode'] = $orderAddress['postal_code'];
                $data[$type]['city'] = $orderAddress['city'];
                if(!empty($orderAddress['care_of'])) {
                    if($orderAddress['street_name'] == 'Packstation') {
                        $data[$type]['postnumber'] = $orderAddress['care_of'];
                        $data[$type]['swagDhlPostnumber'] = $data[$type]['postnumber'];
                    } else {
                        $data[$type]['company'] = $orderAddress['care_of'];
                    }
                }
            } else {
                $data[$type]['salutation'] = 'mr';
                $data[$type]['firstname'] = ' ';
                $data[$type]['lastname'] = 'Klarna Checkout';
                $data[$type]['street'] = ' ';
                $data[$type]['streetnumber'] = ' ';
                $data[$type]['zipcode'] = '00000';
                $data[$type]['city'] = ' ';
            }
            if(!isset($data[$type]['company'])) {
                $data[$type]['company'] = '';
            }
            $data[$type]['department'] = '';

            if(!empty($order[$type . '_address']['country'])) {
                $sql = 'SELECT id FROM s_core_countries WHERE countryiso=?';
                $countryId = Shopware()->Db()->fetchOne($sql, array($order[$type . '_address']['country']));
            } else {
                $countryId = $session['sCountry'];
            }

            $data[$type]['country'] = $countryId;
        }

        $sql = 'SELECT id FROM s_core_paymentmeans WHERE name=?';
        $paymentId = Shopware()->Db()->fetchOne($sql, array('klarna_checkout'));

        // First try login / Reuse paypal account
        //$module->sSYSTEM->_POST = $data['auth'];
        //$module->sLogin(true);

        // Check login status
        if (!empty($session->sUserId)) {
            if($order !== null) {
                $module->sSYSTEM->_POST = $data['shipping'];
                $module->sUpdateShipping();
                $module->sSYSTEM->_POST = $data['billing'];
                $module->sUpdateBilling();
                $data['auth']['passwordConfirmation'] = $data['auth']['password'];
                $module->sSYSTEM->_POST = $data['auth'];
                $module->sUpdateAccount();
            }
            $module->sSYSTEM->_POST = array('sPayment' => $paymentId);
            $module->sUpdatePayment();
        } else {
            $data['payment']['object'] = $module->sGetPaymentMeanById($paymentId);
            if (isset($encoder)) {
                $data["auth"]["encoderName"] = $encoder;
                $data["auth"]["password"] = Shopware()->PasswordEncoder()->encodePassword($data["auth"]["password"], $encoder);
            } else {
                $data['auth']['password'] = md5($data['auth']['password']);
            }
            $session->sRegisterFinished = false;
            $session->sRegister = new ArrayObject($data, ArrayObject::ARRAY_AS_PROPS);
            $module->sSaveRegister();
        }
    }

    public function updateOrderVariables()
    {
        $session = Shopware()->Session();
        $admin = Shopware()->Modules()->Admin();
        /** @var ArrayObject $orderVariables */
        $orderVariables = $session['sOrderVariables'];
        $userData = $admin->sGetUserData();
        $userData['payment'] = $orderVariables['sUserData']['payment'];
        $userData['additional'] = array_merge(
            $orderVariables['sUserData']['additional'],
            $userData['additional']
        );
        $orderVariables['sUserData'] = $userData;
    }

    /**
     * @param $shop Shopware\Models\Shop\Shop
     * @return int|null
     */
    public function getCountryByShop($shop)
    {
        $locale = $shop->getLocale()->getLocale();
        $locale = explode('_', $locale);
        $locale = isset($locale[1]) ? $locale[1] : $locale[0];
        $sql = 'SELECT id FROM s_core_countries WHERE countryiso=? AND active=1';
        $countryId = Shopware()->Db()->fetchOne($sql, array($locale));
        return $countryId ? (int)$countryId : null;
    }

    public function getLocale()
    {
        $shop = $this->Application()->Shop();
        $locale = $shop->getLocale()->getLocale();
        $locale = strtolower(str_replace('_', '-', $locale));
        $locale = $locale == 'nn-no' ? 'nb-no' : $locale;
        return $locale;
    }

    protected function getCheckoutOptions($allowPackstation = true)
    {
        $config = $this->Config();
        $options = array();
        $options['allow_separate_shipping_address'] = (bool)$config->get('allowSeparateShippingAddress');
        $options['packstation_enabled'] = (bool)$config->get('supportPackstation') && $allowPackstation;
        $options['phone_mandatory'] = (bool)$config->get('mandatoryPhone');
        return $options;
    }

    protected function getCheckoutCart($basket)
    {
        $cart = array();
        foreach($basket['content'] as $basketItem) {
            $unitPrice = round(str_replace(',', '.', $basketItem['price']) * 100);
            $cart[] = array(
                'type' => $unitPrice >= 0 ? 'physical' : 'discount',
                'reference' => $basketItem['ordernumber'],
                'name' => $basketItem['articlename'],
                'quantity' => (int)$basketItem['quantity'],
                'unit_price' => (int)$unitPrice,
                'tax_rate' => (int)round($basketItem['tax_rate'] * 100)
            );
        }
        if(!empty($basket['sShippingcosts'])) {
            $shippingAmount = !empty($basket['sShippingcostsWithTax']) ? $basket['sShippingcostsWithTax']: $basket['sShippingcosts'];
            $cart[] = array(
                'type' => 'shipping_fee',
                'reference' => 'SHIPPING',
                'name' => 'Shipping Fee',
                'quantity' => 1,
                'unit_price' => (int)round($shippingAmount * 100),
                'tax_rate' => (int)round($basket['sShippingcostsTax'] * 100),
            );
        }
        return $cart;
    }

    protected function getCheckoutMerchant()
    {
        /** @var \Enlight_Controller_Router_Default $router */
        $router = $this->Application()->Front()->Router();
        $merchant = array();
        $merchant['id'] = $this->Config()->get('merchantId');
        $merchant['terms_uri'] = $router->assemble(array(
            'controller' => 'custom',
            'sCustom' => $this->Config()->get('termSiteId', 4),
            'forceSecure' => true
        ));
        $merchant['checkout_uri'] = $router->assemble(array(
            'action' => 'confirm'
        ));
        $merchant['confirmation_uri'] = $router->assemble(array(
            'controller' => 'payment_klarna',
            'action' => 'return',
            'forceSecure' => true
        )) . "?transactionId={checkout.order.uri}";
        $merchant['push_uri'] = $router->assemble(array(
            'controller' => 'payment_klarna',
            'action' => 'push',
            'forceSecure' => true, 'appendSession' => true
        )) . "&transactionId={checkout.order.uri}";
        return $merchant;
    }

    public function getCheckoutAddress($user, $type = 'billing')
    {
        if(empty($user[$type . 'address']['zipcode']) || $user[$type . 'address']['zipcode'] == '00000') {
            return array();
        }
        $address = array(
            'given_name' => $user[$type . 'address']['firstname'],
            'family_name' => $user[$type . 'address']['lastname'],
            'postal_code' => $user[$type . 'address']['zipcode'],
            'city' => $user[$type . 'address']['city'],
            'country' => $user['additional'][$type == 'billing' ? 'country' : 'countryShipping']['countryiso'],
            'email' => $user['additional']['user']['email'],
            'phone' => $user['billingaddress']['phone'],
        );
        $address['country'] = strtolower($address['country']);
        if($address['country'] == 'de' || $address['country'] == 'nl') {
            $address['title'] = $user[$type . 'address']['salutation'] == 'ms' ? 'Frau' : 'Herr';
            $address['street_name'] = $user[$type . 'address']['street'];
            $address['street_number'] = $user[$type . 'address']['streetnumber'];
        } else {
            $address['street_address'] = trim(
                $user[$type . 'address']['street'] . ' ' . $user[$type . 'address']['streetnumber']
            );
        }

        return $address;
    }

    public function getCheckoutCustomer($user)
    {
        $customer = array(
            'type' => 'person'
        );
        if(!empty($user['billingaddress']['birthday']) && $user['billingaddress']['birthday'] != '0000-00-00') {
            $customer['date_of_birth'] = $user['billingaddress']['birthday'];
        }
        return $customer;
    }

    /**
     *
     * @return array
     */
    public function getLabel()
    {
        return 'Klarna Checkout';
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getVersion()
    {
        $info = json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'plugin.json'), true);

        if ($info) {
            return $info['currentVersion'];
        } else {
            throw new Exception('The plugin has an invalid version file.');
        }
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return array(
            'version' => $this->getVersion(),
            'label' => $this->getLabel(),
            'description' => file_get_contents($this->Path() . 'info.txt')
        );
    }

    /**
     * Creates and returns the klarna client for an event.
     *
     * @param Enlight_Event_EventArgs $args
     * @return \Klarna_Checkout_Connector
     */
    public function onInitResourceKlarnaCheckoutConnector(Enlight_Event_EventArgs $args)
    {
        require_once $this->Path() . 'Components/KlarnaCheckout/Checkout.php';
        if($this->Config()->get('testDrive')) {
            Klarna_Checkout_Order::$baseUri
                = 'https://checkout.testdrive.klarna.com/checkout/orders';
        } else {
            Klarna_Checkout_Order::$baseUri
                = 'https://checkout.klarna.com/checkout/orders';
        }
        Klarna_Checkout_Order::$contentType
            = "application/vnd.klarna.checkout.aggregated-order-v2+json";
        $sharedSecret = $this->Config()->get('sharedSecret');
        $connector = Klarna_Checkout_Connector::create($sharedSecret);
        return $connector;
    }

    /**
     * @param Enlight_Event_EventArgs $args
     * @return Klarna
     */
    public function onInitResourceKlarnaService(Enlight_Event_EventArgs $args)
    {
        require_once $this->Path() . 'Components/Klarna/Klarna.php';
        require_once $this->Path() . 'Components/Klarna/transport/xmlrpc-3.0.0.beta/lib/xmlrpc.inc';

        $k = new Klarna();

        $k->config(
            $this->Config()->get('merchantId'),
            $this->Config()->get('sharedSecret'),
            KlarnaCountry::DE, KlarnaLanguage::DE, KlarnaCurrency::EUR,
            $this->Config()->get('testDrive') ? Klarna::BETA : Klarna::LIVE,
            'pdo',
            array(
                'table' => 's_klarna_pclasses',
                'pdo' => $this->Application()->Db()->getConnection()
            )
        );

        if($this->Config()->get('testDrive')) {
        //    $k->setActivateInfo('flags', KlarnaFlags::TEST_MODE | KlarnaFlags::RSRV_SEND_BY_EMAIL);
        } else {
        //    $k->setActivateInfo('flags', KlarnaFlags::RSRV_SEND_BY_EMAIL);
        }

        return $k;
    }
}
