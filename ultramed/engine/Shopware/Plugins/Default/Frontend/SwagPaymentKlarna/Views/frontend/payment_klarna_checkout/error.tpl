{block name='frontend_checkout_confirm_submit'}
    <div class="error">
        <div class="center">
            <strong>
                   {$KlarnaError.internal_message}
            </strong>
        </div>
    </div>
{/block}