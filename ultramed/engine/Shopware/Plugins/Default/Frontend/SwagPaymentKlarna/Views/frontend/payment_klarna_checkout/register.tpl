{namespace name="frontend/payment_klarna_checkout"}

{block name="frontend_index_header_javascript" append}
    <link rel="stylesheet" href="{link file='frontend/_resources/styles/klarna.css'}" />
{/block}

{block name="frontend_register_index_dealer_register" append}
    {foreach from=$register.payment.payment_means item=payment key=paymentKey}
        {if $payment.name == 'klarna_checkout'}
            {$KlarnaPayment = $payment}
            {break}
        {/if}
    {/foreach}
    {if $KlarnaPayment}
        {$sPayments=$sPayments|array_diff_key:[$paymentKey=>'']}
        <div class="klarna-payment klarna-payment-disable" style="padding-bottom: 20px;">
            {include file="frontend/payment_klarna_checkout/select.tpl"}
        </div>
    {/if}
{/block}

{block name='frontend_register_index_dealer_register' append}
{if $KlarnaMerchantId && $KlarnaLocale == 'de-de'}
{$KlarnaTermsUrl = "https://cdn.klarna.com/1.0/shared/content/legal/terms/{$KlarnaMerchantId}/{$KlarnaLocale|replace:"-":"_"}/checkout"}
    <div class="klarna-terms-register">
        {s name="KlarnaTermsText"}Für die Express-Kasse gelten diese <a href="{$KlarnaTermsUrl}" target="_blank">Nutzungsbedingungen</a>.{/s}
    </div>
{/if}
{/block}

{block name="frontend_index_content_top"}
    {if !$KlarnaHideCheckoutSteps}
        {$smarty.block.parent}
    {/if}
{/block}