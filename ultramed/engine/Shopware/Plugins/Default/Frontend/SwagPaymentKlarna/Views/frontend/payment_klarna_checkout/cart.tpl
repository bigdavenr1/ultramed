{namespace name="frontend/payment_klarna_checkout"}
{block name="frontend_index_header_javascript" append}
    <link rel="stylesheet" href="{link file='frontend/_resources/styles/klarna.css'}" />
{/block}
{block name="frontend_checkout_actions_confirm" prepend}
    {if $KlarnaShowButton}
        <a href="{url controller=payment_klarna action=express forceSecure}" class="{if !$CacheBreaker}button-middle large{else}btn btn-primary{/if} klarna-button">
            {s name="KlarnaButtonText"}{$KlarnaPayment.description}{/s}
        </a>
    {/if}
{/block}
