{namespace name="frontend/payment_klarna_checkout"}
{if $KlarnaShowButton}
    <a href="{url controller=payment_klarna action=express forceSecure}" class="{if !$CacheBreaker && !$swrtVersion}button-middle large{else}btn btn-primary{/if}  klarna-button" title="{s name="KlarnaButtonText"}{$KlarnaPayment.description}{/s}">
        {s name="KlarnaButtonText"}{$KlarnaPayment.description}{/s}
    </a>
{/if}
