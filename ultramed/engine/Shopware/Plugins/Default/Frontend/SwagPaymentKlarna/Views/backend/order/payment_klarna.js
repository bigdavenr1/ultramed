/*
{block name="backend/order/model/attribute/fields" append}
    { name: 'swagKlarnaInvoiceNumber', type: 'string', useNull: true },
    { name: 'swagKlarnaStatus', type: 'string', useNull: true },
{/block}
*/

/*
{block name="backend/order/model/position/fields" append}
    { name: 'shipped', type:'int' },
{/block}
*/

//{block name="backend/order/view/list/list" append}
Ext.define('Shopware.apps.Order.view.list.List-Klarna', {
    override: 'Shopware.apps.Order.view.list.List',
    paymentColumn: function(value, meta, record) {
        var me = this,
            result = me.callOverridden(arguments);
        if(record.getPayment().first()
          && record.getPayment().first().get('name') == 'klarna_checkout') {
            meta.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';
            return '<img src="{link file="backend/_resources/klarna/icon.png"}" data-qtip="' + result + '" />';
        } else {
            return result;
        }
    }
});
//{/block}

//{block name="backend/order/view/batch/form" append}
Ext.define('Shopware.apps.Order.view.batch.Form-Klarna', {
    override: 'Shopware.apps.Order.view.batch.Form',
    createSettingsContainer: function() {
        var me = this,
            container = me.callOverridden(arguments);
        container.insert(0, [{
            name: 'klarnaAction',
            triggerAction: 'all',
            fieldLabel: 'Klarna-Zahlungen',
            forceSelection: true,
            value: 0,
            store: [
                [0, 'keine Aktion'],
                [1, 'Aktivieren'],
                [2, 'Abbrechen / Stornieren']
            ],
            listeners:{
                change: function(field, value) {
                    var form = field.up('form'),
                        documentField = form.down('[name=documentType]');
                    documentField.store.load();
                    switch (value) {
                        case 0: documentField.enable(); break;
                        case 1:documentField.disable().setValue(1); break;
                        case 2: documentField.disable().setValue(4); break;
                        default:break;
                    }
                }
            }
        }]);
        return container;
    }
});
//{/block}

//{block name="backend/order/controller/batch" append}
Ext.define('Shopware.apps.Order.controller.Batch-Klarna', {
    override: 'Shopware.apps.Order.controller.Batch',

    onProcessChanges: function(form) {
        var me = this,
            orders = form.records,
            documentField = form.down('[name=documentType]'),
            values = form.getValues();

        me.klarnaAction = values.klarnaAction;
        if (!Ext.isEmpty(me.klarnaAction)) {
            documentField.enable();
            me.callOverridden(arguments);
            documentField.disable();
        } else {
            me.callOverridden(arguments);
        }
    },

    queueProcess: function(store, progressBar, orders, index, resultStore) {
        var me = this,
            url,
            order = orders[index];

        if(order && me.klarnaAction) {
            store.getProxy().extraParams.docType = 0;
            switch (me.klarnaAction) {
                case 1: url = '{url controller=paymentKlarna action=activate}'; break;
                case 2: url = '{url controller=paymentKlarna action=cancel}'; break;
                default:break;
            }
            Ext.Ajax.request({
                url: url,
                async: false,
                params: { orderId: order.getId()},
                success: function() { },
                failure: function() { }
            });
        }

        me.callOverridden(arguments);
    }
});
//{/block}

//{block name="backend/order/controller/detail" append}
Ext.define('Shopware.apps.Order.controller.Detail-Klarna', {
    override: 'Shopware.apps.Order.controller.Detail',

    init: function() {
        var me = this;

        me.control({
            'order-klarna-panel button': {
                click: me.onButtonClick
            },
            'order-klarna-panel grid': {
                selectionchange: me.onSelectionChange
            }
        });

        me.callOverridden(arguments);
    },
    onShowDetail: function(order, rec) {
        var me = this,
            payment = order.getPayment().first(),
            attribute = order.getAttributes().first(),
            status = attribute.get('swagKlarnaStatus');
        if(rec === true || payment.get('name') != 'klarna_checkout' || status || !order.get('invoiceShipping')) {
            me.callOverridden(arguments);
            return;
        }
        var params = { orderId: order.getId() },
            url = '{url controller=paymentKlarna action=update}',
            result = me.doRequest(url, params);
        me.subApplication.getStore('Order').load({
            callback:function (records) {
                Ext.iterate(records, function(record) {
                    if(record.getId() == order.getId()) {
                        me.onShowDetail(record, true);
                    }
                });
            }
        });
    },
    onSelectionChange: function(sm, selections) {
        var panel = sm.panel,
            order = panel.record,
            activateButton = panel.down('button[action=activate]'),
            refundButton = panel.down('button[action=refund]'),
            hasCancelSelection = false,
            hasActivateSelection = false;
        Ext.iterate(selections, function(selection) {
            if(selection.get('shipped') > 0 && selection.get('statusId') != 2) {
                hasCancelSelection = true;
            }
        });
        Ext.iterate(selections, function(selection) {
            if(selection.get('statusId') == 2 || selection.get('statusId') == 3) {
                return;
            }
            hasActivateSelection = true;
        });
        if(!hasActivateSelection) {
            activateButton.disable();
        } else {
            activateButton.enable();
        }
        if(!hasCancelSelection) {
            refundButton.disable();
        } else {
            refundButton.enable();
        }
    },
    onButtonClick: function(button) {
        var me = this,
            grid = button.up('grid'),
            window = button.up('window'),
            panel = button.up('order-klarna-panel'),
            order = panel.record,
            records = grid.getSelectionModel().getSelection(),
            url, msg, prompt = false, params = { orderId: order.getId() };

        switch (button.action) {
            case 'add':
                var article = grid.down('[name=article]').returnRecord;
                if(article) {
                    grid.store.add({
                        articleId: article.get('id'),
                        articleNumber: article.get('number'),
                        articleName: article.get('name'),
                        inStock: article.get('inStock')
                    });
                }
                return;
            case 'updateOrder':
                records = [];
                grid.store.each(function(record) {
                    records.push(record.data);
                });
                params['positions'] = Ext.JSON.encode(records);
                break;
            case 'refund':
            case 'activate':
                Ext.iterate(records, function(record, index) {
                    params['positions[' + record.getId() + ']'] =  record.get('selection') || record.get('quantity');
                });
                break;
            default:
                break;
        }
        switch (button.action) {
            case 'cancelOrder':
                url = '{url controller=paymentKlarna action=cancel}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'updateOrder':
                url = '{url controller=paymentKlarna action=update}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'creditOrder':
                url = '{url controller=paymentKlarna action=credit}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'activateOrder':
                url = '{url controller=paymentKlarna action=activate}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'refund':
                url = '{url controller=paymentKlarna action=refund}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'activate':
                url = '{url controller=paymentKlarna action=activate}';
                msg = 'Wollen Sie wirklich diese Aktion durchführen?';
                break;
            case 'discount':
                url = '{url controller=paymentKlarna action=discount}';
                msg = 'Bitte geben Sie den Betrag ein:';
                prompt = true;
                break;
        }
        Ext.Msg.show({
            prompt: prompt,
            title: button.text,
            msg: msg,
            buttons: Ext.Msg.OKCANCEL,
            callback: function(btn, text){
                if (btn != 'ok') {
                    return;
                }
                if(text) {
                    params['value'] = text;
                }
                var result = me.doRequest(url, params);
                if(result.success) {
                    Shopware.Notification.createGrowlMessage(button.text, 'Die Aktion wurde erfolgreich ausgeführt.', panel.title);
                } else {
                    Shopware.Notification.createStickyGrowlMessage({
                        title: button.text,
                        text:  'Es ist ein Fehler aufgetreten:<br> ' + result.message
                    }, panel.title);
                }
                me.subApplication.getStore('Order').load({
                    callback:function (records) {
                        Ext.iterate(records, function(record) {
                            if(record.getId() == order.getId()) {
                                me.onUpdateDetailPage(record, window);
                            }
                        });
                    }
                });
            }
        });
    },
    doRequest: function(url, params) {
        var me = this,
            result;
        Ext.Ajax.request({
            url: url,
            params: params,
            async: false,
            success: function(response) {
                result = Ext.decode(response.responseText);
            },
            failure: function(response) {
                console.log(response);
            }
        });
        return result;
    },
    onUpdateDetailPage: function(order, window) {
        var me = this,
            klarnaPanel = window.down('order-klarna-panel'),
            klarnaGrid = window.down('order-klarna-panel grid'),
            positionPanel = window.down('order-position-panel'),
            positionGrid = window.down('order-position-panel grid'),
            documentPanel = window.down('order-document-panel'),
            documentGrid = window.down('order-document-panel grid');
        me.callOverridden(arguments);
        klarnaPanel.record = order;
        klarnaGrid.reconfigure(order.getPositions());
        positionPanel.record = order;
        positionGrid.reconfigure(order.getPositions());
        documentPanel.record = order;
        documentGrid.reconfigure(order.getReceipt());
        me.onUpdateButtons(order, window);
    },
    onUpdateButtons: function(order, window) {
        var me = this,
            attribute = order.getAttributes().first(),
            hasInvoice = !!attribute.get('swagKlarnaInvoiceNumber'),
            status = attribute.get('swagKlarnaStatus'),
            discountButton = window.down('button[action=discount]'),
            activateButton = window.down('button[action=activateOrder]'),
            cancelButton = window.down('button[action=cancelOrder]'),
            creditButton = window.down('button[action=creditOrder]'),
            updateButton = window.down('button[action=updateOrder]');
        if(!hasInvoice || status == 'credit') {
            discountButton.disable();
            creditButton.disable();
        } else {
            discountButton.enable();
            creditButton.enable();
        }
        if(status) {
            activateButton.disable();
            updateButton.disable();
            cancelButton.disable();
        }
    }
});
//{/block}

//{block name="backend/order/view/detail/window" append}
Ext.define('Shopware.apps.Order.view.detail.Window-Klarna', {
    override: 'Shopware.apps.Order.view.detail.Window',

    createTabPanel: function() {
        var me = this,
            panel = me.callOverridden(arguments),
            payment = me.record.getPayment().first();
        if(payment.get('name') == 'klarna_checkout') {
            panel.add({
                xtype: 'order-klarna-panel',
                title: 'Klarna',
                record: me.record,
                taxStore: me.taxStore,
                statusStore: me.statusStore
            });
            panel.down('order-position-panel').disable();
        }
        return panel;
   }
});

Ext.define('Shopware.apps.Order.view.detail.Klarna', {
    extend: 'Ext.container.Container',
    alias:'widget.order-klarna-panel',
    cls: Ext.baseCSSPrefix + 'klarna-panel',
    layout: 'fit',
    style: {
        background: '#fff'
    },
    bodyPadding: 10,
    title: 'Klarna',
    initComponent:function () {
        var me = this;
        me.items = [ me.getGrid() ];
        me.callParent(arguments)
    },
    getGrid: function() {
        var me = this;

        me.rowEditor = Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 1
        });

        me.orderPositionGrid = Ext.create('Shopware.apps.Order.view.list.Position', {
            store: me.record.getPositions(),
            taxStore: me.taxStore,
            statusStore: me.statusStore,
            plugins: [ me.rowEditor ],
            style: {
                borderTop: '1px solid #A4B5C0'
            },
            dockedItems: me.getGridDockedItems(),
            selModel: me.getSelectionModel(),
            getColumns: function() {
                return me.getGridColumns(this);
            }
        });

        return me.orderPositionGrid;
    },
    getSelectionModel:function () {
        var selectionModel = Ext.create('Ext.selection.CheckboxModel', {
            panel: this
        });
        return selectionModel;
    },
    getGridDockedItems: function() {
        var me = this;
        return [{
            xtype: 'toolbar',
            ui: 'shopware-ui',
            dock: 'top',
            border: false,
            items: me.getTopBar()
        },{
            xtype: 'toolbar',
            ui: 'shopware-ui',
            dock: 'top',
            border: false,
            items: me.getSecondBar()
        }];
    },
    getTopBar:function () {
        var me = this,
            attribute = me.record.getAttributes().first(),
            hasInvoice = !!attribute.get('swagKlarnaInvoiceNumber'),
            status = attribute.get('swagKlarnaStatus');
        var items = [];
        items.push({
            iconCls:'sprite-tick-circle-frame',
            text:'Bestellung aktivieren',
            disabled:!!status,
            action:'activateOrder'
        });
        items.push({
            iconCls:'sprite-minus-circle-frame',
            text:'Bestellung stornieren',
            disabled:!!status,
            action:'cancelOrder'
        });
        items.push({
            iconCls:'sprite-minus-circle-frame',
            text:'Bestellung retournieren',
            disabled:!hasInvoice || status == 'credit',
            action:'creditOrder'
        });
        items.push({
            iconCls:'sprite-tick-circle-frame',
            text:'Auswahl aktivieren',
            disabled:true,
            action:'activate'
        });
        items.push({
            iconCls:'sprite-minus-circle-frame',
            text:'Auswahl retournieren',
            disabled:true,
            action:'refund'
        });
        return items;
    },
    getSecondBar:function () {
        var me = this,
            attribute = me.record.getAttributes().first(),
            hasInvoice = !!attribute.get('swagKlarnaInvoiceNumber'),
            status = attribute.get('swagKlarnaStatus');
        var items = [];
        items.push({
            iconCls:'sprite-plus-circle-frame',
            text:'Gutschrift hinzufügen',
            disabled:!hasInvoice || status == 'credit',
            action:'discount'
        });
        items.push({
            xtype:'articlesearch',
            name: 'article',
            disabled:!!status
        });
        items.push({
            iconCls:'sprite-plus-circle-frame',
            text:'Artikel hinzufügen',
            disabled:!!status,
            action:'add'
        });
        items.push({
            iconCls:'sprite-arrow-circle',
            text:'Bestellung aktualisieren',
            disabled:!!status,
            action:'updateOrder'
        });
        return items;
    },
    getGridColumns:function (grid) {
        var me = this;
        return [{
            header: grid.snippets.articleNumber,
            dataIndex: 'articleNumber',
            flex:2
        }, {
            header: grid.snippets.articleName,
            dataIndex: 'articleName',
            flex:2,
            editor: {
                xtype: 'textfield'
            }
        }, {
            header: grid.snippets.quantity,
            dataIndex: 'quantity',
            flex:1,
            editor: {
                xtype: 'numberfield',
                allowBlank: false,
                minValue: 0
            }
        }, {
            header: grid.snippets.price,
            dataIndex: 'price',
            flex:1,
            renderer: grid.priceColumn,
            editor: {
                xtype: 'numberfield',
                allowBlank: false,
                decimalPrecision: 2
            }
        }, {
            header: grid.snippets.total,
            dataIndex: 'total',
            flex:1,
            renderer: grid.totalColumn
        }, {
            header: grid.snippets.status,
            dataIndex: 'statusId',
            flex: 2,
            renderer: grid.statusRenderer,
            editor: {
                allowBlank: false,
                editable: false,
                xtype: 'combobox',
                queryMode: 'local',
                store: grid.statusStore ,
                displayField: 'description',
                valueField: 'id'
            }
        }, {
            header: grid.snippets.tax,
            dataIndex: 'taxId',
            flex:1,
            renderer: grid.taxRenderer,
            editor: {
                xtype: 'combobox',
                editable: false,
                queryMode: 'local',
                allowBlank: false,
                store: grid.taxStore,
                displayField: 'name',
                valueField: 'id'
            }
        }, {
            header: grid.snippets.inStock,
            dataIndex: 'inStock',
            flex:1
        }, {
            header: 'Verschickt',
            dataIndex: 'shipped',
            flex:1
        }, {
            header: 'Auswahl',
            dataIndex: 'selection',
            flex:1,
            editor: {
                xtype: 'numberfield',
                allowBlank: false,
                minValue: 0
            }
        }, {
            xtype:'actioncolumn',
            width:90,
            items:[{
                iconCls:'sprite-inbox',
                action:'openArticle',
                tooltip: grid.snippets.openArticle,
                handler:function (view, rowIndex, colIndex, item) {
                    var store = view.getStore(),
                        record = store.getAt(rowIndex);
                    grid.fireEvent('openArticle', record);
                },
                getClass: function(value, metadata, record) {
                    if (!record.get('articleId'))  {
                        return 'x-hidden';
                    }
                }
            }]
        }];

    }
});


//{/block}