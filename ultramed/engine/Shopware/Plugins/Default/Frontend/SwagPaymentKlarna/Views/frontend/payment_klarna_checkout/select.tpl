{namespace name="frontend/payment_klarna_checkout"}

<form name="" method="POST" action="{url controller=account action=savePayment sTarget=checkout}">
    <ul class="klarna-payment-select">
        <li class="label label-klarna">
            {if !$KlarnaOrder}
                <a href="{url controller=payment_klarna action=express forceSecure}">
                    {s name="KlarnaTabText"}{$KlarnaPayment.description}{/s}
                </a>
            {/if}
        </li>
        <li class="label label-default">
            <label>
                {s name="KlarnaTabOtherText"}Andere Zahlungsarten{/s}
                <input type="radio" name="register[payment]" class="auto_submit" value="{config name=paymentDefault}" />
            </label>
        </li>
    </ul>
</form>