{block name="frontend_index_header_javascript" append}
    <script async src="https://cdn.klarna.com/1.0/code/client/all.js"></script>
    <link rel="stylesheet" href="{link file='frontend/_resources/styles/klarna.css'}" />
{/block}
{block name='frontend_detail_data_price_info' prepend}
    <div style="width:210px; height:70px"
         class="klarna-widget klarna-part-payment"
         data-eid="{$KlarnaMerchantId}"
         data-locale="{$KlarnaLocale|replace:"-":"_"}"
         data-price="{$sArticle.price|replace:".":""|replace:",":""}"
         data-layout="pale"
         data-invoice-fee="">
    </div>
{/block}
