{namespace name="frontend/payment_klarna_checkout"}

{block name='frontend_account_ajax_login_input_password' append}
{if $KlarnaMerchantId && $KlarnaLocale == 'de-de'}
    {$KlarnaTermsUrl = "https://cdn.klarna.com/1.0/shared/content/legal/terms/{$KlarnaMerchantId}/{$KlarnaLocale|replace:"-":"_"}/checkout"}
    <div class="klarna-terms-login">
        {s name="KlarnaTermsText"}Für die Express-Kasse gelten diese <a href="{$KlarnaTermsUrl}" target="_blank">Nutzungsbedingungen</a>.{/s}
    </div>
{/if}
{/block}