{block name="frontend_index_header_javascript" append}
    <link rel="stylesheet" href="{link file='frontend/_resources/styles/klarna.css'}" />
{/block}

{block name="frontend_index_content_top"}
{if $KlarnaOrder}
    <div class="grid_20 first">

        {* Step box *}
        {if !$KlarnaHideCheckoutSteps}
            {include file="frontend/register/steps.tpl" sStepActive="finished"}
        {/if}

        {* AGB is not accepted by user *}
        {if $sAGBError}
            <div class="error agb_confirm">
                <div class="center">
                    <strong>
                        {s name='ConfirmErrorAGB'}{/s}
                    </strong>
                </div>
            </div>
        {/if}
    </div>
{else}
    {$smarty.block.parent}
{/if}
{/block}

{block name="frontend_index_content"}
{foreach from=$sPayments item=payment key=paymentKey}
    {if $payment.name == 'klarna_checkout'}
        {$KlarnaPayment = $payment}
        {break}
    {/if}
{/foreach}
{$sPayments=$sPayments|array_diff_key:[$paymentKey=>'']}
{if $KlarnaPayment && !$KlarnaOrder}
    <div class="klarna-payment klarna-payment-disable">
        {include file="frontend/payment_klarna_checkout/select.tpl"}
    </div>
{/if}
{if $KlarnaOrder}
<div id="confirm" class="grid_16 push_2 first">

    {* Error messages *}
    {block name='frontend_checkout_confirm_error_messages'}
        {include file="frontend/checkout/error_messages.tpl"}
    {/block}

    <div class="outer-confirm-container">
        <div class="personal-information grid_16 first">
            <div class="klarna-payment">
                {include file="frontend/payment_klarna_checkout/select.tpl"}
                <div class="klarna-payment-snippet">
                    {if $sDispatches|count>1}
                        <div class="inner_container">
                            {* Dispatch selection *}
                            {block name='frontend_checkout_confirm_shipping'}
                                {include file="frontend/checkout/confirm_dispatch.tpl"}
                            {/block}
                        </div>
                    {/if}
                    <div class="space"></div>
                    {$KlarnaOrder.gui.snippet}
                </div>
            </div>
        </div>
    </div>

    <div class="space">&nbsp;</div>
</div>
{else}
    {$smarty.block.parent}
{/if}
{/block}
