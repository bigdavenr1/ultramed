<?php
/**
 * Shopware 4.0
 * Copyright � 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Plugins
 * @subpackage Plugin
 * @copyright  Copyright (c) 2014, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Heiner Lohaus
 * @author     $Author$
 */

/**
 * Klarna payment controller
 */
class Shopware_Controllers_Backend_PaymentKlarna extends Enlight_Controller_Action
{
    /**
     *
     */
    public function preDispatch()
    {
        $this->Front()->Plugins()->Json()->setRenderer();
    }

    /**
     * Returns the payment plugin config data.
     *
     * @return Shopware_Plugins_Frontend_SwagPaymentKlarna_Bootstrap
     */
    protected function Plugin()
    {
        return Shopware()->Plugins()->Frontend()->SwagPaymentKlarna();
    }

    /**
     * Helper function to get access on the static declared repository
     *
     * @return Shopware\Models\Order\Repository
     */
    protected function Repository()
    {
        return Shopware()->Models()->getRepository('Shopware\Models\Order\Order');
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    protected function getOrderByIdQuery()
    {
        $repository = $this->Repository();
        $builder = $repository->createQueryBuilder('o');
        $builder->addSelect(array(
            'details',
            'customer',
            'documents',
            'attribute',
            'detailsAttribute'
        ));
        $builder->leftJoin('o.details', 'details')
                ->leftJoin('details.attribute', 'detailsAttribute')
                ->leftJoin('o.customer', 'customer')
                ->leftJoin('o.documents', 'documents')
                ->leftJoin('o.attribute', 'attribute');
        $builder->where('o.id = :orderId');
        $query = $builder->getQuery();
        return $query;
    }

    /**
     * @param $orderId
     * @return Shopware\Models\Order\Order
     */
    protected function getOrderById($orderId)
    {
        $query = $this->getOrderByIdQuery();
        $query->setParameter('orderId', $orderId);
        return $query->getOneOrNullResult();
    }

    protected function registerShopByOrderId($orderId)
    {
        $manager = Shopware()->Models();
        $repository = $manager->getRepository('Shopware\Models\Shop\Shop');

        $order = $this->Repository()->find($orderId);
        if($order === null) {
            throw new \Exception("Order {$orderId} not found");
        }
        $shopId = $order->getLanguageIso();
        if ($shopId === null) {
            $shop = $repository->getActiveDefault();
        } else {
            $shop = $repository->getActiveById($shopId);
        }
        if ($shop === null) {
            throw new \Exception("Shop {$shopId} not found");
        }
        $shop->registerResources(Shopware()->Bootstrap());
        $manager->clear();
        return $shop;
    }

    /**
     * @param $order Shopware\Models\Order\Order
     * @param $documentType
     * @param null $documentNumber
     * @param null $documentAmount
     * @return bool
     */
    protected function createDocument($order, $documentType, $documentNumber = null, $documentAmount = null)
    {
        $documentDir = Shopware()->DocPath('files_documents');
        $document = new Shopware\Models\Order\Document\Document;
        if($documentNumber === null) {
            $documentNumber = $order->getAttribute()->getSwagKlarnaInvoiceNumber();
        }
        if($documentAmount === null) {
            $documentAmount = $order->getInvoiceAmount();
        }

        $documentHash = sha1($documentType . $documentNumber);
        $document->fromArray(array(
            'date' => new DateTime(),
            'typeId' => $documentType,
            'customerId' => $order->getCustomer()->getId(),
            'orderId' => $order->getId(),
            'amount' => $documentAmount,
            'documentId' => $documentNumber,
            'hash' => $documentHash
        ));
        $order->getDocuments()->add($document);
        if($this->Plugin()->Config()->get('testDrive')) {
            $sourceDir = "https://testdrive.klarna.com/invoices/";
        } else {
            $sourceDir = "https://online.klarna.com/invoices/";
        }
        $sourceFile = $sourceDir . $documentNumber . '.pdf';
        $targetFile = $documentDir . $documentHash . '.pdf';
        return copy($sourceFile, $targetFile);
    }

    public function activateAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $positions = $this->Request()->getParam('positions', array());
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);

        $plugin = $this->Plugin();
        $service = $plugin->getService();
        $manager = Shopware()->Models();
        $detailStatus = $manager->find('\Shopware\Models\Order\DetailStatus', 3);
        $paymentStatus = $plugin->Config()->get('activateStatusId');
        if(!empty($paymentStatus)) {
            $paymentStatus = $manager->find('\Shopware\Models\Order\Status', $paymentStatus);
        } else {
            $paymentStatus = null;
        }

        if(($number = $order->getAttribute()->getSwagKlarnaInvoiceNumber()) !== null) {
            $this->View()->assign(array(
                'success' =>  true,
                'number' => $number
            ));
            return;
        }

        /** @var Shopware\Models\Order\Detail $position */
        foreach($order->getDetails() as $position) {
            if(empty($positions)) {
                $selection = $position->getQuantity() - $position->getShipped();
                if(!empty($selection)) {
                    //$service->addArtNo($selection, $position->getArticleNumber());
                    $position->setShipped($position->getShipped() + $selection);
                    $position->setStatus($detailStatus);
                }
            } elseif(!empty($positions[$position->getId()])) {
                $quantity = $position->getQuantity() - $position->getShipped();
                $selection = min($positions[$position->getId()], $quantity);
                if(!empty($selection)) {
                    $service->addArtNo($selection, $position->getArticleNumber());
                    $position->setShipped($position->getShipped() + $selection);
                    if($selection == $quantity) {
                        $position->setStatus($detailStatus);
                    }
                }
            }
        }

        try {
            $result = $service->activate($order->getTransactionId());
            if(!empty($result[1])) {
                $last = true;
                foreach($order->getDetails() as $position) {
                    if((empty($positions) || !empty($positions[$position->getId()]))) {
                        if($position->getAttribute()->getSwagKlarnaInvoiceNumber() === null) {
                            $position->getAttribute()->setSwagKlarnaInvoiceNumber(
                                $result[1]
                            );
                        }
                    } elseif($position->getStatus()->getId() == 0) {
                        $last = false;
                    }
                }
                if(empty($positions) || $last) {
                    $order->setClearedDate(new DateTime());
                    $order->getAttribute()->setSwagKlarnaInvoiceNumber(
                        $result[1]
                    );
                    $order->getAttribute()->setSwagKlarnaStatus(
                        'activate'
                    );
                    if($paymentStatus !== null) {
                        $order->setPaymentStatus($paymentStatus);
                    }
                }
                //$amount = $service->invoiceAmount($result[1]);
                //$this->createDocument($order, 1, $result[1], $amount);
                $manager->flush();
                $this->View()->assign(array(
                    'success' => true,
                    'number' => $result[1]
                ));
            } else {
                $this->View()->assign(array(
                    'success' => false,
                    'status' => $result[0]
                ));
            }
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function creditAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);

        $plugin = $this->Plugin();
        $service = $plugin->getService();
        $manager = Shopware()->Models();
        $status = $manager->find('\Shopware\Models\Order\DetailStatus', 2);

        try {
            $invoices = array(
                $order->getAttribute()->getSwagKlarnaInvoiceNumber()
            );
            /** @var Shopware\Models\Order\Detail $position */
            foreach($order->getDetails() as $position) {
                if($position->getAttribute()->getSwagKlarnaInvoiceNumber() !== null) {
                    $invoices[] = $position->getAttribute()->getSwagKlarnaInvoiceNumber();
                }
                $position->setStatus($status);
            }
            $result = array();
            foreach(array_unique($invoices) as $invoice) {
                $result[] = $service->creditInvoice($invoice);
                //$this->createDocument($order, 4);
            }
            $order->getAttribute()->setSwagKlarnaStatus(
                'credit'
            );
            $manager->flush();
            $this->View()->assign(array(
                'success' => !empty($result),
                'number' => $result
            ));
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function cancelAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);

        $plugin = $this->Plugin();
        $service = $plugin->getService();
        $manager = Shopware()->Models();
        $detailStatus = $manager->find('\Shopware\Models\Order\DetailStatus', 2);
        $paymentStatus = $plugin->Config()->get('cancelStatusId');
        if(!empty($paymentStatus)) {
            $paymentStatus = $manager->find('\Shopware\Models\Order\Status', $paymentStatus);
        } else {
            $paymentStatus = null;
        }

        try {
            /** @var Shopware\Models\Order\Detail $position */
            foreach($order->getDetails() as $position) {
                if(!$position->getStatus()->getId()) {
                    $position->setStatus($detailStatus);
                }
            }
            $result = $service->cancelReservation(
                $order->getTransactionId()
            );
            if($paymentStatus !== null) {
                $order->setPaymentStatus($paymentStatus);
            }
            $order->getAttribute()->setSwagKlarnaStatus(
                'cancel'
            );
            $manager->flush();
            $this->View()->assign(array(
                'success' => $result
            ));
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function refundAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);

        $plugin = $this->Plugin();
        $service = $plugin->getService();
        $manager = Shopware()->Models();
        $status = $manager->find('\Shopware\Models\Order\DetailStatus', 2);
        $positions = $this->Request()->getParam('positions', array());

        try {
            $invoices = array();
            /** @var Shopware\Models\Order\Detail $position */
            foreach($order->getDetails() as $position) {
                if(!empty($positions[$position->getId()])) {
                    $invoiceNumber = $position->getAttribute()->getSwagKlarnaInvoiceNumber();
                    $selection = min($positions[$position->getId()], $position->getShipped());
                    if($invoiceNumber !== null && !empty($selection)) {
                        $invoices[$invoiceNumber][$position->getArticleNumber()] = $selection;
                        $position->setStatus($status);
                        $position->setShipped($position->getShipped() - $selection);
                    }
                }
            }
            $result = array();
            foreach($invoices as $invoiceNumber => $articles) {
                foreach($articles as $articleNumber => $quantity) {
                    $service->addArtNo($quantity, $articleNumber);
                }
                $result[] = $service->creditPart($invoiceNumber);
            }
            $manager->flush();
            $this->View()->assign(array(
                'success' => true,
                'number' => $result
            ));
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function updateAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);

        $plugin = $this->Plugin();
        $config = $plugin->Config();
        $service = $plugin->getService();
        $manager = Shopware()->Models();
        $status = $manager->find('\Shopware\Models\Order\DetailStatus', 0);
        $positions = $this->Request()->getParam('positions', array());
        if(is_string($positions)) {
            $positions = json_decode($positions, true);
        }
        $newPositions = array();

        try {
            if($order->getInvoiceShipping() > 0) {
                $taxRate = round(($order->getInvoiceShipping() - $order->getInvoiceShippingNet()) / $order->getInvoiceShippingNet() * 100);
                if($config->get('convertShippingFee')) {
                    $detail = new Shopware\Models\Order\Detail();
                    $detail->fromArray(array(
                        'order' => $order,
                        'number' => $order->getNumber(),
                        'status' => $status,
                        'taxRate' => $taxRate,
                        'articleId' => 0,
                        'articleNumber' => 'SHIPPING',
                        'articleName' => 'Shipping Fee',
                        'price' => $order->getInvoiceShipping(),
                        'quantity' => 1,
                        'attribute' => array('attribute1' => '')
                    ));
                    $order->getDetails()->add($detail);
                    $order->setInvoiceShipping(0);
                    $order->setInvoiceShippingNet(0);
                    $manager->flush();
                    $newPositions[] = $detail;
                } else {
                    $service->addArticle(
                        1,
                        'SHIPPING',
                        'Shipping Fee',
                        $order->getInvoiceShipping(),
                        $taxRate,
                        0,
                        KlarnaFlags::INC_VAT | KlarnaFlags::IS_SHIPMENT
                    );
                }
            }

            if(!empty($positions)) {
                foreach($positions as $positionData) {
                    $position = null;
                    if(!empty($positionData['id'])) {
                        /** @var \Shopware\Models\Order\Detail $testPosition */
                        foreach($order->getDetails() as $testPosition) {
                            if($testPosition->getId() == $positionData['id']) {
                                $position = $testPosition; break;
                            }
                        }
                    }
                    if($position === null) {
                        $position = new Shopware\Models\Order\Detail();
                        $position->fromArray(array(
                            'order' => $order,
                            'number' => $order->getNumber(),
                            'status' => $status,
                            'attribute' => array('attribute1' => '')
                        ));
                    }
                    if(!empty($positionData['taxId'])
                      && ($position->getTax() === null || $position->getTax()->getId() != $positionData['taxId'])) {
                        /** @var \Shopware\Models\Tax\Tax $tax */
                        $tax = $manager->find('\Shopware\Models\Tax\Tax', $positionData['taxId']);
                        $position->setTax($tax);
                        $positionData['taxRate'] = $tax->getTax();
                    }
                    $position->fromArray(array(
                        'taxRate' => $positionData['taxRate'],
                        'articleId' => $positionData['articleId'],
                        'articleNumber' => $positionData['articleNumber'],
                        'articleName' => $positionData['articleName'],
                        'price' => $positionData['price'],
                        'quantity' => $positionData['quantity']
                    ));
                    $newPositions[] = $position;
                }
                $order->setDetails($newPositions);
            }

            /** @var Shopware\Models\Order\Detail $position */
            foreach($order->getDetails() as $position) {
                $flags = KlarnaFlags::INC_VAT;
                if($position->getMode() != 0 && $position->getPrice() > 0) {
                    $flags |= KlarnaFlags::IS_HANDLING;
                } elseif($position->getArticleNumber() == 'SHIPPING') {
                    $flags |= KlarnaFlags::IS_SHIPMENT;
                }
                $service->addArticle(
                    $position->getQuantity(),
                    $position->getArticleNumber(),
                    $position->getArticleName(),
                    $position->getPrice(),
                    $position->getTaxRate(),
                    0,
                    $flags
                );
            }

//            /** @var Shopware\Models\Customer\Customer $customer */
//            $customer = $order->getCustomer();
//            $billing = $order->getBilling();
//            $billingAddress = new KlarnaAddr(
//                $customer->getEmail(), $billing->getPhone(), null,
//                utf8_decode($billing->getFirstName()), utf8_decode($billing->getLastName()), "",
//                utf8_decode($billing->getStreet()), utf8_decode($billing->getZipCode()),
//                utf8_decode($billing->getCity()), $billing->getCountry()->getIso(),
//                utf8_decode($billing->getStreetNumber())
//            );
//            $service->setAddress(KlarnaFlags::IS_BILLING, $billingAddress);
//            $shipping = $order->getShipping();
//            $shippingAddress = new KlarnaAddr(
//                null, null, null,
//                utf8_decode($shipping->getFirstName()), utf8_decode($shipping->getLastName()), "",
//                utf8_decode($shipping->getStreet()), utf8_decode($shipping->getZipCode()),
//                utf8_decode($shipping->getCity()), $shipping->getCountry()->getIso(),
//                utf8_decode($shipping->getStreetNumber())
//            );
//            $service->setAddress(KlarnaFlags::IS_SHIPPING, $billingAddress);
            $service->update($order->getTransactionId());
            $manager->flush();
            $this->View()->assign(array(
                'success' => true
            ));
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }

    public function discountAction()
    {
        $orderId = $this->Request()->getParam('orderId');
        $this->registerShopByOrderId($orderId);
        $order = $this->getOrderById($orderId);
        $value = (float)str_replace(',', '.', $this->Request()->getParam('value'));
        $value = $value > 0 ? $value*-1 : $value;

        $plugin = $this->Plugin();
        $service = $plugin->getService();
        $manager = Shopware()->Models();

        $invoiceNumber = $order->getAttribute()->getSwagKlarnaInvoiceNumber();
        $status = $manager->find('\Shopware\Models\Order\DetailStatus', 3);
        $tax = null;
        /** @var Shopware\Models\Order\Detail $detail */
        foreach($order->getDetails() as $detail) {
            if($detail->getTax() !== null) {
                $tax = $detail->getTax();
            }
        }

        try {
            $detail = new Shopware\Models\Order\Detail();
            $detail->fromArray(array(
                'order' => $order,
                'number' => $order->getNumber(),
                'tax' => $tax,
                'status' => $status,
                'taxRate' => $tax !== null ? $tax->getTax() : 0,
                'articleId' => 0,
                'articleNumber' => 'DISCOUNT',
                'articleName' => 'Discount',
                'price' => $value,
                'quantity' => 1,
                'attribute' => array('attribute1' => '')
            ));
            $order->getDetails()->add($detail);
            $result = $service->returnAmount(
                $invoiceNumber,
                $detail->getPrice()*-1,
                $detail->getTaxRate(),
                KlarnaFlags::INC_VAT,
                $detail->getArticleName()
            );
            $manager->flush();
            $this->View()->assign(array(
                'success' => true,
                'number' => $result
            ));
        } catch(Exception $e) {
            $this->View()->assign(array(
                'success' => false,
                'message' => $e->getMessage()
            ));
        }
    }
}
