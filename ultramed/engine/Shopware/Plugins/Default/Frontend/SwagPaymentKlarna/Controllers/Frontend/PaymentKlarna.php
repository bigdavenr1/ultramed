<?php
/**
 * Shopware 4.0
 * Copyright � 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Plugins
 * @subpackage Plugin
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Heiner Lohaus
 * @author     $Author$
 */

/**
 * Klarna payment controller
 */
class Shopware_Controllers_Frontend_PaymentKlarna extends Shopware_Controllers_Frontend_Payment
{
    /**
     *
     */
    public function preDispatch()
    {
        if (in_array($this->Request()->getActionName(), array('push'))) {
            $this->Front()->Plugins()->ViewRenderer()->setNoRender();
        }
    }

    /**
     * Index action method.
     *
     * Forwards to correct the action.
     */
    public function indexAction()
    {
        $this->redirect(array('controller' => 'checkout', 'action' => 'confirm'));
    }

    /**
     * Express payment action method.
     */
    public function expressAction()
    {
        $plugin = $this->Plugin();
        $plugin->createAccount();
        $this->redirect(array('controller' => 'checkout', 'action' => 'confirm'));
    }

    /**
     * Return action method
     *
     * Reads the transactionResult and represents it for the customer.
     */
    public function returnAction()
    {
        $transactionId = $this->Request()->getParam('transactionId');
        $plugin = $this->Plugin();
        $connector = $plugin->Connector();

        $order = new Klarna_Checkout_Order($connector, $transactionId);
        $order->fetch();

        $plugin->createAccount($order);
        $plugin->updateOrderVariables();

        if ($order['status'] == 'checkout_complete') {
            $orderNumber = $this->saveOrder(
                $order['reservation'], $order['reference']
            );
            $update = array();
            if(!empty($orderNumber)) {
                $update['merchant_reference'] = array(
                    'orderid1' => (string)$orderNumber,
                );
            }
            $order->update($update);
        }

        if(empty($orderNumber) && !empty($order['merchant_reference']['orderid1'])) {
            $orderNumber = $order['merchant_reference']['orderid1'];
        }

        // Saves postnumber for dhl packstation
        if(!empty($orderNumber)
          && !empty($order['shipping_address']['care_of'])
          && $order['shipping_address']['street_name'] == 'Packstation'
          && $plugin->Config()->get('postnumberField')) {
            $field = $plugin->Config()->get('postnumberField');
            $value = $order['shipping_address']['care_of'];
            $sql = "
                INSERT INTO s_order_attributes (orderID, `$field`)
                SELECT id, ? FROM s_order WHERE ordernumber = ?
                ON DUPLICATE KEY UPDATE `$field` = VALUES(`$field`)
            ";
            Shopware()->Db()->query($sql, array(
                $value, $orderNumber
            ));
        }

        if ($order['status'] == 'created' || $order['status'] == 'checkout_complete') {
            $this->redirect(array(
                'controller' => 'checkout',
                'action' => 'finish',
                'sUniqueID' => $order['reference']
            ));
        } else {
            $this->redirect(array('controller' => 'checkout', 'action' => 'confirm'));
        }
    }

    /**
     * Notify action method
     */
    public function pushAction()
    {
        $transactionId = $this->Request()->getParam('transactionId');
        $plugin = $this->Plugin();
        $connector = $plugin->Connector();

        $order = new Klarna_Checkout_Order($connector, $transactionId);
        $order->fetch();

        $plugin->createAccount($order);
        $plugin->updateOrderVariables();

        if ($order['status'] == 'checkout_complete') {
            $orderNumber = $this->saveOrder(
                $order['reservation'], $order['reference'],
                $plugin->Config()->get('statusId')
            );
            $update = array();
            $update['status'] = 'created';
            $update['merchant_reference'] = array(
                'orderid1' => (string)$orderNumber
            );
            $order->update($update);
        } elseif ($order['status'] == 'created') {
            $this->savePaymentStatus(
                $order['reservation'],  $order['reference'],
                $plugin->Config()->get('statusId')
            );
        }
    }

    /**
     * Returns the payment plugin config data.
     *
     * @return Shopware_Plugins_Frontend_SwagPaymentKlarna_Bootstrap
     */
    public function Plugin()
    {
        return Shopware()->Plugins()->Frontend()->SwagPaymentKlarna();
    }
}
