<?php
/**
 * SQL Storage
 *
 * PHP Version 5.3
 *
 * @category  Payment
 * @package   KlarnaAPI
 * @author    MS Dev <ms.modules@klarna.com>
 * @copyright 2012 Klarna AB (http://klarna.com)
 * @license   http://opensource.org/licenses/BSD-2-Clause BSD-2
 * @link      https://developers.klarna.com/
 */

/**
 * Include the {@link PCStorage} interface.
 */
require_once 'sqlstorage.class.php';

/**
 * PDO storage class for KlarnaPClass
 *
 * @category  Payment
 * @package   KlarnaAPI
 * @author    MS Dev <ms.modules@klarna.com>
 * @copyright 2012 Klarna AB (http://klarna.com)
 * @license   http://opensource.org/licenses/BSD-2-Clause BSD-2
 * @link      https://developers.klarna.com/
 */
class PDOStorage extends SQLStorage
{
    /**
     * return the name of the storage type
     *
     * @return string
     */
    public function getName()
    {
        return "pdo";
    }

    /**
     * Connects to the DB.
     *
     * @param array|string $config
     * @throws Klarna_DatabaseException
     *
     * @return void
     */
    public function connect($config)
    {
        if(is_array($config) && $config['pdo'] instanceof PDO) {
            $this->pdo = $config['pdo'];
            $this->dbTable = $config['table'];
        } else {
            throw new Klarna_DatabaseException('URI is invalid! Missing field or invalid characters used!');
        }
    }
}
