<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 11:45:49
         compiled from "4ba2679259bbeedabc47b1d1f2690e7643208e6e" */ ?>
<?php /*%%SmartyHeaderCode:5514849705541f9cd60d331-98053754%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ba2679259bbeedabc47b1d1f2690e7643208e6e' => 
    array (
      0 => '4ba2679259bbeedabc47b1d1f2690e7643208e6e',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '5514849705541f9cd60d331-98053754',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'billingaddress' => 0,
    'sOrderNumber' => 0,
    'sOrderDay' => 0,
    'sOrderTime' => 0,
    'sOrderDetails' => 0,
    'position' => 0,
    'details' => 0,
    'sShippingCosts' => 0,
    'sAmountNet' => 0,
    'sNet' => 0,
    'sAmount' => 0,
    'additional' => 0,
    'sPaymentTable' => 0,
    'sComment' => 0,
    'shippingaddress' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541f9cd6ae2a0_90930957',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541f9cd6ae2a0_90930957')) {function content_5541f9cd6ae2a0_90930957($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_fill')) include '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/Enlight/Template/Plugins/modifier.fill.php';
if (!is_callable('smarty_modifier_padding')) include '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/Enlight/Template/Plugins/modifier.padding.php';
if (!is_callable('smarty_mb_wordwrap')) include '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/Smarty/plugins/shared.mb_wordwrap.php';
?>Hallo <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['lastname'];?>
,
 
vielen Dank fuer Ihre Bestellung bei <?php echo 'Bellaron';?> (Nummer: <?php echo $_smarty_tpl->tpl_vars['sOrderNumber']->value;?>
) am <?php echo $_smarty_tpl->tpl_vars['sOrderDay']->value;?>
 um <?php echo $_smarty_tpl->tpl_vars['sOrderTime']->value;?>
.
Informationen zu Ihrer Bestellung:
 
Pos. Art.Nr.              Menge         Preis        Summe
<?php  $_smarty_tpl->tpl_vars['details'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['details']->_loop = false;
 $_smarty_tpl->tpl_vars['position'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sOrderDetails']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['details']->key => $_smarty_tpl->tpl_vars['details']->value){
$_smarty_tpl->tpl_vars['details']->_loop = true;
 $_smarty_tpl->tpl_vars['position']->value = $_smarty_tpl->tpl_vars['details']->key;
?>
<?php echo $_smarty_tpl->tpl_vars['position']->value+smarty_modifier_fill(1,4);?>
 <?php echo smarty_modifier_fill($_smarty_tpl->tpl_vars['details']->value['ordernumber'],20);?>
 <?php echo smarty_modifier_fill($_smarty_tpl->tpl_vars['details']->value['quantity'],6);?>
 <?php echo smarty_modifier_padding($_smarty_tpl->tpl_vars['details']->value['price'],8);?>
 EUR <?php echo smarty_modifier_padding($_smarty_tpl->tpl_vars['details']->value['amount'],8);?>
 EUR
<?php echo preg_replace('!^!m',str_repeat(' ',5),smarty_mb_wordwrap($_smarty_tpl->tpl_vars['details']->value['articlename'],49,"\n",false));?>

<?php } ?>
 
Versandkosten: <?php echo $_smarty_tpl->tpl_vars['sShippingCosts']->value;?>

Gesamtkosten Netto: <?php echo $_smarty_tpl->tpl_vars['sAmountNet']->value;?>

<?php if (!$_smarty_tpl->tpl_vars['sNet']->value){?>
Gesamtkosten Brutto: <?php echo $_smarty_tpl->tpl_vars['sAmount']->value;?>

<?php }?>
 
Gewählte Zahlungsart: <?php echo $_smarty_tpl->tpl_vars['additional']->value['payment']['description'];?>

<?php echo $_smarty_tpl->getSubTemplate ("string:".((string)$_smarty_tpl->tpl_vars['additional']->value['payment']['additionaldescription']), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php if ($_smarty_tpl->tpl_vars['additional']->value['payment']['name']=="debit"){?>
Ihre Bankverbindung:
Kontonr: <?php echo $_smarty_tpl->tpl_vars['sPaymentTable']->value['account'];?>

BLZ:<?php echo $_smarty_tpl->tpl_vars['sPaymentTable']->value['bankcode'];?>

Wir ziehen den Betrag in den nächsten Tagen von Ihrem Konto ein.
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['additional']->value['payment']['name']=="prepayment"){?>
 
Unsere Bankverbindung:
<?php echo '';?>
<?php }?>
 
<?php if ($_smarty_tpl->tpl_vars['sComment']->value){?>
Ihr Kommentar:
<?php echo $_smarty_tpl->tpl_vars['sComment']->value;?>

<?php }?>
 
Rechnungsadresse:
<?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['company'];?>

<?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['lastname'];?>

<?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['streetnumber'];?>

<?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['city'];?>

<?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['phone'];?>

<?php echo $_smarty_tpl->tpl_vars['additional']->value['country']['countryname'];?>

 
Lieferadresse:
<?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['company'];?>

<?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['firstname'];?>
 <?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['lastname'];?>

<?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['streetnumber'];?>

<?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['zipcode'];?>
 <?php echo $_smarty_tpl->tpl_vars['shippingaddress']->value['city'];?>

<?php echo $_smarty_tpl->tpl_vars['additional']->value['country']['countryname'];?>

 
<?php if ($_smarty_tpl->tpl_vars['billingaddress']->value['ustid']){?>
Ihre Umsatzsteuer-ID: <?php echo $_smarty_tpl->tpl_vars['billingaddress']->value['ustid'];?>

Bei erfolgreicher Prüfung und sofern Sie aus dem EU-Ausland
bestellen, erhalten Sie Ihre Ware umsatzsteuerbefreit.
<?php }?>
 
 
Vielen Dank dass Sie sich für Bellaron entschieden haben.

Für Fragen zum Produkt stehen wir Ihnen gerne jederzeit zur Verfügung.

Wir wünschen Ihnen viel Freude mit Ihrem Hyaluron-Serum!

Mit freundlichen Grüßen

UltraMed Cosmetics
 
<?php echo '';?>

 <?php }} ?>