<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 09:48:36
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/form/attribute.js" */ ?>
<?php /*%%SmartyHeaderCode:14619216535538a3d44728d2-52120495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b32e61e8addf76edf94926702e9c5fdec69b056f' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/form/attribute.js',
      1 => 1429521447,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14619216535538a3d44728d2-52120495',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538a3d44babd6_61544809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538a3d44babd6_61544809')) {function content_5538a3d44babd6_61544809($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.Attribute', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.Config.model.form.Attribute',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Config/getList/name/attribute';?>',
        api: {
            create: '<?php echo '/backend/Config/saveValues/name/attribute';?>',
            update: '<?php echo '/backend/Config/saveValues/name/attribute';?>',
            destroy: '<?php echo '/backend/Config/deleteValues/name/attribute';?>'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>