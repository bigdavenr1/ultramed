<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 09:48:34
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/model/form/currency.js" */ ?>
<?php /*%%SmartyHeaderCode:18788759095538a3d2ab2cb5-90538216%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e42f2d5669cf4532051ae9a72aead33009d88e70' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/model/form/currency.js',
      1 => 1429521435,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18788759095538a3d2ab2cb5-90538216',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538a3d2ad9642_63423965',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538a3d2ad9642_63423965')) {function content_5538a3d2ad9642_63423965($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Config
 * @subpackage Config
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.model.form.Currency', {
    extend: 'Shopware.apps.Base.model.Currency',

    fields: [
		//
        { name: 'symbol', type: 'string', useNull: true },
        { name: 'symbolPosition', type: 'int', useNull: true },
        { name: 'default', type: 'boolean' },
        { name: 'factor', type: 'float' },
        { name: 'position', type: 'int', useNull: true },
        { name: 'deletable', type: 'boolean', convert: function(v, r) { return !r.data.default; } }
    ]
});
//<?php }} ?>