<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 09:48:12
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/main/navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:492616765538a3bc073490-67308886%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c069f04f082ef87876559b9b6a71d0b829641bc4' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/main/navigation.js',
      1 => 1429521452,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '492616765538a3bc073490-67308886',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538a3bc095116_02844429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538a3bc095116_02844429')) {function content_5538a3bc095116_02844429($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Config
 * @subpackage Config
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Config.store.main.Navigation', {
    extend:'Ext.data.TreeStore',
    model:'Shopware.apps.Config.model.main.Navigation',
    root: {
        id: 'root',
        expanded: true
    },
    proxy:{
        type:'ajax',
        url:'<?php echo '/backend/Config/getNavigation';?>',
        reader:{
            type:'json',
            root:'data'
        }
    }
});
//
<?php }} ?>