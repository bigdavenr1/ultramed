<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 09:48:11
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/view/template/preview.js" */ ?>
<?php /*%%SmartyHeaderCode:21352422205538a3bba43588-97410828%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd5fff62bc960034ea263aabc55c37c9385fbf78c' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/view/template/preview.js',
      1 => 1429521472,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21352422205538a3bba43588-97410828',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538a3bba7c8c8_25426832',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538a3bba7c8c8_25426832')) {function content_5538a3bba7c8c8_25426832($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//

//
Ext.define('Shopware.apps.Config.view.template.Preview', {
    extend: 'Enlight.app.SubWindow',
    alias: 'widget.config-template-preview',

    height: 768,
    width: 1024,

    layout: 'fit',
    basePath: '/templates/',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vorschau: [name]<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    initComponent: function() {
        var me = this;

        me.title = new Ext.Template(me.title).applyTemplate(me.template.data);

        Ext.applyIf(me, {
            items: {
                xtype: 'image',
                src: me.basePath + '/' + me.template.get('previewFull'),
                listeners : {
                    render : function(c) {
                        c.getEl().on('click', function(){ this.fireEvent('click', c); }, c);
                    }
                }
            }
        });

        me.callParent(arguments);
    }
});
//
<?php }} ?>