<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 10:54:08
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/base/component/element/media_selection.js" */ ?>
<?php /*%%SmartyHeaderCode:1600577995538b3304b2f45-29801704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0f801797d8984631974911d78077aafd58360782' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/base/component/element/media_selection.js',
      1 => 1429521424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1600577995538b3304b2f45-29801704',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538b3304bc4b5_81401692',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538b3304bc4b5_81401692')) {function content_5538b3304bc4b5_81401692($_smarty_tpl) {?>/*
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Base
 * @subpackage Component
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
Ext.define('Shopware.apps.Base.view.element.MediaSelection', {
    extend: 'Shopware.MediaManager.MediaSelection',
    alias: [
        'widget.base-element-media',
        'widget.base-element-mediaselection',
        'widget.base-element-mediafield',
        'widget.base-element-mediaselectionfield',
        'widget.config-element-media',
        'widget.config-element-mediaselection'
    ]
});
<?php }} ?>