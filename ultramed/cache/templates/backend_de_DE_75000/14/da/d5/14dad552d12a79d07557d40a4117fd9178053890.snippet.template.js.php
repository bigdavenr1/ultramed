<?php /* Smarty version Smarty-3.1.12, created on 2015-04-23 09:48:11
         compiled from "/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/form/template.js" */ ?>
<?php /*%%SmartyHeaderCode:168925275538a3bba88f72-64013939%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14dad552d12a79d07557d40a4117fd9178053890' => 
    array (
      0 => '/var/www/vhosts/dragon057.startdedicated.de/httpdocs/shopware/templates/_default/backend/config/store/form/template.js',
      1 => 1429521449,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168925275538a3bba88f72-64013939',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5538a3bbae9c50_77932475',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5538a3bbae9c50_77932475')) {function content_5538a3bbae9c50_77932475($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.Template', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.Config.model.form.Template',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 24,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Config/getTemplateList';?>',
        api: {
            create: '<?php echo '/backend/Config/saveTemplate';?>',
            update: '<?php echo '/backend/Config/saveTemplate';?>',
            destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>