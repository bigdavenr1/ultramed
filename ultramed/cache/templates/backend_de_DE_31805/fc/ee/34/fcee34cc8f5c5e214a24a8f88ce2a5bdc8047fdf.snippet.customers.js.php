<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/customers.js" */ ?>
<?php /*%%SmartyHeaderCode:8223833365543478619bd75-08224457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fcee34cc8f5c5e214a24a8f88ce2a5bdc8047fdf' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/customers.js',
      1 => 1430113326,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8223833365543478619bd75-08224457',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347861aea10_70512065',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347861aea10_70512065')) {function content_554347861aea10_70512065($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Customers Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.navigation.Customers', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-navigation-customers',
    remoteSort: true,
    fields: [
        { name: 'week', type: 'timestamp' },
        { name: 'male', type: 'int', defaultValue: 0 },
        { name: 'female', type: 'int', defaultValue: 0 },
        { name: 'registration', type: 'int', defaultValue: 0 },
        { name: 'newCustomersOrders', type: 'int', defaultValue: 0 },
        { name: 'oldCustomersOrders', type: 'int', defaultValue: 0 },
        { name: 'orderCount', type: 'int', defaultValue: 0 },

        { name: 'oldCustomersPercent', type: 'float', convert: function(value, record) {
            var male = record.get('oldCustomersOrders');
            var order = record.get('orderCount');

            return male / order * 100;
        } },


        { name: 'newCustomersPercent', type: 'float', convert: function(value, record) {
            var male = record.get('newCustomersOrders');
            var order = record.get('orderCount');

            return male / order * 100 ;
        } },

        { name: 'malePercent', type: 'float', convert: function(value, record) {
            var male = record.get('male');
            var order = record.get('orderCount');

            return male / order * 100;
        } },
        { name: 'femalePercent', type: 'float', convert: function(value, record) {
            var female = record.get('female');
            var order = record.get('orderCount');

            return female / order * 100;
        } }


    ],
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics/getCustomers';?>',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});
<?php }} ?>