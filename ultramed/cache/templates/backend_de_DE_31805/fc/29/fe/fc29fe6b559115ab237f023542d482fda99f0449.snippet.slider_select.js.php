<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/slider_select.js" */ ?>
<?php /*%%SmartyHeaderCode:1195880494554346113c1c17-90835908%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc29fe6b559115ab237f023542d482fda99f0449' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/slider_select.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1195880494554346113c1c17-90835908',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346113e86d4_13794289',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346113e86d4_13794289')) {function content_554346113e86d4_13794289($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.SliderSelect', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.emotion-components-fields-category-slider-select',
    name: 'slider_type',

    /**
     * Snippets for the component
     * @object
     */
    snippets: {
        fields: {
            'slider_type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'slider_type','default'=>'Slider type','namespace'=>'backend/emotion/view/components/slider_select')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'slider_type','default'=>'Slider type','namespace'=>'backend/emotion/view/components/slider_select'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Slidertyp<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'slider_type','default'=>'Slider type','namespace'=>'backend/emotion/view/components/slider_select'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'empty_text': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/slider_select')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/slider_select'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/slider_select'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        store: {
            'vertical_slider': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'vertical_slider','default'=>'Vertical slider','namespace'=>'backend/emotion/view/components/slider_select')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'vertical_slider','default'=>'Vertical slider','namespace'=>'backend/emotion/view/components/slider_select'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vertikaler Slider<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'vertical_slider','default'=>'Vertical slider','namespace'=>'backend/emotion/view/components/slider_select'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'horizontal_slider': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'horizontal_slider','default'=>'Horizontal slider','namespace'=>'backend/emotion/view/components/slider_select')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'horizontal_slider','default'=>'Horizontal slider','namespace'=>'backend/emotion/view/components/slider_select'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Horizontaler Slider<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'horizontal_slider','default'=>'Horizontal slider','namespace'=>'backend/emotion/view/components/slider_select'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            emptyText: me.snippets.fields.empty_text,
            fieldLabel: me.snippets.fields.slider_type,
            displayField: 'display',
            valueField: 'value',
            queryMode: 'local',
            triggerAction: 'all',
            store: me.createStore()
        });

        me.callParent(arguments);
    },

    /**
     * Creates a local store which will be used
     * for the combo box. We don't need that data.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, snippets = me.snippets.store;

        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'value', 'display' ],
            data: [{
                value: 'horizontal',
                display: snippets.horizontal_slider
            }, {
                value: 'vertical',
                display: snippets.vertical_slider
            }]
        });
    }
});<?php }} ?>