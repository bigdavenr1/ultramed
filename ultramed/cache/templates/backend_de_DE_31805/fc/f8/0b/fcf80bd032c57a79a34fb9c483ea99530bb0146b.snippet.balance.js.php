<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/balance.js" */ ?>
<?php /*%%SmartyHeaderCode:41851451055447c3d4fd217-25819091%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fcf80bd032c57a79a34fb9c483ea99530bb0146b' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/balance.js',
      1 => 1430111760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '41851451055447c3d4fd217-25819091',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d504495_01760276',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d504495_01760276')) {function content_55447c3d504495_01760276($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.model.main.Balance', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'balance', type: 'float' },
        { name: 'balanceFormat', type: 'string' },
        { name: 'currency', type: 'string' },
        { name: 'default', type: 'boolean' }
    ]
});
<?php }} ?>