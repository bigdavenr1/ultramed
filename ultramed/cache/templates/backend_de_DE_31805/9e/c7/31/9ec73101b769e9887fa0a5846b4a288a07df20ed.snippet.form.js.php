<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/form.js" */ ?>
<?php /*%%SmartyHeaderCode:15034185855447a968af371-20748267%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ec73101b769e9887fa0a5846b4a288a07df20ed' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/form.js',
      1 => 1430113398,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15034185855447a968af371-20748267',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a96959102_40286072',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a96959102_40286072')) {function content_55447a96959102_40286072($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site site Form View
 *
 * This file contains the layout of the modules detail form.
 */

//

//
Ext.define('Shopware.apps.Site.view.site.Form', {
	extend: 'Ext.form.Panel',
    alias: 'widget.site-form',
    layout: 'anchor',
    defaults: {
        anchor: '98%',
        padding: 5
    },
    bodyPadding: 5,
    overflowY: 'auto',
    autoWidth: true,

    initComponent: function() {
        var me = this;
		me.dockedItems = [];
        me.items = me.getItems();
		me.toolBar = me.getToolBar();
		me.dockedItems.push(me.toolBar);

        me.callParent(arguments);

        var record = Ext.create('Shopware.apps.Site.model.Nodes');
        me.loadRecord(record);
    },

	getToolBar: function(){
		var me = this,
            items = [];

		me.saveButton = Ext.create('Ext.button.Button',{
			text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formLinkFieldSaveButtonText','default'=>'Save','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldSaveButtonText','default'=>'Save','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldSaveButtonText','default'=>'Save','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			action: 'onSaveSite',
			cls:'primary'
		});
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createSite'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if (!$_tmp1){?>*/
        me.saveButton.disable();
        /*<?php }?>*/
		items.push('->');

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createSite'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'updateSite'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp2||$_tmp3){?>*/
		items.push(me.saveButton);
        /*<?php }?>*/
		return Ext.create('Ext.toolbar.Toolbar',{
			dock: 'bottom',
			ui: 'shopware-ui',
			items: items
		});
	},

    getItems: function() {
        var me = this;
        return [
            {
                xtype: 'fieldset',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formDetailFormEditContentCaption','default'=>'Content','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormEditContentCaption','default'=>'Content','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inhalt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormEditContentCaption','default'=>'Content','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: me.getContentField()
            },
            {
                xtype: 'fieldset',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formDetailFormLinksCaption','default'=>'Link','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormLinksCaption','default'=>'Link','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormLinksCaption','default'=>'Link','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: me.getLinkField()
            },
            {
                xtype: 'fieldset',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formDetailFormOptionalSettingsCaption','default'=>'Settings','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormOptionalSettingsCaption','default'=>'Settings','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formDetailFormOptionalSettingsCaption','default'=>'Settings','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                collapsible: true,
                collapsed: true,
                items: me.getOptionsField()
            }
        ]
    },

    getContentField: function() {
        var me = this;

        return [
            {
                name: 'helperId',
                xtype: 'hidden'
            },
            {
                name: 'parentId',
                xtype: 'hidden'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formContentFieldDescriptionLabel','default'=>'Description','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldDescriptionLabel','default'=>'Description','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Titel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldDescriptionLabel','default'=>'Description','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formContentFieldDescriptionEmptyText','default'=>'Page name','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldDescriptionEmptyText','default'=>'Page name','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Seitentitel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldDescriptionEmptyText','default'=>'Page name','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name: 'description',
                allowBlank: false,
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formContentFieldHtmlEditorLabel','default'=>'Content','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldHtmlEditorLabel','default'=>'Content','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inhalt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formContentFieldHtmlEditorLabel','default'=>'Content','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'tinymce',
                name: 'html',
                anchor:'100%',
                height: 300
            },
                me.getDdSelector()
        ]
    },

    getLinkField: function() {
        var me = this;
		var data = [
			['_parent'],
			['_blank']
		];

        return [
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formLinkFieldAddressLabel','default'=>'Link-Address','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldAddressLabel','default'=>'Link-Address','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link-Adresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldAddressLabel','default'=>'Link-Address','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'link',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formLinkFieldTargetLabel','default'=>'Link-Target','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldTargetLabel','default'=>'Link-Target','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link-Ziel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formLinkFieldTargetLabel','default'=>'Link-Target','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'combo',
				mode:'local',
                name: 'target',
				valueField:'target',
    			displayField:'target',
                anchor:'100%',
				store: new Ext.data.SimpleStore({
					fields:['target'],
					data: data
				})
            }
        ]
    },

    getOptionsField: function() {
        var me = this;

        return [
			{
			   fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldPositionLabel','default'=>'Position','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldPositionLabel','default'=>'Position','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Position<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldPositionLabel','default'=>'Position','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			   xtype: 'textfield',
			   name: 'position',
			   anchor:'100%'
			},
            {
               fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldEmbedCodeLabel','default'=>'Embed-Code','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldEmbedCodeLabel','default'=>'Embed-Code','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Embed-Code<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldEmbedCodeLabel','default'=>'Embed-Code','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
               xtype: 'textfield',
               name: 'embedCode',
               readOnly: true,
               anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplVariableLabel_1','default'=>'Tpl. Variable 1','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_1','default'=>'Tpl. Variable 1','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Variable 1<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_1','default'=>'Tpl. Variable 1','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'tpl1variable',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplPathLabel_1','default'=>'Tpl. Path 1','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_1','default'=>'Tpl. Path 1','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Pfad 1<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_1','default'=>'Tpl. Path 1','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'tpl1path',
                anchor:'100%'
            },
            {
               fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplVariableLabel_2','default'=>'Tpl. Variable 2','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_2','default'=>'Tpl. Variable 2','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Variable 2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_2','default'=>'Tpl. Variable 2','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
               xtype: 'textfield',
               name: 'tpl2variable',
               anchor:'100%'
            },
            {
               fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplPathLabel_2','default'=>'Tpl. Path 2','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_2','default'=>'Tpl. Path 2','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Pfad 2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_2','default'=>'Tpl. Path 2','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
               xtype: 'textfield',
               name: 'tpl2path',
               anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplVariableLabel_3','default'=>'Tpl. Variable 3','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_3','default'=>'Tpl. Variable 3','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Variable 3<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplVariableLabel_3','default'=>'Tpl. Variable 3','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'tpl3variable',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldTplPathLabel_3','default'=>'Tpl. Path 3','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_3','default'=>'Tpl. Path 3','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tpl. Pfad 3<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldTplPathLabel_3','default'=>'Tpl. Path 3','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'tpl3path',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldSEOTitle','default'=>'SEO Title','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldSEOTitle','default'=>'SEO Title','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO Title<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldSEOTitle','default'=>'SEO Title','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'pageTitle',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldMetaKeywords','default'=>'Meta-Keywords','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldMetaKeywords','default'=>'Meta-Keywords','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Meta-Keywords<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldMetaKeywords','default'=>'Meta-Keywords','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'metaKeywords',
                anchor:'100%'
            },
            {
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formSettingsFieldMetaDescription','default'=>'Meta-Description','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldMetaDescription','default'=>'Meta-Description','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Meta-Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formSettingsFieldMetaDescription','default'=>'Meta-Description','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype: 'textfield',
                name: 'metaDescription',
                anchor:'100%'
            }
        ]
    },


    getDdSelector: function() {
        var me = this;

        return {
            name: 'grouping',
            margin: '0 0 0 105',
            xtype:'ddselector',
            fromTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'site'/'ddselector'/'fromTitle','default'=>'Groups','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'site'/'ddselector'/'fromTitle','default'=>'Groups','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gruppen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'site'/'ddselector'/'fromTitle','default'=>'Groups','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            toTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'site'/'ddselector'/'toTitle','default'=>'Assigned groups','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'site'/'ddselector'/'toTitle','default'=>'Assigned groups','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zugewiesene Gruppen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'site'/'ddselector'/'toTitle','default'=>'Assigned groups','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'groupName',
            fromStore: me.groupStore,
            buttons:[ 'add','remove' ],
            gridHeight: 125,
            selectedItems: me.selectedStore,
            buttonsText: {
                add: "Add",
                remove: "Remove"
            },
			fromColumns :[{
				text: 'name',
				flex: 1,
				dataIndex: 'groupName'
			}],
			toColumns :[{
				text: 'name',
				flex: 1,
				dataIndex: 'groupName'
			}]
        }
    }
});
//



<?php }} ?>