<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:35:58
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/view/main/panel.js" */ ?>
<?php /*%%SmartyHeaderCode:127199280755447e5e618d18-76475461%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8468839a1fc1ee706cf2a1f795e963f0432095aa' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/view/main/panel.js',
      1 => 1430113343,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '127199280755447e5e618d18-76475461',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e5e657699_33789908',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e5e657699_33789908')) {function content_55447e5e657699_33789908($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Banner
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**/

/**
 * Shopware UI - Banner View Main Panel
 *
 * View component which features the main panel
 * of the module. It displays the banners.
 */
//
Ext.define('Shopware.apps.Banner.view.main.Panel', {
    extend: 'Ext.container.Container',
    alias : 'widget.banner-view-main-panel',
    layout: 'border',
    style: 'background: #fff',

    /**
     * Dummy category id - will be set later
     * @integer
     */
    categoryId:0,

    /**
     * Initialize the view.main.List and defines the necessary
     * default configuration
     * @return void
     */
    initComponent : function () {
        var me = this;

        me.categoryTree = me.getCategoryTree();
        me.bannerList = me.getBannerList();
        me.toolbar = me.getBannerToolbar();
        me.items = [ me.categoryTree, me.bannerList, me.toolbar ];

        me.callParent(arguments);
    },
    /**
     * Returns the toolbar used to add or delete a banner
     * 
     * @return Ext.toolbar.Toolbar
     */
    getBannerToolbar : function() {
        return Ext.create('Ext.toolbar.Toolbar', {
            region: 'north',
            ui: 'shopware-ui',
            items: [
                /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/ 
                {
                    iconCls : 'sprite-plus-circle',
                    text : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'view'/'main_add','default'=>'Add','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_add','default'=>'Add','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_add','default'=>'Add','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    action : 'addBanner',
                    disabled : true
                },
                /* <?php }?> */
                /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/ 
                {
                    iconCls : 'sprite-minus-circle',
                    text : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'view'/'main_delete','default'=>'Delete','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_delete','default'=>'Delete','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_delete','default'=>'Delete','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    disabled : true,
                    action : 'deleteBanner'
                },
                /* <?php }?> */
                /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?>*/ 
                {
                    iconCls : 'sprite-pencil',
                    text : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'view'/'main_edit','default'=>'Edit','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_edit','default'=>'Edit','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'main_edit','default'=>'Edit','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    disabled : true,
                    action : 'editBanner'
                }
                /*<?php }?>*/
            ]
        });
    },
    /**
     * The Banner data view
     * 
     * @return Ext.Panel
     */
    getBannerList : function() {
        var me = this;

        me.dataView = Ext.create('Ext.view.View', {
            store: me.bannerStore,
            region: 'center',
            tpl: me.getBannerListTemplate(),
            multiSelect: true,
            height: '100%',//just for the selector plugin
            trackOver: true,
            overItemCls: 'x-item-over',
            itemSelector: 'div.thumb-wrap',
            emptyText: '',
            plugins: [ Ext.create('Ext.ux.DataView.DragSelector') ],

            /**
            * Data preparation for the data view
            * eg. truncate the description to max 27 chars
            *
            * @param data
            */
             prepareData : function(data) {
                Ext.apply(data, {
                    description : Ext.util.Format.ellipsis(data.description, 27),
                    img         : data.image,
                   id           : data.id
                });
                return data;
            }
        });

        return Ext.create('Ext.panel.Panel', {
            cls: 'banner-images-view',//only for the css styling
            region: 'center',
            unstyled: true,
            style: 'border-top: 1px solid #c7c7c7',
            autoScroll: true,
            items: [ me.dataView ]
        });
    },
    /**
     * Returns the ExtJS Template for the banner display
     * 
     * @return array of strings
     */
    getBannerListTemplate : function() {
        var basePath = '';
        return [
            '<tpl for=".">',
                '<div class="thumb-wrap" id="{id}">',
                    '<div class="thumb"><img src="<?php $_smarty_tpl->smarty->loadPlugin("smarty_function_flink"); echo smarty_function_flink(array("file" => '', "fullPath" => false), $_smarty_tpl); ?>{image}" title="{description}"></div>',
                    '<span class="x-editable">{description}</span>',
                '</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        ];
    }, 

    /**
     * Builds and returns the category tree
     * 
     * @return Ext.tree.Panel
     */
    getCategoryTree : function() {
        return Ext.create('Ext.tree.Panel', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'view'/'tree_title','default'=>'Catergories','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'tree_title','default'=>'Catergories','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'view'/'tree_title','default'=>'Catergories','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            collapsible: true,
            store: this.categoryStore,
            region: 'west',
            loadMask: false,
            width: 180,
            rootVisible: false,
            useArrows: false
        });
    }
});
//
<?php }} ?>