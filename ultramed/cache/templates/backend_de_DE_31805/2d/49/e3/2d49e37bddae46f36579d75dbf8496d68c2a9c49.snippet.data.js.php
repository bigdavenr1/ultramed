<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/data.js" */ ?>
<?php /*%%SmartyHeaderCode:10485254875543478600bf32-88734840%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d49e37bddae46f36579d75dbf8496d68c2a9c49' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/data.js',
      1 => 1430112842,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10485254875543478600bf32-88734840',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434786055ab7_81432159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434786055ab7_81432159')) {function content_55434786055ab7_81432159($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Data Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.Data', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-data',
    fields: ['node', 'name', 'amount', 'displayDate', { name: 'date', type: 'date', dateFormat: 'timestamp' }],
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    },

    constructor: function (config) {
        this.callParent(arguments);
    }
});
<?php }} ?>