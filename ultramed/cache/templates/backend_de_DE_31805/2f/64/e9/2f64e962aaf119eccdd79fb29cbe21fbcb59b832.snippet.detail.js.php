<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:13
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/view/premium/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:65605594055447e6d5d98b0-35793901%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f64e962aaf119eccdd79fb29cbe21fbcb59b832' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/view/premium/detail.js',
      1 => 1430113392,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '65605594055447e6d5d98b0-35793901',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e6d644fd4_42630164',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e6d644fd4_42630164')) {function content_55447e6d644fd4_42630164($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Premium
 * @subpackage Detail
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Premium.view.premium.Detail', {
    extend:'Enlight.app.Window',
    alias:'widget.premium-main-detail',
    cls:'createWindow',
    modal: true,

    layout:'border',
    autoShow:true,
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form'/'title','default'=>'Premium artice details','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'title','default'=>'Premium artice details','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prämienartikel - Detail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'title','default'=>'Premium artice details','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    border:0,
    width:600,
    height:270,
    stateful:true,
    stateId:'shopware-premium-detail',
    footerButton: false,

    initComponent:function () {
        var me = this;

        me.premiumForm = me.createFormPanel();
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        me.dockedItems = [{
            xtype: 'toolbar',
            ui: 'shopware-ui',
            dock: 'bottom',
            cls: 'shopware-toolbar',
            items: me.createButtons()
        }];
        /*<?php }?>*/

        me.items = [me.premiumForm];
        me.callParent(arguments);
    },

    createFormPanel: function(){
        var me = this;
        var premiumForm = Ext.create('Ext.form.Panel', {
            collapsible:false,
            split:false,
            region:'center',
            defaults:{
                labelStyle:'font-weight: 700; text-align: right;',
                labelWidth:130,
                anchor:'100%'
            },
            bodyPadding:10,
            items:[
                {
                    xtype:'articlesearch',
                    searchFieldName: 'orderNumber',
                    returnValue: 'number',
                    name:'orderNumber',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_ordernumber','default'=>'Order number','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_ordernumber','default'=>'Order number','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_ordernumber','default'=>'Order number','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    supportText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_ordernumber'/'supporttext','default'=>'The order number of the article that will be added as premium article.','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_ordernumber'/'supporttext','default'=>'The order number of the article that will be added as premium article.','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Bestellnummer des Artikels, der als Prämienartikel hinzugefügt wird.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_ordernumber'/'supporttext','default'=>'The order number of the article that will be added as premium article.','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    allowBlank:false,
                    required: true,
                    formFieldConfig: {
                        labelStyle:'font-weight: 700; text-align: right;',
                        labelWidth:130,
                        fieldStyle: 'width: 435px'
                    }
                },
                {
                    xtype:'textfield',
                    name:'orderNumberExport',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_export_ordernumber','default'=>'Export order number','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_export_ordernumber','default'=>'Export order number','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export-Bestellnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_export_ordernumber','default'=>'Export order number','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    supportText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_export_ordernumber'/'supporttext','default'=>'This number is not required. You may leave this field blank.','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_export_ordernumber'/'supporttext','default'=>'This number is not required. You may leave this field blank.','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diese Nummer ist frei wählbar. Sie darf leer bleiben.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_export_ordernumber'/'supporttext','default'=>'This number is not required. You may leave this field blank.','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    allowBlank:true
                },
                {
                    xtype:'combobox',
                    name:'shopId',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_subshop','default'=>'Subshop','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_subshop','default'=>'Subshop','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_subshop','default'=>'Subshop','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: Ext.create('Shopware.apps.Premium.store.Subshops').load(),
                    valueField:'id',
                    displayField:'name',
                    emptyText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_subshop'/'emptytext','default'=>'Please select','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_subshop'/'emptytext','default'=>'Please select','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_subshop'/'emptytext','default'=>'Please select','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    allowBlank:false
                },
                {
                    xtype:'numberfield',
                    name:'startPrice',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_startprice','default'=>'Minimum order value','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_startprice','default'=>'Minimum order value','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mindestbestellwert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_startprice','default'=>'Minimum order value','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    supportText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_startprice'/'supporttext','default'=>'The minimum order value for the premium article.','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_startprice'/'supporttext','default'=>'The minimum order value for the premium article.','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Mindest-Bestellwert für den Prämienartikel.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_startprice'/'supporttext','default'=>'The minimum order value for the premium article.','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    hideTrigger:true,
                    allowBlank:false,
                    keyNavEnabled:false,
                    mouseWheelEnabled:false
                },
                {
                    xtype: 'hidden',
                    name: 'id'
                }
            ]
        });

        if(me.record){
            premiumForm.loadRecord(me.record);
        }

        return premiumForm;
    },

    createButtons: function(){
        var me = this;
        var buttons = ['->',
            {
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_cancel','default'=>'Cancel','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_cancel','default'=>'Cancel','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_cancel','default'=>'Cancel','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                cls: 'secondary',
                scope:me,
                handler:me.destroy
            },
            {
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_save','default'=>'Save','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_save','default'=>'Save','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_save','default'=>'Save','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action:'savePremium',
                cls:'primary'
            }
        ];

        return buttons;
    }
});
//<?php }} ?>