<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/controller/blog.js" */ ?>
<?php /*%%SmartyHeaderCode:714128733554476fc965267-06719688%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a00546e1ba0013ee3caef5cf6f1cd4fde223104f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/controller/blog.js',
      1 => 1430112874,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '714128733554476fc965267-06719688',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc9fc018_98461902',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc9fc018_98461902')) {function content_554476fc9fc018_98461902($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - Blog backend module
 *
 * Detail controller of the blog module. Handles all action around to
 * edit or create and list a blog.
 */
//
//
Ext.define('Shopware.apps.Blog.controller.Blog', {
    /**
     * Extend from the standard ExtJS 4
     * @string
     */
    extend:'Ext.app.Controller',
    /**
     * all references to get the elements by the applicable selector
     */
    refs:[
        { ref:'grid', selector:'blog-blog-list' },
        { ref:'detailWindow', selector:'blog-blog-window' },
        { ref:'optionsPanel', selector:'blog-blog-detail-sidebar-options' },
        { ref:'commentPanel', selector:'blog-blog-detail-comments' }

    ],

    /**
     * Contains all snippets for the controller
     */
    snippets: {
        confirmDeleteSingleBlogArticleTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'confirm_single_blog_article_title','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'confirm_single_blog_article_title','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diesen Blogartikel löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'confirm_single_blog_article_title','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        confirmDeleteSingleBlogArticle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'confirm_single_blog_article','default'=>'Are you sure you want to delete the selected blog article ([0])?','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'confirm_single_blog_article','default'=>'Are you sure you want to delete the selected blog article ([0])?','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sind Sie sicher, dass Sie den ausgewählten Blogartikel löschen möchten ([0])?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'confirm_single_blog_article','default'=>'Are you sure you want to delete the selected blog article ([0])?','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        deleteSingleBlogArticleSuccess: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'single_blog_article'/'success','default'=>'The Blog article has been successfully deleted','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'single_blog_article'/'success','default'=>'The Blog article has been successfully deleted','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Blogartikel wurde erfolgreich gelöscht.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'single_blog_article'/'success','default'=>'The Blog article has been successfully deleted','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        deleteSingleBlogArticleError: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'single_blog_article'/'error','default'=>'An error has occurred while deleting the selected blog article: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'single_blog_article'/'error','default'=>'An error has occurred while deleting the selected blog article: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Löschen des Blogartikels ist folgender Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'single_blog_article'/'error','default'=>'An error has occurred while deleting the selected blog article: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        confirmDeleteMultipleBlogArticles: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'multiple_blog_articles','default'=>'[0] blog articles selected. Are you sure you want to delete the selected blog articles?','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles','default'=>'[0] blog articles selected. Are you sure you want to delete the selected blog articles?','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
[0] Blogartikel ausgewählt. Sind Sie sicher, dass Sie die Ausgewählten Blogartikel löschen möchten?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles','default'=>'[0] blog articles selected. Are you sure you want to delete the selected blog articles?','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        deleteMultipleBlogArticlesSuccess: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'multiple_blog_articles'/'success','default'=>'The blog articles have been successfully deleted.','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles'/'success','default'=>'The blog articles have been successfully deleted.','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Blogartikel wurden erfolgreich gelöscht.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles'/'success','default'=>'The blog articles have been successfully deleted.','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        deleteMultipleBlogArticlesError: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'delete'/'multiple_blog_articles'/'error','default'=>'An error has occurred while deleting the selected blog articles: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles'/'error','default'=>'An error has occurred while deleting the selected blog articles: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Löschen der Blogartikel ist folgender Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'delete'/'multiple_blog_articles'/'error','default'=>'An error has occurred while deleting the selected blog articles: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        onSaveChangesSuccess: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'save'/'success','default'=>'Blog article saved successfully','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'success','default'=>'Blog article saved successfully','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Blogartikel wurde erfolgreich gespeichert.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'success','default'=>'Blog article saved successfully','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        onSaveChangesNotValid: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'save'/'not_valid','default'=>'There were not filled in all required fields','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'not_valid','default'=>'There were not filled in all required fields','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Es wurden nicht alle Pflichtfelder ausgefüllt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'not_valid','default'=>'There were not filled in all required fields','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        assignedArticleExist: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'add'/'assigned_article'/'exist','default'=>'The article [0] has been already assigned to this blog article','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'add'/'assigned_article'/'exist','default'=>'The article [0] has been already assigned to this blog article','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Artikel [0] wurder bereits zugeordnet.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'add'/'assigned_article'/'exist','default'=>'The article [0] has been already assigned to this blog article','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        assignedArticleExistTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'add'/'assigned_article'/'exist'/'title','default'=>'Already exists','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'add'/'assigned_article'/'exist'/'title','default'=>'Already exists','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aritkel bereits zugeordnet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'add'/'assigned_article'/'exist'/'title','default'=>'Already exists','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        onSaveChangesError: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'save'/'error','default'=>'An error has occurred while saving your changes.','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'error','default'=>'An error has occurred while saving your changes.','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Speichern ist ein Fehler aufgetreten.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'save'/'error','default'=>'An error has occurred while saving your changes.','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        chars: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'seo_description'/'chars','default'=>'Chars','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'seo_description'/'chars','default'=>'Chars','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeichen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'seo_description'/'chars','default'=>'Chars','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		growlMessage: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage','default'=>'Blog','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage','default'=>'Blog','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blog<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage','default'=>'Blog','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },


    /**
     * saves the selected category record
     */
    selectedCategoryRecord: null,


    /**
     * Creates the necessary event listener for this
     * specific controller and opens a new Ext.window.Window
     * to display the sub-application
     *
     * @return void
     */
    init:function () {
        var me = this;

        me.control({
            'blog-blog-tree':{
                // event will be fired if an tree item is clicked
                'itemclick': me.onItemClick
            },
            'blog-blog-list button[action=add]':{
                click:me.onCreateBlogArticle
            },
            'blog-blog-list textfield[action=searchBlogArticles]':{
                change:me.onSearchBlog
            },
            'blog-blog-list button[action=deleteBlogArticles]':{
                click:me.onDeleteMultipleBlogArticles
            },
            'blog-blog-list': {
                deleteBlogArticle: me.onDeleteSingleBlogArticle,
                editBlogArticle: me.onEditItem,
                duplicateColumn: me.onDuplicateBlogArticle
            },
            'blog-blog-detail-sidebar-assigned_articles': {
                addAssignedArticle: me.onAddAssignedArticle,
                removeAssignedArticle: me.onRemoveAssignedArticle,
                openArticleModule: me.onOpenArticleModule
            },
            'blog-blog-detail-sidebar-seo': {
                metaDescriptionChanged: me.onMetaDescriptionChange
            },
            'blog-blog-window button[action=save]': {
                click: me.onSaveBlogArticle
            }
        });
    },

    /**
     * Loads the blog list
     *
     * @param view [Ext.tree.View]
     * @param record [Ext.data.Model]
     * @event editSettings
     * @return void
     */
    onItemClick:function (view, record) {
        var me = this,
            listStore = me.subApplication.listStore;
            me.selectedCategoryRecord = record;
            listStore.getProxy().extraParams = {
                categoryId: record.getId()
            };
            listStore.load();
    },

    /**
     * Opens the Ext.window.window which displays
     * the Ext.form.Panel to create a new blog
     *
     * @return void
     */
    onCreateBlogArticle:function () {
        var me = this,
            model = Ext.create('Shopware.apps.Blog.model.Detail');

        //reset the detail Record
        me.detailRecord = null;

        if(me.selectedCategoryRecord && me.selectedCategoryRecord.get("blog")) {
            model.set("categoryId",me.selectedCategoryRecord.getId());
        }

        me.getView('blog.Window').create({
            record: model,
            categoryPathStore: me.subApplication.categoryPathStore,
            templateStore: me.subApplication.templateStore
        });
        me.getDetailWindow().formPanel.loadRecord(model);
    },

    /**
     * Opens the Ext.window.window which displays
     * the Ext.form.Panel to modify an existing blog article
     *
     * @param [object]  view - The view. Is needed to get the right f
     * @param [object]  item - The row which is affected
     * @param [integer] rowIndex - The row number
     * @return void
     */
    onEditItem:function (view, rowIndex) {
        var me = this,
            store = me.subApplication.detailStore,
            record = me.subApplication.listStore.getAt(rowIndex),
            commentStore = me.subApplication.commentStore;

        store.load({
            filters : [{
                property: 'id',
                value: record.get("id")
            }],
            callback: function(records, operation) {
                if (operation.success !== true || !records.length) {
                    return;
                }
                me.detailRecord = records[0];
                commentStore.getProxy().extraParams = {
                    blogId:record.get("id")
                };

                me.getView('blog.Window').create({
                    record: me.detailRecord,
                    categoryPathStore: me.subApplication.categoryPathStore,
                    templateStore: me.subApplication.templateStore,
                    commentStore: commentStore.load()
                });

                me.getCommentPanel().enable();
            }
        });
    },


    /**
     * Opens the Ext.window.window which displays
     * the Ext.form.Panel to duplicate an existing blog article
     *
     * @param [object]  view - The view. Is needed to get the right f
     * @param [integer] rowIndex - The row number
     * @return void
     */
    onDuplicateBlogArticle:function (view, rowIndex) {
        var me = this,
                store = me.subApplication.detailStore,
                record = me.subApplication.listStore.getAt(rowIndex);

        store.load({
            filters : [{
                property: 'id',
                value: record.get("id")
            }],
            callback: function(records, operation) {
                if (operation.success !== true || !records.length) {
                    return;
                }
                me.detailRecord = records[0];

                //delete id to save a new blog with the data of the duplicated one
                me.detailRecord.data.id = '';
                me.detailRecord.internalId = '';
                store.filters.clear();

                me.getView('blog.Window').create({
                    record: me.detailRecord,
                    categoryPathStore: me.subApplication.categoryPathStore,
                    templateStore: me.subApplication.templateStore
                });
            }
        });
    },
    /**
     * Filters the grid with the passed search value to find the right blog article
     *
     * @param field
     * @param value
     * @return void
     */
    onSearchBlog:function (field, value) {
        var me = this,
            searchString = Ext.String.trim(value),
            store = me.subApplication.listStore;
        store.filters.clear();
        store.currentPage = 1;
        store.filter('filter',searchString);
    },

    /**
     * Event listener which deletes a single blog based on the passed
     * grid (e.g. the grid store) and the row index
     *
     * @param [object] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - Position of the event
     * @return void
     */
    onDeleteSingleBlogArticle:function (grid, rowIndex) {
        var me = this,
                store = me.subApplication.listStore,
                record = store.getAt(rowIndex);
        store.currentPage = 1;
        // we do not just delete - we are polite and ask the user if he is sure.
        Ext.MessageBox.confirm(
            me.snippets.confirmDeleteSingleBlogArticleTitle,
            Ext.String.format(me.snippets.confirmDeleteSingleBlogArticle, record.get('title')), function (response) {
            if (response !== 'yes') {
                return false;
            }
            record.destroy({
                callback:function (data, operation) {
                    var records = operation.getRecords(),
                            record = records[0],
                            rawData = record.getProxy().getReader().rawData;

                    if ( operation.success === true ) {
                        Shopware.Notification.createGrowlMessage('',me.snippets.deleteSingleBlogArticleSuccess, me.snippets.growlMessage);
                    } else {
                        Shopware.Notification.createGrowlMessage('',me.snippets.deleteSingleBlogArticleError + rawData.errorMsg, me.snippets.growlMessage);
                    }
                }
            });
            store.load();
        });

    },

    /**
     * Event listener method which deletes multiple blog articles
     *
     * @return void
     */
    onDeleteMultipleBlogArticles:function () {
        var me = this,
                grid = me.getGrid(),
                sm = grid.getSelectionModel(),
                selection = sm.getSelection(),
                store = me.subApplication.listStore,
                noOfElements = selection.length;

        store.currentPage = 1;

        // Get the user to confirm the delete process
        Ext.MessageBox.confirm(
                me.snippets.confirmDeleteSingleBlogArticleTitle,
                Ext.String.format(me.snippets.confirmDeleteMultipleBlogArticles, noOfElements), function (response) {
            if (response !== 'yes') {
                return false;
            }
            if (selection.length > 0) {
                store.remove(selection);
                store.save({
                    callback: function(batch) {
                        var rawData = batch.proxy.getReader().rawData;
                        if (rawData.success === true) {
                            store.load();
                            Shopware.Notification.createGrowlMessage('',me.snippets.deleteMultipleBlogArticlesSuccess, me.snippets.growlMessage);
                        } else {
                            Shopware.Notification.createGrowlMessage('',me.snippets.deleteMultipleBlogArticlesError + rawData.errorMsg , me.snippets.growlMessage);
                        }
                    }
                });
            }
        })
    },

    /**
     * Event will be fired when the user want to add a similar article
     *
     * @event
     */
    onAddAssignedArticle: function(form, grid, searchField) {
        var me = this,
            selected = searchField.returnRecord,
            store = grid.getStore(),
            values = form.getValues();

        if (!form.getForm().isValid() || !(selected instanceof Ext.data.Model)) {
            return false;
        }
        var model = Ext.create('Shopware.apps.Blog.model.AssignedArticles', values);
        model.set('id', selected.get('id'));
        model.set('name', selected.get('name'));
        model.set('number', selected.get('number'));

        //check if the article is already assigned
        var exist = store.getById(model.get('id'));
        if (!(exist instanceof Ext.data.Model)) {
            store.add(model);
            //to hide the red flags
            model.commit();
        } else {
            Shopware.Notification.createGrowlMessage(me.snippets.assignedArticleExistTitle,  Ext.String.format(me.snippets.assignedArticleExist, model.get('number')), me.snippets.growlMessage);
        }
    },

    /**
     * Event will be fired when the user want to remove an assigned similar article
     *
     * @event
     */
    onRemoveAssignedArticle: function(record, grid) {
        var me = this,
            store = grid.getStore();

        if (record instanceof Ext.data.Model) {
            store.remove(record);
        }
    },

    /**
     * open the specific article module page
     *
     * @param field
     * @param value
     * @return void
     */
    onOpenArticleModule:function (record) {
        var me = this;
        Shopware.app.Application.addSubApplication({
            name: 'Shopware.apps.Article',
            action: 'detail',
            params: {
                articleId: record.getId()
            }
        });
    },

    /**
     * Event will be fired when the user changed the seo description
     *
     * @event
     */
    onMetaDescriptionChange: function(textField) {
        var me = this;
        textField.supportTextEl.update(textField.value.length + "/ 150 " + me.snippets.chars);
    },

    /**
     * Event listener method which will be fired when the user
     * clicks the "save"-button in the edit-window.
     *
     * @event click
     * @param [object] btn - pressed Ext.button.Button
     * @return void
     */
    onSaveBlogArticle: function (btn) {
        var me = this,
            formPanel = me.getDetailWindow().formPanel,
            form = formPanel.getForm(),
            listStore = me.subApplication.listStore,
            record = form.getRecord();

        //check if all required fields are valid
        if (!form.isValid()) {
            Shopware.Notification.createGrowlMessage('',me.snippets.onSaveChangesNotValid, me.snippets.growlMessage);
            return;
        }

        var values = form.getFieldValues();

        form.updateRecord(record);

        //just to save empty values
        record.set('authorId', values.authorId);

        record.save({
            callback: function (self,operation) {
                if (operation.success) {
                    //enable the tabpanel
                    me.getCommentPanel().enable();
                    listStore.load();
                    //to remove all red flags
                    Shopware.Notification.createGrowlMessage('',me.snippets.onSaveChangesSuccess, me.snippets.growlMessage);
                } else {
                    Shopware.Notification.createGrowlMessage('',me.snippets.onSaveChangesError, me.snippets.growlMessage);
                }
            }
        });
    }
});
//
<?php }} ?>