<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/model/nodes.js" */ ?>
<?php /*%%SmartyHeaderCode:189476108755447a967a5864-18913889%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a0c19a7ebea1768e85925d315acdb1b4d7fc376f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/model/nodes.js',
      1 => 1430113175,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189476108755447a967a5864-18913889',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a967f1869_50939774',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a967f1869_50939774')) {function content_55447a967f1869_50939774($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
Ext.define('Shopware.apps.Site.model.Nodes', {
	extend: 'Ext.data.Model',
    fields: [
		//
        { name: 'key', type: 'string' },
        { name: 'id', type: 'string', convert: function(v, r) { return r.data.key || v; } },
        { name: 'description', type: 'string' },
        { name: 'name', type: 'string' },
        { name: 'text', convert: function(v, r) { return r.data.name ? r.data.name : v; } },
        { name: 'helperId', type: 'int' },
        { name: 'tpl1variable', type: 'string' },
        { name: 'tpl1path', type: 'string' },
        { name: 'tpl2variable', type: 'string' },
        { name: 'tpl2path', type: 'string' },
        { name: 'tpl3variable', type: 'string' },
        { name: 'tpl3path', type: 'string' },
        { name: 'html', type: 'string' },
        { name: 'parentId', type: 'string' },
        { name: 'html', type: 'string' },
        { name: 'grouping', type: 'string' },
        { name: 'position', type: 'int' },
        { name: 'link', type: 'string' },
        { name: 'target', type: 'string' },
        { name: 'pageTitle', type: 'string' },
        { name: 'metaKeywords', type: 'string' },
        { name: 'metaDescription', type: 'string' }
    ],

	proxy: {
		type: 'ajax',
        api: {
            read: '<?php echo '/backend/Site/getNodes';?>',
            create: '<?php echo '/backend/Site/saveSite';?>',
            update: '<?php echo '/backend/Site/saveSite';?>',
            destroy: '<?php echo '/backend/Site/deleteSite';?>'
        },
		reader: {
			type: 'json',
			root: 'nodes'
		}
	},
    associations: [
        { type: 'hasMany', model: 'Shopware.apps.Site.model.Attribute', name: 'getAttributes',  associationKey: 'attribute' }
    ]
});
//<?php }} ?>