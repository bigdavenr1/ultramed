<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/list/toolbar.js" */ ?>
<?php /*%%SmartyHeaderCode:175709710555434610cff746-69711351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1b6bbcf0dec094a54f5a278e63c6213f786d637' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/list/toolbar.js',
      1 => 1430113369,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175709710555434610cff746-69711351',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610d2dfe9_06651131',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610d2dfe9_06651131')) {function content_55434610d2dfe9_06651131($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Toolbar
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.list.Toolbar', {
	extend: 'Ext.toolbar.Toolbar',
    ui: 'shopware-ui',
    alias: 'widget.emotion-list-toolbar',

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.searchField = Ext.create('Ext.form.field.Text', {
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'search_emotion','default'=>'Search emotion...','namespace'=>'backend/emotion/list/toolbar')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'search_emotion','default'=>'Search emotion...','namespace'=>'backend/emotion/list/toolbar'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche Einkaufswelt...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'search_emotion','default'=>'Search emotion...','namespace'=>'backend/emotion/list/toolbar'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'searchfield',
            width: 200,
            enableKeyEvents:true,
            checkChangeBuffer:500,
            listeners: {
                change: function(field, value) {
                    me.fireEvent('searchEmotions', value);
                }
            }
        });

        me.items = [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'add_emotion','default'=>'Add emotion','namespace'=>'backend/emotion/list/toolbar')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'add_emotion','default'=>'Add emotion','namespace'=>'backend/emotion/list/toolbar'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkaufswelt hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'add_emotion','default'=>'Add emotion','namespace'=>'backend/emotion/list/toolbar'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-plus-circle',
            action: 'emotion-list-toolbar-add'
        }, {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'delete_selected_emotion','default'=>'Delete selected emotions','namespace'=>'backend/emotion/list/toolbar')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'delete_selected_emotion','default'=>'Delete selected emotions','namespace'=>'backend/emotion/list/toolbar'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Einkaufswelt(en) löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'delete_selected_emotion','default'=>'Delete selected emotions','namespace'=>'backend/emotion/list/toolbar'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-minus-circle',
            action: 'emotion-list-toolbar-delete',
            disabled: true,
            handler: function() {
                me.fireEvent('removeEmotions');
            }
        }, '->', me.searchField, {
            xtype: 'tbspacer',
            width: 6
        }];
        me.registerEvents();
        me.callParent(arguments);
    },

    /**
     * Registers additional component events.
     */
    registerEvents: function() {
    	this.addEvents(
    		/**
    		 * Event will be fired when the user insert a value into the search field
    		 *
    		 * @event
    		 * @param [string] The inserted value
    		 */
    		'searchEmotions',
            /**
             * Event will be fired when the user clicks the "remove all selected" button
             *
             * @event
             */
             'removeEmotions'
    	);
    }

});
//<?php }} ?>