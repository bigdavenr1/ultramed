<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/editwindow.js" */ ?>
<?php /*%%SmartyHeaderCode:107476075755447b772ccf23-77561193%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1400a8f443b3221ee9c362d5e7d41f1526b457a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/editwindow.js',
      1 => 1430113370,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107476075755447b772ccf23-77561193',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b772e6584_47177024',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b772e6584_47177024')) {function content_55447b772e6584_47177024($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.view.main.Editwindow', {
    extend: 'Enlight.app.Window',
    alias : 'widget.form-main-editwindow',

    /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
    title : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_edit','default'=>'Forms - Edit','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_edit','default'=>'Forms - Edit','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formular bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_edit','default'=>'Forms - Edit','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    /*<?php }else{ ?>*/
    title : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_details','default'=>'Form details','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_details','default'=>'Form details','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Forms - Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_details','default'=>'Form details','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    /*<?php }?>*/

    layout: 'fit',
    height : '90%',
    width: 860,

    /**
     * Initialize the component
     * @return void
     */
    initComponent : function () {
        var me = this;

        me.items = [{
            xtype:'tabpanel',
            items:[{
                    xtype: 'form-main-formpanel',
                    record: me.formRecord
                }, {
                    xtype: 'form-main-fieldgrid',
                    fieldStore: me.fieldStore,
                    disabled: true
                }
            ]
        }];

        me.callParent(arguments);
    }
});
//
<?php }} ?>