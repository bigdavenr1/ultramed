<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:68925821355447a96828311-15372247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c3e2e9cd7fba9682b059dc219f911bde2eab0a96' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/main/window.js',
      1 => 1430113397,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '68925821355447a96828311-15372247',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a9687e5b0_13052428',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a9687e5b0_13052428')) {function content_55447a9687e5b0_13052428($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site main Window View
 *
 * This file contains the layout for the main window.
 */

//

//
Ext.define('Shopware.apps.Site.view.main.Window', {
    extend: 'Enlight.app.Window',
    alias: 'widget.site-mainWindow',
    layout: 'border',
    width: 1200,
    height: '90%',
    autoShow: true,
    resizable: true,
    maximizable: true,
    minimizable: true,
    stateful: true,
    stateId: 'site',

    initComponent: function() {
        var me = this;

        //set the title
        me.title = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','default'=>'Sites','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','default'=>'Sites','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','default'=>'Sites','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';

        //get all items for this window
        me.items = me.getItems();

        //get the upper toolbar
        me.tbar = me.getUpperToolbar();

        //call parent
        me.callParent(arguments);
    },

    getItems: function() {
        var me = this;
        
        return [
            {
                xtype: 'site-tree',
                region: 'west',
                store: me.nodeStore,
                flex: 0.25
            },
            {
                xtype: 'site-form',
                region: 'center',
                groupStore: me.groupStore,
				selectedStore: me.selectedStore
            }
        ]
    },

    getUpperToolbar: function() {
        var me = this,
            buttons = [];

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createSite'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        buttons.push(Ext.create("Ext.button.Button",{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowCreateSiteButton','default'=>'New site','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowCreateSiteButton','default'=>'New site','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neue Seite<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowCreateSiteButton','default'=>'New site','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'onCreateSite',
            iconCls: 'sprite-blue-document--plus'
        }));
        /*<?php }?>*/

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteSite'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        buttons.push(Ext.create("Ext.button.Button",{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowDeleteSiteButton','default'=>'Delete site','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowDeleteSiteButton','default'=>'Delete site','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Seite löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowDeleteSiteButton','default'=>'Delete site','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'onDeleteSite',
            iconCls: 'sprite-blue-document--minus',
            disabled: true
        }));
        /*<?php }?>*/

        return Ext.create('Ext.toolbar.Toolbar', {
            ui: 'shopware-ui',
            region: 'north',
            items: buttons
        });
    }
});
//<?php }} ?>