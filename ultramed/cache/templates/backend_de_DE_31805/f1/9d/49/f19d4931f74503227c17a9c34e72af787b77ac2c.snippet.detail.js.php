<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:511827319554476fc3c2669-45173313%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f19d4931f74503227c17a9c34e72af787b77ac2c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/detail.js',
      1 => 1430112875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '511827319554476fc3c2669-45173313',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc3ea533_03134459',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc3ea533_03134459')) {function content_554476fc3ea533_03134459($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Detail Form backend module.
 *
 * The Detail model of the blog module represent a data row of the s_blog or the
 * Shopware\Models\Blog\Blog doctrine model, with some additional data for the additional information panel.
 */
//
Ext.define('Shopware.apps.Blog.model.Detail', {
	/**
	* Extends the standard ExtJS 4
	* @string
	*/
    extend : 'Ext.data.Model',
	/**
	* The fields used for this model
	* @array
	*/
    fields : [
		//
        { name : 'id', type : 'int' },
        { name : 'title', type : 'string' },
        { name : 'shortDescription', type : 'string' },
        { name : 'description', type : 'string' },
        { name : 'template', type : 'string' },
        { name : 'active', type : 'boolean' },
        { name : 'authorId', type : 'int', useNull: true },
        { name : 'categoryId', type : 'int', useNull: true  },
        { name : 'tags', type : 'string'},
        { name : 'metaTitle', type : 'string' },
        { name : 'metaKeyWords', type : 'string' },
        { name : 'metaDescription', type : 'string' },
        { name : 'displayDate', type: 'date', dateFormat: 'd.m.Y' },
        { name : 'displayTime', type: 'date', dateFormat: 'H:i' }
    ],
	/**
	* If the name of the field is 'id' extjs assumes automatically that
	* this field is an unique identifier. 
	*/
    idProperty : 'id',
	/**
	* Configure the data communication
	* @object
	*/
    proxy : {
        type : 'ajax',
        api:{
            read:   '<?php echo '/backend/Blog/getDetail';?>',
            create: '<?php echo '/backend/Blog/saveBlogArticle';?>',
            update: '<?php echo '/backend/Blog/saveBlogArticle';?>'
        },
        reader : {
            type : 'json',
            root : 'data',
            totalProperty: 'totalCount'
        }
    },
    associations: [
        { type: 'hasMany', model: 'Shopware.apps.Blog.model.Media', name: 'getMedia', associationKey: 'media' },
        { type: 'hasMany', model: 'Shopware.apps.Blog.model.AssignedArticles', name: 'getAssignedArticles', associationKey: 'assignedArticles' },
        { type:'hasMany', model:'Shopware.apps.Blog.model.Attribute', name:'getAttribute', associationKey:'attribute' }
    ]

});
//
<?php }} ?>