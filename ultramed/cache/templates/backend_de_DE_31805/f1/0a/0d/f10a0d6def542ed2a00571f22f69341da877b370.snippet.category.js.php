<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/category.js" */ ?>
<?php /*%%SmartyHeaderCode:22637298155447e759f6627-18615663%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f10a0d6def542ed2a00571f22f69341da877b370' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/category.js',
      1 => 1430113601,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22637298155447e759f6627-18615663',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75a1ae48_85553807',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75a1ae48_85553807')) {function content_55447e75a1ae48_85553807($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - ProductFeed detail main window.
 *
 * Displays all Detail Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.tab.Category', {
    extend:'Ext.container.Container',
    alias:'widget.product_feed-feed-tab-category',
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'category','default'=>'Blocked categories','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'category','default'=>'Blocked categories','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Geblockte Kategorien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'category','default'=>'Blocked categories','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    padding: 10,
    cls: 'shopware-toolbar',
    layout: 'anchor',

    /**
     * Initialize the controller and defines the necessary default configuration
     * @return void
     */
    initComponent : function() {
        var me = this,
            ids = [];
        if(me.record && me.record.getCategoriesStore) {
            var lockedCategoriesStore = me.record.getCategoriesStore;
            lockedCategoriesStore.each(function(element) {
                ids.push(element.get('id'));
            });
        }
        // make sure we have at least one element.
        if(ids.length == 0) {
            ids.push('0');
        }
        me.availableCategoriesTree.getProxy().extraParams = {
            'preselected[]' : ids
        };
        var tree = me.getTreeSelect(ids, me.availableCategoriesTree);

        me.items = [tree];
        me.callParent(arguments);
    },
    /**
     * Returns the selection box
     *
     * @param ids array of integers
     * @return Ext.tree.Panel
     */
    getTreeSelect : function(ids, store) {
        return Ext.create('Ext.tree.Panel', {
            name: 'categoryIds',
            store: store,
            displayField: 'name',
            rootVisible: false,
            useArrows: true,
            autoscroll: true,
            height: 270,
            queryMode: 'remote',
            expanded: true,
            flex: 1,
            root: {
                id: 1
            }
        });
    }
});
//
<?php }} ?>