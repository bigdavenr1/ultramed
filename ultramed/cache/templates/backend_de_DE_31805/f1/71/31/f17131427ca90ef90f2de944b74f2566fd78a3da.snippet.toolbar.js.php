<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/toolbar.js" */ ?>
<?php /*%%SmartyHeaderCode:2072563000554347857610e8-33985029%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f17131427ca90ef90f2de944b74f2566fd78a3da' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/toolbar.js',
      1 => 1430113331,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2072563000554347857610e8-33985029',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543478579c905_21844771',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543478579c905_21844771')) {function content_5543478579c905_21844771($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Main Toolbar Class
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.main.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.analytics-toolbar',
    ui: 'shopware-ui',

    initComponent: function () {
        var me = this,
            today = new Date();

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'combobox',
                    iconCls: 'sprite-gear',
                    name: 'shop_selection',
                    queryMode: 'remote',
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'shop_name','default'=>'Shop','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'shop_name','default'=>'Shop','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'shop_name','default'=>'Shop','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: me.shopStore,
                    multiSelect: true,
                    displayField: 'name',
                    valueField: 'id'
                },
                {
                    xtype: 'datefield',
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'from_date','default'=>'From','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'from_date','default'=>'From','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Von<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'from_date','default'=>'From','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    labelWidth: 50,
                    width: 150,
                    name: 'from_date',
                    value: new Date(today.getFullYear(), today.getMonth() - 1, today.getDate()),
                    maxValue: today
                },
                {
                    xtype: 'datefield',
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'to_date','default'=>'To','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'to_date','default'=>'To','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'to_date','default'=>'To','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name: 'to_date',
                    width: 130,
                    labelWidth: 30,
                    value: today,
                    maxValue: today
                },
                { xtype: 'tbspacer' },
                {
                    xtype: 'button',
                    iconCls: 'sprite-arrow-circle-135',
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'update','default'=>'Update','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'update','default'=>'Update','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktualisieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'update','default'=>'Update','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name: 'refresh',
                    handler: function () {
                        me.fireEvent('refreshView')
                    }
                },
                {
                    xtype: 'button',
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'export','default'=>'Export','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'export','default'=>'Export','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'export','default'=>'Export','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name: 'export',
                    iconCls: 'sprite-drive-download',
                    handler: function () {
                        me.fireEvent('exportCSV');
                    }
                },
                { xtype: 'tbfill' },
                {
                    showText: true,
                    xtype: 'cycle',
                    prependText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'view','default'=>'Display as','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view','default'=>'Display as','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzeigen als<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view','default'=>'Display as','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 ',
                    action: 'layout',
                    menu: {
                        items: [
                            {
                                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'view_chart','default'=>'Chart','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view_chart','default'=>'Chart','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diagramm<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view_chart','default'=>'Chart','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                                layout: 'chart',
                                iconCls: 'sprite-chart'
                            },
                            {
                                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'view_table','default'=>'Table','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view_table','default'=>'Table','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tabelle<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'view_table','default'=>'Table','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                                layout: 'table',
                                iconCls: 'sprite-table',
                                checked: true
                            }
                        ]
                    }
                }
            ]
        });

        me.callParent(arguments);
    }
});
//<?php }} ?>