<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:03
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/store/user_detail.js" */ ?>
<?php /*%%SmartyHeaderCode:130627709055447cbf9deb58-39104171%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '093c57704ad27515a8cb4ec21187328e03f6bb46' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/store/user_detail.js',
      1 => 1430113186,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '130627709055447cbf9deb58-39104171',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cbf9e5c37_21498452',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cbf9e5c37_21498452')) {function content_55447cbf9e5c37_21498452($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Backend - Store to load user details
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.UserManager.store.UserDetail', {
	extend: 'Ext.data.Store',
	autoLoad: false,
	pageSize: 30,
	model : 'Shopware.apps.UserManager.model.UserDetail'
});
//<?php }} ?>