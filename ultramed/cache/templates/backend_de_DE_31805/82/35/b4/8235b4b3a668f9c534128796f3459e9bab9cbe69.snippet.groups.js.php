<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/store/groups.js" */ ?>
<?php /*%%SmartyHeaderCode:171439807955447a969abbc5-73346020%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8235b4b3a668f9c534128796f3459e9bab9cbe69' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/store/groups.js',
      1 => 1430113176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171439807955447a969abbc5-73346020',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a969b3a62_91509748',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a969b3a62_91509748')) {function content_55447a969b3a62_91509748($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright � 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
Ext.define('Shopware.apps.Site.store.Groups', {
    extend : 'Ext.data.Store',
    model: 'Shopware.apps.Site.model.Groups',
    autoLoad: false
});
//<?php }} ?>