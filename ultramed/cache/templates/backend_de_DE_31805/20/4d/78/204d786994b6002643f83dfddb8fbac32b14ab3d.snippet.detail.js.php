<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:8555338455447e75558838-84553083%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '204d786994b6002643f83dfddb8fbac32b14ab3d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/detail.js',
      1 => 1430113167,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8555338455447e75558838-84553083',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e7558c360_38180357',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e7558c360_38180357')) {function content_55447e7558c360_38180357($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model -  product feed detail backend module.
 *
 * The DetailModel holds all feed data for the detail page
 */
//
Ext.define('Shopware.apps.ProductFeed.model.Detail', {
	/**
	* Extends the standard ExtJS 4
	* @string
	*/
    extend : 'Ext.data.Model',
	/**
	* The fields used for this model
	* @array
	*/
    fields : [
		//
        { name : 'id', type : 'int' },
        { name : 'name', type : 'string' },
        { name : 'fileName', type : 'string' },
        { name : 'partnerId', type : 'string' },
        { name : 'hash', type : 'string' },
        { name : 'active', type : 'int' },
        { name : 'variantExport', type : 'int', useNull:true },
        { name : 'customerGroupId', type : 'int', useNull:true },
        { name : 'languageId', type : 'int', useNull:true },
        { name : 'categoryId', type : 'int', useNull:true },
        { name : 'currencyId', type : 'int', useNull:true },
        { name : 'show', type : 'int' },
        { name : 'countArticles', type : 'int' },
        { name : 'expiry', type : 'date' },
        { name : 'interval', type : 'int', useNull: true },
        { name : 'informTemplate', type : 'int' },
        { name : 'informMail', type : 'int' },
        { name : 'encodingId', type : 'int'},
        { name : 'header', type : 'string'},
        { name : 'body', type : 'string'},
        { name : 'footer', type : 'string'},
        { name : 'priceFilter', type : 'double'},
        { name : 'instockFilter', type : 'int'},
        { name : 'countFilter', type : 'int'},
        { name : 'stockMinFilter', type : 'int'},
        { name : 'activeFilter', type : 'int'},
        { name : 'imageFilter', type : 'int'},
        { name : 'ownFilter', type : 'string'},
        { name : 'formatId', type : 'int'},
        { name : 'lastExport' },
        { name : 'cacheRefreshed' }
    ],
	/**
	* If the name of the field is 'id' extjs assumes autmagical that
	* this field is an unique identifier. 
	*/
    idProperty : 'id',
	/**
	* Configure the data communication
	* @object
	*/
    proxy : {
        type : 'ajax',
        api:{
            read:   '<?php echo '/backend/ProductFeed/getDetailFeed';?>',
            create: '<?php echo '/backend/ProductFeed/saveFeed';?>',
            update: '<?php echo '/backend/ProductFeed/saveFeed';?>',
            destroy:'<?php echo '/backend/ProductFeed/deleteFeed';?>'
        },
        reader : {
            type : 'json',
            root : 'data'
        }
    },
    /**
     * Define the associations of the export model.
     * One customer has a billing, shipping address and a debit information.
     * @array
     */
    associations:[
        {
            type:'hasMany',
            model:'Shopware.apps.ProductFeed.model.Category',
            name:'getCategories',
            associationKey:'categories'
        },
        {
            type:'hasMany',
            model:'Shopware.apps.Base.model.Supplier',
            name:'getSuppliers',
            associationKey:'suppliers'
        },
        {
            type:'hasMany',
            model:'Shopware.apps.Base.model.Article',
            name:'getArticles',
            associationKey:'articles'
        },
        {
            type:'hasMany',
            model:'Shopware.apps.ProductFeed.model.Attribute',
            name:'getAttributes',
            associationKey:'attributes'
        }
    ]
});
//
<?php }} ?>