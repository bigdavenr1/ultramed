<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:10
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/store/info.js" */ ?>
<?php /*%%SmartyHeaderCode:102271364555447c4e0c69f0-07727169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e67597d63de49269647cb0fb21704e4e93adf38d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/store/info.js',
      1 => 1430113164,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '102271364555447c4e0c69f0-07727169',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4e0d45d8_91912336',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4e0d45d8_91912336')) {function content_55447c4e0d45d8_91912336($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2013 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//
Ext.define('Shopware.apps.Performance.store.Info', {
    extend: 'Ext.data.Store',

    fields: [ 'name', 'backend', 'dir', 'size', 'files', 'freeSpace', 'message' ],

    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Cache/getInfo';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>