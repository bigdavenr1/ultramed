<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/detail/settings.js" */ ?>
<?php /*%%SmartyHeaderCode:213034732055434610ec1b29-97107842%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e6781a2665ca4471165ab573cb46f8feb47716df' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/detail/settings.js',
      1 => 1430113368,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '213034732055434610ec1b29-97107842',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461105d006_05155792',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461105d006_05155792')) {function content_5543461105d006_05155792($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Main Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.detail.Settings', {
	extend: 'Ext.form.Panel',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title'/'settings_tab','default'=>'Settings','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title'/'settings_tab','default'=>'Settings','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title'/'settings_tab','default'=>'Settings','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.emotion-detail-settings',
    bodyPadding: 20,
    border: 0,
    bodyBorder: 0,
    autoScroll: true,

    // Default settings for all underlying items
    defaults: {
        labelWidth: 200,
        anchor: '100%'
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.categoryPathStore = Ext.create('Shopware.apps.Emotion.store.CategoryPath');
        me.categoryPathStore.getProxy().extraParams.parents = true;
        me.categoryPathStore.load();

        var gridStore = Ext.create('Shopware.apps.Emotion.store.Grids').load();

        me.nameField = Ext.create('Ext.form.field.Text', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'emotion_name_field','default'=>'Emotion name','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'emotion_name_field','default'=>'Emotion name','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name der Einkaufswelt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'emotion_name_field','default'=>'Emotion name','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'emotion_name_empty','default'=>'My new emotion','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'emotion_name_empty','default'=>'My new emotion','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Meine neue Einkaufswelt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'emotion_name_empty','default'=>'My new emotion','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'name'
        });

        me.landingPageCheckbox = Ext.create('Ext.form.field.Checkbox', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'landingpage_checkbox','default'=>'Landingpage','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_checkbox','default'=>'Landingpage','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Landingpage<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_checkbox','default'=>'Landingpage','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            boxLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'landingpage_box_label','default'=>'This emotion is a landing page','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_box_label','default'=>'This emotion is a landing page','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bei dieser Einkaufswelt handelt es sich um eine Landingpage<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_box_label','default'=>'This emotion is a landing page','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'isLandingPage',
            inputValue: true,
            uncheckedValue: false,
            listeners: {
                scope: me,
                change: function(field, value) {
                    if(value) {
                        me.containerWidthField.setValue(1008);
                        me.categoryNameField.hide().setDisabled(true);
                        me.listingCheckbox.hide();
                        me.landingPageFieldSet.show();
                    } else {
                        me.containerWidthField.setValue(808);
                        me.categoryNameField.show().setDisabled(false);
                        me.landingPageFieldSet.hide();
                        me.listingCheckbox.show();
                    }
                }
            }
        });

        me.categoryNameField = Ext.create('Shopware.form.field.PagingComboBox', {
            anchor: '100%',
            name: 'categoryId',
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_category_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_category_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_category_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            allowBlank: false,
            pageSize: 15,
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_category_field','default'=>'Select a category','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_category_field','default'=>'Select a category','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_category_field','default'=>'Select a category','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: me.categoryPathStore,
            valueField: 'id',
            displayField: 'name'
        });

        me.gridComboBox = Ext.create('Ext.form.field.ComboBox', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_grid_field','default'=>'Select a grid','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_grid_field','default'=>'Select a grid','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Raster auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_grid_field','default'=>'Select a grid','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'gridId',
            allowBlank: false,
            editable: false,
            queryMode: 'remote',
            store: gridStore,
            displayField: 'name',
            valueField: 'id',
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_grid_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_grid_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_grid_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        var tplComboBox = Ext.create('Ext.form.field.ComboBox', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'fieldset'/'select_template','default'=>'Select Template','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'select_template','default'=>'Select Template','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'select_template','default'=>'Select Template','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'templateId',
            valueField: 'id',
            displayField: 'name',
            queryMode: 'remote',
            store: Ext.create('Shopware.apps.Emotion.store.Templates').load(),
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'fieldset'/'select_template_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'select_template_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'select_template_empty','default'=>'Please select...','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        me.containerWidthField = Ext.create('Ext.form.field.Number', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'fieldset'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Container-Breite<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'containerWidth',
            supportText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'fieldset'/'container_width_info','default'=>'Container width in pixel (px)','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'container_width_info','default'=>'Container width in pixel (px)','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Container-Breite in Pixel (px) - Standard: 808px für das Kategorielisting, 1008px für die Startseite<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'fieldset'/'container_width_info','default'=>'Container width in pixel (px)','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        me.activeComboBox = Ext.create('Ext.form.field.Checkbox', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'active','default'=>'Active','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'active','default'=>'Active','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'active','default'=>'Active','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            boxLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'active_box_label','default'=>'Emotion will be visible in the store front','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'active_box_label','default'=>'Emotion will be visible in the store front','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkaufswelt ist in der Storefront sichtbar<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'active_box_label','default'=>'Emotion will be visible in the store front','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'active',
            inputValue: true,
            uncheckedValue:false
        });

        me.listingCheckbox = Ext.create('Ext.form.field.Checkbox', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'listing','default'=>'Listing','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'listing','default'=>'Listing','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie Listing<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'listing','default'=>'Listing','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            boxLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'listing_box_label','default'=>'Listing will be visible under the emotion','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'listing_box_label','default'=>'Listing will be visible under the emotion','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie Listing wird unterhalb der Einkaufswelt sichtbar<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'listing_box_label','default'=>'Listing will be visible under the emotion','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'showListing',
            inputValue: true,
            uncheckedValue: false
        });

        me.timingFieldSet =  me.createTimingFieldSet();
        me.landingPageFieldSet = me.createLandingpageFieldset();

        me.items = [ me.nameField, me.landingPageCheckbox, me.categoryNameField, me.gridComboBox, tplComboBox, me.containerWidthField, me.activeComboBox, me.listingCheckbox, me.timingFieldSet, me.landingPageFieldSet ];
        me.callParent(arguments);

        me.loadRecord(me.emotion);
    },

    createTimingFieldSet: function() {
        var me = this;

        var validFrom = Ext.create('Ext.form.field.Date', {
            anchor: '100%',
            submitFormat: 'd.m.Y',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'start_date','default'=>'Start date','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'start_date','default'=>'Start date','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Startdatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'start_date','default'=>'Start date','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'validFrom'
        });

        var validTo = Ext.create('Ext.form.field.Date', {
            anchor: '100%',
            submitFormat: 'd.m.Y',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'end_date','default'=>'End date','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'end_date','default'=>'End date','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Enddatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'end_date','default'=>'End date','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'validTo'
        });

        var validFromTime = Ext.create('Ext.form.field.Time', {
            name: 'validFromTime',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'start_time','default'=>'Start time','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'start_time','default'=>'Start time','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Startzeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'start_time','default'=>'Start time','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            increment: 30,
            validationEvent: false,
            submitFormat: 'H:i',
            anchor: '100%'
        });

        var validToTime = Ext.create('Ext.form.field.Time', {
            name: 'validToTime',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'end_time','default'=>'End time','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'end_time','default'=>'End time','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Endzeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'end_time','default'=>'End time','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            increment: 30,
            submitFormat: 'H:i',
            anchor: '100%'
        });

        return {
            xtype: 'fieldset',
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'title','default'=>'Time-controlled activation','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'title','default'=>'Time-controlled activation','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeitgesteuerte Aktivierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'title','default'=>'Time-controlled activation','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            layout: 'column',
            items: [{
                xtype: 'container',
                columnWidth: 1,
                margin: '0 0 10',
                items: [{
                    xtype: 'button',
                    iconCls: 'sprite-clock--minus',
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'time_control'/'reset','default'=>'Reset time-controlled activation','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'reset','default'=>'Reset time-controlled activation','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeitgesteuerte Aktivierung zurücksetzen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'time_control'/'reset','default'=>'Reset time-controlled activation','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    handler: function() {
                        var fields = [ validFrom, validTo, validFromTime, validToTime ];

                        Ext.each(fields, function(field) {
                            field.setRawValue(null);
                        });
                    }
                }]
            },{
                xtype: 'container',
                columnWidth: .5,
                margin: '0 10 0 0',
                layout: 'anchor',
                items: [ validFrom, validTo ]
            }, {
                xtype: 'container',
                columnWidth: .5,
                layout: 'anchor',
                margin: '0 0 0 10',
                items: [ validFromTime, validToTime ]
            }]
        };
    },

    createLandingpageFieldset: function() {
        var me = this, fieldset;

        var displayField = Ext.create('Ext.form.field.Display', {
            name: 'link',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'link_action','default'=>'Link to the landingpage','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'link_action','default'=>'Link to the landingpage','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link zur Landingpage<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'link_action','default'=>'Link to the landingpage','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        var mediaSelection = Ext.create('Shopware.MediaManager.MediaSelection', {
            anchor: '100%',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'teaser_image','default'=>'Teaser image','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'teaser_image','default'=>'Teaser image','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Teaser-Bild<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'teaser_image','default'=>'Teaser image','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'landingPageTeaser'
        });

        var seoKeywords = Ext.create('Ext.form.field.Text', {
            name: 'seoKeywords',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'seo_keywords','default'=>'SEO-Keywords','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'seo_keywords','default'=>'SEO-Keywords','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO-Keywords<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'seo_keywords','default'=>'SEO-Keywords','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        var seoDescription = Ext.create('Ext.form.field.TextArea', {
            maxLength:150,
            name: 'seoDescription',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'seo_description','default'=>'SEO-Description','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'seo_description','default'=>'SEO-Description','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO-Description<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'seo_description','default'=>'SEO-Description','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        var returnCats = [];
        if(me.emotion.get('categories') && !Ext.isObject(me.emotion.get('categories'))) {
            var categories =  me.emotion.get('categories');

            Ext.each(categories, function(category) {
                returnCats.push(category.id);
            });
            me.emotion.set('categories', returnCats);
        }

        me.categorySearchField = Ext.create('Ext.ux.form.field.BoxSelect', {
            anchor: '100%',
            width: '100%',
            name: 'categories',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_categories_field','default'=>'Select categorie(s)','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_categories_field','default'=>'Select categorie(s)','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie(n) auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_categories_field','default'=>'Select categorie(s)','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: me.categoryPathStore,
            valueField: 'id',
            displayField: 'name',
            value: returnCats
        });

        var store = Ext.create('Ext.data.Store', {
            fields: ['display', 'value'],
            data: [{
                display: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'position'/'lefttop','default'=>'Left top','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'lefttop','default'=>'Left top','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Links oben<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'lefttop','default'=>'Left top','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                value: 'leftTop'
            },{
                display: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'position'/'leftmiddle','default'=>'Left middle','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'leftmiddle','default'=>'Left middle','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Links mitte<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'leftmiddle','default'=>'Left middle','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                value: 'leftMiddle'
            }, {
                display: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'position'/'leftbottom','default'=>'Left bottom','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'leftbottom','default'=>'Left bottom','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Links unten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'position'/'leftbottom','default'=>'Left bottom','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                value: 'leftBottom'
            }]
        });

        me.positionSelection = Ext.create('Ext.form.field.ComboBox', {
            queryMode: 'local',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'select_position','default'=>'Select position','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_position','default'=>'Select position','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Position auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'select_position','default'=>'Select position','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: store,
            displayField: 'display',
            valueField: 'value',
            name: 'landingPageBlock'
        });

        fieldset = Ext.create('Ext.form.FieldSet', {
            margin: '15 0 0',
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'settings'/'landingpage_settings','default'=>'Landingpage settings','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_settings','default'=>'Landingpage settings','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Landingpage-Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'settings'/'landingpage_settings','default'=>'Landingpage settings','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            layout: 'anchor',
            collapsible: true,
            hidden: true,
            defaults: me.defaults,
            items: [ displayField, mediaSelection, seoKeywords, seoDescription, me.categorySearchField, me.positionSelection ]
        });

        return fieldset;
    }
});
//<?php }} ?>