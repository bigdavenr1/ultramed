<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/list/grid.js" */ ?>
<?php /*%%SmartyHeaderCode:9233558555434610d31258-59836059%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a2fdf2d23e79a50b9f648920abbb0890b34d6fa' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/list/grid.js',
      1 => 1430113369,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9233558555434610d31258-59836059',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610db2c01_22369529',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610db2c01_22369529')) {function content_55434610db2c01_22369529($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Toolbar
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.list.Grid', {
	extend: 'Ext.grid.Panel',
    alias: 'widget.emotion-list-grid',

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.store = Ext.create('Shopware.apps.Emotion.store.List').load();
        me.registerEvents();
        me.columns = me.createColumns();
        me.bbar = me.createPagingToolbar();
        me.selModel = me.createSelectionModel();

        me.callParent(arguments);
    },

    registerEvents: function() {
        this.addEvents(
            'deleteemotion',
            'selectionChange',
            'editemotion',
            'duplicateemotion'
        )
    },

    createSelectionModel: function() {
        var me = this;

        return Ext.create('Ext.selection.CheckboxModel', {
            listeners:{
                // Unlocks the save button if the user has checked at least one checkbox
                selectionchange:function (sm, selections) {
                    me.fireEvent('selectionChange', selections);
                }
            }
        });
    },

    createPagingToolbar: function() {
        var me = this,
            toolbar = Ext.create('Ext.toolbar.Paging', {
            store: me.store
        });

        return toolbar;
    },

    createColumns: function() {
        var me = this;
        var columns = [{
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'category_name','default'=>'Category name','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'category_name','default'=>'Category name','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategoriename<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'category_name','default'=>'Category name','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'emotions.categoriesNames',
            flex: 2,
            renderer: me.categoryColumn,
            sortable: false
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'name','default'=>'Name','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'name','default'=>'Name','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'name','default'=>'Name','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'emotions.name',
            flex: 2,
            renderer: me.nameColumn
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'type','default'=>'Type','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'type','default'=>'Type','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'type','default'=>'Type','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 1,
            renderer: function(view, meta, record) {
                if(!record) {
                    return false;
                }

                if(record.get('isLandingPage')) {
                    return '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'renderer'/'landingpage','default'=>'Landingpage','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'renderer'/'landingpage','default'=>'Landingpage','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Landingpage<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'renderer'/'landingpage','default'=>'Landingpage','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                } else {
                    return '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'renderer'/'emotion','default'=>'Emotion','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'renderer'/'emotion','default'=>'Emotion','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkaufswelt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'renderer'/'emotion','default'=>'Emotion','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                }
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Container Breite<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'container_width','default'=>'Container width','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'emotions.containerWidth',
            flex: 1,
            renderer: me.containerWidthColumn
        }, {
            xtype: 'datecolumn',
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'date','default'=>'Last edited','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'date','default'=>'Last edited','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zuletzt bearbeitet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'date','default'=>'Last edited','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'emotions.modified',
            flex: 1,
            renderer: me.modifiedColumn
        }, {
            xtype: 'actioncolumn',
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column'/'action','default'=>'Actions','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'action','default'=>'Actions','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktion(en)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column'/'action','default'=>'Actions','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            width: 75,
            items: [
			/*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
			{
                iconCls: 'sprite-minus-circle',
                handler: function (view, rowIndex, colIndex, item, opts, record) {
                    me.fireEvent('deleteemotion', record);
                }
            },
			/*<?php }?>*/
			/*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
			{
                iconCls: 'sprite-pencil',
                handler: function(view, rowIndex, colIndex) {
                    me.fireEvent('editemotion', me, view, rowIndex, colIndex);
                }
            }
            /*<?php }?>*/
			]
        }];

        return columns;
    },

    /**
     * Column renderer function for the category name column.
     * @param [string] value    - The field value
     * @param [string] metaData - The model meta data
     * @param [string] record   - The whole data model
     */
    categoryColumn: function(value, metaData, record) {
        var names = record.get('categoriesNames');

        if (names.length) {
            return '<strong>' + names + '</strong>';
        } else {
            return '<strong><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'render'/'no_category','default'=>'No category selected','namespace'=>'backend/emotion/list/grid')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'render'/'no_category','default'=>'No category selected','namespace'=>'backend/emotion/list/grid'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine Kategorie zugewiesen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'render'/'no_category','default'=>'No category selected','namespace'=>'backend/emotion/list/grid'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</strong>';
        }
    },

    /**
     * Column renderer function for the emotion name column.
     * @param [string] value    - The field value
     * @param [string] metaData - The model meta data
     * @param [string] record   - The whole data model
     */
    nameColumn: function(value, metaData, record) {
        return record.get('name');
    },
    /**
     * Column renderer function for the category name column.
     * @param [string] value    - The field value
     * @param [string] metaData - The model meta data
     * @param [string] record   - The whole data model
     */
    containerWidthColumn: function(value, metaData, record) {
        return Ext.String.format('[0]px', record.get('containerWidth'));
    },
    /**
     * Column renderer function for the modified column
     * @param [string] value    - The field value
     * @param [string] metaData - The model meta data
     * @param [string] record   - The whole data model
     */
    modifiedColumn: function(value, metaData, record) {
       return Ext.util.Format.date(record.get('modified')) + ' ' + Ext.util.Format.date(record.get('modified'), 'H:i:s');
    }



});
//<?php }} ?>