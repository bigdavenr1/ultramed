<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/store/plugin.js" */ ?>
<?php /*%%SmartyHeaderCode:148729503455447ce3d81716-44379374%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2aca98de40b094710ffed4a7a316b9b6ed0e8338' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/store/plugin.js',
      1 => 1430111757,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148729503455447ce3d81716-44379374',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3d8d537_04990642',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3d8d537_04990642')) {function content_55447ce3d8d537_04990642($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Main
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 * Shopware Plugin Manager - Plugin Store
 * The plugin store contains all plugins of the shopware shop.
 * The plugins grouped by the active field.
 */
//
Ext.define('Shopware.apps.PluginManager.store.Plugin', {
    /**
     * Extend for the standard ExtJS 4
     * @string
     */
    extend:'Ext.data.Store',
    /**
     * Define the used model for this store
     * @string
     */
    model:'Shopware.apps.PluginManager.model.Plugin',

    /**
     * True to defer any sorting operation to the server. If false, sorting is done locally on the client.
     */
    remoteSort: true,

    /**
     * Grouping field for the grid grouping feature.
     */
    groupField: 'active',

    groupDir: 'DESC',

    /**
     * True to defer any filtering operation to the server. If false, filtering is done locally on the client.
     */
    remoteFilter: true,

    /**
     * The number of records considered to form a 'page'. This is used to power the built-in paging using the
     * nextPage and previousPage functions when the grid is paged using a PagingScroller Defaults to 25.
     */
    pageSize: 50,

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '<?php echo '/backend/PluginManager/pluginList';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'data',
            totalProperty:'total'
        }
    }


});
//

<?php }} ?>