<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:41:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/store/form/template.js" */ ?>
<?php /*%%SmartyHeaderCode:1023067288554471aa3a4291-17541606%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4ec23f8d489cdc0223307926b66f7881f88407f2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/store/form/template.js',
      1 => 1430113353,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1023067288554471aa3a4291-17541606',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554471aa3b7e48_76277553',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554471aa3b7e48_76277553')) {function content_554471aa3b7e48_76277553($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.Template', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.Config.model.form.Template',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 24,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Config/getTemplateList';?>',
        api: {
            create: '<?php echo '/backend/Config/saveTemplate';?>',
            update: '<?php echo '/backend/Config/saveTemplate';?>',
            destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>