<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:40
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/account.js" */ ?>
<?php /*%%SmartyHeaderCode:137884099955447ce4060ba4-96904074%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8ddc37a01b60535798cba3166a07700db67a06a5' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/account.js',
      1 => 1430111754,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '137884099955447ce4060ba4-96904074',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce4119649_93498442',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce4119649_93498442')) {function content_55447ce4119649_93498442($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage Controller
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.controller.Account', {

    /**
     * The parent class that this class extends.
     * @string
     */
    extend:'Ext.app.Controller',

    /**
     * References for the controller for easier accessing.
     * @array
     */
    refs: [
        { ref: 'mainWindow', selector: 'plugin-manager-main-window' },
        { ref: 'managerNavigation', selector: 'plugin-manager-manager-navigation' },
        { ref: 'storeNavigation', selector: 'plugin-manager-store-navigation' }
    ],

    licenseViewCreated: false,

	snippets: {
		account:{
			title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin manager<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			downloadsuccessful: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin wurde erfolgreich heruntergeladen.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			downloadfailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim download des Plugins ist ein Fehler aufgetreten[0]Bitte prüfen Sie zuerst Ihre Dateirechte auf den Ordnern /files/downloads und engine/Shopware/Plugins/Community<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			downloadfailedlicense: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin konnte nicht heruntergeladen werden, bitte überprüfen Sie ihre Dateirechte und ob Sie das gewünschte Plugin lizensiert haben.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			updatesuccessful: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin [0] wurde erfolgreich aktualisiert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			updatefailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim update des Plugins ist ein Fehler aufgetreten. <br>Soll das Backup wiederhergestellt werden?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			backupsuccessful: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'backupsuccessful','default'=>'Backup loaded successfully','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'backupsuccessful','default'=>'Backup loaded successfully','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Backup erfolgreich wieder hergestellt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'backupsuccessful','default'=>'Backup loaded successfully','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			backupfailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'backupfailed','default'=>'Backup could not be loaded!','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'backupfailed','default'=>'Backup could not be loaded!','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Backup konnte nicht wieder hergestellt werden!<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'backupfailed','default'=>'Backup could not be loaded!','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			loginfailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'loginfailed','default'=>'Login failed','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'loginfailed','default'=>'Login failed','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Login konnte nicht erfolgreich ausgeführt werden.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'loginfailed','default'=>'Login failed','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
		}
	},

    /**
     * A template method that is called when your application boots.
     * It is called before the Application's launch function is executed
     * so gives a hook point to run any code before your Viewport is created.
     *
     * @return void
     */
    init: function () {
        var me = this;

        me.control({
            'plugin-manager-account-login-window': {
                'login': me.onDoLogin
            },
            'plugin-manager-account-login-window textfield': {
                specialkey: function(field, event) {
                    var win = field.up('window'),
                        form = win.down('form'),
                        targetParams = win.targetParams;

                    if(event.getKey() !== event.ENTER) {
                        return false;
                    }
                    me.onDoLogin(win, form, targetParams);
                }
            },

            'plugin-manager-manager-grid': {
                updateDummyPlugin: me.onUpdateDummyPlugin
            },

            'plugin-manager-manager-navigation': {
                'openAccount': me.onOpenAccount,
                'openLicense': me.onOpenLicense,
                'openUpdates': me.onOpenUpdates
            },
            'plugin-manager-store-navigation': {
                'openAccount': me.onOpenAccount,
                'openLicense': me.onOpenLicense,
                'openUpdates': me.onOpenUpdates
            },
            'plugin-manager-account-licenses': {
                downloadplugin: me.onDownloadPlugin
            },
            'plugin-manager-account-updates': {
                updateplugin: me.onUpdatePlugin
            }
        });
    },

    onDownloadPlugin: function(grid, rowIndex, colIndex, item, eOpts, record) {
		var me = this;

        if (!record || record.get('download').length === 0) {
            return;
        }

		if(grid) {
			grid.setLoading(true);
		}

        Ext.Ajax.request({
            url:'<?php echo '/backend/Store/download';?>',
            method: 'POST',
            params: {
                url: record.get('download')
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText),
                    message;

				if(grid) {
					grid.setLoading(false);
				}

                if (response.success) {
                    Shopware.Notification.createGrowlMessage(me.snippets.account.title, me.snippets.account.downloadsuccessful);
                } else {
                    message = me.snippets.account.downloadfailed;
                    if (response.message) {
                        message = Ext.String.format(message, ':<br>' + response.message + '<br>')
                    } else {
                        message = Ext.String.format(message, ' ');
                    }
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.account.title,
                       text: message,
                       log: true
                    });
                }
            }
        });
    },

    onUpdateDummyPlugin: function(grid, rowIndex, colIndex, item, eOpts, record) {
        var me = this;
        var window = me.getMainWindow();

        if (window) {
            window.setLoading(true);
        }

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/downloadDummy';?>',
            method: 'POST',
            params: {
                name: record.get('name')
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);

                if (window) {
                    window.setLoading(false);
                }

                if (response.success === true) {
                       var pluginStore = me.subApplication.pluginStore;
                       pluginStore.load({
                           callback: function(records, operation, success) {
                               Ext.Array.each(records, function(localRecord) {
                                   if (record.get('id') == localRecord.get('id')) {
                                       var controller = me.getController('Manager');

                                       localRecord.set('wasActivated', 0);
                                       localRecord.set('wasInstalled', 0);
                                       localRecord.set('installed', new Date());
                                       localRecord.set('capabilityDummy', true);

                                       controller.onInstallPlugin(localRecord, me.subApplication.pluginStore);
                                   }
                               });
                               // do something after the load finishes
                           }
                       });
                } else {
                    var message = response.message + '';
                    if (message.length === 0) {
                        message = me.snippets.account.downloadfailedlicense;
                    }
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.account.title,
                       text: message,
                       log: true
                    });
                }
            }
        });
    },

    onUpdatePlugin: function(grid, rowIndex, colIndex, item, eOpts, record) {
        var me = this;
        var window = me.getMainWindow();

        if (window) {
            window.setLoading(true);
        }

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/downloadUpdate';?>',
            method: 'POST',
            params: {
                ordernumber: record.get('ordernumber'),
                articleId: record.get('articleId'),
                name: record.get('name')
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);

                if (window) {
                    window.setLoading(false);
                }

                if (response.success === true) {
                    if (response.activated) {
                        record.set('wasActivated', 1);
                    } else {
                        record.set('wasActivated', 0);
                    }
                    if (response.installed)  {
                        record.set('wasInstalled', 1);
                    } else {
                        record.set('wasInstalled', 0);
                    }

                    me.refreshPluginList(record);
                } else {
                    var message = response.message + '';
                    if (message.length === 0) {
                        message = me.snippets.account.downloadfailedlicense
                    }
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.account.title,
                       text: message,
                       log: true
                    });
                }
            }
        });
    },

    refreshPluginList: function(record) {
        var me = this;

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/refreshPluginList';?>',
            method: 'POST',
            callback: function(request, opts, operation) {
                if (record) {
                    var response = Ext.decode(operation.responseText);
                    me.updatePlugin(record);
                }
            }
        });
    },

    updatePlugin: function(record) {
        var me = this;

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/updatePlugin';?>',
            method: 'POST',
            params: {
                name: record.get('name'),
                availableVersion: record.get('availableVersion'),
                activated: record.get('wasActivated'),
                installed: record.get('wasInstalled')
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);

               if (response.success) {
                   if (record.get('capabilityDummy')) {
//                       var pluginStore = me.subApplication.pluginStore;
//                       pluginStore.load({
//                           callback: function(records, operation, success) {
//                               Ext.Array.each(records, function(localRecord) {
//                                   if (record.get('id') == localRecord.get('id')) {
//                                       var controller = me.getController('Manager');
//
//                                       console.log("Updated record", localRecord.data);
//
//
//                                       localRecord.set('installed', new Date());
//                                       localRecord.set('capabilityDummy', false);
//
//                                       controller.onInstallPlugin(localRecord, me.subApplication.pluginStore);
//                                   }
//                               });
//                               // do something after the load finishes
//                           }
//                       });
                   }

                   var message = Ext.String.format(me.snippets.account.updatesuccessful, record.get('name'));
                    Shopware.Notification.createGrowlMessage(me.snippets.account.title, message);
                    if (response.configRequired) {
                        var plugin = me.subApplication.pluginStore.getById(record.get('pluginId'));
                        me.subApplication.getController('Manager').onEditPlugin(null, null, null, null, null, plugin);
                    }
                    if (response.invalidateCache) {
                        var managerCtl = me.subApplication.getController('Manager');
                        managerCtl.displayCacheClearMessage(response.invalidateCache, record);
                    }
                } else {
                    Ext.MessageBox.confirm(me.snippets.account.title, me.snippets.account.updatefailed, function(btn) {
                        if(btn == 'yes') {
                            me.restorePluginBackup(record);
                        } else {
                            return false;
                        }
                    });
                }
            }
        });
    },

    restorePluginBackup: function(record) {
        var me = this;
        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/restorePlugin';?>',
            method: 'POST',
            params: {
                name: record.get('name'),
                activated: record.get('wasActivated'),
                installed: record.get('wasInstalled'),
                version: record.get('currentVersion')
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);
                if (response.success) {
                    Shopware.Notification.createGrowlMessage(me.snippets.account.title, me.snippets.account.backupsuccessful);
                    me.refreshPluginList(null);
                } else {
                    if (response.message) {
                        Shopware.Notification.createGrowlMessage(me.snippets.account.title, response.message);
                    } else {
                        Shopware.Notification.createStickyGrowlMessage({
                           title: me.snippets.account.title,
                           text: me.snippets.account.backupfailed,
                           log: true
                        });
                    }
                }
            }
        });
    },

    onOpenLogin: function(targetParams) {
        var me = this;
        me.getView('account.LoginWindow').create({ targetParams: targetParams, account: me.subApplication.myAccount });
    },

    onDoLogin: function(view, formPanel, targetParams) {
        var me = this,
            values = formPanel.getValues();

        if (!formPanel.getForm().isValid()) {
            return;
        }
        view.setLoading(true);
        me.subApplication.myAccount = Ext.create('Shopware.apps.PluginManager.model.Account', values);
        me.subApplication.myAccount.save({
            success: function() {
                view.setLoading(false);
                view.destroy();
                var storeCtl = me.subApplication.getController('Store');
                var ctl = me.subApplication.getController(targetParams.controller);
                if(targetParams.action == 'sendBuyRequest') {
                    ctl.sendBuyRequest(targetParams.record, targetParams.detail);
                } else if(targetParams.action == 'sendTaxRequest') {
                    ctl.sendTaxRequest(targetParams.record, targetParams.detail);
                } else if(targetParams.action == 'onOpenLicense') {
                    ctl.onOpenLicense(targetParams.view, targetParams.record);
                } else if(targetParams.action == 'onOpenUpdates') {
                    ctl.onOpenUpdates(targetParams.view, targetParams.record);
                // At last allow more dynamic callbacks
                } else if(targetParams.action) {
                    ctl[targetParams.action](targetParams.params);
                }
                me.subApplication.licencedProductStore.load();
                me.subApplication.updatesStore.load();
                storeCtl.refreshAccountNavigation();
            },
            failure: function(records, operation) {
                var rawData = operation.records[0].getProxy().reader.rawData;
                view.setLoading(false);
                if (rawData.message) {
                    Shopware.Notification.createGrowlMessage(me.snippets.account.title, rawData.message);
                } else {
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.account.title,
                       text: me.snippets.account.loginfailed,
                       log: true
                    });
                }
            }
        });
    },

    onOpenConfirm: function(price, record, detail) {
        var me = this;
        me.getView('account.Confirm').create({
            price: price,
            record: record,
            detail: detail
        });
    },

    onOpenAccount: function(view, record) {
        var me = this;
        if(me.subApplication.myAccount.get('accountUrl')) {
            window.open(me.subApplication.myAccount.get('accountUrl'));
        } else {
            window.open('https://account.shopware.de');
        }
    },
    onOpenLicense: function(view, record) {
        var me = this,
            mainWindow = me.getMainWindow(),
            activeTab = mainWindow.tabPanel.getActiveTab(),
            navigation,
            container;

        if(!me.checkLogin()) {
            me.onOpenLogin({
                controller: 'Account',
                action: 'onOpenLicense',
                record: record,
                view: view
            });
            return false;
        }

        // Set selected record
        var store = view.store;
        store.each(function(item) {
            item.set('selected', false);
        });
        record.set('selected', true);

        if(activeTab.initialTitle === 'manager') {
            navigation = me.getManagerNavigation();
            var store = navigation.extensionCategoryStore;
            store.each(function(item) {
                item.set('selected', false);
            });
            container = mainWindow.managerContainer;
        } else {
            navigation = me.getStoreNavigation();
            var store = navigation.categoryStore;
            store.each(function(item) {
                item.set('selected', false);
            });
            container = mainWindow.storeContainer;
        }

        if(container.items.getCount() > 1) {
            container.items.getAt(container.items.getCount() -1).destroy();
        }
        if (me.subApplication.licencedProductStore.getCount() === 0) {
            me.subApplication.licencedProductStore.load();
        }
        var view = me.getView('account.Licenses').create({
            licensedStore: me.subApplication.licencedProductStore
        });
        container.add(view);
        container.getLayout().setActiveItem(1);
    },

    onOpenUpdates: function(view, record) {
        var me = this;
        if(!me.checkLogin()) {
           me.onOpenLogin({
               controller: 'Account',
               action: 'onOpenUpdates',
               record: record,
               view: view
           });
           return false;
        }

        var managerNavigation = me.getManagerNavigation().accountCategoryStore,
            storeNavigation = me.getStoreNavigation().accountCategoryStore,
            stores = [ managerNavigation, storeNavigation ],
            mainWindow = me.getMainWindow(),
            activeTab = mainWindow.tabPanel.getActiveTab(),
            container;


        Ext.each(stores, function(store) {
            var storeRecord = store.getAt(store.getCount()-1);
            storeRecord.set('badge', record.get('badge'));
        });

        if(activeTab.initialTitle === 'manager') {
            var store = me.getManagerNavigation().extensionCategoryStore;
            store.each(function(item) {
                item.set('selected', false);
            });
            container = mainWindow.managerContainer;
        } else {
            var store = me.getStoreNavigation().categoryStore;
            store.each(function(item) {
                item.set('selected', false);
            });
            container = mainWindow.storeContainer;
        }

        // Set selected record
        var store = view.store;
        store.each(function(item) {
            item.set('selected', false);
        });
        record.set('selected', true);

        if(container.items.getCount() > 1) {
            container.items.getAt(container.items.getCount() -1).destroy();
        }
        var view = me.getView('account.Updates').create({
            updatesStore: me.subApplication.updatesStore
        });

        container.add(view);
        container.getLayout().setActiveItem(1);

    },

    checkLogin: function() {
        var me = this,
            record = me.subApplication.myAccount;

        return record.get('shopwareID').length || record.get('account_id').length;
    }
});
//
<?php }} ?>