<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:14
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/store/logs.js" */ ?>
<?php /*%%SmartyHeaderCode:6171468755447ccaf38163-43221056%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '33ed341570b34adce521c837e99b347a2212f45f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/store/logs.js',
      1 => 1430113143,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6171468755447ccaf38163-43221056',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ccaf40846_23766004',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ccaf40846_23766004')) {function content_55447ccaf40846_23766004($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Log
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware - Logs store
 *
 * This store contains all logs.
 */
//
Ext.define('Shopware.apps.Log.store.Logs', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',
    /**
    * Auto load the store after the component
    * is initialized
    * @boolean
    */
    autoLoad: false,
    /**
    * Amount of data loaded at once
    * @integer
    */
    pageSize: 20,
    remoteFilter: true,
    remoteSort: true,
    /**
    * Define the used model for this store
    * @string
    */
    model : 'Shopware.apps.Log.model.Log',

    // Default sorting for the store
    sortOnLoad: true,
    sorters: {
        property: 'date',
        direction: 'DESC'
    }
});
//<?php }} ?>