<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments.js" */ ?>
<?php /*%%SmartyHeaderCode:1791536694554476fc5c90b3-41785679%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '33dd97c8bc9de8135d7779eb3a9568f86fa94a2d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments.js',
      1 => 1430113596,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1791536694554476fc5c90b3-41785679',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc60bd43_35912298',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc60bd43_35912298')) {function content_554476fc60bd43_35912298($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Blog list main window.
 *
 * Displays all List Blog Information
 */
/**
 * Default blog list view. Extends a grid view.
 */
//
Ext.define('Shopware.apps.Blog.view.blog.detail.Comments', {
    extend:'Ext.panel.Panel',
    border: false,
    alias:'widget.blog-blog-detail-comments',
    region:'center',
    autoScroll:false,
    layout:'border',
    disabled:true,
    ui:'shopware-ui',

    /**
     * Initialize the Shopware.apps.Blog.view.main.List and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.items = [
            {
                xtype:'blog-blog-detail-comments-grid',
                commentStore:me.commentStore
            },
            {
                xtype:'blog-blog-detail-comments-info_panel'
            }
        ];
        me.toolbar = me.getToolbar();
        me.dockedItems = [ me.toolbar ];

        me.callParent(arguments);
    },
    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar:function () {
        return Ext.create('Ext.toolbar.Toolbar', {
            dock:'top',
            ui:'shopware-ui',
            items:[
		/* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'comments'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?> */
                {
                    iconCls:'sprite-plus-circle',
                    text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'button'/'add','default'=>'Accept selected comments','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'button'/'add','default'=>'Accept selected comments','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Kommentare freischalten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'button'/'add','default'=>'Accept selected comments','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    disabled:true,
                    action:'acceptSelectedComments'
                },
                {

                    iconCls:'sprite-minus-circle-frame',
                    text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'button'/'delete','default'=>'Delete selected comments','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'button'/'delete','default'=>'Delete selected comments','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Kommentare löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'button'/'delete','default'=>'Delete selected comments','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    disabled:true,
                    action:'deleteSelectedComments'
                },
		/* <?php }?> */
                '->',
                {
                    xtype:'textfield',
                    name:'searchfield',
                    action:'searchBlogComments',
                    width:170,
                    cls:'searchfield',
                    enableKeyEvents:true,
                    checkChangeBuffer:500,
                    emptyText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                },
                { xtype:'tbspacer', width:6 }
            ]
        });
    }
});
//
<?php }} ?>