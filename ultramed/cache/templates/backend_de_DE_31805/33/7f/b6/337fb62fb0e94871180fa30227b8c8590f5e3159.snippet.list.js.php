<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/list.js" */ ?>
<?php /*%%SmartyHeaderCode:1285413547554346116292f0-52134795%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '337fb62fb0e94871180fa30227b8c8590f5e3159' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/list.js',
      1 => 1430113370,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1285413547554346116292f0-52134795',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346116829b9_43128588',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346116829b9_43128588')) {function content_554346116829b9_43128588($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Toolbar
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.templates.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.emotion-templates-list',

    /**
     * Snippets which are used by this component.
     * @Object
     */
    snippets: {
        invalid_template: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die zur Verfügung gestellte Datei scheint keine valide Template-Datei zu sein.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        columns: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            file: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'list'/'columns'/'file','default'=>'Template file','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'list'/'columns'/'file','default'=>'Template file','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template-Datei<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'list'/'columns'/'file','default'=>'Template file','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            actions: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktion(en)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        tooltips: {
            edit: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            duplicate: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Duplizieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            remove: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Entfernen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return { Void }
     */
    initComponent: function() {
        var me = this;

        me.addEvents('selectionChange', 'editEntry', 'duplicate', 'remove');

        me.store = Ext.create('Shopware.apps.Emotion.store.Templates').load();
        me.columns = me.createColumns();
        me.selModel = me.createSelectionModel();
        me.plugins = [ me.createEditor() ];
        me.bbar = me.createPagingToolbar();

        me.callParent(arguments);
    },

    /**
     * Creates the column model for the grid panel
     *
     * @returns { Array } columns
     */
    createColumns: function() {
        var me = this;

        return [{
            dataIndex: 'name',
            header: me.snippets.columns.name,
            flex: 1,
            renderer: me.nameRenderer,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'file',
            header: me.snippets.columns.file,
            flex: 1,
            renderer: me.fileRenderer,
            editor: {
                xtype: 'textfield',
                allowBlank: false,
                validator: function(value) {
                    return (/^((.*)\.tpl)$/.test(value)) ? true : me.snippets.invalid_template;
                }
            }
        }, {
            xtype: 'actioncolumn',
            header: me.snippets.columns.actions,
            width: 85,
            items: [{
                iconCls: 'sprite-pencil',
                tooltip: me.snippets.tooltips.edit,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('editEntry', grid, rec, row, col);
                }
            }, {
                iconCls: 'sprite-blue-folder--plus',
                tooltip: me.snippets.tooltips.duplicate,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('duplicate', grid, rec, row, col);
                }
            }, {
                iconCls: 'sprite-minus-circle',
                tooltip: me.snippets.tooltips.remove,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('remove', grid, rec, row, col);
                },
                getClass: function(value, metadata, record) {
                    if (record.get('id') < 2) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            }]
        }];
    },

    /**
     * Creates the selection model.
     *
     * @returns { Ext.selection.CheckboxModel }
     */
    createSelectionModel: function() {
        var me = this;

        return Ext.create('Ext.selection.CheckboxModel', {
            listeners:{
                selectionchange:function (sm, selections) {
                    me.fireEvent('selectionChange', selections);
                }
            }
        });
    },

    /**
     * Creates the paging toolbar at the bottom of the list.
     *
     * @returns { Ext.toolbar.Paging }
     */
    createPagingToolbar: function() {
        var me = this,
            toolbar = Ext.create('Ext.toolbar.Paging', {
            store: me.store,
            pageSize: 20
        });

        return toolbar;
    },

    /**
     * Creates the row editor
     *
     * @returns { Ext.grid.plugin.RowEditing }
     */
    createEditor: function() {
        return Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
    },

    /**
     * Column renderer for the `name` column.
     *
     * The method wraps the value in `strong`-tags.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    nameRenderer: function(value) {
        return Ext.String.format('<strong>[0]</strong>', value);
    },

    /**
     * Column renderer for the `file` column.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    fileRenderer: function(value) {
        if(!value) {
            return 'index.tpl'
        }
        return value;
    }
});
//<?php }} ?>