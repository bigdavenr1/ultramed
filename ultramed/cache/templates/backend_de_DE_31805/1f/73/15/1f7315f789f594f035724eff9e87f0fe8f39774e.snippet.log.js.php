<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:14
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/model/log.js" */ ?>
<?php /*%%SmartyHeaderCode:115810013155447ccae70775-32876789%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f7315f789f594f035724eff9e87f0fe8f39774e' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/model/log.js',
      1 => 1430113143,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115810013155447ccae70775-32876789',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ccae8e003_80782471',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ccae8e003_80782471')) {function content_55447ccae8e003_80782471($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Log
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware - Log model
 *
 * This model represents a single log of s_core_log.
 */
//
Ext.define('Shopware.apps.Log.model.Log', {
    /**
    * Extends the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Model',
    /**
    * The fields used for this model
    * @array
    */
    fields: [
		//
		'id',
        'type',
        'key',
        'text',
		{ 	name: 'date',
			type: 'date',
			dateFormat:'Y-m-d'
		},
        'user',
        'ip_address',
        'user_agent',
        'value4'
    ],
    /**
    * Configure the data communication
    * @object
    */
    proxy: {
        type: 'ajax',
        /**
        * Configure the url mapping for the different
        * @object
        */
        api: {
            //read out all articles
            read: '<?php echo '/backend/log/getLogs';?>',
          	destroy: '<?php echo '/backend/log/deleteLogs';?>'
        },
        /**
        * Configure the data reader
        * @object
        */
        reader: {
            type: 'json',
            root: 'data',
            //total values, used for paging
            totalProperty: 'total'
        }
    }
});
//<?php }} ?>