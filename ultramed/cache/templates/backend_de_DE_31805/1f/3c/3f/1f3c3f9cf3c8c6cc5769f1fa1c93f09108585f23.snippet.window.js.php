<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:207517530455434610cb23b4-24163882%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f3c3f9cf3c8c6cc5769f1fa1c93f09108585f23' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/main/window.js',
      1 => 1430113370,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '207517530455434610cb23b4-24163882',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610cf8be0_44440646',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610cf8be0_44440646')) {function content_55434610cf8be0_44440646($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Main Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.main.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'title','default'=>'Emotion','namespace'=>'backend/emotion/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'Emotion','namespace'=>'backend/emotion/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkaufswelt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'Emotion','namespace'=>'backend/emotion/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.emotion-main-window',
    border: false,
    autoShow: true,
    layout: 'fit',
    height: '90%',
    width: 800,
    stateful: true,
    stateId: 'emotion-main-window',

    /**
     * Object which are used in this component
     * @Object
     */
    snippets: {
        tab: {
            overview: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tab'/'overview','default'=>'Overview','namespace'=>'backend/emotion/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'overview','default'=>'Overview','namespace'=>'backend/emotion/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Übersicht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'overview','default'=>'Overview','namespace'=>'backend/emotion/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            custom_grids: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tab'/'custom_grids','default'=>'Grids management','namespace'=>'backend/emotion/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'custom_grids','default'=>'Grids management','namespace'=>'backend/emotion/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Raster-Verwaltung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'custom_grids','default'=>'Grids management','namespace'=>'backend/emotion/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            custom_templates: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tab'/'custom_templates','default'=>'Templates management','namespace'=>'backend/emotion/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'custom_templates','default'=>'Templates management','namespace'=>'backend/emotion/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template-Verwaltung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'custom_templates','default'=>'Templates management','namespace'=>'backend/emotion/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            expert_settings: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tab'/'expert_settings','default'=>'Expert settings','namespace'=>'backend/emotion/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'expert_settings','default'=>'Expert settings','namespace'=>'backend/emotion/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Experten-Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tab'/'expert_settings','default'=>'Expert settings','namespace'=>'backend/emotion/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.items = me.createTabPanel();

        me.callParent(arguments);
    },

    /**
     * Creates the tab panel which holds off the different
     * areas of the emotion module.
     *
     * @returns { Ext.tab.Panel } the tab panel which contains the different areas
     */
    createTabPanel: function() {
        var me = this;

        me.tabPanel = Ext.create('Ext.tab.Panel', {
            items: [ me.createOverviewTab(), me.createExpertSettingsTab() ]
        });

        return me.tabPanel;
    },

    /**
     * Creates a container which holds off the upper toolbar and the
     * actual grid panel.
     *
     * The container will be used as the `overview` tab.
     *
     * @returns { Ext.container.Container }
     */
    createOverviewTab: function() {
        var me = this;

        me.overviewContainer = Ext.create('Ext.container.Container', {
            layout: 'border',
            title: me.snippets.tab.overview,
            items: [{
                xtype: 'emotion-list-toolbar',
                region: 'north'
            }, {
                xtype: 'emotion-list-grid',
                region: 'center'
            }]
        });

        return me.overviewContainer;
    },

    /**
     * Creates the container which represents the expert settings tab.
     *
     * The tab contains the custom grids and custom templates tabs.
     *
     * @returns { Ext.tab.Panel }
     */
    createExpertSettingsTab: function() {
        var me = this;

        me.expertSettingsTabPanel = Ext.create('Ext.tab.Panel', {
            title: me.snippets.tab.expert_settings,
            items: [ me.createCustomGridsTab(), me.createCustomTemplatesTab() ]
        });

        return me.expertSettingsTabPanel;
    },

    /**
     * Creates the container which represents the custom grids tab.
     *
     * @returns { Ext.container.Container }
     */
    createCustomGridsTab: function() {
        var me = this;

        me.customGridsContainer = Ext.create('Ext.container.Container', {
            layout: 'border',
            title: me.snippets.tab.custom_grids,
            items: [{
                xtype: 'emotion-grids-toolbar',
                region: 'north'
            }, {
                xtype: 'emotion-grids-list',
                region: 'center',
                store: Ext.create('Shopware.apps.Emotion.store.Grids').load()
            }]
        });

        return me.customGridsContainer;
    },

    /**
     * Creates the container which represents the custom templates tab.
     *
     * @returns { Ext.container.Container }
     */
    createCustomTemplatesTab: function() {
        var me = this;

        me.customTemplatesTab = Ext.create('Ext.container.Container', {
            layout: 'border',
            title: me.snippets.tab.custom_templates,
            items: [{
                xtype: 'emotion-templates-toolbar',
                region: 'north'
            }, {
                xtype: 'emotion-templates-list',
                region: 'center'
            }]
        });

        return me.customTemplatesTab;
    }
});
//<?php }} ?>