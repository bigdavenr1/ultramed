<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/attribute.js" */ ?>
<?php /*%%SmartyHeaderCode:104903945555447e755d5281-25357478%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '04fca21ecd01d241c89acd4c88e69b8f438ec8e7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/attribute.js',
      1 => 1430113166,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '104903945555447e755d5281-25357478',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e755e3124_18704192',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e755e3124_18704192')) {function content_55447e755e3124_18704192($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Model for the attributes associations in the detail model
 */
//
Ext.define('Shopware.apps.ProductFeed.model.Attribute', {

    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     *
     * @array
     */
    fields : [
		//
        { name: 'id',      type: 'int' },
        { name: 'productFeedId', type: 'int', useNull: true }
    ]
});
//
<?php }} ?>