<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/daytime.js" */ ?>
<?php /*%%SmartyHeaderCode:14867432755434785876f17-51291169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '044ea5a52e0764b62b433e9181625460b56e5ada' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/daytime.js',
      1 => 1430113329,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14867432755434785876f17-51291169',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543478591cb42_41847297',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543478591cb42_41847297')) {function content_5543478591cb42_41847297($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Time Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Daytime', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-daytime',
    legend: {
        position: 'right'
    },
    animate: true,


    initComponent: function () {
        var me = this;

        me.series = [];
        me.axes = [
            {
                type: 'Time',
                position: 'bottom',
                fields: ['date'],
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'daytime'/'titleBottom','default'=>'Time','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'daytime'/'titleBottom','default'=>'Time','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'daytime'/'titleBottom','default'=>'Time','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                step: [Ext.Date.HOUR, 1],
                dateFormat: 'H:00'
            }
        ];

        if (me.shopSelection != Ext.undefined && me.shopSelection.length > 0) {
            Ext.each(me.shopSelection, function (shopId) {
                var shop = me.shopStore.getById(shopId);

                if (!(shop instanceof Ext.data.Model)) {
                    return true;
                }

                me.series.push(
                    me.createLineSeries(
                        {
                            title: shop.data.name,
                            xField: 'date',
                            yField: 'turnover' + shopId
                        },
                        {
                            width: 150,
                            height: 45,
                            renderer: function (storeItem) {
                                me.renderShopData(storeItem, this, shop);
                            }
                        }
                    )
                );
            });
        } else {
            me.series = [
                me.createLineSeries(
                    {
                        xField: 'date',
                        yField: 'turnover',
                        title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                    },
                    {
                        width: 150,
                        height: 45,
                        renderer: function (storeItem) {
                            me.renderShopData(storeItem, this, null);
                        }
                    }
                )
            ];
        }

        me.axes.push({
            type: 'Numeric',
            minimum: 0,
            position: 'left',
            fields: me.getAxesFields('turnover'),
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        me.callParent(arguments);
    },

    renderShopData: function(storeItem, tip, shop) {
        var me = this,
            field = 'turnover';

        if (shop) {
            field += shop.get('id');
        }

        var sales = Ext.util.Format.currency(
            storeItem.get(field),
            me.subApp.currencySign,
            2,
            (me.subApp.currencyAtEnd == 1)
        );

        tip.setTitle(
            Ext.Date.format(storeItem.get('date'), 'H:00')+
            '<br><br>&nbsp;' + sales
        );
    }

});
//
<?php }} ?>