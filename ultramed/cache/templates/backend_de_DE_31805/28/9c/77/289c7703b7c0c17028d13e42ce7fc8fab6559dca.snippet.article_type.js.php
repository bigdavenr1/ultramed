<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article_type.js" */ ?>
<?php /*%%SmartyHeaderCode:77258212055434611290762-70382457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '289c7703b7c0c17028d13e42ce7fc8fab6559dca' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article_type.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '77258212055434611290762-70382457',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346112df890_10420838',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346112df890_10420838')) {function content_554346112df890_10420838($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.ArticleType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.emotion-components-fields-article-type',
    name: 'article_type',

    /**
     * Snippets for the component
     * @object
     */
    snippets: {
        fields: {
            'article_type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'article_type','default'=>'Type of article','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'article_type','default'=>'Type of article','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikeltyp<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'article_type','default'=>'Type of article','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'empty_text': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        store: {
            'selected_article': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'selected_article','default'=>'Selected article','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'selected_article','default'=>'Selected article','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'selected_article','default'=>'Selected article','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'newcomer': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'newcomer','default'=>'Newcomer article','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'newcomer','default'=>'Newcomer article','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Newcomer Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'newcomer','default'=>'Newcomer article','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'topseller': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'topseller','default'=>'Topseller article','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'topseller','default'=>'Topseller article','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Topseller Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'topseller','default'=>'Topseller article','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'random_article': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/article')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/article'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zufälliger Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/article'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            emptyText: me.snippets.fields.empty_text,
            fieldLabel: me.snippets.fields.article_type,
            displayField: 'display',
            valueField: 'value',
            queryMode: 'local',
            triggerAction: 'all',
            store: me.createStore()
        });

        me.callParent(arguments);
        me.on('change', me.onArticleSelectChange, me);
    },

    /**
     * Event listeners which triggers when the user changs the value
     * of the select field.
     *
     * @public
     * @event change
     * @param [object] field - Ext.form.field.ComboBox
     * @param [string] value - The selected value
     */
    onArticleSelectChange: function(field, value) {
        var me = this;

        // Terminate the article search field
        if(!me.articleSearch) {
            me.articleSearch = me.up('fieldset').down('emotion-components-fields-article');
        }

        // Show/hide article search field based on selected entry
        me.articleSearch.setVisible(value !== 'selected_article' ? false : true);
    },

    /**
     * Creates a local store which will be used
     * for the combo box. We don't need that data.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, snippets = me.snippets.store;

        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'value', 'display' ],
            data: [{
                value: 'selected_article',
                display: snippets.selected_article
            }, {
                value: 'newcomer',
                display: snippets.newcomer
            }, {
                value: 'topseller',
                display: snippets.topseller
            }, {
                value: 'random_article',
                display: snippets.random_article
            }]
        });
    }
});<?php }} ?>