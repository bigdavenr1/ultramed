<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/component.js" */ ?>
<?php /*%%SmartyHeaderCode:174153556955434610b9a757-06683005%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d7a413d4bc6493cec96df37bacf777409cbab9e' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/component.js',
      1 => 1430112888,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '174153556955434610b9a757-06683005',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610bf3cc3_61604910',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610bf3cc3_61604910')) {function content_55434610bf3cc3_61604910($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Emotion backend module.
 */

//
//
Ext.define('Shopware.apps.Emotion.model.Component', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    snippets: {
        //

        'Artikel': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article','default'=>'Article','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article','default'=>'Article','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article','default'=>'Article','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Kategorie-Teaser': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'category_teaser','default'=>'Category teaser','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'category_teaser','default'=>'Category teaser','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie-Teaser<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'category_teaser','default'=>'Category teaser','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Blog-Artikel' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog_article','default'=>'Blog article','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_article','default'=>'Blog article','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blog-Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_article','default'=>'Blog article','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Banner-Slider' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'banner_slider','default'=>'Banner slider','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'banner_slider','default'=>'Banner slider','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Banner-Slider<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'banner_slider','default'=>'Banner slider','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Youtube-Video' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'youtube','default'=>'Youtube video','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'youtube','default'=>'Youtube video','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Youtube-Video<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'youtube','default'=>'Youtube video','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Hersteller-Slider' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manufacturer','default'=>'Manufacturer slider','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer','default'=>'Manufacturer slider','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hersteller-Slider<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer','default'=>'Manufacturer slider','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'Artikel-Slider' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider','default'=>'Article slider','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider','default'=>'Article slider','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Slider<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider','default'=>'Article slider','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'HTML-Element' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'html_element','default'=>'HTML element','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'html_element','default'=>'HTML element','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
HTML-Element<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'html_element','default'=>'HTML element','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'iFrame-Element' : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'iframe','default'=>'iFrame element','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'iframe','default'=>'iFrame element','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
iFrame-Element<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'iframe','default'=>'iFrame element','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * The fields used for this model
     * @array
     */
    fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'pluginId', type: 'int', useNull: true },

        { name: 'fieldLabel', type: 'string', convert: function(value, record) {
            var name = record.get('name'),
                fieldLabel = name;

            if (record.snippets[name]) {
                fieldLabel = record.snippets[name];
            }
            return fieldLabel;
        } },

        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'xType', type: 'string' },
        { name: 'template', type: 'string' },
        { name: 'cls', type: 'string' }
    ],

    associations: [
        { type: 'hasMany', model: 'Shopware.apps.Emotion.model.Field', name: 'getFields', associationKey: 'fields'}
    ]

});
//
<?php }} ?>