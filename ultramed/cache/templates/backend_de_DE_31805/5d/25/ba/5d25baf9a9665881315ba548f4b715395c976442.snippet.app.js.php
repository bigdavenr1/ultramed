<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:51
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/app.js" */ ?>
<?php /*%%SmartyHeaderCode:75328962655447c3b42c300-57807210%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5d25baf9a9665881315ba548f4b715395c976442' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/app.js',
      1 => 1430111723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75328962655447c3b42c300-57807210',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3b479eb7_54500066',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3b479eb7_54500066')) {function content_55447c3b479eb7_54500066($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//
Ext.define('Shopware.apps.PaymentPaypal', {

    extend: 'Enlight.app.SubApplication',

    bulkLoad: true,
    loadPath: '<?php echo '/backend/PaymentPaypal/load';?>',

    params: {},

    controllers: [ 'Main' ],

    stores: [
        'main.List'
    ],
    models: [
        'main.List'
    ],
    views: [
        'main.Window', 'main.List'
    ],

    /**
     * This method will be called when all dependencies are solved and
     * all member controllers, models, views and stores are initialized.
     */
    launch: function() {
        var me = this;
        me.controller = me.getController('Main');
        return me.controller.mainWindow;
    }
});
//

<?php }} ?>