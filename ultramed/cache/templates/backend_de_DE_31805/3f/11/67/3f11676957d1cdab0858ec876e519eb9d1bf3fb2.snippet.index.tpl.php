<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:40
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/search/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:544519409554477085ad8a3-67183207%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3f11676957d1cdab0858ec876e519eb9d1bf3fb2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/search/index.tpl',
      1 => 1430112766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '544519409554477085ad8a3-67183207',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'searchResult' => 0,
    'result' => 0,
    'group' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554477086faef2_70456807',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554477086faef2_70456807')) {function content_554477086faef2_70456807($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_currency')) include '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/Enlight/Template/Plugins/modifier.currency.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/Smarty/plugins/modifier.truncate.php';
?>

<div class="search-wrapper">
	<div class="arrow-top"></div>
	<?php if (!$_smarty_tpl->tpl_vars['searchResult']->value['articles']&&!$_smarty_tpl->tpl_vars['searchResult']->value['customers']&&!$_smarty_tpl->tpl_vars['searchResult']->value['orders']){?>
		
			<div class="header">
				<div class="inner">
					<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"title/empty_search",'default'=>'No search results','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/empty_search",'default'=>'No search results','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ihre Suche ergab kein Ergebnis.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/empty_search",'default'=>'No search results','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

				</div>
			</div>
		
		
		
			<div class="result-container">
				<div class="empty"><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"item/empty",'default'=>'No search results','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"item/empty",'default'=>'No search results','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Es wurde kein Ergebnis zu Ihren Suchbegriff gefunden.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"item/empty",'default'=>'No search results','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
			</div>
		
	<?php }else{ ?>
		<?php  $_smarty_tpl->tpl_vars['result'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['result']->_loop = false;
 $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['searchResult']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['result']->key => $_smarty_tpl->tpl_vars['result']->value){
$_smarty_tpl->tpl_vars['result']->_loop = true;
 $_smarty_tpl->tpl_vars['group']->value = $_smarty_tpl->tpl_vars['result']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['result']->value){?>
				
					<div class="row <?php echo $_smarty_tpl->tpl_vars['group']->value;?>
">
					
						
							<div class="header">
								<div class="inner">
									<?php if ($_smarty_tpl->tpl_vars['group']->value==='articles'){?>
										<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"title/articles",'default'=>'Article','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/articles",'default'=>'Article','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/articles",'default'=>'Article','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:
									<?php }elseif($_smarty_tpl->tpl_vars['group']->value==='customers'){?>
										<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"title/customers",'default'=>'Customers','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/customers",'default'=>'Customers','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/customers",'default'=>'Customers','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:
									<?php }else{ ?>
										<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"title/orders",'default'=>'Orders','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/orders",'default'=>'Orders','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"title/orders",'default'=>'Orders','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:
									<?php }?>
								</div>
							</div>
						
						
						
							<div class="result-container">
								<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['result']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
									<a onclick="openSearchResult('<?php echo $_smarty_tpl->tpl_vars['group']->value;?>
', <?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
);return false;" href="#"<?php if ((1 & $_smarty_tpl->tpl_vars['item']->iteration / 2)){?> class="odd"<?php }?>>
										<?php if ($_smarty_tpl->tpl_vars['group']->value==='orders'){?><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"item/order",'default'=>'Order','namespace'=>'backend/search/index')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"item/order",'default'=>'Order','namespace'=>'backend/search/index'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"item/order",'default'=>'Order','namespace'=>'backend/search/index'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 - <span class="right"><?php echo smarty_modifier_currency($_smarty_tpl->tpl_vars['item']->value['totalAmount']);?>
</span><?php }?><span class="name" style="display:inline-block;width: 155px"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value['name'],60);?>
</span>
										<?php if ($_smarty_tpl->tpl_vars['group']->value==='articles'&&$_smarty_tpl->tpl_vars['item']->value['ordernumber']){?><span class="right"><?php echo $_smarty_tpl->tpl_vars['item']->value['ordernumber'];?>
</span><?php }?>
										
										<?php if ($_smarty_tpl->tpl_vars['item']->value['description']){?>
											<span class="desc"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['item']->value['description'],45);?>
</span>
										<?php }elseif($_smarty_tpl->tpl_vars['item']->value['description_long']){?>
											<span class="desc"><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['item']->value['description_long']),45);?>
</span>
										<?php }?>
									</a>
								<?php } ?>
							</div>
						
					</div>
				
			<?php }?>
		<?php } ?>
	<?php }?>
</div>
<?php }} ?>