<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/model/payment.js" */ ?>
<?php /*%%SmartyHeaderCode:8153484005541e663ba5b56-40806263%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3f0c547fc68fd40db781e1343b7fdcbb5d7d355d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/model/payment.js',
      1 => 1430113161,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8153484005541e663ba5b56-40806263',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663bcfed8_40868962',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663bcfed8_40868962')) {function content_5541e663bcfed8_40868962($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Payment list backend module.
 *
 * The Payment-Model represents a single Payment-mean.
 */
//
Ext.define('Shopware.apps.Payment.model.Payment', {
    /**
     * Extends the default extjs 4 model
     * @string
     */
    extend : 'Ext.data.Model',
     /**
     * Set an alias to make the handling a bit easier
      * @string
     */
    alias : 'model.payment',
    /**
     * Defined items used by that model
     *
     * We use a reduces feature set here - just necessary fields are selected
     *
     * @array
     */
    fields : [
		//
        { name : 'text',     type: 'string' },
        { name : 'id',       type: 'int' },
        { name : 'name',       type: 'string' },
        { name : 'description',       type: 'string' },
        { name : 'template',       type: 'string' },
        { name : 'class',       type: 'string' },
        { name : 'table',       type: 'string' },
        { name : 'hide',       type: 'int' },
        { name : 'additionalDescription',       type: 'string' },
        { name : 'debitPercent',       type: 'string' },
        { name : 'surcharge',       type: 'string' },
        { name : 'surchargeString',       type: 'string' },
        { name : 'position',       type: 'int' },
        { name : 'active',       type: 'boolean' },
        { name : 'esdActive',       type: 'boolean' },
        { name : 'embedIFrame',       type: 'string' },
        { name : 'hideProspect',       type: 'boolean' },
        { name : 'action',       type: 'string' },
        { name : 'pluginId',       type: 'int' },
        { name : 'iconCls',  type: 'string' },
        { name : 'surcharge', type: 'double' },
        { name: 'source', type: 'int' }
    ],

    associations: [
        { type:'hasMany', model:'Shopware.apps.Payment.model.Country',  name:'getCountries', associationKey:'countries' },
        { type:'hasMany', model:'Shopware.apps.Payment.model.Attribute',  name:'getAttributes', associationKey:'attribute' },
        { type:'hasMany', model:'Shopware.apps.Base.model.Shop',  name:'getShops', associationKey:'shops' }
    ],

    proxy : {
        type : 'ajax',
        api : {
            read : '<?php echo '/backend/payment/getPayments';?>',
            create : '<?php echo '/backend/payment/createPayments';?>',
            update : '<?php echo '/backend/payment/updatePayments';?>',
            destroy : '<?php echo '/backend/payment/deletePayment';?>'
        },
        reader : {
            type : 'json',
            root: 'data'
        }
    }


});
//
<?php }} ?>