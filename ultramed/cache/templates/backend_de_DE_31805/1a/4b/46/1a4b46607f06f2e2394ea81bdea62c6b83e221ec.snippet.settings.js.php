<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/grids/settings.js" */ ?>
<?php /*%%SmartyHeaderCode:14168781895543461168a906-18395039%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a4b46607f06f2e2394ea81bdea62c6b83e221ec' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/grids/settings.js',
      1 => 1430113369,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14168781895543461168a906-18395039',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461170e471_82325520',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461170e471_82325520')) {function content_5543461170e471_82325520($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Main Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.grids.Settings', {
    extend: 'Enlight.app.Window',
    alias: 'widget.emotion-view-grids-settings',
    width: 600,
    height: 530,
    autoScroll: true,
    autoShow: true,
    layout: 'fit',

    /**
     * Snippets which are used by this component.
     * @Object
     */
    snippets: {
        title_new: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'title_new','default'=>'Create new grid','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'title_new','default'=>'Create new grid','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neues Raster erstellen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'title_new','default'=>'Create new grid','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        title_edit: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'title_edit','default'=>'Edit existing grid','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'title_edit','default'=>'Edit existing grid','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestehendes Raster bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'title_edit','default'=>'Edit existing grid','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        fieldset_title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'fieldset','default'=>'Define grid','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'fieldset','default'=>'Define grid','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Raster definieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'fieldset','default'=>'Define grid','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        fields: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cols: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'cols','default'=>'Number of columns','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'cols','default'=>'Number of columns','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Spalten-Anzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'cols','default'=>'Number of columns','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            rows: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'rows','default'=>'Number of rows','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'rows','default'=>'Number of rows','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeilen-Anzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'rows','default'=>'Number of rows','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cell_height: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'cell_height','default'=>'Cell height','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'cell_height','default'=>'Cell height','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zellenhöhe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'cell_height','default'=>'Cell height','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            article_height: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'article_height','default'=>'Article element height','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'article_height','default'=>'Article element height','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Element Höhe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'article_height','default'=>'Article element height','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            gutter: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'settings'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abstand<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'settings'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        support: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'name','default'=>'Initial label of the grid.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'name','default'=>'Initial label of the grid.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Initiale Bezeichnung des Rasters.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'name','default'=>'Initial label of the grid.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cols: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'cols','default'=>'Could not be modified within the designer.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'cols','default'=>'Could not be modified within the designer.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kann im Designer nicht angepasst werden.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'cols','default'=>'Could not be modified within the designer.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            rows: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'rows','default'=>'Initial number of rows. New rows can be added in the designer.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'rows','default'=>'Initial number of rows. New rows can be added in the designer.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Initiale Anzahl an Zeilen. Die Anzahl kann im Designer nicht verändert werden.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'rows','default'=>'Initial number of rows. New rows can be added in the designer.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cell_height: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'cell_height','default'=>'Height of a cell (in px) in the storefront.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'cell_height','default'=>'Height of a cell (in px) in the storefront.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Höhe einer Zelle in der Storefront. Angabe erfolgt in Pixeln.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'cell_height','default'=>'Height of a cell (in px) in the storefront.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            article_height: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'article_height','default'=>'Number of cells, which occupies the article element.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'article_height','default'=>'Number of cells, which occupies the article element.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zellenanzahl, die das Artikel-Element einnimmt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'article_height','default'=>'Number of cells, which occupies the article element.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            gutter: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'settings'/'support'/'gutter','default'=>'Margin (in px) between the elements in the storefront.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'gutter','default'=>'Margin (in px) between the elements in the storefront.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abstand zwischen den Elementen in der Storefront. Angabe erfolgt in Pixeln.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'settings'/'support'/'gutter','default'=>'Margin (in px) between the elements in the storefront.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        buttons: {
            cancel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            save: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.title = me.snippets[(me.hasOwnProperty('record') ? 'title_edit' : 'title_new' )];

        me.formPanel = Ext.create('Ext.form.Panel', {
            bodyPadding: 20,
            border: 0,
            bodyBorder: 0,
            items: [{
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '155',
                    anchor: '100%'
                },
                title: me.snippets.fieldset_title,
                items: me.createFormItems()
            }]
        });
        me.items = [ me.formPanel ];
        me.bbar = me.createActionButtons();

        if(me.hasOwnProperty('record')) {
            me.formPanel.loadRecord(me.record);
        }

        me.callParent(arguments);
    },

    /**
     * Creates the toolbar which contains the action buttons.
     *
     * @returns { Ext.toolbar.Toolbar }
     */
    createActionButtons: function() {
        var me = this;

        return Ext.create('Ext.toolbar.Toolbar', {
            docked: 'bottom',
            items: [ '->', {
                xtype: 'button',
                text: me.snippets.buttons.cancel,
                cls: 'secondary',
                handler: function() {
                    me.destroy();
                }
            }, {
                xtype: 'button',
                text: me.snippets.buttons.save,
                cls: 'primary',
                action: 'emotion-save-grid'
            }]
        });
    },

    /**
     * Creates the form items for the settings window.
     *
     *
     * @returns { Array }
     */
    createFormItems: function() {
        var me = this, label = me.snippets.fields,
            support = me.snippets.support;

        var name = Ext.create('Ext.form.field.Text', {
            name: 'name',
            fieldLabel: label.name,
            allowBlank: false,
            supportText: support.name
        });

        var colsCount = Ext.create('Ext.form.field.Number', {
            name: 'cols',
            fieldLabel: label.cols,
            allowBlank: false,
            minValue: 0,
            supportText: support.cols
        });

        var rowsCount = Ext.create('Ext.form.field.Number', {
            name: 'rows',
            fieldLabel: label.rows,
            allowBlank: false,
            minValue: 0,
            supportText: support.rows
        });

        var cellHeight = Ext.create('Ext.form.field.Number', {
            name: 'cellHeight',
            fieldLabel: label.cell_height,
            allowBlank: false,
            minValue: 0,
            supportText: support.cell_height
        });

        var articleHeight = Ext.create('Ext.form.field.Number', {
            name: 'articleHeight',
            fieldLabel: label.article_height,
            allowBlank: false,
            minValue: 0,
            supportText: support.article_height
        });

        var gutter = Ext.create('Ext.form.field.Number', {
            name: 'gutter',
            fieldLabel: label.gutter,
            allowBlank: false,
            minValue: 0,
            supportText: support.gutter
        });

        return [ name, colsCount, rowsCount, cellHeight, articleHeight, gutter ];
    }
});
//<?php }} ?>