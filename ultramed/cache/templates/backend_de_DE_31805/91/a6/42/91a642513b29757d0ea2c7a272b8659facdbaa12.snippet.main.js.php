<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:20308923725541e663dbfee8-71343581%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91a642513b29757d0ea2c7a272b8659facdbaa12' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/controller/main.js',
      1 => 1430113160,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20308923725541e663dbfee8-71343581',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663dc8db4_70874761',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663dc8db4_70874761')) {function content_5541e663dc8db4_70874761($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - Payment list backend module
 *
 * Main controller of the payment module.
 * It only creates the main-window.
 * shopware AG (c) 2012. All rights reserved.
 */

//
Ext.define('Shopware.apps.Payment.controller.Main', {
    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.app.Controller',

    requires: [ 'Shopware.apps.Payment.controller.Payment' ],

    /**
     * Init-function to create the main-window and assign the paymentStore
     */
    init: function() {
        var me = this;

        me.subApplication.paymentStore = me.subApplication.getStore('Payments');
        me.subApplication.paymentStore.load();

        me.mainWindow = me.getView('main.Window').create({
            paymentStore: me.subApplication.paymentStore
        });
        me.callParent(arguments);
    }
});
//<?php }} ?>