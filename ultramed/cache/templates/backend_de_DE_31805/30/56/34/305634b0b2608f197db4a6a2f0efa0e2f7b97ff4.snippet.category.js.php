<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:35:58
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/store/category.js" */ ?>
<?php /*%%SmartyHeaderCode:78618982855447e5e7890e1-18866394%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '305634b0b2608f197db4a6a2f0efa0e2f7b97ff4' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/store/category.js',
      1 => 1430112855,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '78618982855447e5e7890e1-18866394',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e5e7999e7_35791324',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e5e7999e7_35791324')) {function content_55447e5e7999e7_35791324($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Banner
 * @subpackage Category
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Category
 *
 * Backend - Defines the category store
 * 
 * This store will be loaded automatically and will just request 30 items at once.
 * It will utilize the Banner Category Model @see Shopware.apps.Banner.model.Category
 */
//
Ext.define('Shopware.apps.Banner.store.Category', {
    extend : 'Shopware.store.CategoryTree',
    alias : 'store.category',
    autoLoad : false,
    pageSize : 30,
    proxy : {
        type : 'ajax',
        api : {
            read : '<?php echo '/backend/banner/getList';?>'
        },
        reader : {
            type : 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>