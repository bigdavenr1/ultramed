<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:61705165055447cd7ce1954-87842811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fe3495edb2fc30b3d5e071fb7ed441792afa0f12' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/controller/main.js',
      1 => 1430113170,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '61705165055447cd7ce1954-87842811',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd7ce6e17_14652314',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd7ce6e17_14652314')) {function content_55447cd7ce6e17_14652314($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - RiskManagement backend module
 *
 * Main controller of the risk-management module.
 * It only creates the main-window.
 * shopware AG (c) 2012. All rights reserved.
 */

Ext.define('Shopware.apps.RiskManagement.controller.Main', {
    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.app.Controller',

    requires: [ 'Shopware.apps.RiskManagement.controller.RiskManagement' ],

    /**
     * Init-function to create the main-window and assign the riskStore
     */
    init: function() {
        var me = this;

        me.subApplication.paymentStore = me.subApplication.getStore('Payments');
        me.subApplication.areasStore = me.subApplication.getStore('Areas').load();
        me.subApplication.subShopStore = me.subApplication.getStore('Subshops').load();

        me.mainWindow = me.getView('main.Window').create({
			paymentStore: me.subApplication.paymentStore,
			areasStore: me.subApplication.areasStore,
			subShopStore: me.subApplication.subShopStore
        });
        me.callParent(arguments);
    }
});<?php }} ?>