<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:09
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/settings/fields/seo.js" */ ?>
<?php /*%%SmartyHeaderCode:54966518655447c4daa3ab6-46355720%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '44f80673e16504a5f96f454ef1fc626fb4a13fd7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/settings/fields/seo.js',
      1 => 1430113620,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '54966518655447c4daa3ab6-46355720',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4db0a672_73706505',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4db0a672_73706505')) {function content_55447c4db0a672_73706505($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Customer
 * @subpackage Detail
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * SEO fieldSet for
 */
//
Ext.define('Shopware.apps.Performance.view.tabs.settings.fields.Seo', {
    /**
     * Define that the base field set is an extension of the "Base" fieldSet
     * @string
     */
    extend:'Shopware.apps.Performance.view.tabs.settings.fields.Base',

    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.performance-tabs-settings-seo',

    /**
     * Description of the fieldSet
     */
    caption: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tabs'/'settings'/'seo'/'title','default'=>'SEO','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'settings'/'seo'/'title','default'=>'SEO','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'settings'/'seo'/'title','default'=>'SEO','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',


    /**
     * Component event method which is fired when the component
     * is initials. The component is initials when the user
     * want to create a new customer or edit an existing customer
     * @return void
     */
    initComponent:function () {
        var me = this;

        me.items = me.getItems();
        me.callParent(arguments);

    },

    getItems: function() {
        var me = this;

        return [
            {
                xtype: 'fieldset',
                defaults: me.defaults,
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'information','default'=>'Information','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'information','default'=>'Information','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Information<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'information','default'=>'Information','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [
                    me.createDecriptionContainer("<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'seo'/'info','default'=>'Die SEO-Urls werden in bestimmten Abständen in Shopware aktualisiert. Sie können die Aktualisierung manuell starten oder aber zwischen der Aktualisierung im Live-Betrieb und der Aktualisierung via Cronjob wählen. <br><br>Sofern Sie viel Traffic haben, empfiehlt sich die Generierung der SEO-Routen über einen Cronjob durchführen zu lassen.','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'info','default'=>'Die SEO-Urls werden in bestimmten Abständen in Shopware aktualisiert. Sie können die Aktualisierung manuell starten oder aber zwischen der Aktualisierung im Live-Betrieb und der Aktualisierung via Cronjob wählen. <br><br>Sofern Sie viel Traffic haben, empfiehlt sich die Generierung der SEO-Routen über einen Cronjob durchführen zu lassen.','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die SEO-Urls werden in bestimmten Abständen in Shopware aktualisiert. Sie können die Aktualisierung manuell starten oder aber zwischen der Aktualisierung im Live-Betrieb und der Aktualisierung via Cronjob wählen. <br><br>Sofern Sie viel Traffic haben, empfiehlt sich die Generierung der SEO-Routen über einen Cronjob durchführen zu lassen.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'info','default'=>'Die SEO-Urls werden in bestimmten Abständen in Shopware aktualisiert. Sie können die Aktualisierung manuell starten oder aber zwischen der Aktualisierung im Live-Betrieb und der Aktualisierung via Cronjob wählen. <br><br>Sofern Sie viel Traffic haben, empfiehlt sich die Generierung der SEO-Routen über einen Cronjob durchführen zu lassen.','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
")
                ]
            },
            {
                xtype: 'fieldset',
                defaults: me.defaults,
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'configuration','default'=>'Configuration','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'configuration','default'=>'Configuration','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'configuration','default'=>'Configuration','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [
                    {
                        xtype: 'performance-multi-request-button',
                        event: 'seo',
                        showEvent: 'showMultiRequestTasks',
                        title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'seo'/'buildIndex','default'=>'Rebuild seo url index','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'buildIndex','default'=>'Rebuild seo url index','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO-Index neu aufbauen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'buildIndex','default'=>'Rebuild seo url index','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                    },
                    {
                        fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'refreshStrategy','default'=>'Refresh strategy','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy','default'=>'Refresh strategy','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktualisierungsstrategie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy','default'=>'Refresh strategy','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        helpText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'refreshStrategy'/'help','default'=>'How do you want to refresh this information?<br><br>\' +
                                \'<b>Manually</b>: Refresh by clicking the *build Index* button<br>\' +
                                \'<b>CronJob</b>: Refresh with a CronJob (recommended)<br>\' +
                                \'<b>Live</b>: Refresh in live operation (not recommended for large shops)','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'help','default'=>'How do you want to refresh this information?<br><br>\' +
                                \'<b>Manually</b>: Refresh by clicking the *build Index* button<br>\' +
                                \'<b>CronJob</b>: Refresh with a CronJob (recommended)<br>\' +
                                \'<b>Live</b>: Refresh in live operation (not recommended for large shops)','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wie wollen Sie diese Informationen aktualisieren?<br><br>' +
                                '<b>Manuell</b>: Aktualisierung über den Button *Index neu aufbauen*<br>' +
                                '<b>Cronjob</b>: Aktualisierung über einen Cronjob (empfohlen)<br>' +
                                '<b>Live</b>: Live-Aktualisierung (nicht empfohlen)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'help','default'=>'How do you want to refresh this information?<br><br>\' +
                                \'<b>Manually</b>: Refresh by clicking the *build Index* button<br>\' +
                                \'<b>CronJob</b>: Refresh with a CronJob (recommended)<br>\' +
                                \'<b>Live</b>: Refresh in live operation (not recommended for large shops)','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        name: 'seo[seoRefreshStrategy]',
                        xtype: 'combo',
                        valueField: 'id',
                        editable: false,
                        displayField: 'name',
                        store: Ext.create('Ext.data.Store', {
                            fields: [
                                { name: 'id', type: 'int' },
                                { name: 'name', type: 'string' }
                            ],
                            data: [
                                { id: 1, name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'refreshStrategy'/'manual','default'=>'Manually','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'manual','default'=>'Manually','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Manuell<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'manual','default'=>'Manually','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' },
                                { id: 2, name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'refreshStrategy'/'cronJob','default'=>'Over cron job','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'cronJob','default'=>'Over cron job','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cronjob<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'cronJob','default'=>'Over cron job','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' },
                                { id: 3, name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'refreshStrategy'/'live','default'=>'Live','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'live','default'=>'Live','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Live<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'refreshStrategy'/'live','default'=>'Live','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' }
                            ]
                        })
                    },
                    {
                        fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'seo'/'routerCache','default'=>'Router cache','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'routerCache','default'=>'Router cache','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
SEO-Cache<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'routerCache','default'=>'Router cache','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        name: 'seo[routercache]',
                        xtype: 'numberfield',
                        minValue: 3600
                    } , {
                        fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldset'/'seo'/'lastUpdate','default'=>'Last update','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'lastUpdate','default'=>'Last update','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Letzte Aktualisierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldset'/'seo'/'lastUpdate','default'=>'Last update','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        name: 'seo[routerlastupdateDate]',
                        submitFormat: 'd.m.Y',
                        xtype: 'datefield'
                    } , {
                        fieldLabel: ' ',
                        labelSeparator: '',
                        name: 'seo[routerlastupdateTime]',
                        xtype: 'hidden'
                    }
                ]
            }
        ];
    }

});
//
<?php }} ?>