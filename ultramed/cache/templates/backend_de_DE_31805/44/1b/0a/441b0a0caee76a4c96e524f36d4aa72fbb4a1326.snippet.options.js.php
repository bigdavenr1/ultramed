<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/options.js" */ ?>
<?php /*%%SmartyHeaderCode:48491928755447ce39f9945-75757653%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '441b0a0caee76a4c96e524f36d4aa72fbb4a1326' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/options.js',
      1 => 1430111765,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '48491928755447ce39f9945-75757653',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3a43c30_68244884',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3a43c30_68244884')) {function content_55447ce3a43c30_68244884($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Plugin Manager
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.manager.Options', {
    /**
     * Define that the plugin manager main window is an extension of the enlight application window
     * @string
     */
    extend:'Enlight.app.Window',
    /**
     * Set base css class prefix and module individual css class for css styling
     * @string
     */
    cls:Ext.baseCSSPrefix + 'plugin-manager-options-window',
    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.plugin-manager-manager-options',
    /**
     * Set no border for the window
     * @boolean
     */
    border:false,
    /**
     * True to automatically show the component upon creation.
     * @boolean
     */
    autoShow:true,
    /**
     * Set border layout for the window
     * @string
     */
    layout:'fit',
    /**
     * Define window width
     * @integer
     */
    width:480,
    /**
     * Define window height
     * @integer
     */
    height:150,
    /**
     * True to display the 'maximize' tool button and allow the user to maximize the window, false to hide the button and disallow maximizing the window.
     * @boolean
     */
    maximizable:false,
    /**
     * True to display the 'minimize' tool button and allow the user to minimize the window, false to hide the button and disallow minimizing the window.
     * @boolean
     */
    minimizable:false,

    /**
     * Don't create a footer button.
     * @boolean
     */
    footerBtn: false,

    /**
     * A flag which causes the object to attempt to restore the state of internal properties from a saved state on startup.
     */
    stateful:true,
    /**
     * The unique id for this object to use for state management purposes.
     */
    stateId:'shopware-plugin-manager-manager-options-window',

    /**
     * Title of the window.
     * @string
     */
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'options'/'title','default'=>'Plugin installed','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'title','default'=>'Plugin installed','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin installiert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'title','default'=>'Plugin installed','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    bodyPadding: 10,

	snippets:{
		activate_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'options'/'activate_plugin','default'=>'Activate plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'activate_plugin','default'=>'Activate plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin aktivieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'activate_plugin','default'=>'Activate plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		configure_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'options'/'configure_plugin','default'=>'Configure plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'configure_plugin','default'=>'Configure plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin konfigurieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'configure_plugin','default'=>'Configure plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		back: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'options'/'back','default'=>'Back','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'back','default'=>'Back','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zurück<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'back','default'=>'Back','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		successful_install: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'options'/'successful_install','default'=>'The plugin [0] have been installed successfully. \\n<br>\\n<br>\\nHow do you want to proceed?','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'successful_install','default'=>'The plugin [0] have been installed successfully. \\n<br>\\n<br>\\nHow do you want to proceed?','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Plugin [0] wurde erfolgreich auf Ihrem System installiert. \n<br>\n<br>\nWie möchten Sie fortfahren?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'options'/'successful_install','default'=>'The plugin [0] have been installed successfully. \\n<br>\\n<br>\\nHow do you want to proceed?','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addEvents('activatePlugin', 'configurePlugin');

        me.items = me.createElements();
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
                {
                    xtype: 'button',
                    text: me.snippets.activate_plugin,
                    cls: 'secondary',
                    handler: function() {
                        me.fireEvent('activatePlugin', me);
                    }
                },
                {
                    xtype: 'button',
                    margin: '0 10',
                    cls: 'secondary',
                    text: me.snippets.configure_plugin,
                    handler: function() {
                        me.fireEvent('configurePlugin', me);
                    }
                },
        /*<?php }?>*/
                {
                    xtype: 'button',
                    text: me.snippets.back,
                    cls: 'secondary',
                    handler: function() {
                        me.destroy()
                    }
                }
            ]
        }];

        me.callParent(arguments);
    },

    createElements: function() {
        var me = this, text = '';

        text = '<center>' + me.snippets.successful_install + '</center>';
        text = Ext.String.format(text, me.record.get('label'));

        me.noticeContainer = Ext.create('Ext.container.Container', {
            html: text,
            style: 'color: #999; font-style: italic;'
        });

        return [ me.noticeContainer ];
    }
});
//
<?php }} ?>