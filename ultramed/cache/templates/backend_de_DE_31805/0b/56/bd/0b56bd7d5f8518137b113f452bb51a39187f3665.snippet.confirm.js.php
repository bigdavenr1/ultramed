<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/account/confirm.js" */ ?>
<?php /*%%SmartyHeaderCode:1944149255447ce3c04f52-48967812%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b56bd7d5f8518137b113f452bb51a39187f3665' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/account/confirm.js',
      1 => 1430111763,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1944149255447ce3c04f52-48967812',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3c48c22_09884722',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3c48c22_09884722')) {function content_55447ce3c48c22_09884722($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.account.Confirm', {
    extend: 'Enlight.app.Window',
    alias: 'widget.plugin-manager-account-confirm',
    border: 0,
    autoShow: true,
    maximizable: false,
    minimizable: false,
    cls: Ext.baseCSSPrefix + 'plugin-manager-account-confirm',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'title','default'=>'Plugin manager - check order','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'title','default'=>'Plugin manager - check order','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin-Manager - Bestellung überprüfen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'title','default'=>'Plugin manager - check order','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

	snippets:{
		abort_buy: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'abort_buy','default'=>'Abort purchase','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'abort_buy','default'=>'Abort purchase','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kauf abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'abort_buy','default'=>'Abort purchase','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		order_pay: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'order_pay','default'=>'Order now','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'order_pay','default'=>'Order now','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungspflichtig bestellen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'order_pay','default'=>'Order now','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		order_number: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'order_number','default'=>'Order number','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'order_number','default'=>'Order number','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'order_number','default'=>'Order number','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		amount: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'amount','default'=>'Amount','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'amount','default'=>'Amount','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzahl:<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'amount','default'=>'Amount','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		price: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'price','default'=>'Price','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'price','default'=>'Price','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Preis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'price','default'=>'Price','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		full_price: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'confirm'/'full_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'full_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
* Preise zuzüglich gesetzl. MwSt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'confirm'/'full_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addEvents('confirmbuy');

        me.width = 450;
        me.height = 300;
        me.view = me.createView();
        me.items = [ me.view ];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: ['->', {
                text: me.snippets.abort_buy,
                cls: 'secondary',
                handler: function() {
                    me.destroy();
                }
            },
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/{
                text: me.snippets.order_pay,
                cls: 'primary',
                handler: function() {
                    me.fireEvent('confirmbuy', me.record, me.detail);
                    me.destroy();
                }
            }
        /*<?php }?>*/]
        }];

        me.callParent(arguments);
    },

    createView: function() {
        var me = this;

        return Ext.create('Ext.view.View', {
            tpl: me.createViewTemplate(),
            data: [{
                name: me.record.get('name'),
                description: Ext.String.ellipsis(Ext.util.Format.stripTags(me.record.get('description')), 180),
                price: me.price,
                ordernumber: me.detail.get('ordernumber'),
                amount: 1
            }]
        });
    },

    createViewTemplate: function() {
		var me = this;

        return new Ext.XTemplate(
            '<tpl for="."><div class="outer-container">',
                '<h3>{name}</h3>',
                '<div class="detail">',
                    '<div class="ordernumber">'+me.snippets.order_number+': {ordernumber}</div>',
                    '<div class="amount">'+me.snippets.amount+': {amount}x</div>',

                    '<p>{description}</p>',
                '</div>',
                '<div class="price">',
                    '<p class="price-display">',
                        '<strong>'+me.snippets.price+':</strong>{price}&nbsp;&euro;*',
                        '<span class="tax-notice">'+me.snippets.full_price+'</span>',
                    '</p>',
                '</div>',
            '</div></tpl>'
        );
    }
});
//
<?php }} ?>