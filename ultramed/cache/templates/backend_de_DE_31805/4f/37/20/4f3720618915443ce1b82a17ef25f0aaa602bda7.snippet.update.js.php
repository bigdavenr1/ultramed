<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/update.js" */ ?>
<?php /*%%SmartyHeaderCode:26974550955447ce37e1524-45285721%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f3720618915443ce1b82a17ef25f0aaa602bda7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/update.js',
      1 => 1430111756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '26974550955447ce37e1524-45285721',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce37f08f7_32455132',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce37f08f7_32455132')) {function content_55447ce37f08f7_32455132($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Main
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 * Shopware Plugin Manager - Update Model
 */
//
Ext.define('Shopware.apps.PluginManager.model.Update', {

    /**
    * Extends the standard Ext Model
    * @string
    */
    extend: 'Ext.data.Model',

   /**
    * @array
    */
    fields: [
       //
       { name: 'plugin', type: 'string' },
       { name: 'name', type: 'string' },
       { name: 'currentVersion', type: 'string' },
       { name: 'availableVersion', type: 'string' },
       { name: 'pluginId', type: 'int' },
       { name: 'articleId', type: 'int' },
       { name: 'ordernumber', type: 'string' },
       { name: 'wasInstalled', type: 'int', useNull: true },
       { name: 'wasActivated', type: 'int', useNull: true },
   ]
});
//

<?php }} ?>