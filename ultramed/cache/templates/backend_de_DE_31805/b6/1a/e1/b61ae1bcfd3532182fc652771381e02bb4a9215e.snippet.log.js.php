<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:14
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/controller/log.js" */ ?>
<?php /*%%SmartyHeaderCode:69859603655447ccb014df0-33973436%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b61ae1bcfd3532182fc652771381e02bb4a9215e' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/controller/log.js',
      1 => 1430113143,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '69859603655447ccb014df0-33973436',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ccb078ec0_05837349',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ccb078ec0_05837349')) {function content_55447ccb078ec0_05837349($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Log
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware Controller - Log list backend module
 *
 * Main controller of the log module.
 * It only creates the main-window.
 * shopware AG (c) 2012. All rights reserved.
 */

//
Ext.define('Shopware.apps.Log.controller.Log', {
    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.app.Controller',

	/**
	* Creates the necessary event listener for this
	* specific controller and opens a new Ext.window.Window
	* @return void
	*/
	init: function() {
		var me = this;

		me.control({
			'log-main-list actioncolumn':{
				deleteColumn: me.onDeleteSingleLog
			},
			'log-main-list toolbar combobox': {
				change: me.onSelectFilter
			},

			'log-main-list button[action=deleteMultipleLogs]':{
				click: me.onDeleteMultipleLogs
			}
		});
	},

	/**
	 * This function is called when the user wants to delete more than one log at once.
	 * It handles the deleting of the logs.
	 *
	 * @param btn Contains the button in the toolbar
	 */
	onDeleteMultipleLogs: function(btn){
		var win = btn.up('window'),
			grid = win.down('grid'),
			selModel = grid.selModel,
			store = grid.getStore(),
			selection = selModel.getSelection(),
			message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'deleteMultipleLogs'/'content','default'=>'You have marked [0] logs. Are you sure you want to delete them?','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteMultipleLogs'/'content','default'=>'You have marked [0] logs. Are you sure you want to delete them?','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sie haben [0] Logs markiert. Sollen diese wirklich gelöscht werden?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteMultipleLogs'/'content','default'=>'You have marked [0] logs. Are you sure you want to delete them?','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', selection.length);

		//Create a message-box, which has to be confirmed by the user
		Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'deleteMultipleLogs'/'title','default'=>'Delete logs','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteMultipleLogs'/'title','default'=>'Delete logs','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logs löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteMultipleLogs'/'title','default'=>'Delete logs','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response){
			//If the user doesn't want to delete the articles
			if (response !== 'yes')
			{
				return false;
			}

			//each selection
			Ext.each(selection, function(item){
				store.remove(item);
			});
			store.sync({
				callback: function(batch, operation) {
					var rawData = batch.proxy.getReader().rawData;
					if(rawData.success){
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'title','default'=>'Logs deleted','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'title','default'=>'Logs deleted','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Logs gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'title','default'=>'Logs deleted','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'content','default'=>'The logs were successfully deleted','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'content','default'=>'The logs were successfully deleted','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Logs wurden erfolgreich gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'success'/'content','default'=>'The logs were successfully deleted','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
						grid.getStore().load();
					}else{
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteMultipleLogs'/'error'/'title','default'=>'An error occurred','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'error'/'title','default'=>'An error occurred','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ein Fehler ist aufgetreten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteMultipleLogs'/'error'/'title','default'=>'An error occurred','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}
				}
			})
		});
	},

	/**
	 * This function is called when the user wants to delete a single log.
	 * It handles the deleting of the log.
	 *
	 * @param rowIndex Contains the rowIndex of the selection, that should be deleted
	 */
	onDeleteSingleLog: function(rowIndex){
		var store = this.subApplication.stores.items[0],
			logModel = store.data.items[rowIndex],
			message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'deleteSingleLog'/'content','default'=>'Are you sure you want to delete this log?','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteSingleLog'/'content','default'=>'Are you sure you want to delete this log?','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sind sie sicher, dass sie diesen Log löschen wollen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteSingleLog'/'content','default'=>'Are you sure you want to delete this log?','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');

		Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'message'/'deleteSingleLog'/'title','default'=>'Delete log','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteSingleLog'/'title','default'=>'Delete log','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'message'/'deleteSingleLog'/'title','default'=>'Delete log','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response){
			//If the user doesn't want to delete the articles
			if (response !== 'yes')
			{
				return false;
			}
			logModel.destroy({
				callback: function(data, operation){
					var records = operation.getRecords(),
						record = records[0],
						rawData = record.getProxy().getReader().rawData;
					if(operation.success){
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'title','default'=>'Log deleted','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'title','default'=>'Log deleted','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'title','default'=>'Log deleted','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'content','default'=>'The log has been deleted successfully.','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'content','default'=>'The log has been deleted successfully.','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Log wurde erfolgreich gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'success'/'content','default'=>'The log has been deleted successfully.','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}else{
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'deleteSingleLog'/'error'/'title','default'=>'An error has occurred','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'error'/'title','default'=>'An error has occurred','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ein Fehler ist aufgetreten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'deleteSingleLog'/'error'/'title','default'=>'An error has occurred','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', rawData.errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}
					store.load();
				}
			});
		})
	},

	/**
	 * This function is called, when the user selects a filter by using the combobox in the toolbar.
	 * It handles the filtering of the store.
	 *
	 * @param combobox Contains the combobox itself
	 * @param newValue Contains the new selected and active value
	 */
	onSelectFilter: function(combobox, newValue){
		var win = combobox.up('window'),
			grid   = win.down('grid'),
			store  = grid.getStore();

		//When you delete the filter of the combobox this function is called twice
		//1st time it's an empty string, 2nd time it is null
		if(newValue === null) {
			return;
		}
		//If the value is an empty string
		if(newValue.length == 0) {
			store.clearFilter();
		}else{
			//contains the displayText of the selected value in the combobox
			var selectedDisplayText = combobox.store.data.findBy(function(item){
				if(item.internalId == newValue) {
					return true;
				}
			}).data.name;
			//This won't reload the store
			store.filters.clear();
			//Loads the store with a special filter
			store.filter('searchValue',selectedDisplayText);
		}
	}
});
//<?php }} ?>