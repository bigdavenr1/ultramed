<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:203945747355447c3d6dc485-04034988%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6b25d0f123ace05c51597aa03bdacb936ac4027' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/detail.js',
      1 => 1430111761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203945747355447c3d6dc485-04034988',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d6e4771_38419624',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d6e4771_38419624')) {function content_55447c3d6e4771_38419624($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.store.main.Detail', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.PaymentPaypal.model.main.Detail',
    proxy: {
        type: 'ajax',
        url : '<?php echo '/backend/PaymentPaypal/getDetails';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    },
    remoteSort: true,
    remoteFilter: true
});
<?php }} ?>