<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/store/view.js" */ ?>
<?php /*%%SmartyHeaderCode:6106746855447ce3cd3bb3-40050196%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c018e688eb233eb8537478560aeb82920321a859' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/store/view.js',
      1 => 1430111766,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6106746855447ce3cd3bb3-40050196',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3d1df85_05787538',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3d1df85_05787538')) {function content_55447ce3d1df85_05787538($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.store.View', {
    extend: 'Ext.panel.Panel',
    unstyled: true,
    alias: 'widget.plugin-manager-store-view',
    autoScroll: true,
    border: 0,
    cls: Ext.baseCSSPrefix + 'plugin-manager-store-view',

	snippets: {
		vat_price: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'store'/'view'/'vat_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'vat_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
* Preise zuzüglich gesetzlicher Mehrwertsteuer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'vat_price','default'=>'* All prices incl. VAT','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		show_all: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'store'/'view'/'show_all','default'=>'Show all','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'show_all','default'=>'Show all','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Alle anzeigen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'show_all','default'=>'Show all','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		hint: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'store'/'view'/'hint','default'=>'Hint','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'hint','default'=>'Hint','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tipp<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'hint','default'=>'Hint','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		of: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'store'/'view'/'of','default'=>' of ','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'of','default'=>' of ','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
 von <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'of','default'=>' of ','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		free: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'store'/'view'/'free','default'=>'Free','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'free','default'=>'Free','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gratis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'store'/'view'/'free','default'=>'Free','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component
     *
     * @public
     * @constructor
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addEvents('changeCategory', 'openArticle');
        me.topSellerView = me.createHighlightsView();
        me.categoryView = me.createCategoryView();

        me.items = [ me.topSellerView, me.categoryView ];
        me.dockedItems = [ me.createTaxNotice() ];

        me.callParent(arguments);
    },

    createTaxNotice: function() {
		var me = this;
        return Ext.create('Ext.container.Container', {
            dock: 'bottom',
            cls: Ext.baseCSSPrefix + 'tax-notice',
            html: me.snippets.vat_price
        });
    },

    /**
     * Creates the highlight (= topseller) view.
     *
     * @public
     * @return [object] Ext.view.View
     */
    createHighlightsView: function() {
        var me = this, store = me.topSellerStore;

        return Ext.create('Ext.view.View', {
            itemSelector: '.clickable',
            store: store,
            tpl: me.createStoreViewTemplate('highlights', 2),
            cls: Ext.baseCSSPrefix + 'community-highlights',
            listeners: {
                scope: me,
                afterrender: function(view) {
                    var el = view.getEl();
                    el.on('click', function(e, t) {
                        var href = t.getAttribute('href'),
                            articleId, categoryId;

                        if(href == '#open-details') {
                            articleId = t.getAttribute('data-articleId');
                            categoryId = t.getAttribute('data-categoryId');
                            categoryId = ~~(1 * categoryId);

                            var record = me.communityStore.getById(categoryId);
                            me.fireEvent('openArticle', articleId, record, 'community');
                            window.location.hash = '';
                        } else {
                            return false;
                        }
                    }, me, { delegate: '.should-clickable' });
                }
            }
        });
    },

    createCategoryView: function() {
        var me = this, store = me.communityStore;
        return Ext.create('Ext.view.View', {
            itemSelector: '.clickable',
            store: store,
            tpl: me.createStoreViewTemplate('listing', 3),
            cls: Ext.baseCSSPrefix + 'community-categories',
            listeners: {
                scope: me,
                afterrender: function(view) {
                    var el = view.getEl();
                    el.on('click', function(e, t) {
                        var href = t.getAttribute('href'),
                            categoryId, articleId, record;

                        if(href == '#show-all') {
                            categoryId = t.getAttribute('data-action');
                            categoryId = ~~(1 * categoryId);

                            record = me.categoryStore.getById(categoryId);
                            me.fireEvent('changeCategory', me.categoryView, record, t);
                            window.location.hash = '';
                        } else if(href == '#open-details') {
                            articleId = t.getAttribute('data-articleId');
                            categoryId = t.getAttribute('data-categoryId');
                            categoryId = ~~(1 * categoryId);

                            record = me.communityStore.getById(categoryId);
                            me.fireEvent('openArticle', articleId, record, 'community');
                            window.location.hash = '';
                        } else {
                            return false;
                        }
                    }, me, { delegate: '.should-clickable' });
                }
            }
        });
    },

    createStoreViewTemplate: function(cls, perPage) {
        var me = this;

        return new Ext.XTemplate(
            '<div class="' + cls + '">',
                    '<tpl for=".">',
                        '<tpl if="this.checkCategoryFilled(values) === true">',
                        '<section>',
                            '<div class="section-header">',
                                '<h2>{description}</h2>',
                                '<a class="should-clickable" data-action="{id}" href="#show-all">'+me.snippets.show_all+' &raquo;</a>',
                            '</div>',
                            '{[this.createRows(values, '+ perPage +')]}',
                        '</section>',
                        '</tpl>',
                    '</tpl>',
            '</div>',
            {
                checkCategoryFilled: function(values) {
                    return !!values.products.length;
                },
                createRows: function(values, perRow) {
                    var maxCount = values.products.length,
                        rowCount = Math.ceil(maxCount / perRow),
                        rows = '', index = 0;

                    for(var j = 0; j < rowCount; j++) {
                        var row = '<div class="row ' + (j % 2 ? 'row-alt' : 'row') + ' clearfix">';
                        for(var i = 0; i < perRow; i++) {
                            if(values.products[index]) {
                                row += this.createColumn(values.products[index], perRow, values.id);
                            }
                            index++;
                        }
                        row += '</div>';
                        rows += row;
                    }
                    return rows;
                },

                createColumn: function(values, perRow, categoryId) {
                    var column = '<div class="column">';

                    if(values.addons && values.addons.highlight) {
                        column += '<div class="highlight"><strong>'+me.snippets.hint+'</strong></div>';
                    }

                    column += '<div class="thumb">' + this.createThumbnail(values, perRow) + '</div>';
                    column += this.createDetails(values, categoryId);
                    column += '</div>';
                    return column;
                },

                createDetails: function(values, categoryId) {
                    var details = '<div class="detail">';
                    if(values.name) {
                        details += '<h3>' + Ext.String.ellipsis(values.name, 40) + '</h3>';
                    }

                    if(values.attributes && values.supplierName) {
                        details += '<span class="version">v' + values.attributes.version + me.snippets.of + Ext.String.ellipsis(values.supplierName, 18) + '</span>';
                    }

                    if(values.description) {
                        details += '<p class="description">' + Ext.util.Format.stripTags(values.description) + '</p>';
                    }

                    var price = '';
                    Ext.each(values.details, function(item) {
                        if(item.rent_version === true) {
                            price = item.price;
                        }
                    });

                    if(!price) {
                        price = values.details[0].price;
                    } else {
                        price = 'ab ' + price;
                    }

                    if(price == 0) {
                        price = me.snippets.free;
                    } else {
                        price += '&euro;&nbsp;*';
                    }
                    if (values.vote_average) {
                        var starCount = ~~(1 * values.vote_average) * 2;
                        details += '<div class="stars star' + starCount + '"></div>'
                    }

                    details += '<div class="action">';
                        details += '<div class="price">' + price + '</div>';
                        details += '<a class="should-clickable buy" data-categoryId="'+categoryId+'" data-articleId="' + values.id + '" href="#open-details">Details &raquo;</a>';
                    details += '</div>';

                    details += '</div>';
                    return details;
                },

                createThumbnail: function(values, perRow) {
                    var images = values.images, image, ret = '';

                    if(values.attributes.certificate == '1') {
                        ret += '<div class="certificated"></div>';
                    }
                    // No picture image
                    if(!images.length) {
                        ret += '<div class="inner-thumb" style="background-image: url(/templates/_default/frontend/_resources/images/no_picture.jpg)">'+values.name+'</div>';
                        return ret;
                    }

                    // Get the thumbnail
                    if(images[0].thumbnails && images[0].thumbnails[3]) {

                        if(perRow == 2) {
                            image = images[0].thumbnails[3];
                        } else {
                            image = images[0].thumbnails[2];
                        }
                    }

                    ret += '<div class="inner-thumb" style="background-image: url(' + image + ')">'+values.name+'</div>';
                    return ret;
                }
            }
        );
    }
});
//
<?php }} ?>