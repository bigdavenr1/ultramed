<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article_slider_type.js" */ ?>
<?php /*%%SmartyHeaderCode:203885366055434611476ce4-53667654%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd812f592043755bcf00dcd2b7b3cb72c52fe5210' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article_slider_type.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203885366055434611476ce4-53667654',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346114b06f0_22705796',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346114b06f0_22705796')) {function content_554346114b06f0_22705796($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.ArticleSliderType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.emotion-components-fields-article-slider-type',
    name: 'article_slider_type',

    /**
     * Snippets for the component
     * @object
     */
    snippets: {
        fields: {
            'article_slider_type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_type'/'fields'/'article_slider_type','default'=>'Artikeltyp','namespace'=>'backend/emotion/view/components/article_slider_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'fields'/'article_slider_type','default'=>'Artikeltyp','namespace'=>'backend/emotion/view/components/article_slider_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikeltyp<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'fields'/'article_slider_type','default'=>'Artikeltyp','namespace'=>'backend/emotion/view/components/article_slider_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'empty_text': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article_slider_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article_slider_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/article_slider_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        store: {
            'selected_article': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_type'/'store'/'selected_article','default'=>'Selected article(s)','namespace'=>'backend/emotion/view/components/article_slider_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'selected_article','default'=>'Selected article(s)','namespace'=>'backend/emotion/view/components/article_slider_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'selected_article','default'=>'Selected article(s)','namespace'=>'backend/emotion/view/components/article_slider_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'newcomer': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_type'/'store'/'newcomer','default'=>'Newcomer articles','namespace'=>'backend/emotion/view/components/article_slider_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'newcomer','default'=>'Newcomer articles','namespace'=>'backend/emotion/view/components/article_slider_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Newcomer Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'newcomer','default'=>'Newcomer articles','namespace'=>'backend/emotion/view/components/article_slider_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'topseller': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_type'/'store'/'topseller','default'=>'Topseller articles','namespace'=>'backend/emotion/view/components/article_slider_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'topseller','default'=>'Topseller articles','namespace'=>'backend/emotion/view/components/article_slider_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Topseller Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_type'/'store'/'topseller','default'=>'Topseller articles','namespace'=>'backend/emotion/view/components/article_slider_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            emptyText: me.snippets.fields.empty_text,
            fieldLabel: me.snippets.fields.article_slider_type,
            displayField: 'display',
            valueField: 'value',
            queryMode: 'local',
            triggerAction: 'all',
            store: me.createStore()
        });

        me.callParent(arguments);
    },

    /**
     * Creates a local store which will be used
     * for the combo box. We don't need that data.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, snippets = me.snippets.store;

        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'value', 'display' ],
            data: [{
                value: 'selected_article',
                display: snippets.selected_article
            }, {
                value: 'newcomer',
                display: snippets.newcomer
            }, {
                value: 'topseller',
                display: snippets.topseller
            }]
        });
    }
});<?php }} ?>