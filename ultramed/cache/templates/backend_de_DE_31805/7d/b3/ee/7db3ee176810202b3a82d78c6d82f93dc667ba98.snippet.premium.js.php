<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:13
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/controller/premium.js" */ ?>
<?php /*%%SmartyHeaderCode:129180657955447e6d6d4513-46495722%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7db3ee176810202b3a82d78c6d82f93dc667ba98' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/controller/premium.js',
      1 => 1430113165,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '129180657955447e6d6d4513-46495722',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e6d7331f8_23591248',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e6d7331f8_23591248')) {function content_55447e6d7331f8_23591248($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Premium
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Premium.controller.Premium', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.app.Controller',

    /**
    * Creates the necessary event listener for this
    * specific controller and opens a new Ext.window.Window
    * @return void
    */
    init: function() {
        var me = this;

        me.control({
            'premium-main-list textfield[action=searchPremiumArticle]':{
                fieldchange: me.onSearch
            },
            //The add-button on the toolbar
            'premium-main-list button[action=add]':{
                click: me.onOpenCreateWindow
            },
            //The delete-button on the toolbar
            'premium-main-list button[action=deleteMultipleArticles]':{
                click: me.onDeleteMultipleArticles
            },
            // The save-button from the create-window
            'window button[action=savePremium]': {
                click: me.onCreatePremium
            },
            'premium-main-list':{
                deleteColumn: me.onDeleteSingleArticle,
                editColumn: me.onOpenEditWindow
            }
        });

    },

   /**
    * Opens the detail-window
    * @event click
    * @return void
    */
    onOpenCreateWindow: function(){
        this.getView('premium.Detail').create();
    },

    /**
     * The user wants to edit an article
     * @event render
     * @param [object] view Contains the view
     * @param [object] item Contains the clicked item
     * @param [int] rowIndex Contains the row-index
     * @return void
     */
    onOpenEditWindow : function (view, item, rowIndex) {
        var store = this.subApplication.premiumStore,
            record = store.getAt(rowIndex);

        //Create edit-window
        this.getView('premium.Detail').create({ record : record, mainStore : store });
    },

    /**
     * Function to create a new article
     * @event click
     * @param [object] btn Contains the clicked button
     * @return void
     */
    onCreatePremium: function(btn) {
        var win = btn.up('window'),
            form = win.down('form'),
            values = form.getForm().getValues(),
            store = this.subApplication.premiumStore;

        var model = Ext.create('Shopware.apps.Premium.model.Premium', values);

        win.close();
        model.save({
            callback: function(data, operation){
                var records = operation.getRecords(),
                    record = records[0],
                    rawData = record.getProxy().getReader().rawData;
                if(operation.success){
                    Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage_title'/'createPremiumSuccess','default'=>'The article was successfully created','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'createPremiumSuccess','default'=>'The article was successfully created','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel erstellt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'createPremiumSuccess','default'=>'The article was successfully created','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage_message'/'createPremiumSuccess','default'=>'The article was successfully saved','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_message'/'createPremiumSuccess','default'=>'The article was successfully saved','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Artikel wurde erfolgreich erstellt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_message'/'createPremiumSuccess','default'=>'The article was successfully saved','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prämienartikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                }else{
                    Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage'/'error','default'=>'An error has occurred','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'error','default'=>'An error has occurred','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ein Fehler ist aufgetreten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage'/'error','default'=>'An error has occurred','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', rawData.errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prämienartikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                }
                store.load();
            }
        });
    },

    /**
     * Function to delete multiple articles
     * Every marked article will be deleted
     * @event click
     * @param [object] btn Contains the clicked button
     * @return [boolean|null]
     */
    onDeleteMultipleArticles: function(btn){
        var win = btn.up('window'),
                grid = win.down('grid'),
                selModel = grid.selModel,
				store = grid.getStore(),
                selection = selModel.getSelection(),
                me = this,
                message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'messagebox_multipleDelete'/'message','default'=>'You have marked [0] articles. Are you sure you want to delete them?','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_multipleDelete'/'message','default'=>'You have marked [0] articles. Are you sure you want to delete them?','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Es sind [0] Artikel markiert. Sollen diese gelöscht werden?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_multipleDelete'/'message','default'=>'You have marked [0] articles. Are you sure you want to delete them?','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', selection.length);

        //Create a message-box, which has to be confirmed by the user
        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'messagebox_multipleDelete'/'title','default'=>'Delete articles','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_multipleDelete'/'title','default'=>'Delete articles','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_multipleDelete'/'title','default'=>'Delete articles','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response){
            //If the user doesn't want to delete the articles
            if (response !== 'yes')
            {
                return false;
            }

            Ext.each(selection, function(item){
				store.remove(item);
			});
			store.sync({
				callback: function(batch, operation) {
					var rawData = batch.proxy.getReader().rawData;
					if (rawData.success) {
						me.subApplication.premiumStore.load();
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage_title'/'deleteMultipleSuccess','default'=>'Articles deleted','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'deleteMultipleSuccess','default'=>'Articles deleted','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'deleteMultipleSuccess','default'=>'Articles deleted','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage_message'/'deleteMultipleSuccess','default'=>'The articles were successfully deleted','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_message'/'deleteMultipleSuccess','default'=>'The articles were successfully deleted','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Artikel wurden erfolgreich gelöscht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_message'/'deleteMultipleSuccess','default'=>'The articles were successfully deleted','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prämienartikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}else{
						Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'growlMessage_title'/'deleteMultipleError','default'=>'An error occurred','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'deleteMultipleError','default'=>'An error occurred','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ein Fehler ist aufgetreten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'growlMessage_title'/'deleteMultipleError','default'=>'An error occurred','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', rawData.errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prämienartikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}
				}
			})
        });
    },

    /**
     * Function to delete one single article
     * Is used, when the user clicks on the delete-button in the action-column
     * @event click
     * @param [object] view Contains the view
     * @param [int] rowIndex Contains the row-index
     * @return mixed
     */
    onDeleteSingleArticle: function(view, rowIndex){
        var store = this.subApplication.premiumStore,
            values = store.data.items[rowIndex].data,
            message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'messagebox_singleDelete'/'message','default'=>'Are you sure you want to delete <b> [0] </b> ?','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_singleDelete'/'message','default'=>'Are you sure you want to delete <b> [0] </b> ?','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Soll <b> [0] </b> gelöscht werden?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_singleDelete'/'message','default'=>'Are you sure you want to delete <b> [0] </b> ?','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', values.name);

        //Create a message-box, which has to be confirmed by the user
        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'messagebox_singleDelete'/'title','default'=>'Delete article','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_singleDelete'/'title','default'=>'Delete article','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'messagebox_singleDelete'/'title','default'=>'Delete article','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response){
            //If the user doesn't want to delete the article
            if(response != 'yes')
            {
                return false;
            }
            var model = Ext.create('Shopware.apps.Premium.model.Premium', values);
            model.destroy({
                callback: function(){
                    store.load();
                }
            });
        });

    },

    /**
     * @event fieldchange
     * Function to search for articles by using a store-filter
     * @param [object] field Contains the searchfield
     * @return void
     */
    onSearch: function(field){
        var me = this,
            store = me.subApplication.premiumStore;

        //If the search-value is empty, reset the filter
        if(field.getValue().length == 0){
            store.clearFilter();
        }else{
            //This won't reload the store
            store.filters.clear();
            //Loads the store with a special filter
            store.filter('searchValue',field.getValue());
        }
    }
});<?php }} ?>