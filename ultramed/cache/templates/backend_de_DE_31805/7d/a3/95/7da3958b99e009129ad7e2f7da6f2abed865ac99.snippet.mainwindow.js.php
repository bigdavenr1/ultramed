<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:44:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/import_export/view/main/mainwindow.js" */ ?>
<?php /*%%SmartyHeaderCode:76528456755447245d69de6-49856683%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7da3958b99e009129ad7e2f7da6f2abed865ac99' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/import_export/view/main/mainwindow.js',
      1 => 1430113372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76528456755447245d69de6-49856683',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447245ecf677_37308741',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447245ecf677_37308741')) {function content_55447245ecf677_37308741($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ImportExport
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
//
Ext.define('Shopware.apps.ImportExport.view.main.Mainwindow', {
    extend: 'Enlight.app.Window',
    alias : 'widget.importexport-main-mainwindow',
    width: 600,

    stateful: true,
    stateId: 'shopware-importexport-mainwindow',

    height: '90%',
    autoScroll: true,

    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },

    /**
     * Contains all snippets for the view component
     * @object
     */
    snippets:{
        title : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title','default'=>'Import / Export','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title','default'=>'Import / Export','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Import / Export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title','default'=>'Import / Export','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        titleExportArticlesCategories: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_articles_categories','default'=>'Export Articles and Categories','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_articles_categories','default'=>'Export Articles and Categories','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel und Kategorien exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_articles_categories','default'=>'Export Articles and Categories','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        titleExportOrders:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_export_orders','default'=>'Export Orders','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_export_orders','default'=>'Export Orders','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellungen exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_export_orders','default'=>'Export Orders','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        titleExportMisc:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_export_misc','default'=>'Export Misc','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_export_misc','default'=>'Export Misc','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sonstiges exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_export_misc','default'=>'Export Misc','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        titleImport:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_import','default'=>'Import','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_import','default'=>'Import','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Importieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_import','default'=>'Import','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        orderNumberFrom:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'order_number_From','default'=>'Ordernumber from','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'order_number_From','default'=>'Ordernumber from','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnummern von<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'order_number_From','default'=>'Ordernumber from','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        dateFrom:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'date_from','default'=>'Date from','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'date_from','default'=>'Date from','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vom (Datum)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'date_from','default'=>'Date from','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        dateTo:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'date_to','default'=>'Date to','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'date_to','default'=>'Date to','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bis (Datum)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'date_to','default'=>'Date to','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        orderState:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'order_state','default'=>'Order state','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'order_state','default'=>'Order state','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'order_state','default'=>'Order state','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        paymentState:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_state','default'=>'Payment state','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_state','default'=>'Payment state','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bezahlstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_state','default'=>'Payment state','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        choose:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'choose','default'=>'Please choose','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'choose','default'=>'Please choose','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte ausw\u00E4hlen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'choose','default'=>'Please choose','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        chooseButton:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'choose_button','default'=>'Choose','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'choose_button','default'=>'Choose','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
W\u00E4hlen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'choose_button','default'=>'Choose','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        format: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'format','default'=>'Format','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'format','default'=>'Format','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Format<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'format','default'=>'Format','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        updateOrderState:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'update_order_state','default'=>'Update Orderstate','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_order_state','default'=>'Update Orderstate','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellstatus updaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_order_state','default'=>'Update Orderstate','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        offset: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'offset','default'=>'Offset','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'offset','default'=>'Offset','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Versatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'offset','default'=>'Offset','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        limit:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'limit','default'=>'Limit','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'limit','default'=>'Limit','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Limit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'limit','default'=>'Limit','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		start: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'start','default'=>'Start','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'start','default'=>'Start','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Start<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'start','default'=>'Start','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		data: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'data','default'=>'Data','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'data','default'=>'Data','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Daten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'data','default'=>'Data','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		uploading: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'uploading','default'=>'uploading...','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'uploading','default'=>'uploading...','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
aktualisiert...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'uploading','default'=>'uploading...','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        exportType:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'export_type','default'=>'Export Type','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_type','default'=>'Export Type','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export-Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_type','default'=>'Export Type','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        exportVariants: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'export_variants','default'=>'Export Variants','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_variants','default'=>'Export Variants','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Varianten exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_variants','default'=>'Export Variants','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        exportTranslations: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'export_translations','default'=>'Export Translations','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_translations','default'=>'Export Translations','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
\u00DCbersetzungen exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_translations','default'=>'Export Translations','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        exportCustomergroupPrices: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'export_customergroup_prices','default'=>'Export Customergroup Prices','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_customergroup_prices','default'=>'Export Customergroup Prices','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppen-Preise exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'export_customergroup_prices','default'=>'Export Customergroup Prices','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		categories: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'categories','default'=>'Categories','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'categories','default'=>'Categories','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'categories','default'=>'Categories','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		articles: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'articles','default'=>'Articles','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'articles','default'=>'Articles','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'articles','default'=>'Articles','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		customers: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'customers','default'=>'Customers','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'customers','default'=>'Customers','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'customers','default'=>'Customers','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		in_stock: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'in_stock','default'=>'In stock','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'in_stock','default'=>'In stock','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Auf Lager<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'in_stock','default'=>'In stock','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		not_in_stock: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'not_in_stock','default'=>'Articles not in stock','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'not_in_stock','default'=>'Articles not in stock','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel nicht auf Lager<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'not_in_stock','default'=>'Articles not in stock','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		prices: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'prices','default'=>'Article prices','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'prices','default'=>'Article prices','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikelpreise<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'prices','default'=>'Article prices','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		article_images: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_images','default'=>'Article images','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_images','default'=>'Article images','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikelbilder<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_images','default'=>'Article images','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		newsletter: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'newsletter','default'=>'Newsletter receiver','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'newsletter','default'=>'Newsletter receiver','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Newsletterempf\u00E4nger<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'newsletter','default'=>'Newsletter receiver','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		article: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article','default'=>'Article','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article','default'=>'Article','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article','default'=>'Article','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		neither: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'neither','default'=>'none','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'neither','default'=>'none','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'neither','default'=>'none','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		all_before_import: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'all_before_import','default'=>'all before import','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'all_before_import','default'=>'all before import','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Alle vor dem Import<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'all_before_import','default'=>'all before import','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    	not_imported: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'not_imported','default'=>'not imported','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'not_imported','default'=>'not imported','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nicht importierte<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'not_imported','default'=>'not imported','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		empty: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'empty','default'=>'empty','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'empty','default'=>'empty','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Leere<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'empty','default'=>'empty','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		file: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'file','default'=>'File','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file','default'=>'File','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datei<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file','default'=>'File','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		noticeMessage: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'notice_message','default'=>'The import / export options do possibly not support all of your maintained fields. Please read our \\<a href=\\\'http://wiki.shopware.de/Datenaustausch_detail_308.html\\\' target=\\\'_blank\\\' \\>wiki\\</a\\> documentation before using the module.','namespace'=>'backend/import_export/view/mainwindow')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'notice_message','default'=>'The import / export options do possibly not support all of your maintained fields. Please read our \\<a href=\\\'http://wiki.shopware.de/Datenaustausch_detail_308.html\\\' target=\\\'_blank\\\' \\>wiki\\</a\\> documentation before using the module.','namespace'=>'backend/import_export/view/mainwindow'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Import/Export-Möglichkeiten unterstützen gegebenenfalls nicht alle von Ihnen gepflegten Felder, prüfen Sie daher bitte unbedingt die Dokumentation bevor Sie mit dem Modul arbeiten.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'notice_message','default'=>'The import / export options do possibly not support all of your maintained fields. Please read our \\<a href=\\\'http://wiki.shopware.de/Datenaustausch_detail_308.html\\\' target=\\\'_blank\\\' \\>wiki\\</a\\> documentation before using the module.','namespace'=>'backend/import_export/view/mainwindow'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

		var block = Shopware.Notification.createBlockMessage(me.snippets.noticeMessage, 'notice');
        me.title = me.snippets.title;

        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'read'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?> */
        me.items = [
			block,
            me.getExportArticlesForm(),
            me.getExportOrdersForm(),
            me.getExportMiscForm(),
            me.getImportForm()
        ];
        /* <?php }?> */

        me.callParent(arguments);
    },

    /**
     * @return [Ext.form.Panel]
     */
    getExportOrdersForm: function() {
        var me = this;

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'bottom',
            cls: 'shopware-toolbar',
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'export'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?> */
            items: [ '->',
                {
                    text: me.snippets.start,
                    cls: 'primary',
                    formBind: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (!form.isValid()) {
                            return;
                        }

                        form.submit({
                            method: 'GET',
                            url: '<?php echo '/backend/ImportExport/exportOrders';?>'
                        });
                    }
                }
            ]
        /* <?php }?> */
        });

        var orderStatusStore = Ext.create('Shopware.store.OrderStatus');

        return Ext.create('Ext.form.Panel', {
            title: me.snippets.titleExportOrders,
            bodyPadding: 5,
            standardSubmit: true,
            target: 'iframe',
            layout: 'anchor',
            dockedItems: toolbar,
            defaults: {
                anchor: '100%',
                labelWidth: 300
            },
            defaultType: 'textfield',
            items: [
                {
                    fieldLabel: me.snippets.orderNumberFrom,
                    name: 'ordernumberFrom'
                },
                {
                    xtype: 'datefield',
                    fieldLabel: me.snippets.dateFrom,
                    name: 'dateFrom',
                    maxValue: new Date(),
                    submitFormat: 'd.m.Y'
                },
                {
                    xtype: 'datefield',
                    fieldLabel: me.snippets.dateTo,
                    name: 'dateTo',
                    maxValue: new Date(),
                    submitFormat: 'd.m.Y'
                },
                {
                    xtype: 'combobox',
                    name: 'orderstate',
                    fieldLabel: me.snippets.orderState,
                    emptyText: me.snippets.choose,
                    store: orderStatusStore,
                    editable: false,
                    displayField: 'description',
                    valueField: 'id'
                },
                {
                    xtype: 'combobox',
                    name: 'paymentstate',
                    fieldLabel: me.snippets.paymentState,
                    emptyText: me.snippets.choose,
                    store: Ext.create('Shopware.store.PaymentStatus'),
                    editable: false,
                    displayField: 'description',
                    valueField: 'id'
                },
                {
                    xtype: 'combobox',
                    name: 'updateOrderstate',
                    fieldLabel:  me.snippets.updateOrderState,
                    emptyText: me.snippets.choose,
                    store: orderStatusStore,
                    editable: false,
                    displayField: 'description',
                    valueField: 'id'
                },
                {
                    xtype: 'combobox',
                    fieldLabel: me.snippets.format,
                    name: 'format',
                    listeners: {
                        'afterrender': function () {
                            this.setValue(this.store.getAt('0').get('id'));
                        }
                    },
                    store: me.getFormatComboStore(),

                    forceSelection: true,
                    allowBlank: false,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    displayField: 'label',
                    valueField: 'id'
                }
            ]
        });
    },

    /**
     * @return [Ext.form.Panel]
     */
    getExportArticlesForm: function() {
        var me = this;

        var exportVariantsCheckbox = Ext.create('Ext.form.Checkbox', {
            name: 'exportVariants',
            fieldLabel: me.snippets.exportVariants,
            inputValue: 1,
            uncheckedValue: 0,
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var exportTranslationsCheckbox = Ext.create('Ext.form.Checkbox', {
            name: 'exportArticleTranslations',
            fieldLabel: me.snippets.exportTranslations,
            inputValue: 1,
            uncheckedValue: 0,
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var exportCustomergroupPricesCheckbox = Ext.create('Ext.form.Checkbox', {
            name: 'exportCustomergroupPrices',
            fieldLabel: me.snippets.exportCustomergroupPrices,
            inputValue: 1,
            uncheckedValue: 0,
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var limitField = Ext.create('Ext.form.Number', {
            fieldLabel: me.snippets.limit,
            name: 'limit',
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var offsetField = Ext.create('Ext.form.Number', {
            fieldLabel: me.snippets.offset,
            name: 'offset',
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'bottom',
            cls: 'shopware-toolbar',
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'export'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?> */
            items: [ '->',
                {
                    text: me.snippets.start,
                    cls: 'primary',
                    formBind: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (!form.isValid()) {
                            return;
                        }

                        var values = form.getValues();
                        var url = '';

                        if (values.type === 'categories') {
                            url = '<?php echo '/backend/ImportExport/exportCategories';?>';
                        } else if (values.type === 'articles') {
                            url = '<?php echo '/backend/ImportExport/exportArticles';?>';
                        }

                        form.submit({
                            method: 'GET',
                            url: url
                        });
                    }
                }
            ]
        /* <?php }?> */
        });

        return Ext.create('Ext.form.Panel', {
            title: me.snippets.titleExportArticlesCategories,
            bodyPadding: 5,
            standardSubmit: true,
            target: 'iframe',
            layout: 'anchor',
            dockedItems: toolbar,
            defaults: {
                anchor: '100%',
                labelWidth: 300
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: me.snippets.data,
                    name: 'type',
                    listeners: {
                        'afterrender': function () {
                            this.setValue(this.store.getAt('0').get('id'));
                        },
                        'change': function(view, newValue) {
                            if (newValue === 'articles') {
                                exportVariantsCheckbox.show();
                                exportCustomergroupPricesCheckbox.show();
                                exportTranslationsCheckbox.show();
                                limitField.show();
                                offsetField.show();
                            } else {
                                exportVariantsCheckbox.hide();
                                exportCustomergroupPricesCheckbox.hide();
                                exportTranslationsCheckbox.hide();
                                limitField.hide();
                                offsetField.hide();
                            }
                        }
                    },
                    store: me.getDataComboStore(),

                    forceSelection: true,
                    allowBlank: false,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    displayField: 'label',
                    valueField: 'id'
                },
                {
                    xtype: 'combobox',
                    fieldLabel: me.snippets.format,
                    name: 'format',
                    listeners: {
                        'afterrender': function () {
                            this.setValue(this.store.getAt('0').get('id'));
                        }
                    },
                    store: me.getFormatComboStore(),

                    forceSelection: true,
                    allowBlank: false,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    displayField: 'label',
                    valueField: 'id'
                },
                exportVariantsCheckbox,
                exportCustomergroupPricesCheckbox,
                exportTranslationsCheckbox,
                limitField,
                offsetField
            ]
        });
    },

    /**
     * @return [Ext.form.Panel]
     */
    getExportMiscForm: function() {
        var me = this;

        var exportVariantsCheckbox = Ext.create('Ext.form.Checkbox', {
            name: 'exportVariants',
            fieldLabel: me.snippets.exportVariants,
            inputValue: 1,
            uncheckedValue: 0,
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var limitField = Ext.create('Ext.form.Number', {
            fieldLabel: me.snippets.limit,
            name: 'limit',
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        var offsetField = Ext.create('Ext.form.Number', {
            fieldLabel: me.snippets.offset,
            name: 'offset',
            hidden: true,
            anchor: '100%',
            labelWidth: 300
        });

        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'export'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?> */
        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'bottom',
            cls: 'shopware-toolbar',
            items: [ '->',
                {
                    text: me.snippets.start,
                    cls: 'primary',
                    formBind: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (!form.isValid()) {
                            return;
                        }

                        var values = form.getValues();
                        var url = '';

                        if (values.type === 'customers') {
                            url = '<?php echo '/backend/ImportExport/exportCustomers';?>';
                        } else if (values.type === 'instock') {
                            url = '<?php echo '/backend/ImportExport/exportInStock';?>';
                        } else if (values.type === 'notinstock') {
                            url = '<?php echo '/backend/ImportExport/exportNotInStock';?>';
                        } else if (values.type === 'newsletter') {
                            url = '<?php echo '/backend/ImportExport/exportNewsletter';?>';
                        } else if (values.type === 'prices') {
                            url = '<?php echo '/backend/ImportExport/exportPrices';?>';
                        } else if (values.type === 'images') {
                            url = '<?php echo '/backend/ImportExport/exportArticleImages';?>';
                        } else {
                            return;
                        }

                        form.submit({
                            method: 'GET',
                            url: url
                        });
                    }
                }
            ]
        });
        /* <?php }?> */

        return Ext.create('Ext.form.Panel', {
            title: me.snippets.titleExportMisc,
            bodyPadding: 5,
            standardSubmit: true,
            target: 'iframe',
            layout: 'anchor',
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'export'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php if ($_tmp5){?> */
            dockedItems: toolbar,
        /* <?php }?> */
            defaults: {
                anchor: '100%',
                labelWidth: 300
            },
            defaultType: 'textfield',
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: me.snippets.exportType,
                    name: 'type',
                    listeners: {
                        'afterrender': function () {
                            this.setValue(this.store.getAt('0').get('id'));
                        },
                        'change': function(view, newValue) {
                            if (newValue === 'customers' || newValue === 'newsletter' || newValue === 'images') {
                                exportVariantsCheckbox.hide();
                                limitField.hide();
                                offsetField.hide();
                            } else {
                                exportVariantsCheckbox.show();
                                limitField.show();
                                offsetField.show();
                            }
                        }
                    },
                    store: me.getMiscComboStore(),
                    forceSelection: true,
                    allowBlank: false,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    displayField: 'label',
                    valueField: 'id'
                },
                exportVariantsCheckbox,
                limitField,
                offsetField
            ]
        });
    },

    /**
     * @return [Ext.form.Panel]
     */
    getImportForm: function() {
        var me = this;

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'bottom',
            cls: 'shopware-toolbar',
        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'import'),$_smarty_tpl);?>
<?php $_tmp6=ob_get_clean();?><?php if ($_tmp6){?> */
            items: [ '->',
                {
                    text: me.snippets.start,
                    cls: 'primary',
                    formBind: true,
                    handler: function () {
                        var form = this.up('form').getForm();
                        if (!form.isValid()) {
                            return;
                        }

                        form.submit({
                            url: ' <?php echo '/backend/ImportExport/import';?>',
                            waitMsg: me.snippets.uploading,
                            success: function (fp, o) {
                                Ext.Msg.alert('Result', o.result.message);
                            },
                            failure: function (fp, o) {
                                Ext.Msg.alert('Fehler', o.result.message);
                            }
                        });
                    }
                }
            ]
        /* <?php }?> */
        });

        var deleteCategoriesComboBox = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Delete categories',
            name: 'deleteCategories',
            listeners: {
                'afterrender': function () {
                    this.setValue(this.store.getAt('0').get('id'));
                }
            },
            store: me.getDeleteCategoriesComboStore(),
            forceSelection: true,
            allowBlank: false,
            editable: false,
            mode: 'local',
            triggerAction: 'all',
            displayField: 'label',
            valueField: 'id',
            enabled: false,
            anchor: '100%',
            labelWidth: 300
        });

        var deleteArticlesComboBox = Ext.create('Ext.form.ComboBox', {
            fieldLabel: 'Delete Articles',
            name: 'deleteArticles',
            listeners: {
                'afterrender': function () {
                    this.setValue(this.store.getAt('0').get('id'));
                }
            },
            store: me.getDeleteArticlesComboStore(),
            forceSelection: true,
            allowBlank: false,
            editable: false,
            mode: 'local',
            triggerAction: 'all',
            displayField: 'label',
            valueField: 'id',
            enabled: false,
            anchor: '100%',
            labelWidth: 300
        });

        return Ext.create('Ext.form.Panel', {
            xtype: 'form',
            title: me.snippets.titleImport,
            bodyPadding: 5,
            layout: 'anchor',
            dockedItems: toolbar,
            defaults: {
                anchor: '100%',
                labelWidth: 300
            },
            items: [
                {
                    xtype: 'combobox',
                    fieldLabel: me.snippets.data,
                    name: 'type',
                    store: me.getImportComboStore(),
                    emptyText: me.snippets.choose,
                    forceSelection: true,
                    allowBlank: false,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    displayField: 'label',
                    valueField: 'id',
                    listeners: {
                        'change': function (field, newValue) {
                        }
                    }
                },
                {
                    xtype: 'filefield',
                    emptyText: me.snippets.choose,
                    buttonText:  me.snippets.chooseButton,
                    name: 'file',
                    fieldLabel: me.snippets.file,
                    allowBlank: false
                }
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getFormatComboStore: function() {
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                ['csv', 'CSV'],
                ['excel', 'Excel'],
                ['xml', 'XML']
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getDataComboStore: function() {
		var me = this;
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                ['categories', me.snippets.categories],
                ['articles', me.snippets.articles]
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getMiscComboStore: function() {
		var me = this;
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                ['customers', me.snippets.customers],
                ['instock', me.snippets.in_stock],
                ['notinstock', me.snippets.not_in_stock],
                ['prices', me.snippets.prices],
                ['images', me.snippets.article_images],
                ['newsletter', me.snippets.newsletter]
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getImportComboStore: function() {
		var me = this;
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                ['customers', me.snippets.customers],
                ['instock', me.snippets.in_stock],
                ['newsletter', me.snippets.newsletter],
                ['prices', me.snippets.prices],
                ['articles', me.snippets.article],
                ['images', me.snippets.article_images],
                ['categories', me.snippets.categories]
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getDeleteCategoriesComboStore: function() {
		var me = this;
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                [0, me.snippets.neither],
                [1, me.snippets.all_before_import],
                [2, me.snippets.not_imported],
                [3, me.snippets.empty]
            ]
        });
    },

    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getDeleteArticlesComboStore: function() {
		var me = this;
        return new Ext.data.SimpleStore({
            fields: ['id', 'label'],
            data: [
                [0, me.snippets.neither],
                [1, me.snippets.all_before_import],
                [2, me.snippets.not_imported]
            ]
        });
    }
});
//
<?php }} ?>