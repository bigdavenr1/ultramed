<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/table.js" */ ?>
<?php /*%%SmartyHeaderCode:2130125420554347857a0726-60298159%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b970aacf271a3fae145e35143349298dbc12cb97' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/table.js',
      1 => 1430113331,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2130125420554347857a0726-60298159',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347857b2b83_63813868',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347857b2b83_63813868')) {function content_554347857b2b83_63813868($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Table Base Class
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.main.Table', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.analytics-table',

    initComponent: function () {
        var me = this;

        me.createPagingbar();

        me.callParent(arguments);
    },

    createPagingbar: function() {
        var me = this;

        Ext.applyIf(me, {
            dockedItems: [
                {
                    xtype: 'pagingtoolbar',
                    displayInfo: true,
                    store: me.store,
                    dock: 'bottom'
                }
            ]
        });
    },

    initStoreIndices: function (indexName, text, params) {
        var me = this,
            columns = me.columns,
            columnItems = !!columns.items ? columns.items : columns,
            column;

        if (!me.shopSelection) {
            return;
        }

        indexName = indexName || 'amount';
        text = text || '[0]';
        params = params || { };

        Ext.each(me.shopSelection, function (shopId) {
            var shop = me.shopStore.getById(shopId);

            column = Ext.merge({
                dataIndex: indexName + shop.get('id'),
                text: Ext.String.format(text, shop.get('name'))
            }, params);

            columnItems.push(column);
        });
    },

    initShopColumns: function(columns) {
        var me = this;

        if (!me.columns) {
            me.columns = [];
        }
        Ext.each(me.shopSelection, function (shopId) {
            var shop = me.shopStore.getById(shopId);

            Ext.each(columns, function(config) {
                var column = Ext.clone(config);

                column = Ext.merge(column, {
                    dataIndex: config.dataIndex + shopId,
                    text: Ext.String.format(config.text, shop.get('name'))
                });
                
                me.columns.items.push(column);
            });
        });
        
    },

    getColumns: function () {
        return this.columns;
    }
});
//
<?php }} ?>