<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/detail/settings.js" */ ?>
<?php /*%%SmartyHeaderCode:79957965455447ce3b28c19-87576823%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e95828438a8bcaea03b65cd678f2370f881a2cf8' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/detail/settings.js',
      1 => 1430111764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '79957965455447ce3b28c19-87576823',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3baed76_85429294',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3baed76_85429294')) {function content_55447ce3baed76_85429294($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.detail.Settings', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.plugin-manager-detail-settings',
    autoScroll: true,
    border: 0,
    layout: 'auto',
    cls: Ext.baseCSSPrefix + 'plugin-manager-detail-settings',
    items: [],

	snippets:{
		properties: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'properties','default'=>'Plugin properties','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'properties','default'=>'Plugin properties','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin Eigenschaften<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'properties','default'=>'Plugin properties','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		key: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'key','default'=>'Key','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'key','default'=>'Key','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Schlüssel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'key','default'=>'Key','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'name','default'=>'Name','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'name','default'=>'Name','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'name','default'=>'Name','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		author: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'author','default'=>'Author','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'author','default'=>'Author','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Author<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'author','default'=>'Author','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		copyright: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'copyright','default'=>'Copyright','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'copyright','default'=>'Copyright','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Copyright<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'copyright','default'=>'Copyright','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		support: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'support','default'=>'Support','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'support','default'=>'Support','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Support<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'support','default'=>'Support','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		link: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'link','default'=>'Link','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'link','default'=>'Link','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'link','default'=>'Link','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		active: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		version: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Version<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		namespace: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'namespace','default'=>'Namespace','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'namespace','default'=>'Namespace','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Namespace<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'namespace','default'=>'Namespace','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		source: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'source','default'=>'Source','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'source','default'=>'Source','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Source<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'source','default'=>'Source','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		added: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinzugefügt am<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		installed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'installed','default'=>'Installed on','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'installed','default'=>'Installed on','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Installiert am<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'installed','default'=>'Installed on','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		updated: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'updated','default'=>'Last update on','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'updated','default'=>'Last update on','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Letztes Update am<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'updated','default'=>'Last update on','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		activate_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'settings'/'activate_plugin','default'=>'Active plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'activate_plugin','default'=>'Active plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin aktivieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'settings'/'activate_plugin','default'=>'Active plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this, configForms = [], formId;

        me.items = [];

        if (me.plugin instanceof Ext.data.Model) {
            me.items.push(me.createDescriptionFieldSet());
        }

        if (me.plugin) {
            configForms = me.plugin.get('configForms');
        }

        if(me.plugin) {
            me.checkboxContainer = me.createActiveCheckbox();
            me.items.push(me.checkboxContainer);
        }


        me.callParent(arguments);

        if(configForms.length) {
            configForms = configForms[0];
            formId = configForms.id;
            me.form = Ext.create('Shopware.form.PluginPanel', { formId: formId, descriptionField: false });
            me.add(me.form);
            me.doComponentLayout();
        }
    },

    /**
     * Creates the description field set for the plugin informations.
     */
    createDescriptionFieldSet: function() {
        var me = this;

        return Ext.create('Ext.form.FieldSet', {
            xtype: 'fieldset',
            layout: 'column',
            margin: '10 10 0',
            cls: Ext.baseCSSPrefix + 'plugin-manager-plugin-properties',
            bodyPadding: 5,
            title: me.snippets.properties,
            items: [
                me.createLeftContainer(),
                me.createRightContainer(),
                me.createBottomContainer()
            ]
        });
    },

    createLeftContainer: function() {
        var me = this;

        return Ext.create('Ext.container.Container', {
            defaults: {
                labelWidth: 130,
                xtype: 'displayfield'
            },
            columnWidth: 0.5,
            items: [{
                value: me.plugin.get('name'),
                fieldLabel: me.snippets.key
            }, {
                value: me.plugin.get('label'),
                fieldLabel: me.snippets.name
            },
            {
                value: me.plugin.get('author'),
                fieldLabel: me.snippets.author
            },
            {
                value: me.plugin.get('copyright'),
                fieldLabel: me.snippets.copyright
            },
            {
                value: me.plugin.get('support'),
                fieldLabel: me.snippets.support
            },
            {
                value: me.plugin.get('link'),
                fieldLabel: me.snippets.link
            }]
        });
    },

    createRightContainer: function() {
        var me = this;

        return Ext.create('Ext.container.Container', {
            defaults: {
                xtype: 'displayfield',
                labelWidth: 130
            },
            columnWidth: 0.5,
            items: [{
                value: me.plugin.get('version'),
                fieldLabel: me.snippets.version
            },
            {
                value: me.plugin.get('namespace'),
                fieldLabel: me.snippets.namespace
            },
            {
                value: me.plugin.get('source'),
                fieldLabel: me.snippets.source
            },
            {
                value: me.plugin.get('added'),
                fieldLabel: me.snippets.added
            },
            {
                value: me.plugin.get('installed'),
                fieldLabel: me.snippets.installed
            },
            {
                value: me.plugin.get('updated'),
                fieldLabel: me.snippets.updated
            }]
        });
    },

    createBottomContainer: function() {
        var me = this;

        return Ext.create('Ext.container.Container', {
            margin: '10 0 0',
            layout: {
                type: 'anchor'
            },
            columnWidth: 1,
            items: [{
                xtype: 'displayfield',
                labelWidth: 130,
                value: me.plugin.get('description'),
                fieldLabel: me.snippets.description,
                anchor: '100%'
            }]
        });
    },


    /**
     * Creates the active checkbox within a container.
     *
     * @public
     * @return [object] Ext.container.Container
     */
    createActiveCheckbox: function() {
        var me = this;

        me.checkbox = Ext.create('Ext.form.field.Checkbox', {
            fieldLabel: me.snippets.active,
            inputValue: true,
            labelWidth: 130,
            uncheckedValue: false,
            boxLabel: me.snippets.activate_plugin,
            checked: me.plugin.get('active')
        });

        return Ext.create('Ext.container.Container', {
            padding: '10 10 10 30',
            items: [ me.checkbox ]
        });
    }
});
//
<?php }} ?>