<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/week.js" */ ?>
<?php /*%%SmartyHeaderCode:867644149554347857ed7c8-79953151%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e93caf7719c9293a6b9d6c8b95a44370288afd4c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/week.js',
      1 => 1430113330,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '867644149554347857ed7c8-79953151',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785816376_47762409',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785816376_47762409')) {function content_55434785816376_47762409($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Week Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Week', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-week',
    legend: {
        position: 'right'
    },

    initComponent: function () {
        var me = this;

        me.axes = [
            {
                type: 'Time',
                position: 'bottom',
                fields: ['date'],
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'week'/'titleBottom','default'=>'Date','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'week'/'titleBottom','default'=>'Date','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'week'/'titleBottom','default'=>'Date','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dateFormat: '\\K\\W W, Y',
                minorTickSteps: 6,
                step: [Ext.Date.HOUR, 7 * 24],
                label: {
                    rotate: {
                        degrees: 315
                    }
                }
            }
        ];

        me.series = [];

        if (me.shopSelection != Ext.undefined && me.shopSelection.length > 0) {
            Ext.each(me.shopSelection, function (shopId) {
                var shop = me.shopStore.getById(shopId);

                if (!(shop instanceof Ext.data.Model)) {
                    return true;
                }

                me.series.push({
                    type: 'line',
                    title: shop.data.name,
                    axis: ['left', 'bottom'],
                    xField: 'date',
                    yField: 'turnover' + shop.data.id,
                    smooth: true,
                    fill: true,
                    tips: {
                        trackMouse: true,
                        width: 180,
                        highlight: {
                            size: 7,
                            radius: 7
                        },
                        height: 60,
                        renderer: function (storeItem, item) {
                            var value = Ext.util.Format.currency(
                                storeItem.get('turnover' + shop.data.id),
                                me.subApp.currencySign,
                                2,
                                (me.subApp.currencyAtEnd == 1)
                            );
                            this.setTitle(
                                Ext.Date.format(storeItem.get('date'), '\\K\\W W, Y') + '<br><br>&nbsp;' +
                                value
                            );
                        }
                    }
                });
            });
        } else {
            me.series = [
                {
                    type: 'line',
                    axis: ['left', 'bottom'],
                    xField: 'date',
                    yField: 'turnover',
                    fill: true,
                    smooth: true,
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    tips: {
                        trackMouse: true,
                        width: 180,
                        height: 45,
                        layout: 'fit',
                        items: {
                            xtype: 'container',
                            layout: 'hbox',
                            items: [me.tipChart, me.tipGrid]
                        },
                        renderer: function (storeItem) {
                            var value = Ext.util.Format.currency(
                                storeItem.get('turnover'),
                                me.subApp.currencySign,
                                2,
                                (me.subApp.currencyAtEnd == 1)
                            );
                            this.setTitle(
                                Ext.Date.format(storeItem.get('date'), '\\K\\W W, Y') + '<br><br>&nbsp;' +
                                value
                            )

                        }
                    }
                }
            ];
        }

        me.axes.push({
            type: 'Numeric',
            minimum: 0,
            grid: true,
            position: 'left',
            fields: me.getAxesFields('turnover'),
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });
        me.callParent(arguments);

    }
});
//<?php }} ?>