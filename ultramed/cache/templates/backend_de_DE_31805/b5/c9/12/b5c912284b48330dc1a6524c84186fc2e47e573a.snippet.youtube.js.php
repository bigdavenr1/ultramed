<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/youtube.js" */ ?>
<?php /*%%SmartyHeaderCode:12809563755434611561248-87834628%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b5c912284b48330dc1a6524c84186fc2e47e573a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/youtube.js',
      1 => 1430113367,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12809563755434611561248-87834628',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346115761b7_81340834',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346115761b7_81340834')) {function content_554346115761b7_81340834($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
//
Ext.define('Shopware.apps.Emotion.view.components.Youtube', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.emotion-components-youtube',

    snippets: {
        video_id: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'video_id','default'=>'Youtube video id','namespace'=>'backend/emotion/view/components/youtube')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'video_id','default'=>'Youtube video id','namespace'=>'backend/emotion/view/components/youtube'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Youtube-Video ID<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'video_id','default'=>'Youtube video id','namespace'=>'backend/emotion/view/components/youtube'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        video_hd: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'video_hd','default'=>'Use HD videos','namespace'=>'backend/emotion/view/components/youtube')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'video_hd','default'=>'Use HD videos','namespace'=>'backend/emotion/view/components/youtube'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
HD Video verwenden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'video_hd','default'=>'Use HD videos','namespace'=>'backend/emotion/view/components/youtube'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    }
});
//
<?php }} ?>