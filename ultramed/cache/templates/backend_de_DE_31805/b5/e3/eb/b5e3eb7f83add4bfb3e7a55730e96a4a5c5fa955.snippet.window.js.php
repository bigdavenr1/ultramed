<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/detail/window.js" */ ?>
<?php /*%%SmartyHeaderCode:115628611855447ce3852ba1-15569837%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b5e3eb7f83add4bfb3e7a55730e96a4a5c5fa955' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/detail/window.js',
      1 => 1430111764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '115628611855447ce3852ba1-15569837',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'storeApiAvailable' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce38a7c52_98881349',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce38a7c52_98881349')) {function content_55447ce38a7c52_98881349($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.detail.Window', {
    /**
     * Define that the plugin manager main window is an extension of the enlight application window
     * @string
     */
    extend:'Enlight.app.Window',
    /**
     * Set base css class prefix and module individual css class for css styling
     * @string
     */
    cls:Ext.baseCSSPrefix + 'plugin-manager-detail-window',
    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.plugin-manager-detail-window',
    /**
     * Set no border for the window
     * @boolean
     */
    border:false,
    /**
     * True to automatically show the component upon creation.
     * @boolean
     */
    autoShow:true,
    /**
     * Set border layout for the window
     * @string
     */
    layout:'fit',
    /**
     * Define window width
     * @integer
     */
    width:800,
    /**
     * Define window height
     * @integer
     */
    height:'90%',
    /**
     * True to display the 'maximize' tool button and allow the user to maximize the window, false to hide the button and disallow maximizing the window.
     * @boolean
     */
    maximizable:false,
    /**
     * True to display the 'close' tool button and allow the user to close the window, false to hide the button and disallow closing the window.
     * @boolean
     */
    closable: true,
    /**
     * True to display the 'minimize' tool button and allow the user to minimize the window, false to hide the button and disallow minimizing the window.
     * @boolean
     */
    minimizable:false,
    /**
     * A flag which causes the object to attempt to restore the state of internal properties from a saved state on startup.
     */
    stateful:true,
    /**
     * The unique id for this object to use for state management purposes.
     */
    stateId:'shopware-plugin-manager-detail-window',

	snippets:{
		buy_install_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'buy_install_plugin','default'=>'Buy & install plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'buy_install_plugin','default'=>'Buy & install plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin kaufen & installieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'buy_install_plugin','default'=>'Buy & install plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		detail_site: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'detail_site','default'=>'Detail site','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'detail_site','default'=>'Detail site','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Detailseite<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'detail_site','default'=>'Detail site','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		install_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin installieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		cancel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		save_plugin_settings: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'save_plugin_settings','default'=>'Save plugin settings','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'save_plugin_settings','default'=>'Save plugin settings','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin-Einstellungen speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'save_plugin_settings','default'=>'Save plugin settings','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'description','default'=>'Description','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		settings: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'settings','default'=>'Settings','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'settings','default'=>'Settings','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'settings','default'=>'Settings','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		report_error: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'window'/'report_error','default'=>'Report an error','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'report_error','default'=>'Report an error','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Fehler melden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'window'/'report_error','default'=>'Report an error','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this, buttonText = me.snippets.buy_install_plugin;

        me.addEvents('installPlugin', 'saveConfiguration', 'pluginTabChanged');

        if(me.record && me.record.data && me.record.get('name')) {
            me.title = me.snippets.detail_site+' - ' + me.record.get('name');
        } else {
            me.title = me.snippets.detail_site+' - ' + me.plugin.get('label');
        }
        if (me.record && me.record.getDetail() && me.record.getDetail().first() && me.record.getDetail().first().get('price') === 0) {
            buttonText = me.snippets.install_plugin;
        }

        me.tabPanel = me.createTabPanel();
        me.items = me.tabPanel;

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: ['->', {
                text: me.snippets.cancel,
                cls: 'secondary',
                handler: function() {
                    me.destroy();
                }
            }
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
			,	{
                text: me.flag === 'community' ? buttonText : me.snippets.save_plugin_settings,
                cls: 'primary',
                handler: function(button) {
                    me.fireEvent((me.flag === 'community' ? 'installPlugin' : 'saveConfiguration'), me, button)
                }
            }
        /*<?php }?>*/]
        }];

        me.callParent(arguments);
    },

    /**
     * Creates a tab panel which holds off the different sections
     * of the detail page.
     *
     * @public
     * @return [object] Ext.tab.Panel
     */
    createTabPanel: function() {
        var me = this;
        var tabs = [];

        me.productWrapper = null;

        /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
        if(me.record && me.record.get('name')) {
            tabs.push({
                title: me.snippets.description,
                xtype: 'plugin-manager-detail-description',
                article: me.record,
                voteStore: me.voteStore
            });
        } else {
           me.productWrapper = Ext.create('Ext.container.Container', {
               title: me.snippets.description,
               plugin: me.plugin,
               name: 'product-wrapper',
               voteStore: me.voteStore,
               layout: 'fit'
           });
           tabs.push(me.productWrapper);
        }
        /** <?php }?> */
        tabs.push({
            title: me.snippets.settings,
            xtype: 'plugin-manager-detail-settings',
            plugin: me.plugin,
            disabled: me.flag === 'community',
            hidden: me.flag === 'community'
        });

        if(me.flag === 'community') {
            tabs.push({
               title: me.snippets.report_error,
               disabled: true
           });
        }

        return Ext.create('Ext.tab.Panel', {
            xtype: 'tabpanel',
            /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
            activeTab: (me.flag === 'community' || (me.record && !me.record.get('name')) ? 0 : 1),
            /** <?php }else{ ?> */
            activeTab: 0,
            /** <?php }?> */
            plain: true,
            items: tabs,
            listeners: {
                beforetabchange: function(tabPanel, newCard, oldCard, eOpts ) {
                    me.fireEvent('pluginTabChanged', me, tabPanel, newCard, oldCard);
                }
            }
        });
    }
});
//
<?php }} ?>