<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/category_image_type.js" */ ?>
<?php /*%%SmartyHeaderCode:510737576554346112e3406-71215497%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5355bd4d336c45b54e7fb667b3e73e6ecdbbea83' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/category_image_type.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '510737576554346112e3406-71215497',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461131b752_95619931',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461131b752_95619931')) {function content_5543461131b752_95619931($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.CategoryImageType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.emotion-components-fields-category-image-type',
    name: 'image_type',

    /**
     * Snippets for the component
     * @object
     */
    snippets: {
        fields: {
            'image_type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'article_type','default'=>'Type','namespace'=>'backend/emotion/view/components/category_image_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'article_type','default'=>'Type','namespace'=>'backend/emotion/view/components/category_image_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'article_type','default'=>'Type','namespace'=>'backend/emotion/view/components/category_image_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'empty_text': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_image_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_image_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_image_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        store: {
            'selected_image': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'selected_image','default'=>'Selected image','namespace'=>'backend/emotion/view/components/category_image_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'selected_image','default'=>'Selected image','namespace'=>'backend/emotion/view/components/category_image_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewähltes Bild<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'selected_image','default'=>'Selected image','namespace'=>'backend/emotion/view/components/category_image_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'random_article_image': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/category_image_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/category_image_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zufälliger Artikel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article'/'store'/'random_article','default'=>'Random article','namespace'=>'backend/emotion/view/components/category_image_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            emptyText: me.snippets.fields.empty_text,
            fieldLabel: me.snippets.fields.image_type,
            displayField: 'display',
            valueField: 'value',
            queryMode: 'local',
            triggerAction: 'all',
            store: me.createStore()
        });

        me.callParent(arguments);
        me.on('change', me.onArticleSelectChange, me);
    },

    /**
     * Event listeners which triggers when the user changs the value
     * of the select field.
     *
     * @public
     * @event change
     * @param [object] field - Ext.form.field.ComboBox
     * @param [string] value - The selected value
     */
    onArticleSelectChange: function(field, value) {
        var me = this;

        // Terminate the article search field
        if(!me.mediaSelection) {
            me.mediaSelection = me.up('fieldset').down('mediaselectionfield');
        }

        // Show/hide article search field based on selected entry
        me.mediaSelection.setVisible(value !== 'selected_image' ? false : true);
    },

    /**
     * Creates a local store which will be used
     * for the combo box. We don't need that data.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, snippets = me.snippets.store;

        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'value', 'display' ],
            data: [{
                value: 'selected_image',
                display: snippets.selected_image
            }, {
                value: 'random_article_image',
                display: snippets.random_article_image
            }]
        });
    }
});<?php }} ?>