<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/action.js" */ ?>
<?php /*%%SmartyHeaderCode:149000177755447c3d6448c0-47143867%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fbdf97730dcd0aecac74b81cd0dcaa41d593f4c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/action.js',
      1 => 1430111761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '149000177755447c3d6448c0-47143867',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d6cbf81_01539953',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d6cbf81_01539953')) {function content_55447c3d6cbf81_01539953($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//

//
Ext.define('Shopware.apps.PaymentPaypal.view.main.Action', {
    extend: 'Ext.window.Window',
    alias: 'widget.paypal-main-action',

    layout: 'fit',
    width: 400,
    height: 330,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'title','default'=>'Payment action: ','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'title','default'=>'Payment action: ','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsaktion: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'title','default'=>'Payment action: ','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' + me.paymentActionName,
            items: me.getItems(),
            buttons: me.getButtons()
        });

        me.callParent(arguments);

        me.show();
        me.down('form').getForm().setValues(me.detailData);
    },

    getItems: function() {
        var me = this, action = me.paymentAction;
        return [{
            xtype: 'form',
            layout: 'anchor',
            bodyPadding: 10,
            border: 0,
            autoScroll: true,
            defaults: {
                anchor: '100%',
                labelWidth: 160,
                xtype: 'textfield'
            },
            items: [{
                xtype: 'hidden',
                name: 'paymentAction',
                value: action
            }, {
                name: 'transactionId',
                readOnly: true,
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Transaktionscode<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'orderNumber',
                readOnly: true,
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnumer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'paymentCurrency',
                readOnly: true,
                hidden: action == 'void',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-number',
                name: 'paymentAmount',
                hidden: action == 'void',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'amount','default'=>'Book amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'amount','default'=>'Book amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Buchungsbetrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'amount','default'=>'Book amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-boolean',
                name: 'paymentFull',
                hidden: action != 'refund',
                handler: function(btn, value) {
                    var field = me.down('[name=paymentAmount]');
                    field[value ? 'hide' : 'show']();
                },
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'complete_amount','default'=>'Complete amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'complete_amount','default'=>'Complete amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kompletter Betrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'complete_amount','default'=>'Complete amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-boolean',
                name: 'paymentLast',
                hidden: action != 'capture' && action != 'book',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'last','default'=>'Last capture','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'last','default'=>'Last capture','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Letzter Einzug<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'last','default'=>'Last capture','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'note',
                xtype: 'textarea',
                hidden: action == 'auth',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'field'/'note','default'=>'Note to the customer','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'note','default'=>'Note to the customer','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinweis an den Kunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'field'/'note','default'=>'Note to the customer','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }]
        }];
    },

    getButtons: function() {
        var me = this;
        return [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'abort_text','default'=>'Abort','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'abort_text','default'=>'Abort','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'abort_text','default'=>'Abort','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'secondary',
            handler: function(btn) {
                me.close();
            }
        }, {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'execute_text','default'=>'Execute','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'execute_text','default'=>'Execute','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausführen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'execute_text','default'=>'Execute','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            handler: function(btn) {
                var form = me.down('form');
                if(!form.getForm().isValid()) {
                    return;
                }
                me.close();
                Ext.MessageBox.wait('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'wait_message','default'=>'Processing ...','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'wait_message','default'=>'Processing ...','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wird ausgeführt ...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'wait_message','default'=>'Processing ...','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'wait_title','default'=>'Perform the payment action','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'wait_title','default'=>'Perform the payment action','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausführen der Zahlungsaktion<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'wait_title','default'=>'Perform the payment action','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                form.getForm().submit({
                    url: '<?php echo '/backend/PaymentPaypal/doAction';?>',
                    success: me.onActionSuccess,
                    failure: me.onActionFailure,
                    scope: this
                });
            }
        }];
    },

    onActionSuccess: function(form, action) {
        Ext.Msg.alert('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'success_title','default'=>'Success','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'success_title','default'=>'Success','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erfolg<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'success_title','default'=>'Success','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'action'/'success_message','default'=>'The payment action completed successfully.','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'success_message','default'=>'The payment action completed successfully.','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Zahlungsaktion wurde erfolgreich ausgeführt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'action'/'success_message','default'=>'The payment action completed successfully.','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
    },

    onActionFailure: function(form, action) {
        switch (action.failureType) {
            case Ext.form.action.Action.CLIENT_INVALID:
                Ext.Msg.alert('Error', 'Form fields may not be submitted with invalid values');
                break;
            case Ext.form.action.Action.CONNECT_FAILURE:
                Ext.Msg.alert('Error', 'Ajax communication failed');
                break;
            default:
            case Ext.form.action.Action.SERVER_INVALID:
                Ext.Msg.alert('Failure', action.result.message);
                break;
        }
    }
});
//
<?php }} ?>