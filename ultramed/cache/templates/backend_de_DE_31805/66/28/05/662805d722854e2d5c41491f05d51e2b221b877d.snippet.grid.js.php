<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments/grid.js" */ ?>
<?php /*%%SmartyHeaderCode:824886711554476fc60f459-31691666%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '662805d722854e2d5c41491f05d51e2b221b877d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments/grid.js',
      1 => 1430113617,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '824886711554476fc60f459-31691666',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc673d33_78726597',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc673d33_78726597')) {function content_554476fc673d33_78726597($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Blog comments grid.
 *
 * Displays all Blog Comment Information
 */
/**
 * Default blog comment list view. Extends a grid panel.
 */
//
Ext.define('Shopware.apps.Blog.view.blog.detail.comments.Grid', {
    extend:'Ext.grid.Panel',
    border: false,
    alias:'widget.blog-blog-detail-comments-grid',
    region:'center',
    autoScroll:true,
    store:'List',
    ui:'shopware-ui',
    split: true,
    selType:'cellmodel',
    /**
     * Initialize the Shopware.apps.Blog.view.blog.detail.comments and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;
        me.selModel = me.getGridSelModel();

        me.columns = me.getColumns();
        me.pagingbar = me.getPagingBar();
        me.store = me.commentStore;
        me.dockedItems = [ me.pagingbar ];
        me.callParent(arguments);
    },
    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(
                /**
                 * Event will be fired when the user clicks the delete icon in the
                 * action column
                 *
                 * @event deleteBlogComment
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'deleteBlogComment',

                /**
                 * Event will be fired when the user clicks the accept icon in the
                 * action column
                 *
                 * @event acceptBlogComment
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'acceptBlogComment',

                /**
                 * Event will be fired when the changed the selection
                 *
                 * @event acceptBlogComment
                 * @param [integer] sm - selection model
                 * @param [object] selection
                 */
                'selectionChange'
        );

        return true;
    },
    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns:function () {
        var me = this;

        var columnsData = [
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'column'/'status','default'=>'Status','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'status','default'=>'Status','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'status','default'=>'Status','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'active',
                renderer: me.activeColumnRenderer,
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'column'/'date','default'=>'Date','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'date','default'=>'Date','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'date','default'=>'Date','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'creationDate',
                renderer: me.dateRenderer,
                flex:3
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'column'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autor<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'name',
//                renderer: me.viewsRenderer,
                flex:3
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'column'/'headline','default'=>'Headline','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'headline','default'=>'Headline','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Überschrift<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'column'/'headline','default'=>'Headline','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'headline',
//
                flex:3
            },
            {
                xtype:'actioncolumn',
                width:50,
                items:me.getActionColumnItems()
            }
        ];
        return columnsData;
    },
    /**
     * Creates the items of the action column
     *
     * @return [array] action column items
     */
    getActionColumnItems: function () {
        var me = this,
            actionColumnData = [];


        actionColumnData.push({
            iconCls:'sprite-plus-circle',
            cls:'addBtn',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'action_column'/'add','default'=>'Accept comment','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'action_column'/'add','default'=>'Accept comment','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kommentar freischalten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'action_column'/'add','default'=>'Accept comment','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            getClass: function(value, metadata, record) {
                if (record.get("active")) {
                    return 'x-hidden';
                }
            },
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('acceptBlogComment', view, rowIndex, colIndex, item);
            }
        });

        actionColumnData.push({
            iconCls:'sprite-minus-circle-frame',
            action:'delete',
            cls:'delete',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'comments'/'action_column'/'delete','default'=>'Deleted comment','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'action_column'/'delete','default'=>'Deleted comment','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kommentar löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'comments'/'action_column'/'delete','default'=>'Deleted comment','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('deleteBlogComment', view, rowIndex, colIndex, item);
            }
        });

        return actionColumnData;
    },

    /**
     * Creates the paging toolbar for the blog grid to allow
     * and store paging. The paging toolbar uses the same store as the Grid
     *
     * @return Ext.toolbar.Paging The paging toolbar for the customer grid
     */
    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store:me.commentStore,
            dock:'bottom',
            displayInfo:true
        });

    },
    /**
     * Creates the grid selection model for checkboxes
     *
     * @return [Ext.selection.CheckboxModel] grid selection model
     */
    getGridSelModel:function () {
        var me = this,
            selModel = Ext.create('Ext.selection.CheckboxModel', {
            listeners:{
                // Unlocks the delete button if the user has checked at least one checkbox
                selectionchange:function (sm, selections) {
                    me.fireEvent('selectionChange', sm, selections);

                }
            }
        });
        return selModel;
    },

    /**
     * title Renderer Method
     *
     * @param value
     */
    titleRenderer:function (value) {
        return Ext.String.format('<strong style="font-weight: 700">{0}</strong>', value);
    },

    /**
     * Renderer function of the DisplayDate column
     *
     * @param value
     * @param metaData
     * @param record
     */
    dateRenderer: function(value, metaData, record) {
        if (record.get('creationDate') === Ext.undefined) {
            return record.get('creationDate');
        }
        return Ext.util.Format.date(record.get('creationDate')) + ' ' + Ext.util.Format.date(record.get('creationDate'), timeFormat);
    },

    /**
     * Renderer for the active flag
     *
     * @param [object] - value
     */
    activeColumnRenderer: function(value) {
        if (value) {
            return '<div class="sprite-tick"  style="width: 25px; height: 25px">&nbsp;</div>';
        } else {
            return '<div class="sprite-cross" style="width: 25px; height: 25px">&nbsp;</div>';
        }
    }
});
//
<?php }} ?>