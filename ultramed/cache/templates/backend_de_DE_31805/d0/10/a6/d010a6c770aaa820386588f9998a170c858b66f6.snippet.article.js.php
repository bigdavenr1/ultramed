<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article.js" */ ?>
<?php /*%%SmartyHeaderCode:185374138355434611261cd8-59259088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd010a6c770aaa820386588f9998a170c858b66f6' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/article.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '185374138355434611261cd8-59259088',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461128dec5_03098175',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461128dec5_03098175')) {function content_5543461128dec5_03098175($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
Ext.define('Shopware.apps.Emotion.view.components.fields.Article', {
    extend: 'Shopware.form.field.ArticleSearch',
    alias: 'widget.emotion-components-fields-article',
    hiddenReturnValue: 'number',
    returnValue: 'number',

    initComponent: function() {
        var me = this;
        me.hiddenFieldName = me.name;
        me.callParent(arguments);
    },

    createHiddenField: function() {
        var me = this,
            input = Ext.create('Ext.form.field.Hidden', {
            name: me.hiddenFieldName,
            valueType: me.valueType,
            fieldId: me.fieldId
        });
        return input;
    }

});<?php }} ?>