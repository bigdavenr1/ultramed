<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/list.js" */ ?>
<?php /*%%SmartyHeaderCode:46486671554476fc4a09e6-08780199%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c40141ea39135b9ca7ce62b0470bc36118790334' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/list.js',
      1 => 1430113345,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '46486671554476fc4a09e6-08780199',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc51eba1_43268050',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc51eba1_43268050')) {function content_554476fc51eba1_43268050($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Blog list main window.
 *
 * Displays all Blog Information
 */
/**
 * Default blog list view. Extends a grid panel
 */
//
Ext.define('Shopware.apps.Blog.view.blog.List', {
    extend:'Ext.grid.Panel',
    border: false,
    alias:'widget.blog-blog-list',
    region:'center',
    autoScroll:true,
    store:'List',
    ui:'shopware-ui',
    selType:'cellmodel',
    /**
     * Initialize the Shopware.apps.Blog.view.blog.List and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.registerEvents();

        me.selModel = me.getGridSelModel();

        me.columns = me.getColumns();
        me.toolbar = me.getToolbar();
        me.pagingbar = me.getPagingBar();
        me.store = me.listStore;
        me.dockedItems = [ me.toolbar, me.pagingbar ];
        me.callParent(arguments);
    },
    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(
                /**
                 * Event will be fired when the user clicks the delete icon in the
                 * action column
                 *
                 * @event deleteColumn
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'deleteBlogArticle',

                /**
                 * Event will be fired when the user clicks the delete icon in the
                 * action column
                 *
                 * @event deleteColumn
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'editBlogArticle',

                /**
                 * Event will be fired when the user clicks the duplicate icon in the
                 * action column
                 *
                 * @event duplicateColumn
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'duplicateColumn'
        );

        return true;
    },
    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns:function () {
        var me = this;

        var columnsData = [
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Titel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'title',
                renderer: me.titleRenderer,
                flex:6
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'number_of_comments','default'=>'Pending comments','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'number_of_comments','default'=>'Pending comments','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nicht bearbeitete Kommentare<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'number_of_comments','default'=>'Pending comments','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'numberOfComments',
                renderer: me.greenRenderer,
                flex:3
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'views','default'=>'Views','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'views','default'=>'Views','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aufrufe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'views','default'=>'Views','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'views',
                renderer: me.greenRenderer,
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'date','default'=>'Display at','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'date','default'=>'Display at','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzeigezeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'date','default'=>'Display at','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'displayDate',
                renderer: me.dateRenderer,
                flex:3
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'active',
                renderer: me.activeColumnRenderer,
                flex:1
            },
            {
                xtype:'actioncolumn',
                width:90,
                items:me.getActionColumnItems()
            }
        ];
        return columnsData;
    },
    /**
     * Creates the items of the action column
     *
     * @return [array] action column items
     */
    getActionColumnItems: function () {
        var me = this,
            actionColumnData = [];


        actionColumnData.push({
            iconCls:'sprite-pencil',
            cls:'editBtn',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'edit','default'=>'Edit this blog article','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit this blog article','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diesen Blogartikel bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit this blog article','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('editBlogArticle', view, rowIndex, colIndex, item);
            }
        });

        actionColumnData.push({
            iconCls:'sprite-minus-circle-frame',
            action:'delete',
            cls:'delete',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'delete','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diesen Blogartikel löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete this blog article','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('deleteBlogArticle', view, rowIndex, colIndex, item);
            }
        });

        actionColumnData.push({
            iconCls:'sprite-blue-document-copy',
            cls:'duplicate',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'duplicate','default'=>'Duplicate this blog','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'duplicate','default'=>'Duplicate this blog','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Diesen Blogartikel duplizieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'duplicate','default'=>'Duplicate this blog','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('duplicateColumn', view, rowIndex, colIndex, item);
            }

        });

        return actionColumnData;
    },
    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar:function () {
        return Ext.create('Ext.toolbar.Toolbar',
            {
                dock:'top',
                ui:'shopware-ui',
                items:[
			/* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?> */
                    {
                        iconCls:'sprite-plus-circle',
                        text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'button'/'add','default'=>'Add blog article','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'add','default'=>'Add blog article','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blogartikel hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'add','default'=>'Add blog article','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        action:'add'
                    },
			/* <?php }?> */
			/* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?> */
                    {

                        iconCls:'sprite-minus-circle-frame',
                        text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'button'/'delete','default'=>'Delete selected blog articles','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'delete','default'=>'Delete selected blog articles','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Blogartikel löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'delete','default'=>'Delete selected blog articles','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        disabled:true,
                        action:'deleteBlogArticles'

                    },
			/* <?php }?> */
                    '->',
                    {
                        xtype:'textfield',
                        name:'searchfield',
                        action:'searchBlogArticles',
                        width:170,
                        cls: 'searchfield',
                        enableKeyEvents:true,
                        checkChangeBuffer: 500,
                        emptyText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'field'/'search','default'=>'Search...','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                    },
                    { xtype:'tbspacer', width:6 }
                ]
            });
    },
    /**
     * Creates the paging toolbar for the blog grid to allow
     * and store paging. The paging toolbar uses the same store as the Grid
     *
     * @return Ext.toolbar.Paging The paging toolbar for the customer grid
     */
    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store:me.listStore,
            dock:'bottom',
            displayInfo:true
        });

    },
    /**
     * Creates the grid selection model for checkboxes
     *
     * @return [Ext.selection.CheckboxModel] grid selection model
     */
    getGridSelModel:function () {
        var selModel = Ext.create('Ext.selection.CheckboxModel', {
            listeners:{
                // Unlocks the delete button if the user has checked at least one checkbox
                selectionchange:function (sm, selections) {
                    var owner = this.view.ownerCt,
                    btn = owner.down('button[action=deleteBlogArticles]');
                    btn.setDisabled(!selections.length);
                }
            }
        });
        return selModel;
    },

    /**
     * Renderer for the views column
     *
     * @param [object] - value
     */
    greenRenderer: function(value) {
        return '<span style="color:green; font-weight: 700;">' + value + '</span>';
    },

    /**
     * title Renderer Method
     *
     * @param value
     */
    titleRenderer:function (value) {
        return Ext.String.format('<strong style="font-weight: 700">{0}</strong>', value);
    },

    /**
     * Renderer function of the DisplayDate column
     *
     * @param value
     * @param metaData
     * @param record
     */
    dateRenderer: function(value, metaData, record) {
        if (record.get('displayDate') === Ext.undefined) {
            return record.get('displayDate');
        }
        return Ext.util.Format.date(record.get('displayDate')) + ' ' + Ext.util.Format.date(record.get('displayDate'), timeFormat);
    },

    /**
     * Renderer for the active flag
     *
     * @param [object] - value
     */
    activeColumnRenderer: function(value) {
        if (value) {
            return '<div class="sprite-tick"  style="width: 25px; height: 25px">&nbsp;</div>';
        } else {
            return '<div class="sprite-cross" style="width: 25px; height: 25px">&nbsp;</div>';
        }
    }
});
//
<?php }} ?>