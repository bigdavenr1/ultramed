<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/store/navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:176513642555447ce3d22258-01228837%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51c43af0e3a15ef4b057d27cc2117bad3dc96107' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/store/navigation.js',
      1 => 1430111765,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176513642555447ce3d22258-01228837',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3d5fe40_58153486',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3d5fe40_58153486')) {function content_55447ce3d5fe40_58153486($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.store.Navigation', {
    extend: 'Ext.container.Container',
    alias: 'widget.plugin-manager-store-navigation',
    border: 0,
    padding: 10,
    autoScroll:true,
    cls: Ext.baseCSSPrefix + 'plugin-manager-navigation',

    /**
     * Snippets for the components bundled in a object.
     * @object
     */
    snippets: {
        my_account: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'headline'/'my_account','default'=>'My account','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'headline'/'my_account','default'=>'My account','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mein Konto<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'headline'/'my_account','default'=>'My account','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        categories: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'headline'/'categories','default'=>'Categories','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'headline'/'categories','default'=>'Categories','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'headline'/'categories','default'=>'Categories','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initializes the component
     *
     * @public
     * @constructor
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.registerAdditionalEvents();
        me.searchField = me.createSearchfield();
        me.categoryView = me.createCategoryView();
        me.accountView = me.createAccountView();

        me.items = [ me.searchField, me.accountView, me.categoryView ];
        me.callParent(arguments);
    },

    /**
     * Registers additional events for the component.
     *
     * @public
     * @return void
     */
    registerAdditionalEvents: function() {
        var me = this;

        me.addEvents(
            'searchCommunityStore',
            'changeCategory',
            'openAccount',
            'openLicense',
            'openUpdates'
        );
    },

    /**
     * Creates a search field which will be used
     * to search into the community store.
     *
     * @return [array]
     */
    createSearchfield: function() {
        var me = this;

        return Ext.create('Ext.form.field.Text', {
            xtype: 'textfield',
            name: 'communitySearch',
            cls: 'searchfield',
            width: 200,
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'search'/'empty','default'=>'Search in the community store...','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'search'/'empty','default'=>'Search in the community store...','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Im Community-Store suchen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'search'/'empty','default'=>'Search in the community store...','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            listeners: {
                scope: me,
                buffer: 500,
                change: function(field, newValue, oldValue, eOpts) {
                    me.fireEvent('searchCommunityStore', field, newValue, oldValue, eOpts);
                }
            }
        });
    },

    /**
     * Creates the store and the view which are necessary for the category
     * listing.
     *
     * @public
     * @return [object] Ext.view.View
     */
    createCategoryView: function() {
        var me = this;

        return Ext.create('Ext.view.View', {
            store: me.categoryStore,
            cls: Ext.baseCSSPrefix + 'category-navigation',
            tpl: me.createCategoryViewTemplate(),
            itemSelector: '.clickable',
            listeners: {
                scope: me,
                itemclick: function(view, record, dom, index) {
                    me.fireEvent('changeCategory', view, record, dom, index);
                }
            }
        });
    },

    /**
     * Creates the XTemplate which is used for the category listing.
     *
     * @public
     * @return [object] Ext.XTemplate
     */
    createCategoryViewTemplate: function() {
        var me = this;
        return new Ext.XTemplate(
           '<div class="outer-container">',
                '<h2 class="headline">' + me.snippets.categories + '</h2>',
                '<ul class="categories">',
                    '<tpl for=".">',
                        '<li>',

                            '<tpl if="selected == true">',
                                '<span class="clickable active" data-action="{id}">{description}</span>',
                           '</tpl>',

                           '<tpl if="selected != true">',
                                '<span class="clickable" data-action="{id}">{description}</span>',
                           '</tpl>',
                        '</li>',
                    '</tpl>',
                '</ul>',
           '</div>'
        );
    },

    /**
     * Creates the account view and it's associated store.
     *
     * @public
     * @return [object] Ext.view.View
     */
    createAccountView: function() {
        var me = this, updateCount = 0;

        me.accountCategoryStore = Ext.create('Ext.data.Store', {
            fields: [ 'name', 'badge', 'selected', 'action' ],
            data: [
                { name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'open_account','default'=>'Open account','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'open_account','default'=>'Open account','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Konto öffnen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'open_account','default'=>'Open account','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', badge: 0, selected: false, action: 'openAccount' },
                { name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'purchases_licenses','default'=>'My purchases / licenses','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'purchases_licenses','default'=>'My purchases / licenses','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Meine Einkäufe / Lizenzen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'purchases_licenses','default'=>'My purchases / licenses','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', badge: 0, selected: false, action: 'openLicense' },
                { key: 'updates', name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'navigation'/'updates','default'=>'Updates','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'updates','default'=>'Updates','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Updates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'navigation'/'updates','default'=>'Updates','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', badge: 0, selected: false, action: 'openUpdates' }
            ]
        });

        me.accountNavigation = Ext.create('Ext.view.View', {
            store: me.accountCategoryStore,
            cls: Ext.baseCSSPrefix + 'account-navigation',
            tpl: me.createAccountViewTemplate(),
            itemSelector: '.clickable',
            listeners: {
                scope: me,
                itemclick: function(view, record, element) {
                    var event, i, attr;

                    for(i in element.attributes) {
                        attr = element.attributes[i];

                        if(attr.name === 'data-action') {
                            event = attr.value;
                            break;
                        }
                    }
                    if(!event || !event.length) {
                        return false;
                    }

                    me.fireEvent(event, view, record);
                }
            }
        });
        return me.accountNavigation;
    },

    /**
     * Creates the XTemplate which is used for the account listing.
     *
     * @public
     * @return [object] Ext.XTemplate
     */
    createAccountViewTemplate: function() {
        var me = this;

        return new Ext.XTemplate(
           '<div class="outer-container">',
                '<h2 class="headline">' + me.snippets.my_account + '</h2>',
                '<ul class="categories">',
                    '<tpl for=".">',
                        '<li>',
                            '<tpl if="selected == true">',
                                '<span data-action="{action}" class="active clickable">{name}</span>',
                            '</tpl>',

                            '<tpl if="selected != true">',
                                '<span data-action="{action}" class="clickable">{name}</span>',
                            '</tpl>',

                            '<tpl if="badge &gt; 0">',
                                '<span class="badge">{badge}</span>',
                            '</tpl>',
                        '</li>',
                    '</tpl>',
                '</ul>',
            '</div>'
        );
    }
});
//
<?php }} ?>