<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:25660286655447ce3de9406-09399410%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a757bd72c1f740e05b47c8e2fc9cdfce319b58b8' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/main.js',
      1 => 1430111754,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25660286655447ce3de9406-09399410',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'storeApiAvailable' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3e0d3b1_14146951',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3e0d3b1_14146951')) {function content_55447ce3e0d3b1_14146951($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage Controller
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 *
 */
//
//
Ext.define('Shopware.apps.PluginManager.controller.Main', {

    /**
     * The parent class that this class extends.
     * @string
     */
    extend:'Ext.app.Controller',

    /**
     * Class property which holds the main application if it is created
     *
     * @default null
     * @object
     */
    mainWindow: null,

    /**
     * A template method that is called when your application boots.
     * It is called before the Application's launch function is executed
     * so gives a hook point to run any code before your Viewport is created.
     *
     * @return void
     */
    init:function () {
        var me = this;

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'read'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        me.subApplication.pluginStore = me.getStore('Plugin');
        me.subApplication.pluginStore.getProxy().extraParams.category = 'Community';
        me.subApplication.pluginStore.load({
            callback: function(records, operation, success) {
                if(success) {
                    return true;
                }

                var error = Ext.util.Format.stripTags(operation.error);
                Ext.MessageBox.alert('Error-Report', error);
            }
        });

        /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
        me.subApplication.communityStore = me.getStore('Community');
        me.subApplication.topSellerStore = me.getStore('TopSeller');
        me.subApplication.categoryStore = me.getStore('Category');

        me.subApplication.licencedProductStore = me.getStore('LicencedProduct');
        me.subApplication.updatesStore = me.getStore('Updates').load();
        me.subApplication.myAccount = Ext.create('Shopware.apps.PluginManager.model.Account');
        /** <?php }?> */

        me.getView('main.Window').create({
            categoryStore: me.subApplication.categoryStore,
            pluginStore: me.subApplication.pluginStore,
            /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
            communityStore: me.subApplication.communityStore,
            accountStore: me.subApplication.accountStore,
            licencedProductStore: me.subApplication.licencedProductStore,
            topSellerStore: me.subApplication.topSellerStore,
            updatesStore: me.subApplication.updatesStore
            /** <?php }?> */
        });
		/** <?php }?> */

        me.callParent(arguments);
    }

});
//
<?php }} ?>