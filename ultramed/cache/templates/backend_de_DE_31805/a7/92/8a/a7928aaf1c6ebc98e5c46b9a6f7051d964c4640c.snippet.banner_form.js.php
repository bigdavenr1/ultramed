<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:35:58
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/view/main/banner_form.js" */ ?>
<?php /*%%SmartyHeaderCode:74559967855447e5e6fb027-25819556%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7928aaf1c6ebc98e5c46b9a6f7051d964c4640c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/view/main/banner_form.js',
      1 => 1430113342,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '74559967855447e5e6fb027-25819556',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e5e781ef1_75458995',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e5e781ef1_75458995')) {function content_55447e5e781ef1_75458995($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Banner
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**/

/**
 * Shopware UI - Banner View Main Form
 *
 * View component which features a form panel to edit
 * a existing banner.
 */
//
Ext.define('Shopware.apps.Banner.view.main.BannerForm', {
    extend : 'Enlight.app.Window',
    alias: 'widget.banner-view-main-banner-form',
    cls : 'addWindow',
    autoShow : true,
    border : 0,
    width : 700,
    height : 350,
    bodyPadding: 5,
    basePath: '<?php $_smarty_tpl->smarty->loadPlugin("smarty_function_flink"); echo smarty_function_flink(array("file" => '', "fullPath" => false), $_smarty_tpl); ?>/',

    /**
     * Initializes the component
     *
     * @return void
     */
    initComponent: function() {
        var me      = this;
        me.items    = me.createFormPanel();
        me.dockedItems = [{
            xtype: 'toolbar',
            ui: 'shopware-ui',
            dock: 'bottom',
            cls: 'shopware-toolbar',
            items: me.createActionButtons()
        }];


        me.callParent(arguments);
        // Load record
        me.formPanel.getForm().loadRecord(this.record);

    },

    /**
     * Creates the main form panel for this component.
     *
     * @return [object] generated Ext.form.Panel
     */
    createFormPanel: function() {
        var me = this,
            descField, linkField, validFrom, validUntil;

        // Description field
        descField = Ext.create('Ext.form.field.Text', {
            name        : 'description', //
            anchor      : '100%',
            allowBlank  : false,
            fieldLabel  : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'description','default'=>'Description','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'description','default'=>'Description','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'description','default'=>'Description','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'description_support','default'=>'Description of the banner e.g. Jackets-Winter-Special2013','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'description_support','default'=>'Description of the banner e.g. Jackets-Winter-Special2013','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung des Banners z.B. Jacken-Winter-Special2013<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'description_support','default'=>'Description of the banner e.g. Jackets-Winter-Special2013','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        // Link field
        linkField = Ext.create('Ext.form.field.Text', {
            name        : 'link',
            anchor      : '100%',
            fieldLabel  : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'link','default'=>'Link','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link','default'=>'Link','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link','default'=>'Link','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'link_support','default'=>'Link which will be called up if the banner has been clicked.','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_support','default'=>'Link which will be called up if the banner has been clicked.','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link der beim anklicken des Banner aufgerufen wird.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_support','default'=>'Link which will be called up if the banner has been clicked.','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            emptyText   : 'http://'
        });

        var store = Ext.create('Ext.data.Store', {
            fields: ['id', 'value', 'display'],
            data: [
                { value: '_blank', display: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'link_target'/'external','default'=>'External','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'external','default'=>'External','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
External<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'external','default'=>'External','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' },
                { value: '_parent', display: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'link_target'/'internal','default'=>'Shopware','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'internal','default'=>'Shopware','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopware<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'internal','default'=>'Shopware','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' }
            ]
        });

        me.linkTarget = Ext.create('Ext.form.field.ComboBox', {
            name:'linkTarget',
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'link_target'/'field','default'=>'Link target','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'field','default'=>'Link target','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link target<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'link_target'/'field','default'=>'Link target','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: store,
            valueField:'value',
            displayField:'display',
            editable:false
        });


        // Get timing containers
        validFrom   = me.createValidFromContainer();
        validUntil  = me.createValidUntilContainer();
        
        // Media selection field
        var dropZone = Ext.create('Shopware.MediaManager.MediaSelection', {
            fieldLabel      : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'banner','default'=>'Banner','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner','default'=>'Banner','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Banner<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner','default'=>'Banner','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name            : 'media-manager-selection',
            supportText     : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'banner_support','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner_support','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Banner Auswahl durch den Media Manager. Die Auswahl ist auf 1 Element beschränkt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner_support','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            helpText        : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'banner_help','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner_help','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Banner Auswahl durch den Media Manager. Die Auswahl ist auf 1 Element beschränkt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'banner_help','default'=>'Banner image selection via the Media Manager. The selection is limited to one media.','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            multiSelect     : false,
            anchor          : '100%'
        });

        // Actual form panel
        me.formPanel = Ext.create('Ext.form.Panel', {
            border      : false,
            layout      : 'anchor',
            defaults: {
                labelStyle  : 'font-weight: 700; text-align: right;'
            },
            items       : [ descField, linkField, me.linkTarget, validFrom, validUntil, dropZone ]
        });

        return me.formPanel;
    },

    /**
     * Creates a container which includes the "valid from" field
     *
     * @return [object] generated Ext.container.Container
     */
    createValidFromContainer: function() {
        var me = this;

        me.validFromField = Ext.create('Ext.form.field.Date', {
                submitFormat: 'd.m.Y',
                fieldLabel  : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'from_label','default'=>'Active from','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_label','default'=>'Active from','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv von<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_label','default'=>'Active from','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name        : 'validFromDate',
                supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'from_support','default'=>'Format: dd.mm.jjjj','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_support','default'=>'Format: dd.mm.jjjj','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Format: dd.mm.jjjj<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_support','default'=>'Format: dd.mm.jjjj','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                columnWidth : .6,
                minValue    : new Date(),
                value       : new Date(),
                allowBlank  : true,
                listeners: {
                    change: function(field, newValue) {
                        me.validToField.setMinValue(newValue);
                    }
                }
            }
        );

        return Ext.create('Ext.container.Container', {
            layout      : 'column',
            anchor      : '100%',
            defaults: {
                labelStyle  : 'font-weight: 700; text-align: right;'
            },
            items   : [
                ,me.validFromField,
            {
                margin      : '0 0 0 10',
                submitFormat: 'H:i',
                xtype       : 'timefield',
                name        : 'validFromTime',
                supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'from_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Format: hh:mm<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'from_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                columnWidth : .4,
                minDate     : new Date()
            }]
        })
    },

    /**
     * Creates a container which includes the "valid until" field
     *
     * @return [object] generated Ext.container.Container
     */
    createValidUntilContainer: function() {
        var me = this;

        me.validToField = Ext.create('Ext.form.field.Date', {
            submitFormat: 'd.m.Y',
            fieldLabel  : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'to_date_label','default'=>'Active till','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_date_label','default'=>'Active till','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv bis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_date_label','default'=>'Active till','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name        : 'validToDate',
            supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'to_date_support','default'=>'Format jjjj.mm.tt','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_date_support','default'=>'Format jjjj.mm.tt','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Format dd.mm.jjjj<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_date_support','default'=>'Format jjjj.mm.tt','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            columnWidth : .60,
            allowBlank  : true,
            listeners: {
                change: function(field, newValue) {
                    me.validFromField.setMaxValue(newValue);
                }
            }
        });

        return Ext.create('Ext.container.Container', {
            layout      : 'column',
            anchor      : '100%',
            defaults        : {
                labelStyle  : 'font-weight: 700; text-align: right;'
            },
            items       : [
                me.validToField,
            {
                margin      : '0 0 0 10',
                xtype       : 'timefield',
                name        : 'validToTime',
                submitFormat: 'H:i',
                supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'to_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Format: hh:mm<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'to_time_support','default'=>'Format: hh:mm','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                columnWidth : .40
            }]
        })
    },

    /**
     * Creates the action buttons for the component.
     *
     * @return [array] - Array of Ext.button.Button's
     */
    createActionButtons: function() {
        var me = this;

        return ['->', {
            text    : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'cancel','default'=>'Cancel','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'cancel','default'=>'Cancel','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'cancel','default'=>'Cancel','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            scope   : me,
            handler : me.destroy
        }, {
            text    : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_add'/'save','default'=>'Save','namespace'=>'backend/banner/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'save','default'=>'Save','namespace'=>'backend/banner/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_add'/'save','default'=>'Save','namespace'=>'backend/banner/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action  : 'saveBannerEdit',
            cls: 'primary'
        }];
    }
});
//
<?php }} ?>