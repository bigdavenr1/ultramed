<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/category_selection.js" */ ?>
<?php /*%%SmartyHeaderCode:3876490835543461131f235-98900314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a725db8ff45dadfed9bad587d4a135e62f0de67f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/category_selection.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3876490835543461131f235-98900314',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434611339b77_03916903',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434611339b77_03916903')) {function content_55434611339b77_03916903($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.CategorySelection', {
    extend: 'Shopware.form.field.PagingComboBox',
    alias: 'widget.emotion-components-fields-category-selection',
    name: 'category_selection',

    /**
     * Snippets for the field.
     * @object
     */
    snippets: {
        fields: {
            please_select: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fields'/'please_select','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_selection')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fields'/'please_select','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_selection'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fields'/'please_select','default'=>'Please select...','namespace'=>'backend/emotion/view/components/category_selection'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            category_select: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fields'/'category_select','default'=>'Select category','namespace'=>'backend/emotion/view/components/category_selection')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fields'/'category_select','default'=>'Select category','namespace'=>'backend/emotion/view/components/category_selection'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie-Auswahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fields'/'category_select','default'=>'Select category','namespace'=>'backend/emotion/view/components/category_selection'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            pageSize: 15,
            triggerAction: 'all',
            fieldLabel: me.snippets.fields.category_select,
            valueField: 'id',
            displayField: 'name',
            emptyText: me.snippets.fields.please_select,
            store: me.createStore()
        });

        me.callParent(arguments);
    },

    /**
     * Creates a store which will be used
     * for the combo box.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, store = Ext.create('Shopware.apps.Emotion.store.CategoryPath', { pageSize: 15 });
        store.getProxy().extraParams.parents = true;

        store.load({
            callback: function() {
                var record = store.getById(~~(1 * me.getValue()));
                me.select(record);
            }
        });

        return store;
    }
});<?php }} ?>