<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/banner.js" */ ?>
<?php /*%%SmartyHeaderCode:1758445680554346110e62b9-10514414%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be0b806de70cbd660b875a92281fe8d35e026344' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/banner.js',
      1 => 1430113365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1758445680554346110e62b9-10514414',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434611120c79_06424628',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434611120c79_06424628')) {function content_55434611120c79_06424628($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.Banner', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.emotion-components-banner',

    snippets: {
        file: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'file','default'=>'Image','namespace'=>'backend/emotion/view/components/banner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file','default'=>'Image','namespace'=>'backend/emotion/view/components/banner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bild<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file','default'=>'Image','namespace'=>'backend/emotion/view/components/banner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        link: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'link','default'=>'Link','namespace'=>'backend/emotion/view/components/banner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'link','default'=>'Link','namespace'=>'backend/emotion/view/components/banner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'link','default'=>'Link','namespace'=>'backend/emotion/view/components/banner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    basePath: '<?php $_smarty_tpl->smarty->loadPlugin("smarty_function_flink"); echo smarty_function_flink(array("file" => '', "fullPath" => false), $_smarty_tpl); ?>',

    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.addEvents('openMappingWindow');

        me.mediaSelection = me.down('mediaselectionfield');
        me.mediaSelection.on('selectMedia', me.onSelectMedia, me);

        var bannerFile = me.getBannerFile(me.getSettings('record').get('data'));
        if(bannerFile && bannerFile.value && bannerFile.value.length) {
            me.onSelectMedia('', bannerFile.value);
        }
    },

    getBannerFile: function(data) {
        var record = null;
        Ext.each(data, function(item) {
            if (item.key == 'file') {
                record = item;
                return false;
            }
        });
        return record;
    },

    onSelectMedia: function(element, media) {
        var me = this;
        if(!me.previewImage) {
            me.previewFieldset = this.createPreviewImage(media);
        } else {
            me.previewImage.setSrc(me.basePath + media[0].get('path'));
        }
        me.selectedMedia = Ext.isArray(media) ? media[0] : media;
        me.add(me.previewFieldset);
    },

    createPreviewImage: function(media) {
        var me = this;

        me.previewImage = Ext.create('Ext.Img', {
            src: me.basePath + (Ext.isArray(media) ? media[0].get('path') : media),
            style: {
                'display': 'block',
                'max-width': '100%'
            }
        });

        return Ext.create('Ext.form.FieldSet', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'preview','default'=>'Preview image','namespace'=>'backend/emotion/view/components/banner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'preview','default'=>'Preview image','namespace'=>'backend/emotion/view/components/banner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vorschaubild<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'preview','default'=>'Preview image','namespace'=>'backend/emotion/view/components/banner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [ me.createMappingButton(), me.previewImage ]
        });
    },

    createMappingButton: function() {
       var me = this;
       var button = Ext.create('Ext.button.Button', {
           text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mapping','default'=>'Create image mapping','namespace'=>'backend/emotion/view/components/banner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mapping','default'=>'Create image mapping','namespace'=>'backend/emotion/view/components/banner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bild-Mapping anlegen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mapping','default'=>'Create image mapping','namespace'=>'backend/emotion/view/components/banner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
           iconCls: 'sprite-layer-select',
           cls: 'small secondary',
           handler: function() {
                me.fireEvent('openMappingWindow', me, me.selectedMedia, me.previewImage, me.getSettings('record'));
           }
       });

       return Ext.create('Ext.container.Container', {
           margin: '0 0 10',
           items: [ button ]
       });
    }
});<?php }} ?>