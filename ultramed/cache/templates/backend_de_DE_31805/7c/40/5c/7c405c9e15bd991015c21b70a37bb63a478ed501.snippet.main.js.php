<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:09
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/cache/main.js" */ ?>
<?php /*%%SmartyHeaderCode:183642634055447c4d85dee1-16716134%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c405c9e15bd991015c21b70a37bb63a478ed501' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/cache/main.js',
      1 => 1430113600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183642634055447c4d85dee1-16716134',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4d8a8852_82064342',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4d8a8852_82064342')) {function content_55447c4d8a8852_82064342($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    CanceledOrder
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

//
Ext.define('Shopware.apps.Performance.view.tabs.cache.Main', {
    extend: 'Ext.form.Panel',
    alias: 'widget.performance-tabs-cache-main',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tabs'/'cache'/'title','default'=>'Cache','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'cache'/'title','default'=>'Cache','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cache<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'cache'/'title','default'=>'Cache','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    layout:'vbox',

    defaults:  {
        width: '100%'
    },

    /**
     * Initializes the component, sets up toolbar and pagingbar and and registers some events
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        // Create the items of the container
        me.items = me.getItems();

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'shopware-ui',
            cls: 'shopware-toolbar',
            items: me.getButtons()
        }];

        me.callParent(arguments);

    },

    /**
     * @return Array
     */
    getItems: function() {
        var me = this;
        var info = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'cache'/'info'/'text','default'=>'Erfahren Sie genaueres über das Performance Module unter : <a href=http://wiki.shopware.de/Shopcache-leeren_detail_845_641.html target=_blank>Wiki</a>  ','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'cache'/'info'/'text','default'=>'Erfahren Sie genaueres über das Performance Module unter : <a href=http://wiki.shopware.de/Shopcache-leeren_detail_845_641.html target=_blank>Wiki</a>  ','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erfahren Sie Genaueres über das Performance-Modul im <a href=\'http://wiki.shopware.de/performance-module\' target=\'_blank\'>Wiki</a>.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'cache'/'info'/'text','default'=>'Erfahren Sie genaueres über das Performance Module unter : <a href=http://wiki.shopware.de/Shopcache-leeren_detail_845_641.html target=_blank>Wiki</a>  ','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';

        return [
        {
            xtype: 'container',
            html: info,
            height: 30,
            padding: 10
        },
        {
            xtype: 'performance-tabs-cache-info',
            store: me.infoStore,
            flex: 1
        }, {
            xtype: 'performance-tabs-cache-form',
            flex: 1
        }];
    },

    /**
     * @return Array
     */
    getButtons: function() {
        var me = this;
        
        return ['->', {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form'/'buttons'/'select_all','default'=>'Select all','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'buttons'/'select_all','default'=>'Select all','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Alle auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'buttons'/'select_all','default'=>'Select all','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'select-all',
            cls: 'secondary'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form'/'buttons'/'submit','default'=>'Clear','namespace'=>'backend/performance/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'buttons'/'submit','default'=>'Clear','namespace'=>'backend/performance/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Leeren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form'/'buttons'/'submit','default'=>'Clear','namespace'=>'backend/performance/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'clear',
            cls: 'primary'
        }];
    }

});
//<?php }} ?>