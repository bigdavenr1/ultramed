<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/visitors.js" */ ?>
<?php /*%%SmartyHeaderCode:136045075555434785a784e1-98303330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9d46f744090924766cb8ab156ca230fd6a8934ca' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/visitors.js',
      1 => 1430113330,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '136045075555434785a784e1-98303330',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785acf611_68386532',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785acf611_68386532')) {function content_55434785acf611_68386532($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Month Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Visitors', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-visitors',
    legend: {
        position: 'right'
    },


    initComponent: function () {
        var me = this, impressionTip = { }, visitTip = { };

        me.axes = [
            {
                type: 'Time',
                position: 'bottom',
                fields: ['datum'],
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'titleBottom','default'=>'Month','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'titleBottom','default'=>'Month','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Monat<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'titleBottom','default'=>'Month','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                step: [ Ext.Date.DAY, 1 ],
                dateFormat: 'D, M, Y',
                label: {
                    rotate: {
                        degrees: 315
                    }
                }
            }
        ];

        me.series = [];

        if (me.shopSelection && me.shopSelection.length > 0) {
            me.tipStore = Ext.create('Ext.data.JsonStore', {
                fields: ['name', 'data']
            });

            me.impressionGrid = {
                xtype: 'grid',
                store: me.tipStore,
                height: 130,
                flex: 1,
                columns: [
                    {
                        text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        dataIndex: 'name',
                        flex: 1
                    },
                    {
                        xtype: 'numbercolumn',
                        text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'visitors'/'chart'/'tip'/'impressions','default'=>'Impressions','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'visitors'/'chart'/'tip'/'impressions','default'=>'Impressions','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Eindrücke<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'visitors'/'chart'/'tip'/'impressions','default'=>'Impressions','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        dataIndex: 'data',
                        align: 'right',
                        flex: 1
                    }
                ]
            };
            me.visitsGrid = {
                xtype: 'grid',
                store: me.tipStore,
                height: 130,
                flex: 1,
                columns: [
                    {
                        text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"visitors/chart/tip/name",'default'=>'Shop','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        dataIndex: 'name',
                        flex: 1
                    },
                    {
                        xtype: 'numbercolumn',
                        text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'visitors'/'chart'/'tip'/'visits','default'=>'Visits','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'visitors'/'chart'/'tip'/'visits','default'=>'Visits','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Besucher<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'visitors'/'chart'/'tip'/'visits','default'=>'Visits','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        dataIndex: 'data',
                        align: 'right',
                        flex: 1
                    }
                ]
            };

            impressionTip = {
                width: 580,
                height: 130,
                items: {
                xtype: 'container',
                    layout: 'fit',
                    items: [ me.impressionGrid ]
                },
                renderer: function (storeItem) {
                    this.setTitle(
                        '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gesamt Eindrücke<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 ' +
                        Ext.Date.format(storeItem.get('datum'), 'D, M, Y')
                    );
                    me.getSubShopData(storeItem, 'totalImpressions');
                }
            };

            visitTip = {
                width: 580,
                height: 130,
                items: {
                    xtype: 'container',
                    layout: 'fit',
                    items: [me.visitsGrid]
                },
                renderer: function (storeItem) {
                    this.setTitle(
                        '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gesamt Besucher<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 ' +
                        Ext.Date.format(storeItem.get('datum'), 'D, M, Y')
                    );
                    me.getSubShopData(storeItem, 'totalVisits');
                }
            };

        } else {
            visitTip = {
                width: 180,
                height: 30,
                renderer: function(storeItem) {
                    this.setTitle(
                        Ext.Date.format(storeItem.get('datum'), 'D, M, Y') + ':&nbsp;' +
                        storeItem.get('totalVisits')
                    )
                }
            };

            impressionTip = {
                width: 180,
                height: 30,
                renderer: function(storeItem) {
                    this.setTitle(
                        Ext.Date.format(storeItem.get('datum'), 'D, M, Y') + ':&nbsp;' +
                        storeItem.get('totalImpressions')
                    )
                }
            };
        }

        me.series = [
            me.createLineSeries(
                {
                    xField: 'datum',
                    yField: 'totalImpressions',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gesamt Eindrücke<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_impression','default'=>'Total impressions','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                },
                impressionTip
            ),
            me.createLineSeries(
                {
                    xField: 'datum',
                    yField: 'totalVisits',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gesamt Besucher<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'legend_visits','default'=>'Total visits','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                },
                visitTip
            )
        ];

        me.axes.push({
            type: 'Numeric',
            grid: true,
            position: 'left',
            fields: ['totalImpressions', 'totalVisits'],
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'visitors'/'count','default'=>'Count','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'count','default'=>'Count','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'visitors'/'count','default'=>'Count','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });

        me.callParent(arguments);
    },


    getSubShopData: function(storeItem, field) {
        var me = this, data = [];

        Ext.each(me.shopSelection, function (shopId) {
            var shop = me.shopStore.getById(shopId);

            data.push(
                { name: shop.get('name'), data: storeItem.get(field + shopId) }
            );
        });

        me.tipStore.loadData(data);
    }

});
//<?php }} ?>