<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/formpanel.js" */ ?>
<?php /*%%SmartyHeaderCode:70759752055447b77383605-08380959%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9da54fd40e9277151629bd5b05ce656209361e93' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/formpanel.js',
      1 => 1430113371,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '70759752055447b77383605-08380959',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b773df3c7_60475473',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b773df3c7_60475473')) {function content_55447b773df3c7_60475473($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.view.main.Formpanel', {
    extend  : 'Ext.form.Panel',
    alias: 'widget.form-main-formpanel',
    title : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_master','default'=>'Master Data','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_master','default'=>'Master Data','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stammdaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_master','default'=>'Master Data','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    autoScroll: true,
    monitorValid: true,
    bodyPadding: 10,

    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        xtype: 'textfield',
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if (!$_tmp1){?>*/
        readOnly: true,
        /*<?php }?>*/
        labelStyle: 'font-weight: 700; text-align: right;',
        layout: 'anchor',
        labelWidth: 130,
        anchor: '99%'
    },

    /**
     * Sets up the ui component
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.items = me.getItems();
        me.dockedItems = me.getButtons();

        me.callParent(arguments);

        if (me.record !== undefined) {
            me.loadRecord(me.record);
        }
    },

    /**
     * Creates buttons shown in form panel
     *
     * @return array
     */
    getButtons: function() {
        return Ext.create('Ext.toolbar.Toolbar', {
            ui: 'shopware-ui',
            dock: 'bottom',
            cls: 'shopware-toolbar',
            items: ['->', /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
            {
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_save_form','default'=>'Save','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_save_form','default'=>'Save','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_save_form','default'=>'Save','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action: 'save',
                cls: 'primary',
                formBind: true
            }
            /*<?php }?>*/]
        });
    },

    /**
     * Creates items shown in form panel
     *
     * @return array
     */
    getItems: function() {
        var me = this,
            record = me.record,
            linkToForm = '',
            variableHint = '',
            names = [];

        if (record !== undefined) {
            linkToForm =  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_linktoform','default'=>'Link to Form','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_linktoform','default'=>'Link to Form','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Link zum Formular<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_linktoform','default'=>'Link to Form','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: ' + 'shopware.php?sViewport=ticket&sFid=' + record.get('id');

            record.getFields().each(function(item) {
                /*  */
                names.push("{sVars." + item.get('name') + "}");
                /*  */
            });

            variableHint = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'support_text_variables','default'=>'Available Variables','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_variables','default'=>'Available Variables','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verfügbare Variablen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_variables','default'=>'Available Variables','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: ' + names.join(', ');
        }

        return [{
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_name','default'=>'Name','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_name','default'=>'Name','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_name','default'=>'Name','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name       : 'name',
            allowBlank: false,
            supportText : linkToForm
        }, {
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_email','default'=>'Email','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_email','default'=>'Email','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
eMail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_email','default'=>'Email','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name       : 'email',
            vtype      : 'email',
            allowBlank : false
        }, {
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_subject','default'=>'Subject','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_subject','default'=>'Subject','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Betreff<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_subject','default'=>'Subject','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name       : 'emailSubject',
            allowBlank: false
        }, {
            xtype: 'codemirrorfield',
            mode: 'smarty',
            height: 180,
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_emailtemplate','default'=>'Email template','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_emailtemplate','default'=>'Email template','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
eMail Template<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_emailtemplate','default'=>'Email template','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'emailTemplate',
            supportText: variableHint
        }, {
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if (!$_tmp3){?>*/
            readOnly: true,
            height: 350,
            /*<?php }?>*/
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_headertext','default'=>'Form-Header','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_headertext','default'=>'Form-Header','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formular-Kopf<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_headertext','default'=>'Form-Header','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name       : 'text',
            supportText : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'support_text_headertext','default'=>'Will be displayed above the form','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_headertext','default'=>'Will be displayed above the form','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wird über dem Formular dargestellt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_headertext','default'=>'Will be displayed above the form','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            xtype:'tinymce'
        }, {
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if (!$_tmp4){?>*/
            readOnly: true,
            /*<?php }?>*/
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'label_confirmationtext','default'=>'Form-Confirmation','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_confirmationtext','default'=>'Form-Confirmation','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formular-Bestätigung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'label_confirmationtext','default'=>'Form-Confirmation','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            supportText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'support_text_confirmationtext','default'=>'Will be displayed after a successful submission','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_confirmationtext','default'=>'Will be displayed after a successful submission','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wird nach erfolgreichem Absenden dargestellt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'support_text_confirmationtext','default'=>'Will be displayed after a successful submission','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name       : 'text2',
            height: 350,
            xtype:'tinymce'
        }, {
            xtype: 'hidden',
            name: 'id'
        }];
    }
});
//
<?php }} ?>