<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/statistic/window.js" */ ?>
<?php /*%%SmartyHeaderCode:162667405155447ea467ada0-27251712%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f20bfb759ac943457f15fcda57c0481be5eadf71' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/statistic/window.js',
      1 => 1430113390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '162667405155447ea467ada0-27251712',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea46c32d8_25203720',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea46c32d8_25203720')) {function content_55447ea46c32d8_25203720($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Partner statistic main window.
 *
 * Displays all Statistic Partner Information
 */
//
Ext.define('Shopware.apps.Partner.view.statistic.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'statistic_title','default'=>'Partner statistics','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'statistic_title','default'=>'Partner statistics','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner - Statistik<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'statistic_title','default'=>'Partner statistics','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.partner-statistic-window',
    border: false,
    autoShow: true,
    layout: 'fit',
    height: 620,
    /**
     * Display no footer button for the detail window
     * @boolean
     */
    footerButton:false,
    width: 925,

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.registerEvents();
        me.items = me.createPanel();
        me.callParent(arguments);
    },

    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(
                /**
                 * Event will be fired wenn the downloadStatistic Button is clicked
                 *
                 * @event downloadStatistic
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'downloadStatistic'
        );

        return true;
    },

    /**
     * creates the form panel
     */
    createPanel:function () {
        var me = this;
        return Ext.create('Ext.panel.Panel', {
            layout:{
                layout:'fit',
                type:'vbox',
                align:'stretch'
            },
            defaults:{ flex:1 },
            items:[
                {
                    xtype:'partner-statistic-chart',
                    region:'center',
                    store:me.statisticChartStore.load()
                },
                {
                    xtype:'partner-statistic-list',
                    listStore:me.statisticListStore.load()
                }
            ],
            dockedItems:[ me.createToolbar() ]

        });
    },


    /**
     * Creates the toolbar for the order tab.
     * The toolbar contains two date fields (from, to) which allows the user to filter the chart store.
     *
     * @return [Ext.toolbar.Toolbar] - Toolbar for the order tab which contains the from and to date field to filter the chart
     */
    createToolbar:function () {
        var me = this,
            today = new Date();

        me.fromDateField = Ext.create('Ext.form.field.Date', {
            labelWidth:45,
            name:'fromDate',
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'window'/'date'/'from','default'=>'From','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'window'/'date'/'from','default'=>'From','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Von<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'window'/'date'/'from','default'=>'From','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            value:new Date(today.getFullYear() - 1, today.getMonth() , today.getDate())
        });

        me.toDateField = Ext.create('Ext.form.field.Date', {
            labelWidth:30,
            name:'toDate',
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'window'/'date'/'to','default'=>'To','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'window'/'date'/'to','default'=>'To','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'window'/'date'/'to','default'=>'To','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            value:today
        });



        return Ext.create('Ext.toolbar.Toolbar', {
            ui:'shopware-ui',
            padding: '10 0 5',
            items:[
                { xtype:'tbspacer', width:10 },
                me.fromDateField,
                { xtype:'tbspacer', width:10 },
                me.toDateField,
                { xtype:'tbspacer', width:10 },
                {
                    iconCls:'sprite-drive-download',
                    text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'button'/'download_csv','default'=>'Download statistics','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'button'/'download_csv','default'=>'Download statistics','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Statistik Download<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'button'/'download_csv','default'=>'Download statistics','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    action:'downloadStatistic'
                }
            ]
        });
    }
});
//
<?php }} ?>