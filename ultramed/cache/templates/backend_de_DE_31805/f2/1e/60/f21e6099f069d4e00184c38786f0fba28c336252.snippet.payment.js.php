<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/payment.js" */ ?>
<?php /*%%SmartyHeaderCode:100091667255447cd78b0127-42139241%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f21e6099f069d4e00184c38786f0fba28c336252' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/payment.js',
      1 => 1430113170,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '100091667255447cd78b0127-42139241',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd78d8be2_11681384',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd78d8be2_11681384')) {function content_55447cd78d8be2_11681384($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Payment model
 *
 * This model contains a single payment and its ruleSets.
 */
//
Ext.define('Shopware.apps.RiskManagement.model.Payment', {
    /**
    * Extends the standard ExtJS 4
    * @string
    */
    extend: 'Shopware.apps.Base.model.Payment',
    /**
    * The fields used for this model
    * @array
    */
	fields : [
		//
		 { name : 'template',       type: 'string' },
		 { name : 'class',       type: 'string' },
		 { name : 'table',       type: 'string' },
		 { name : 'hide',       type: 'int' },
		 { name : 'additionalDescription',       type: 'string' },
		 { name : 'debitPercent',       type: 'string' },
		 { name : 'surcharge',       type: 'string' },
		 { name : 'surchargeString',       type: 'string' },
		 { name : 'esdActive',       type: 'boolean' },
		 { name : 'embedIFrame',       type: 'string' },
		 { name : 'hideProspect',       type: 'boolean' },
		 { name : 'action',       type: 'string' },
		 { name : 'pluginId',       type: 'int' },
		 { name : 'iconCls',  type: 'string' },
		 { name : 'surcharge', type: 'double' },
		 { name: 'source', type: 'int' }
	 ],

	associations: [
		{ type:'hasMany', model:'Shopware.apps.RiskManagement.model.Rule',  name:'getRuleSets', associationKey:'ruleSets' }
	]
});
//<?php }} ?>