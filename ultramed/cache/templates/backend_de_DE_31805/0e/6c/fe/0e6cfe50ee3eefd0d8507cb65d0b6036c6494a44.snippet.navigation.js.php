<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:201662515455434785e88635-44411895%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e6cfe50ee3eefd0d8507cb65d0b6036c6494a44' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation.js',
      1 => 1430112842,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201662515455434785e88635-44411895',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785f37571_28017130',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785f37571_28017130')) {function content_55434785f37571_28017130($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Navigation Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.store.Navigation', {
    extend: 'Ext.data.TreeStore',
    model: 'Shopware.apps.Analytics.model.Navigation',
    root: {
        expanded: true,
        children: [
            {
                id: 'overview',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'quick_overview','default'=>'Ouick-Overview','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'quick_overview','default'=>'Ouick-Overview','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Schnell-Übersicht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'quick_overview','default'=>'Ouick-Overview','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-overview',
                iconCls: 'sprite-chart',
                comparable: true,
                leaf: true
            },
            {
                id: 'rating',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'rating_overview','default'=>'Conversion-Overview','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'rating_overview','default'=>'Conversion-Overview','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Conversion-Übersicht<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'rating_overview','default'=>'Conversion-Overview','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-rating',
                iconCls: 'sprite-star',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'referrer_revenue',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'turnover_referrer','default'=>'Turnover by referrer','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_referrer','default'=>'Turnover by referrer','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Referrer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_referrer','default'=>'Turnover by referrer','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-referrer_revenue',
                iconCls: 'sprite-application-home',
                comparable: true,
                leaf: true
            },
            {
                id: 'partner_revenue',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'turnover_partner','default'=>'Turnover by partner','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_partner','default'=>'Turnover by partner','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Partnern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_partner','default'=>'Turnover by partner','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-partner_revenue',
                iconCls: 'sprite-xfn-colleague',
                comparable: true,
                leaf: true
            },
            {
                id: 'customer-group',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'turnover_customergroup','default'=>'Turnover by customer group','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_customergroup','default'=>'Turnover by customer group','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Kundengruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'turnover_customergroup','default'=>'Turnover by customer group','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-customer-groups',
                iconCls: 'sprite-users',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'referrer_visitors',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'visitor_source','default'=>'Visitor access source','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'visitor_source','default'=>'Visitor access source','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Besucher Zugriffsquellen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'visitor_source','default'=>'Visitor access source','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-referrer_visitors',
                iconCls: 'sprite-application-search-result',
                comparable: true,
                leaf: true
            },
            {
                id: 'article_sales',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'items_sales','default'=>'Item by sales','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'items_sales','default'=>'Item by sales','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel nach Verkäufen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'items_sales','default'=>'Item by sales','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-article_sales',
                iconCls: 'sprite-inbox',
                comparable: true,
                leaf: true
            },
            {
                id: 'customers',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'customers','default'=>'Portion New-/RegularCustomer','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'customers','default'=>'Portion New-/RegularCustomer','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anteil Neu-/Stammkunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'customers','default'=>'Portion New-/RegularCustomer','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-customers',
                iconCls: 'sprite-user-silhouette-question',
                leaf: true
            },
            {
                id: 'customer_age',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'customer_age','default'=>'Customer age','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'customer_age','default'=>'Customer age','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundenalter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'customer_age','default'=>'Customer age','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-customer_age',
                iconCls: 'sprite-user',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'month',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'month','default'=>'Turnover by month','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'month','default'=>'Turnover by month','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Monaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'month','default'=>'Turnover by month','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-month',
                iconCls: 'sprite-calendar-month',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'week',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'calendarWeeks','default'=>'Turnover by calendar weeks','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'calendarWeeks','default'=>'Turnover by calendar weeks','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Kalender-Wochen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'calendarWeeks','default'=>'Turnover by calendar weeks','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-calendar_weeks',
                iconCls: 'sprite-calendar-select-week',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'weekday',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'weekdays','default'=>'Turnover by weekdays','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'weekdays','default'=>'Turnover by weekdays','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Wochentagen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'weekdays','default'=>'Turnover by weekdays','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-weekdays',
                iconCls: 'sprite-calendar-select-days',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'daytime',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'time','default'=>'Turnover by time of day','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'time','default'=>'Turnover by time of day','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Uhrzeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'time','default'=>'Turnover by time of day','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-time',
                iconCls: 'sprite-clock',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'category',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'categories','default'=>'Turnover by categories','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'categories','default'=>'Turnover by categories','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Kategorien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'categories','default'=>'Turnover by categories','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-categories',
                iconCls: 'sprite-category',
                leaf: true
            },
            {
                id: 'country',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'countries','default'=>'Turnover by countries','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'countries','default'=>'Turnover by countries','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Ländern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'countries','default'=>'Turnover by countries','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-countries',
                iconCls: 'sprite-locale',
                leaf: true,
                multiShop: true,
                comparable: true
            },
            {
                id: 'payment',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'payment','default'=>'Turnover by payment','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'payment','default'=>'Turnover by payment','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'payment','default'=>'Turnover by payment','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-payment',
                iconCls: 'sprite-moneys',
                leaf: true,
                multiShop: true
            },
            {
                id: 'dispatch',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'shippingMethods','default'=>'Turnover by shipping methods','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'shippingMethods','default'=>'Turnover by shipping methods','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Versandarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'shippingMethods','default'=>'Turnover by shipping methods','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-shipping_methods',
                iconCls: 'sprite-truck-box-label',
                leaf: true,
                multiShop: true
            },
            {
                id: 'supplier',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'salesBy'/'vendors','default'=>'Turnover by vendors','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'vendors','default'=>'Turnover by vendors','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz nach Herstellern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'salesBy'/'vendors','default'=>'Turnover by vendors','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-vendors',
                iconCls: 'sprite-toolbox',
                leaf: true
            },
            {
                id: 'search',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'search','default'=>'Popular search terms','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'search','default'=>'Popular search terms','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beliebte Suchbegriffe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'search','default'=>'Popular search terms','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-search',
                iconCls: 'sprite-magnifier',
                leaf: true
            },
            {
                id: 'visitors',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'visitors','default'=>'Visitors','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'visitors','default'=>'Visitors','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Besucher<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'visitors','default'=>'Visitors','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-visitors',
                iconCls: 'sprite-chart-up-color',
                comparable: true,
                leaf: true,
                multiShop: true
            },
            {
                id: 'article_impression',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'nav'/'article_impressions','default'=>'Item by calls(Impressions)','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'article_impressions','default'=>'Item by calls(Impressions)','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel nach Aufrufen(Impressionen)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'nav'/'article_impressions','default'=>'Item by calls(Impressions)','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store: 'analytics-store-navigation-article_impressions',
                iconCls: 'sprite-chart-up-color',
                comparable: true,
                leaf: true,
                multiShop: true
            },

            //

        ]
    },

    constructor: function (config) {
        var me = this;

        config.root = Ext.clone(me.root);

        me.callParent(arguments);
    }
});
//
<?php }} ?>