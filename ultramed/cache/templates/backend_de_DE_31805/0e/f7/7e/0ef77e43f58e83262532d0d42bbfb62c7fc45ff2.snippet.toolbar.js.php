<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/toolbar.js" */ ?>
<?php /*%%SmartyHeaderCode:178074715555434611715322-07500536%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ef77e43f58e83262532d0d42bbfb62c7fc45ff2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/toolbar.js',
      1 => 1430113370,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '178074715555434611715322-07500536',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434611736e12_11902230',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434611736e12_11902230')) {function content_55434611736e12_11902230($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Toolbar
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.templates.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.emotion-templates-toolbar',
    ui: 'shopware-ui',

    /**
     * Snippets which are used by this component.
     * @Object
     */
    snippets: {
        addBtn: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'btn'/'add','default'=>'Add new template','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'btn'/'add','default'=>'Add new template','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neues Template anlegen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'btn'/'add','default'=>'Add new template','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        delBtn: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'btn'/'del','default'=>'Delete selected template(s)','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'btn'/'del','default'=>'Delete selected template(s)','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte(s) Template löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'btn'/'del','default'=>'Delete selected template(s)','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        searchField: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'search_grids','default'=>'Search template...','namespace'=>'backend/emotion/templates/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'search_grids','default'=>'Search template...','namespace'=>'backend/emotion/templates/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche Template...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'search_grids','default'=>'Search template...','namespace'=>'backend/emotion/templates/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @returns { Void }
     */
    initComponent: function() {
        var me = this;

        me.addEvents('searchGrids');
        me.items = me.createButtons();
        me.callParent(arguments);
    },

    /**
     * Creates the action buttons for the component.
     *
     * @returns { Array }
     */
    createButtons: function() {
        var me = this;

        me.addBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.addBtn,
            action: 'emotion-templates-new-template',
            iconCls: 'sprite-plus-circle'
        });

        me.deleteBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.delBtn,
            action: 'emotion-templates-delete-marked-templates',
            iconCls: 'sprite-minus-circle',
            disabled: true
        });

        me.searchField = Ext.create('Ext.form.field.Text', {
            emptyText: me.snippets.searchField,
            cls: 'searchfield',
            width: 200,
            enableKeyEvents:true,
            checkChangeBuffer:500,
            listeners: {
                change: function(field, value) {
                    me.fireEvent('searchGrids', value);
                }
            }
        });

        return [ me.addBtn, me.deleteBtn, { xtype: 'tbfill' }, me.searchField, { xtype: 'tbspacer', width: 6 } ];
    }
});
//<?php }} ?>