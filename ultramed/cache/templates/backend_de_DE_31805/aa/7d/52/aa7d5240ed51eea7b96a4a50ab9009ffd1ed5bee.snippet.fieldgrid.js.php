<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/fieldgrid.js" */ ?>
<?php /*%%SmartyHeaderCode:126077415255447b772ea3a0-17142911%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa7d5240ed51eea7b96a4a50ab9009ffd1ed5bee' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/view/main/fieldgrid.js',
      1 => 1430113371,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '126077415255447b772ea3a0-17142911',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b7737b877_49608495',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b7737b877_49608495')) {function content_55447b7737b877_49608495($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.view.main.Fieldgrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.form-main-fieldgrid',
    title : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_fields','default'=>'Fields','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_fields','default'=>'Fields','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Felder<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_fields','default'=>'Fields','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    autoScroll: true,
    selType: 'rowmodel',
    sortableColumns: false,

    /**
     * Contains snippets for this view
     * @object
     */
    messages: {
         tooltipValue: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tooltip_value','default'=>'For selections, checkboxes or radios use a semicolon to separate the values','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_value','default'=>'For selections, checkboxes or radios use a semicolon to separate the values','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Für Selects, Checkboxes oder Radiobuttons bitte das Semikolon (;) zum trennen der Werte verwenden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_value','default'=>'For selections, checkboxes or radios use a semicolon to separate the values','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         tooltipName: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tooltip_name','default'=>'To enter two inputs, use a semicolon to separate the names','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_name','default'=>'To enter two inputs, use a semicolon to separate the names','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Um zwei Werte zu verwenden, die Werte bitte mit Semikolon(;) trennen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_name','default'=>'To enter two inputs, use a semicolon to separate the names','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         tooltipSmartyEnabled: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tooltip_smarty','default'=>'Smarty code is allowed','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_smarty','default'=>'Smarty code is allowed','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Smarty Code ist erlaubt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_smarty','default'=>'Smarty code is allowed','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         tooltipDragDrop: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tooltip_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sie können die Felder per Drag & Drop umsortieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         hintDragDrop: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'hint_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'hint_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sie können die Felder per Drag & Drop umsortieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'hint_dragdrop','default'=>'You can move rows via Drag & Drop','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         saveBtnText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'rowedit_save','default'=>'Save','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'rowedit_save','default'=>'Save','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'rowedit_save','default'=>'Save','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
         cancelBtnText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'rowedit_cancel','default'=>'Cancel','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'rowedit_cancel','default'=>'Cancel','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'rowedit_cancel','default'=>'Cancel','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Sets up the ui component
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.store       = me.fieldStore;

        me.columns     = me.getColumns();
        me.viewConfig  = me.getViewConfig();
        me.editor      = me.getRowEditingPlugin();
        me.plugins     = [ me.editor, me.getHeaderToolTipPlugin() ];
        me.dockedItems = [ me.getToolbar(),  me.getPagingbar() ];

        me.callParent(arguments);
    },

    /**
     * Creates headertooltip plugin
     *
     * @return [Shopware.grid.HeaderToolTip]
     */
    getHeaderToolTipPlugin: function() {
        var headerToolTipPlugin = Ext.create('Shopware.grid.HeaderToolTip', {
            showIcons: true
        });

        return headerToolTipPlugin;
    },

    /**
     * Creates row editing plugin
     *
     * @return [Ext.grid.plugin.RowEditing]
     */
    getRowEditingPlugin: function() {
        var me = this, rowEditingPlugin = Ext.create('Ext.grid.plugin.RowEditing', {
            saveBtnText : me.messages.saveBtnText,
            cancelBtnText : me.messages.cancelBtnText,
            errorSummary: false
        });

        return rowEditingPlugin;
    },

    /**
     * Creates gridviewdragdrop plugin
     *
     * @return [object]
     */
    getViewConfig: function() {
        var viewConfig = {
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
            plugins: {
                pluginId: 'my-gridviewdragdrop',
                ptype: 'gridviewdragdrop'
            }
            /*<?php }?>*/
        };

        return viewConfig;
    },

    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns: function() {
        var me = this;

        var columns = [
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        {
            header: '&#009868;',
            width: 24,
            hideable: false,
            renderer : me.renderSorthandleColumn,
        },
        /*<?php }?>*/
        {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_name','default'=>'Name','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_name','default'=>'Name','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_name','default'=>'Name','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'name',
            tooltip: me.messages.tooltipName,
            flex: 1,
            hideable: false,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_label','default'=>'Label','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_label','default'=>'Label','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bezeichnung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_label','default'=>'Label','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'label',
            flex: 1,
            hideable: false,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_typ','default'=>'Typ','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_typ','default'=>'Typ','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_typ','default'=>'Typ','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'typ',
            flex: 1,
            hideable: false,
            editor: {
                xtype: 'combo',
                allowBlank: false,
                editable: false,
                mode: 'local',
                triggerAction: 'all',
                displayField: 'label',
                valueField: 'id',
                store: me.getTypComboStore()
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_class','default'=>'Class','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_class','default'=>'Class','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aussehen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_class','default'=>'Class','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'class',
            flex: 1,
            hideable: false,
            editor: {
                xtype: 'combo',
                allowBlank: false,
                editable: false,
                mode: 'local',
                triggerAction: 'all',
                displayField: 'label',
                valueField: 'id',
                store: me.getClassComboStore()
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_value','default'=>'Value','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_value','default'=>'Value','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Optionen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_value','default'=>'Value','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'value',
            tooltip: me.messages.tooltipValue,
            flex: 1,
            hideable: false,
            editor: {
                xtype:'textfield'
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_note','default'=>'Note','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_note','default'=>'Note','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kommentar<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_note','default'=>'Note','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'note',
            tooltip: me.messages.tooltipSmartyEnabled,
            flex: 1,
            hideable: false,
            editor: {
                xtype:'textfield'
            }
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_errormsg','default'=>'Error Message','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_errormsg','default'=>'Error Message','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Fehlermeldung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_errormsg','default'=>'Error Message','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            tooltip: me.messages.tooltipSmartyEnabled,
            dataIndex: 'errorMsg',
            flex: 1,
            hideable: false,
            editor: {
                xtype:'textfield'
            }
        }, {
            xtype: 'booleancolumn',
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_required','default'=>'Required','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_required','default'=>'Required','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Eingabe erforderlich<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_required','default'=>'Required','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'required',
            flex: 1,
            hideable: false,
            editor: {
                xtype: 'checkbox',
                inputValue: true,
                uncheckedValue: false
            }
        }

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?>*/
        ,{
            /**
             * Special column type which provides
             * clickable icons in each row
             */
            xtype: 'actioncolumn',
            width: 24,
            hideable: false,
            items: [{
                iconCls: 'sprite-minus-circle-frame',
                action: 'delete',
                cls: 'delete',
                tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tooltip_delete_field','default'=>'Delete this field','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_delete_field','default'=>'Delete this field','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Dieses Feld löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tooltip_delete_field','default'=>'Delete this field','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }]
        }
        /*<?php }?>*/
        ];

        return columns;
    },



    /**
     * Creates store object used for the typ column
     *
     * @return [Ext.data.SimpleStore]
     */
    getTypComboStore: function() {
        return new Ext.data.SimpleStore({
            fields:['id', 'label'],
            data: [
                ['text', 'Text'],
                ['text2', 'Text2'],
                ['checkbox', 'Checkbox'],
                ['email', 'Email'],
                ['select', 'select'],
                ['textarea', 'textarea']
            ]
        });
    },

    /**
     * Creates store object used for the class column
     *
     * @return [Ext.data.SimpleStore]
     */
    getClassComboStore: function() {
        return new Ext.data.SimpleStore({
            fields:['id', 'label'],
            data: [
                ['normal', 'normal'],
                ['strasse;nr', 'strasse;nr'],
                ['plz;ort', 'plz;ort']
            ]
        });
    },

    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar: function() {
        var me = this;

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            ui: 'shopware-ui',
            cls: 'shopware-toolbar',
            items: [

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?>*/
            {
                iconCls: 'sprite-plus-circle-frame',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar_add_field','default'=>'Add Field','namespace'=>'backend/form/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar_add_field','default'=>'Add Field','namespace'=>'backend/form/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Feld hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar_add_field','default'=>'Add Field','namespace'=>'backend/form/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action: 'add'
            },
            /*<?php }?>*/

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php if ($_tmp5){?>*/
            {
                xtype: 'tbfill'
            }, {
                xtype: 'container',
                html: '<p style="padding: 5px">' + me.messages.hintDragDrop + '</p>'
            }
            /*<?php }?>*/
            ]
        });

        return toolbar;
    },

    /**
     * Creates pagingbar
     *
     * @return Ext.toolbar.Paging
     */
    getPagingbar: function () {
        var pagingbar =  Ext.create('Ext.toolbar.Paging', {
            store: this.store,
            dock:'bottom',
            displayInfo: true
        });

        return pagingbar;
    },

    /**
     * Renderer for sorthandle-column
     *
     * @param [string] value
     */
    renderSorthandleColumn: function (value,  metadata) {
        var me = this;

        metadata.tdAttr = 'data-qtip="' + me.messages.hintDragDrop +'"';

        return '<div style="cursor: n-resize;">&#009868;</div>';
    }
});
//
<?php }} ?>