<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:48
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/app.js" */ ?>
<?php /*%%SmartyHeaderCode:134299686755447cb00235c5-30855311%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9c639bc97c5708112035b2a7c282fd1102473370' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/app.js',
      1 => 1430112767,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134299686755447cb00235c5-30855311',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb0089a78_66113501',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb0089a78_66113501')) {function content_55447cb0089a78_66113501($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Systeminfo', {
	/**
	* Extends from our special controller, which handles the
	* sub-application behavior and the event bus
	* @string
	*/
    extend : 'Enlight.app.SubApplication',
	/**
	* The name of the module. Used for internal purpose
	* @string
	*/
	name: 'Shopware.apps.Systeminfo',
	/**
	* Sets the loading path for the sub-application.
	*
	* Note that you'll need a "loadAction" in your
	* controller (server-side)
	* @string
	*/
    loadPath : '<?php echo '/backend/systeminfo/load';?>',
    /**
     * Enable bulk loading
     * @boolean
     */
    bulkLoad: true,

    /**
    * Required views for controller
    * @array
    */
    views: [ 'main.Window','systeminfo.Configlist', 'systeminfo.Pathlist', 'systeminfo.Filelist', 'systeminfo.Versionlist','systeminfo.Phpinfo', 'systeminfo.Encoder' ],
    /**
    * Required stores for controller
    * @array
    */
    stores: [ 'Configs', 'Paths', 'Files', 'Versions', 'Encoder' ],
    /**
    * Required models for controller
    * @array
    */
    models: [ 'Config', 'Path', 'File', 'Version', 'Encoder' ],

	/**
	* Requires controllers for sub-application
	* @array
	*/
    controllers : [ 'Systeminfo', 'Main' ],
        /**
     * Returns the main application window for this is expected
     * by the Enlight.app.SubApplication class.
     * The class sets a new event listener on the "destroy" event of
     * the main application window to perform the destroying of the
     * whole sub application when the user closes the main application window.
     *
     * This method will be called when all dependencies are solved and
     * all member controllers, models, views and stores are initialized.
     *
     * @private
     * @return [object] mainWindow - the main application window based on Enlight.app.Window
     */
    launch:function () {
        var me = this,
            mainController = me.getController('Main');

        return mainController.mainWindow;
    }
});
//<?php }} ?>