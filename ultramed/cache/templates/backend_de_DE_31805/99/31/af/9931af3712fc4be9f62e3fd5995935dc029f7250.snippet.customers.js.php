<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/customers.js" */ ?>
<?php /*%%SmartyHeaderCode:83270581355434785a27228-18624902%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9931af3712fc4be9f62e3fd5995935dc029f7250' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/customers.js',
      1 => 1430113329,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83270581355434785a27228-18624902',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785a4e202_07478568',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785a4e202_07478568')) {function content_55434785a4e202_07478568($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Customers Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Customers', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-customers',
    legend: {
        position: 'right'
    },

    axes: [
        {
            type: 'Numeric',
            minimum: 0,
            grid: true,
            position: 'left',
            fields: ['newCustomersPercent', 'oldCustomersPercent'],
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'customers'/'percent'/'title','default'=>'Percent','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'customers'/'percent'/'title','default'=>'Percent','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Prozentanteil<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'customers'/'percent'/'title','default'=>'Percent','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        {
            type: 'Category',
            position: 'bottom',
            fields: ['week'],
            label: {
                rotate: {
                    degrees: 315
                }
            },
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'customers'/'days'/'title','default'=>'Days','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'customers'/'days'/'title','default'=>'Days','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'customers'/'days'/'title','default'=>'Days','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    ],

    initComponent: function () {
        var me = this;


        me.series = [
            me.createLineSeries(
                { xField: 'week', yField: 'newCustomersPercent', title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"chart/customers/new_customers_legend",'default'=>'New customers','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"chart/customers/new_customers_legend",'default'=>'New customers','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neukunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"chart/customers/new_customers_legend",'default'=>'New customers','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' },
                {
                    width: 210,
                    height: 45,
                    renderer: function(storeItem) {
                        var data = Ext.util.Format.number(storeItem.get('newCustomersPercent'), '0.00') + ' %';
                        this.setTitle(storeItem.get('week') + ': ' + data);
                    }
                }
            ),
            me.createLineSeries(
                { xField: 'week', yField: 'oldCustomersPercent', title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"chart/customers/old_customers_legend",'default'=>'Old customers','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"chart/customers/old_customers_legend",'default'=>'Old customers','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stammkunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"chart/customers/old_customers_legend",'default'=>'Old customers','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' },
                {
                    width: 210,
                    height: 45,
                    renderer: function(storeItem) {
                        var data = Ext.util.Format.number(storeItem.get('oldCustomersPercent'), '0.00') + ' %';
                        this.setTitle(storeItem.get('week') + ': ' + data);
                    }
                }
            )
        ];

        me.callParent(arguments);
    }
});
//
<?php }} ?>