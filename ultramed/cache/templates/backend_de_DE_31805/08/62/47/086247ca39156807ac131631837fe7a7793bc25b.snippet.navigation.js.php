<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/model/navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:376979151554347856a7f43-14436936%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '086247ca39156807ac131631837fe7a7793bc25b' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/model/navigation.js',
      1 => 1430112842,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '376979151554347856a7f43-14436936',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347856ce880_65191176',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347856ce880_65191176')) {function content_554347856ce880_65191176($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Navigation Model
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 */
//
Ext.define('Shopware.apps.Analytics.model.Navigation', {
    extend: 'Ext.data.Model',
    fields: [
        //
        { name: 'id', type: 'string' },
        { name: 'text', type: 'string' },
        { name: 'leaf', type: 'boolean' },
        { name: 'loaded', type: 'boolean', defaultValue: false },
        { name: 'action' },
        { name: 'expanded', defaultValue: true },
        { name: 'children' },
        { name: 'store' },
        { name: 'comparable' },
        { name: 'leaf', type: 'boolean' }
    ]
});
//<?php }} ?>