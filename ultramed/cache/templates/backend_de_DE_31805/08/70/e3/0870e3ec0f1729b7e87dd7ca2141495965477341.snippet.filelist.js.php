<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/filelist.js" */ ?>
<?php /*%%SmartyHeaderCode:95255084455447cb1243025-85560573%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0870e3ec0f1729b7e87dd7ca2141495965477341' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/filelist.js',
      1 => 1430113401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95255084455447cb1243025-85560573',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb1263617_24896557',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb1263617_24896557')) {function content_55447cb1263617_24896557($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Grid for the shopware-files
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Systeminfo.view.systeminfo.Filelist', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.grid.Panel',

    ui: 'shopware-ui',

    /**
     * ID to access the component out of other components
     */
    id: 'systeminfo-main-filelist',

    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('systeminfo-main-filelist')
    * @string
    */
    alias: 'widget.systeminfo-main-filelist',
    /**
    * The window uses a border layout, so we need to set
    * a region for the grid panel
    * @string
    */
    region: 'center',
    /**
    * The view needs to be scrollable
    * @string
    */
    autoScroll: true,

	border: 0,
    /**
    * Set the used store. You just need to set the store name
    * due to the fact that the store is defined in the same
    * namespace
    * @string
    */
    store: 'Files',

    /**
     * This function is called when the component is initiated
     * It creates the columns
     */
    initComponent: function(){
        this.columns = this.getColumns();
        this.callParent(arguments);
    },

    /**
     * Creates the columns
     * @return array columns Contains the columns
     */
    getColumns: function(){
        var me = this;

        var columns = [
            {
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'file_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'name',
                flex: 1
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'file_grid'/'column'/'required','default'=>'Available','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'required','default'=>'Available','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verfügbar<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'required','default'=>'Available','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'available',
                width: '65px',
                renderer: me.renderStatus
           },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'file_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'file_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'result',
                width: '65px',
                renderer: me.renderStatus
            }
        ];
        return columns;
    },

    /**
     * Function to render the status. 1 = a green tick, everything else = a red cross
     * @param value The value of the field
     */
    renderStatus: function(value){
        if(value==1){
            return Ext.String.format('<div style="height: 16px; width: 16px" class="sprite-tick"></div>')
        }else{
            return Ext.String.format('<div style="height: 16px; width: 16px" class="sprite-cross"></div>')
        }
    }
});
//
<?php }} ?>