<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:09
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/model/customer.js" */ ?>
<?php /*%%SmartyHeaderCode:152518363255447c4d6bc995-43759678%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec59c259bf3a363996f9f447e4fd806007f44639' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/model/customer.js',
      1 => 1430113163,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152518363255447c4d6bc995-43759678',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4d6dc8d0_00006157',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4d6dc8d0_00006157')) {function content_55447c4d6dc8d0_00006157($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Performance
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Base config model which holds references to the config items
 */
//
Ext.define('Shopware.apps.Performance.model.Customer', {

    /**
     * Extends the standard Ext Model
     * @string
     */
    extend:'Ext.data.Model',

    /**
     * Contains the model fields
     * @array
     */
    fields:[
		//
        { name:'id', type:'int' },

        { name:'similarRefreshStrategy', type:'int' },
        { name:'alsoBoughtShow', type:'bool' },
        { name:'similarViewedShow', type:'bool' },
        { name:'similarValidationTime', type:'int' },
        { name:'similarActive', type:'bool' }
    ]

});
//
<?php }} ?>