<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/sidebar/attributes.js" */ ?>
<?php /*%%SmartyHeaderCode:710821560554476fc8791f9-23724307%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd6cde7b3df728b17294ac1b4aa0035dfe1db1f56' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/sidebar/attributes.js',
      1 => 1430113618,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '710821560554476fc8791f9-23724307',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc8c1395_20118548',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc8c1395_20118548')) {function content_554476fc8c1395_20118548($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
/**
 * Shopware UI - Blog detail sidebar options window.
 *
 * Displays all Detail Blog Information
 */
//
Ext.define('Shopware.apps.Blog.view.blog.detail.sidebar.Attributes', {
    extend:'Ext.panel.Panel',
    alias:'widget.blog-blog-detail-sidebar-attributes',
    border: 0,
    bodyPadding: 10,
    collapsed: false,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'title','default'=>'Attributes','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'title','default'=>'Attributes','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfelder<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'title','default'=>'Attributes','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    layout: {
        type: 'border'
    },

    /**
     * Initialize the Shopware.apps.Blog.view.blog.detail.sidebar.attributes and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.attributesPanel = Ext.create('Ext.panel.Panel', {
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'panel'/'attributes','default'=>'Blog attributes','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'panel'/'attributes','default'=>'Blog attributes','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blog Freitextfelder<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'panel'/'attributes','default'=>'Blog attributes','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            margin: '0 0 10 0',
            layout: {
                type: 'anchor'
            },
            bodyPadding: 10,
            region:'center',
            closable: false,
            collapsible: false,
            autoScroll: true,
            defaults:{
                labelWidth:100,
                anchor: '100%',
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items: me.createOptionsForm()
        });

        me.items = [ me.attributesPanel ];

        me.callParent(arguments);
    },

    /**
     * creates the general form and layout
     *
     * @return [Array] computed form
     */
    createOptionsForm:function () {
        var me = this;
        return [
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute1','default'=>'Attribute 1','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute1','default'=>'Attribute 1','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 1<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute1','default'=>'Attribute 1','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute1]'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute2','default'=>'Attribute 2','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute2','default'=>'Attribute 2','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute2','default'=>'Attribute 2','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute2]'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute3','default'=>'Attribute 3','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute3','default'=>'Attribute 3','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 3<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute3','default'=>'Attribute 3','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute3]'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute4','default'=>'Attribute 4','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute4','default'=>'Attribute 4','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 4<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute4','default'=>'Attribute 4','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute4]'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute5','default'=>'Attribute 5','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute5','default'=>'Attribute 5','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 5<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute5','default'=>'Attribute 5','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute5]'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute6','default'=>'Attribute 6','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute6','default'=>'Attribute 6','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freitextfeld 6<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'attributes'/'field'/'attribute6','default'=>'Attribute 6','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'attribute[attribute6]'
            }
        ]
    }
});
//
<?php }} ?>