<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/model/form.js" */ ?>
<?php /*%%SmartyHeaderCode:71238453855447b771a1395-23412718%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7e9b934b4464e7db03cd4c44d5af3955b6873081' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/model/form.js',
      1 => 1430112891,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71238453855447b771a1395-23412718',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b771f7c78_52953076',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b771f7c78_52953076')) {function content_55447b771f7c78_52953076($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.model.Form', {

    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * Function to copy the form
     *
     * @param [callback]
     */
    copy: function(callback) {
        Ext.Ajax.request({
            url: '<?php echo '/backend/form/copyForm';?>',
            method: 'POST',
            params : { id : this.data.id },
            success: function(response, opts) {
                if(typeof(callback) !== 'function') {
                    return false;
                }

                callback.call(this, arguments);
            }
        });
    },

    /**
     * The fields used for this model
     *
     * @array
     */
    fields : [
		//
        { name : 'id',    type : 'int' },
        { name : 'name',  type : 'string' },
        { name : 'email', type : 'email' },
        { name : 'emailSubject', type : 'string' },
        { name : 'emailTemplate', type : 'string' },
        { name : 'text', type : 'string' },
        { name : 'text2', type : 'string' }
    ],

    validations: [
        { field: 'name',  type: 'presence'},
        { field: 'email',  type: 'presence'},
        { field: 'emailSubject',  type: 'presence'},
        { field: 'emailTemplate', type: 'presence'}
    ],

    associations: [
        { type: 'hasMany', model: 'Shopware.apps.Form.model.Field', name: 'getFields', associationKey: 'fields'},
        { type: 'hasMany', model: 'Shopware.apps.Form.model.Attribute', name: 'getAttributes', associationKey: 'attribute'}
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy: {
        type: 'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '<?php echo '/backend/form/getForms';?>',
            create: '<?php echo '/backend/form/createForm';?>',
            update: '<?php echo '/backend/form/updateForm';?>',
            destroy: '<?php echo '/backend/form/removeForm';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>