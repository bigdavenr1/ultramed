<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/controller/tree.js" */ ?>
<?php /*%%SmartyHeaderCode:16005052055447a969c0535-50106903%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'daf82e6b40dd6c410a3aceb32b7f76cd4abc7488' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/controller/tree.js',
      1 => 1430113175,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16005052055447a969c0535-50106903',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a96a61456_98823064',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a96a61456_98823064')) {function content_55447a96a61456_98823064($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site main Controller
 *
 * This file handles the navigation tree containing the actual sites.
 */

//

//
Ext.define('Shopware.apps.Site.controller.Tree', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
    extend: 'Ext.app.Controller',

    /**
     * Define references for the different parts of the application. The
     * references are parsed by ExtJS and Getter methods are automatically created.
     *
     * Example: { ref : 'grid', selector : 'grid' } transforms to this.getGrid();
     *          { ref : 'addBtn', selector : 'button[action=add]' } transforms to this.getAddBtn()
     *
     * @object
     */
    refs:[
        { ref:'mainWindow', selector:'site-mainWindow' },
        { ref:'confirmationBox', selector:'site-confirmationBox' },
        { ref:'detailForm', selector:'site-form' },
        { ref:'navigationTree', selector:'site-tree' },
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteGroup'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        { ref:'deleteGroupButton', selector:'site-tree button[action=onDeleteGroup]' },
        /*<?php }?>*/
        { ref:'saveSiteButton', selector:'site-form button[action=onSaveSite]' },
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteSite'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        { ref:'deleteSiteButton', selector:'site-mainWindow button[action=onDeleteSite]' },
        /*<?php }?>*/
        { ref:'groupSelector', selector:'site-form itemselector[name=grouping]' }
    ],

    /**
     * Creates the necessary event listener for this
     * controller and the main window
     *
     * @return void
     */
    init: function() {
        var me = this;

        me.control({
            //fires, when the user tries to create a new group
            'site-tree button[action=onCreateGroup]': {
                click: me.onCreateGroup
            },
            //fires, when the user tries to delete a group
            'site-tree button[action=onDeleteGroup]': {
                click: me.onDeleteGroup
            },
            //fires everytime an item in the tree is clicked eg. the selection is changed
            'site-tree': {
                itemclick: me.onItemClick
            },
            //fires, when the user confirms the new groupname and variable
            '[action=onCreateGroupSubmit]': {
                click: me.onCreateGroupSubmit
            },
            //fires when the user tries to close the createGroup window
            '[action=onCreateGroupWindowClose]': {
                click: me.onCreateGroupWindowClose
            }
        });

        me.callParent(arguments);
    },

    /**
     * Event listener method which is called when an item in the tree is clicked.
     * Depending on the type (leaf or non-leaf), it will activate the necessary buttons for user interaction
     * Further, if the item is a Site, it will generate an array from the grouping string, which is necessary for the checkboxes to work correctly
     * It will then call form.loadRecord() to display the record in the detail form.
     * An embed code for the site will be created as well.
     * If the item is not a site, it will just set button states accordingly.
     *
     * @param item
     * @param record
     */
    onItemClick: function(item,record) {

        var me = this,
            form = me.getDetailForm(),
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteGroup'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?>*/
            deleteGroupButton = me.getDeleteGroupButton(),
            /*<?php }?>*/
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteSite'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?>*/
            deleteSiteButton = me.getDeleteSiteButton(),
            /*<?php }?>*/
            saveSiteButton = me.getSaveSiteButton(),

			ddselector = form.down('ddselector'),
			groupStore = ddselector.fromStore,
			selectedStore = ddselector.toStore;

        //determine if the item is a group or a site
        if (record.data.parentId != 'root' || record.isLeaf()) {

            //set button states
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteGroup'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php if ($_tmp5){?>*/
            deleteGroupButton.disable();
            /*<?php }?>*/
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteSite'),$_smarty_tpl);?>
<?php $_tmp6=ob_get_clean();?><?php if ($_tmp6){?>*/
            deleteSiteButton.enable();
            /*<?php }?>*/
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'updateSite'),$_smarty_tpl);?>
<?php $_tmp7=ob_get_clean();?><?php if ($_tmp7){?>*/
            form.saveButton.enable();
            /*<?php }else{ ?>*/
            form.saveButton.disable();
            /*<?php }?>*/

			groupStore.load({
				params: {
					grouping: record.data.grouping
				}
			});
			selectedStore.load({
				params: {
					grouping: record.data.grouping
				}
			});
            //load record into the form
            //hotfix find a better solution for this after beta
            //record.data.description = record.data.description.split("(")[0];
            form.loadRecord(record);

            //build and set the embed code
            //the preceding '<' is necessary to display the string without interference from the script renderer
            var embedCode = '<' + 'a href="{url controller=custom sCustom=' + record.data.helperId + '}" title="' + record.data.description + '">' + record.data.description +'</a>';
            form.down('textfield[name=embedCode]').setValue(embedCode);

        } else {
            //set button states
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteGroup'),$_smarty_tpl);?>
<?php $_tmp8=ob_get_clean();?><?php if ($_tmp8){?>*/
            deleteGroupButton.enable();
            /*<?php }?>*/
            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteSite'),$_smarty_tpl);?>
<?php $_tmp9=ob_get_clean();?><?php if ($_tmp9){?>*/
            deleteSiteButton.disable();
            /*<?php }?>*/
        }

    },

    /**
     * Event listener method which is called when the user tries to create a new group
     * This will open the createGroupWindow where User interaction is handled.
     */
    onCreateGroup: function() {
        var me = this;
        me.createGroupWindow().show();
    },

    /**
     * Event listener method which will be called when the onDeleteGroup event was fired.
     * A confirmation dialog will open, which, in turn, will send an ajax request containing the group name to the site php controller that handles deletion.
     */
    onDeleteGroup: function() {
        var me = this,
            tree = me.getNavigationTree(),
            selection = tree.getSelectionModel().getSelection()[0],
            groupName = selection.data.text,
            templateVariable = selection.data.id;

        Ext.Msg.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteGroupConfirmationBoxCaption','default'=>'Delete this group?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupConfirmationBoxCaption','default'=>'Delete this group?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gruppe löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupConfirmationBoxCaption','default'=>'Delete this group?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteGroupConfirmationBoxText','default'=>'Are you sure you want to delete the group \\\'[0]\\\'?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupConfirmationBoxText','default'=>'Are you sure you want to delete the group \\\'[0]\\\'?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wollen Sie die Gruppe \'[0]\' wirklich löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupConfirmationBoxText','default'=>'Are you sure you want to delete the group \\\'[0]\\\'?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', groupName), function(btn){
            if (btn == 'yes'){
                Ext.Ajax.request({
                    url : '<?php echo '/backend/Site/deleteGroup';?>',
                    scope:this,
                    params: {
                        templateVar: templateVariable
                    },
                    callback: function() {
                        //reload the stores and display a success message
                        me.getStore('Nodes').load();
                        me.getStore('Groups').load();
//                        me.getGroupSelector().items.items[0].items.items[0].items.items[0].getStore().load();

                        Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteGroupSuccess','default'=>'The group has been deleted successfully.','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupSuccess','default'=>'The group has been deleted successfully.','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Gruppe wurde erfolgreich gelöscht.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupSuccess','default'=>'The group has been deleted successfully.','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    },
                    success: function(){
                        
                        
                    },
                    failure: function(response) {
                        //display an error message, followed by the actual error text
                        var responseObject = Ext.decode(response.responseText),
                            errorMsg = responseObject.message;
                        Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteGroupError','default'=>'An error has occurred while trying to delete the group: ','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupError','default'=>'An error has occurred while trying to delete the group: ','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Löschen der Gruppe ist ein Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteGroupError','default'=>'An error has occurred while trying to delete the group: ','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' + errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    }
                });
            }
        });
    },

    /**
     * Event listener method which will be called when the createGroupWindow event was fired.
     * This creates the createGroup dialog window.
     */
    createGroupWindow: function() {
        var me = this;

        return me.getView('site.GroupDialog').create({});
    },

    /**
     * Event listener method which will be called when the onCreateGroupSubmit event was fired.
     * A confirmation dialog will open, which will in turn send an ajax request containing the group name and template variable to the site php controller.
     * It will also display an error message if either the groupName or the templateVariable is already being used
     * @param btn
     */
    onCreateGroupSubmit: function(btn) {
        var me = this,
            dialogWindow = btn.up('window'),
            groupName = dialogWindow.down('textfield[name=description]').getValue(),
            templateVar = dialogWindow.down('textfield[name=templateVar]').getValue();
			//send ajax request containing groupName and templateVariable
			Ext.Ajax.request({
				url : '<?php echo '/backend/Site/createGroup';?>',
				scope: me,
				params: {
					groupName: groupName,
					templateVar: templateVar
				},
				success: function(response){
					//get the response object
					var responseObject = Ext.decode(response.responseText);

					if (responseObject.success) {
						//destroy the window, reload the stores
						dialogWindow.destroy();
						me.getStore('Nodes').load();
						me.getStore('Groups').load();
//                            me.getGroupSelector().items.items[0].items.items[0].items.items[0].getStore().load();

						//display a success message
						Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateGroupSuccess','default'=>'The group has been created successfully.','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupSuccess','default'=>'The group has been created successfully.','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Gruppe wurde erfolgreich erstellt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupSuccess','default'=>'The group has been created successfully.','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					} else {
						if (responseObject.message == 'nameExists') {
							dialogWindow.destroy();
							Shopware.Notification.createGrowlMessage('',Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateGroupGroupNameExisting','default'=>'The group \\\'[0]\\\' already exists.','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupGroupNameExisting','default'=>'The group \\\'[0]\\\' already exists.','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Gruppe \'[0]\' existiert bereits.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupGroupNameExisting','default'=>'The group \\\'[0]\\\' already exists.','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', groupName));
                            return;
						}
						if (responseObject.message == 'variableExists') {
							dialogWindow.destroy();
							Shopware.Notification.createGrowlMessage('',Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateGroupTemplateVariableExisting','default'=>'The template variable \\\'[0]\\\' is already in use.','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupTemplateVariableExisting','default'=>'The template variable \\\'[0]\\\' is already in use.','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Template-Variable \'[0]\' wird bereits verwendet.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupTemplateVariableExisting','default'=>'The template variable \\\'[0]\\\' is already in use.','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', templateVar));
                            return;
						}
                        dialogWindow.destroy();
                        Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Erstellen der Gruppe ist ein Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' + responseObject.message, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
					}
				},
				failure: function(response) {
					//get the response object
					var responseObject = Ext.decode(response.responseText),
						errorMsg = responseObject.message;

					//display an error message followed by the actual error
					Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Erstellen der Gruppe ist ein Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateGroupError','default'=>'An error has occurred while trying to create the group: ','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' + errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
				}
			});
    },

    /**
     * Event listener method which will be called when the onCreateGroupWindowClose event was fired.
     * This will just close the window.
     * @param btn
     */
    onCreateGroupWindowClose: function(btn) {
        var dialogWindow = btn.up('window');
        dialogWindow.destroy();
    }
});
//<?php }} ?>