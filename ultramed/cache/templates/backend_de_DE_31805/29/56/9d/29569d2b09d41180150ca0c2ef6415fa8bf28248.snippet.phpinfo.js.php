<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/phpinfo.js" */ ?>
<?php /*%%SmartyHeaderCode:176547200755447cb127fcc5-27299376%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '29569d2b09d41180150ca0c2ef6415fa8bf28248' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/phpinfo.js',
      1 => 1430113401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '176547200755447cb127fcc5-27299376',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb128a657_45011206',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb128a657_45011206')) {function content_55447cb128a657_45011206($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - View for PHPInfo
 *
 * todo@all: Documentation
 *
 */
//
Ext.define('Shopware.apps.Systeminfo.view.systeminfo.Phpinfo', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.panel.Panel',

    ui: 'shopware-ui',

    /**
     * ID to access the component out of other components
     */
    id: 'systeminfo-main-phpinfo',

    /**
     * Loads the phpinfo()-function
     */
    html: '<iframe frameborder="0" style="overflow-x: hidden;" height="100%" src="<?php echo '/backend/systeminfo/info';?>"></iframe>',

    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('systeminfo-main-list')
    * @string
    */
    alias: 'widget.systeminfo-main-phpinfo'
});
//<?php }} ?>