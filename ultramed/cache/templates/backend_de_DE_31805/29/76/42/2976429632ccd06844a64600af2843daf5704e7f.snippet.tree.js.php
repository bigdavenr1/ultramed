<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/tree.js" */ ?>
<?php /*%%SmartyHeaderCode:1291164710554476fc524888-60748838%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2976429632ccd06844a64600af2843daf5704e7f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/tree.js',
      1 => 1430113345,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1291164710554476fc524888-60748838',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc52d965_70306933',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc52d965_70306933')) {function content_554476fc52d965_70306933($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Tree
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/*  */

/**
 * Shopware UI - Category management main window.
 *
 * This component contains a category tree. This tree is uses to manage 
 * the hierarchical order of categories. Its provides methods to create, delete, rearrange categories.
 */
// 
Ext.define('Shopware.apps.Blog.view.blog.Tree', {
    /**
    * Parent Element Ext.tree.Panel
    * @string
    */
    extend: 'Ext.tree.Panel',
    /**
     * Register the alias for this class.
     * @string 
     */
    alias : 'widget.blog-blog-tree',
    /**
     * True to make the panel collapsible and have an expand/collapse toggle 
     * Tool added into the header tool button area. 
     * False to keep the panel sized either statically, or by an owning layout manager, with no toggle Tool.
     * 
     * @boolean
     */
    collapsible: false,

    split: true,

    region   : 'west',

    /**
     * False to hide the root node.
     * @boolean
     */
    rootVisible: true,
    /**
     * True to use Vista-style arrows in the tree.
     * @boolean
     */
    useArrows: false,
    /**
     * The width of this component in pixels.
     * @integer 
     */
    width: 250,

    /**
     * Name of the root node. We have to show the root node in order to move a subcategory under the root.
     * @string 
     */
    rootNodeName : 'Shopware',
    
     /**
     * Initialize the controller and defines the necessary default configuration
     */
    initComponent : function() {
        var me = this;
         me.store = me.treeStore;

        // rename root node to shopware.
        var rootNode = me.treeStore.getRootNode();
        rootNode.data.text = me.rootNodeName;

        me.callParent(arguments);
    }
});
//
<?php }} ?>