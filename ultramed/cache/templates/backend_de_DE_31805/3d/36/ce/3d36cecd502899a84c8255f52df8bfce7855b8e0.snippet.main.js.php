<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/main.js" */ ?>
<?php /*%%SmartyHeaderCode:1094096038554476fc56a3a7-57981978%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d36cecd502899a84c8255f52df8bfce7855b8e0' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/main.js',
      1 => 1430113596,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1094096038554476fc56a3a7-57981978',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc5c47c1_07932879',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc5c47c1_07932879')) {function content_554476fc5c47c1_07932879($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
/**
 * Shopware UI - Blog detail main window.
 *
 * Displays all Detail Blog Information
 */
//
Ext.define('Shopware.apps.Blog.view.blog.detail.Main', {
    extend:'Ext.container.Container',
    alias:'widget.blog-blog-detail-main',
    border: 0,
    bodyPadding: 10,
    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    /**
     * Initialize the Shopware.apps.Blog.view.blog.detail.mail and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.generalFieldset = Ext.create('Ext.panel.Panel', {
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field_set'/'general_data','default'=>'General data','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field_set'/'general_data','default'=>'General data','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stammdaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field_set'/'general_data','default'=>'General data','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            margin: '0 0 10 0',
            layout: {
                type: 'anchor'
            },
            bodyPadding: 10,
            flex: 3,
            closable: false,
            collapsible: true,
            autoScroll: true,
            defaults:{
                labelWidth:120,
                minWidth:250,
                anchor: '100%',
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items: me.createGeneralForm()
        });

        me.contentFieldset = Ext.create('Ext.panel.Panel', {
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field_set'/'content_data','default'=>'Content','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field_set'/'content_data','default'=>'Content','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inhalt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field_set'/'content_data','default'=>'Content','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            layout: {
                type: 'anchor'
            },
            flex: 7,
            autoScroll:true,
            bodyPadding: 10,
            defaults:{
                anchor: '100%'
            },
            items: me.createContentForm()
        });

        me.items = [ me.generalFieldset, me.contentFieldset ];

        me.callParent(arguments);
    },

    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(
            /**
             * Event will be fired when the user changes the customer-account field
             *
             * @event mapCustomerAccount
             * @param [Ext.form.field.Field] this
             * @param [object] newValue
             * @param [object] oldValue
             * @param [object] eOpts
             */
            'mapCustomerAccount'
        );

        return true;
    },


    /**
     * creates the general form and layout
     *
     * @return [Array] computed form
     */
    createGeneralForm:function () {
        var me = this;
        me.mainTitle = Ext.create('Ext.form.field.Text', {
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Titel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            allowBlank:false,
            required:true,
            labelWidth:120,
            name:'title'
        });

        return [
            me.mainTitle,
            {
                xtype:'combobox',
                name:'authorId',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autor<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'author','default'=>'Author','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store:Ext.create('Shopware.store.User').load(),
                valueField:'id',
                editable:true,
                displayField:'name',
                pageSize: 10
            },
            {
                xtype:'checkbox',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'active','default'=>'Active','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                inputValue:1,
                uncheckedValue:0,
                name:'active',
                boxLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'active'/'help','default'=>'Blog article will be shown in the storefront','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'active'/'help','default'=>'Blog article will be shown in the storefront','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blogartikel wird im Frontend angezeigt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'active'/'help','default'=>'Blog article will be shown in the storefront','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'

            }
        ]
    },

    /**
     * creates the general form and layout
     *
     * @return [Array] computed form
     */
    createContentForm:function () {
        var me = this;
        return [
            {
                xtype: 'textarea',
                labelWidth:120,
                minWidth:250,
                height:40,
                labelStyle:'font-weight: 700;',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'short_description','default'=>'Short description','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'short_description','default'=>'Short description','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kurzbeschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'short_description','default'=>'Short description','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                allowBlank:false,
                required:true,
                name:'shortDescription'
            },
            {
                xtype: 'container',
                html: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'main'/'field'/'short_description'/'help','default'=>'The short description will be displayed in the listing of the store front.','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'short_description'/'help','default'=>'The short description will be displayed in the listing of the store front.','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Kurzbeschreibung wird im Frontend in der Auflistung angezeigt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'main'/'field'/'short_description'/'help','default'=>'The short description will be displayed in the listing of the store front.','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                margin: '0 0 8 125',
                style: 'font-size: 11px; color: #999; font-style: italic;'
            },
            {
                xtype: 'tinymce',
                height: 370,
                name: 'description'
            }
        ]
    }
});
//
<?php }} ?>