<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:52
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/list.js" */ ?>
<?php /*%%SmartyHeaderCode:80933157055447c3c4d06d9-06414507%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60bda8d2ae0cb5c42dae9233ada0e51eb89c9a36' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/list.js',
      1 => 1430111762,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80933157055447c3c4d06d9-06414507',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3c55e3f9_53986299',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3c55e3f9_53986299')) {function content_55447c3c55e3f9_53986299($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//

Ext.define('Shopware.apps.PaymentPaypal.view.main.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.paypal-main-list',

    store: 'main.List',

//    layout: 'fit',
//    viewConfig: {
//        stripeRows: true
//    },

    initComponent: function() {
        var me = this;
        Ext.applyIf(me, {
            dockedItems: [
                me.getPagingToolbar(),
                me.getToolbar()
            ],
            columns: me.getColumns()
        });


        me.addEvents('shopSelectionChanged');


        this.callParent();
        me.store.clearFilter(true);
        me.store.load();
    },

    getColumns: function() {
        var me = this;
        return [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'date_text','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'date_text','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestelldatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'date_text','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 3,
            xtype: 'datecolumn',
            format: Ext.Date.defaultFormat + ' H:i:s',
            dataIndex: 'orderDate'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'order_number_text','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'order_number_text','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnumer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'order_number_text','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            dataIndex: 'orderNumber'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'transaction_id_text','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'transaction_id_text','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Transaktionscode<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'transaction_id_text','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 3,
            dataIndex: 'transactionId'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'payment_method_text','default'=>'Payment method','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'payment_method_text','default'=>'Payment method','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'payment_method_text','default'=>'Payment method','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            dataIndex: 'paymentDescription'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'customer_text','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'customer_text','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'customer_text','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 3,
            dataIndex: 'customer'
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'shop_text','default'=>'Shop','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'shop_text','default'=>'Shop','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'shop_text','default'=>'Shop','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'shopName',
            flex:2
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'currency_text','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'currency_text','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'currency_text','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            dataIndex: 'currency'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'amount_text','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'amount_text','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Betrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'amount_text','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            align: 'right',
            renderer : function(value, column, model) {
                return model.data.amountFormat;
            },
            dataIndex: 'amount'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'order_status_text','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'order_status_text','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'order_status_text','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            dataIndex: 'statusId',
            renderer : function(value, column, model) {
                return model.data.statusDescription;
            }
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'columns'/'payment_status_text','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'payment_status_text','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'columns'/'payment_status_text','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 2,
            dataIndex: 'cleardID',
            renderer : function(value, column, model) {
                return model.data.clearedDescription;
            }
        },{
            xtype:'actioncolumn',
            width: 70,
            sortable: false,
            items: [{

                iconCls: 'sprite-user--pencil',
                tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'actioncolumn'/'customer_tooltip','default'=>'Open customer details','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'customer_tooltip','default'=>'Open customer details','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundendetails öffnen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'customer_tooltip','default'=>'Open customer details','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                handler: function(grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Shopware.app.Application.addSubApplication({
                        name: 'Shopware.apps.Customer',
                        action: 'detail',
                        params: {
                            customerId: record.get('customerId')
                        }
                    });
                }
            }, {
                iconCls: 'sprite-sticky-notes-pin',
                tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'actioncolumn'/'order_tooltip','default'=>'Open order details','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'order_tooltip','default'=>'Open order details','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestelldetails öffnen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'order_tooltip','default'=>'Open order details','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                handler: function(grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex);
                    Shopware.app.Application.addSubApplication({
                        name: 'Shopware.apps.Order',
                        params: {
                            orderId: record.get('id')
                        }
                    });
                }
            }, {
                iconCls: 'sprite-document-invoice',
                tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'actioncolumn'/'invoice_tooltip','default'=>'Open invoice','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'invoice_tooltip','default'=>'Open invoice','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rechnung öffnen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'actioncolumn'/'invoice_tooltip','default'=>'Open invoice','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                getClass: function(value, metadata, record) {
                    if(!record.get('invoiceId')) {
                        return 'x-hidden';
                    }
                },
                handler: function(grid, rowIndex, colIndex) {
                    var record = grid.getStore().getAt(rowIndex),
                        link = "<?php echo '/backend/order/openPdf';?>"
                            + "?id=" + record.get('invoiceHash');
                    window.open(link, '_blank');
                }
            }]
        }];
    },

    getPagingToolbar: function() {
        var me = this;
        return {
            xtype: 'pagingtoolbar',
            displayInfo: true,
            store: me.store,
            dock: 'bottom'
        };
    },

    getToolbar: function() {
        var me = this;
        return {
            xtype: 'toolbar',
            ui: 'shopware-ui',
            dock: 'top',
            border: false,
            items: me.getTopBar()
        };
    },

    getTopBar:function () {
        var me = this,
            items = [],
            shopStore = Ext.create('Shopware.apps.Base.store.Shop');

        shopStore.clearFilter();

        items.push({
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'balance'/'label','default'=>'Your PayPal balance','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'balance'/'label','default'=>'Your PayPal balance','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ihr PayPal-Guthaben<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'balance'/'label','default'=>'Your PayPal balance','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            labelWidth: 150,
            xtype: 'textfield',
            flex: 3,
            readOnly: true,
            name: 'balance'
        }, {
            xtype: 'combo',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'shop'/'label','default'=>'Select shop','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'shop'/'label','default'=>'Select shop','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopauswahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'shop'/'label','default'=>'Select shop','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: shopStore,
            checkChangeBuffer: 100,
            allowEmpty: true,
            emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'showAll','default'=>'Show all','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'showAll','default'=>'Show all','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Alle zeigen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'showAll','default'=>'Show all','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            displayField: 'name',
            valueField: 'id',
            flex: 2,
            name: 'shopId',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts) {
                    me.fireEvent('shopSelectionChanged', newValue);
                }
            }
        }, '->', {
            xtype: 'textfield',
            name:'searchfield',
            cls:'searchfield',
            width: 100,
            emptyText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'search'/'text','default'=>'Search...','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'search'/'text','default'=>'Search...','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suchen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'search'/'text','default'=>'Search...','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            enableKeyEvents:true,
            checkChangeBuffer:500
        }, {
            xtype:'tbspacer', width:6
        });
        return items;
    }
});
<?php }} ?>