<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/balance.js" */ ?>
<?php /*%%SmartyHeaderCode:186964225655447c3d6d1782-24930801%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '602f1ccc31007e55472ebe771b970346ced42c3f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/balance.js',
      1 => 1430111761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '186964225655447c3d6d1782-24930801',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d6d9c00_77934261',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d6d9c00_77934261')) {function content_55447c3d6d9c00_77934261($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.store.main.Balance', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.PaymentPaypal.model.main.Balance',
    proxy: {
        type: 'ajax',
        url : '<?php echo '/backend/PaymentPaypal/getBalance';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
<?php }} ?>