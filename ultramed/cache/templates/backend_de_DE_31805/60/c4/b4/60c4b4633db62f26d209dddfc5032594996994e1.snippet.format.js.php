<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/format.js" */ ?>
<?php /*%%SmartyHeaderCode:148557510855447e7581c323-67300445%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60c4b4633db62f26d209dddfc5032594996994e1' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/format.js',
      1 => 1430113601,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148557510855447e7581c323-67300445',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75877810_39991990',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75877810_39991990')) {function content_55447e75877810_39991990($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Tab View.
 *
 * Displays all tab format Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.tab.Format', {
    extend:'Ext.container.Container',
    alias:'widget.product_feed-feed-tab-format',
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'format','default'=>'Format','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'format','default'=>'Format','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formatierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'format','default'=>'Format','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    border: 0,
    padding: 10,
    cls: 'shopware-toolbar',
    layout: 'anchor',
    defaults:{
        anchor:'100%',
        labelStyle:'font-weight: 700;',
        xtype:'combobox'
    },
    //Data for the Format Comboboxes
    encoding:[
        [1, 'ISO-8859-1'],
        [2, 'UTF-8']
    ],
    fileFormat:[
        [1, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'file_format_csv','default'=>'CSV','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_csv','default'=>'CSV','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
CSV<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_csv','default'=>'CSV','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [2, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'file_format_txt_tab','default'=>'TXT with tab delimiter','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_txt_tab','default'=>'TXT with tab delimiter','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
TXT mit Tab als Trennzeichen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_txt_tab','default'=>'TXT with tab delimiter','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [3, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'file_format_XML','default'=>'XML','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_XML','default'=>'XML','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
XML<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_XML','default'=>'XML','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [4, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'file_format_txt_pipe','default'=>'TXT with pipe delimiter','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_txt_pipe','default'=>'TXT with pipe delimiter','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
TXT mit Pipe als Trennzeichen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format_txt_pipe','default'=>'TXT with pipe delimiter','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
']
    ],
    /**
     * Initialize the Shopware.apps.ProductFeed.view.feed.detail and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.items = me.getItems();

        me.callParent(arguments);
    },
    /**
     * creates all fields for the general form on the left side
     */
    getItems:function () {
        var me = this;
        return [
            {
                name:'encodingId',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'encoding','default'=>'Encoding','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'encoding','default'=>'Encoding','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeichenkodierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'encoding','default'=>'Encoding','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store:new Ext.data.SimpleStore({
                    fields:['id', 'text'], data:me.encoding
                }),
                valueField:'id',
                displayField:'text',
                value:1,
                mode:'local',
                editable: false
            },
            {
                name:'formatId',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_format'/'field'/'file_format','default'=>'File format','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format','default'=>'File format','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Dateiformat<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_format'/'field'/'file_format','default'=>'File format','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store:new Ext.data.SimpleStore({
                    fields:['id', 'text'], data:me.fileFormat
                }),
                valueField:'id',
                displayField:'text',
                value:1,
                mode:'local',
                editable: false
            }
        ];
    }

});
//
<?php }} ?>