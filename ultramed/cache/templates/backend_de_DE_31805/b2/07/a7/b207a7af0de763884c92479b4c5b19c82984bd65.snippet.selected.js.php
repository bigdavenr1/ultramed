<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/model/selected.js" */ ?>
<?php /*%%SmartyHeaderCode:166377913655447a968146c5-37857307%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b207a7af0de763884c92479b4c5b19c82984bd65' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/model/selected.js',
      1 => 1430113176,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '166377913655447a968146c5-37857307',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a96825c89_89028033',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a96825c89_89028033')) {function content_55447a96825c89_89028033($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
Ext.define('Shopware.apps.Site.model.Selected', {
	extend: 'Ext.data.Model',
    idProperty: 'templateVariable',
    root: 'groups',
    fields: [
		//
        { name : 'id', type: 'int' },
        { name : 'templateVariable', type: 'string', mapping: 'key' },
        { name : 'groupName', type: 'string', mapping: 'name' }
    ],

    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/site/getSelectedGroups';?>',
        reader: {
            type: 'json',
            root: 'groups'
        }
    }
});
//<?php }} ?>