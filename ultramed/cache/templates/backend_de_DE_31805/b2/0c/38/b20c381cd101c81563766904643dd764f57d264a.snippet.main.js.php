<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:161697794155447b7740a861-61960083%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b20c381cd101c81563766904643dd764f57d264a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/controller/main.js',
      1 => 1430112890,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '161697794155447b7740a861-61960083',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b77473941_98569434',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b77473941_98569434')) {function content_55447b77473941_98569434($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.controller.Main', {

    /**
     * Extend from the standard ExtJS 4
     * @string
     */
    extend: 'Ext.app.Controller',



    /**
     * Define references for the different parts of our application. The
     * references are parsed by ExtJS and Getter methods are automatically created.
     *
     * Example: { ref : 'grid', selector : 'grid' } transforms to this.getGrid();
     *          { ref : 'addBtn', selector : 'button[action=add]' } transforms to this.getAddBtn()
     *
     * @array
     */
    refs: [
        { ref: 'formgrid', selector: 'form-main-formgrid' }
    ],

    /**
     * Class property which holds the main application if it is created
     *
     * @default null
     * @object
     */
    mainWindow: null,

    /**
     * Creates the necessary event listener for this
     * specific controller and opens a new Ext.window.Window
     * @return void
     */
    init: function() {
        var me = this;

        me.control({
            'form-main-editwindow': {
                beforeclose: me.onBeforeClose
            },

            'form-main-formpanel button[action=save]': {
                click: me.onSaveForm
            },

            'form-main-formgrid button[action=add]': {
                click: me.onOpenAddWindow
            },

            'form-main-formgrid button[action=delete]': {
                click: me.onDeleteMultipleForms
            },

            'form-main-formgrid textfield[action=searchForms]' : {
                change: me.onSearch
            },

            'form-main-formgrid actioncolumn': {
                render: function (view) {
                    view.scope = this;
                    view.handler = this.handleActionColumn;
                }
            }
        });

        me.mainWindow = me.getView('main.Mainwindow').create({
            formStore: me.getStore('Form')
        }).show();

        me.callParent(arguments);
    },

    /**
     * Helper method which handles all clicks of the action column
     *
     * @event render
     * @param [Ext.grid.View] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - On which row position has been clicked
     * @param [integer] colIndex - On which coulmn position has been clicked
     * @param [object] item - The item that has been clicked
     * @return void
     */
    handleActionColumn: function (grid, rowIndex, colIndex, item) {
        var me = this.scope;

        switch (item.action) {
            case 'edit':
                me.onOpenEditWindow(grid, rowIndex, colIndex, item);
                break;
            case 'delete':
                me.onDeleteSingleForm(grid, rowIndex, colIndex, item);
                break;
            case 'copy':
                me.onCopyForm(grid, rowIndex, colIndex, item);
                break;
            default:
                break;
        }
    },


    /**
     * @event beforeclose
     * @return void
     */
    onBeforeClose: function() {
        this.getStore('Form').load();
    },

    /**
     * Event listener which copies a single Form based on the passed
     * grid (e.g. the grid store) and the row index
     *
     * @param [Ext.grid.View] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - On which row position has been clicked
     * @param [integer] colIndex - On which coulmn position has been clicked
     * @param [object] item - The item that has been clicked
     * @return void
     */
    onCopyForm: function(grid, rowIndex, colIndex, item) {
        var store  = grid.getStore(),
            record = store.getAt(rowIndex);

        var message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_copy_form_message','default'=>'Are you sure you want to duplicate the selected form ([0])?','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_copy_form_message','default'=>'Are you sure you want to duplicate the selected form ([0])?','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Möchten Sie das ausgewählte Formular ([0]) wirklich duplizieren?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_copy_form_message','default'=>'Are you sure you want to duplicate the selected form ([0])?','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', record.get('name'));

        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_copy_form_title','default'=>'Duplicate form','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_copy_form_title','default'=>'Duplicate form','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formular duplizieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_copy_form_title','default'=>'Duplicate form','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response) {
            if (response !== 'yes') {
                return false;
            }

            record.copy(function() {
                store.load();
            });
        });
    },

    /**
     * Event listener which deletes a single Form based on the passed
     * grid (e.g. the grid store) and the row index
     *
     * @param [Ext.grid.View] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - On which row position has been clicked
     * @param [integer] colIndex - On which coulmn position has been clicked
     * @param [object] item - The item that has been clicked
     * @return void
     */
    onDeleteSingleForm: function (grid, rowIndex, colIndex, item) {
        var store  = grid.getStore(),
            record = store.getAt(rowIndex);

        var message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_form_message','default'=>'Are you sure you want to delete the selected form ([0])?','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_form_message','default'=>'Are you sure you want to delete the selected form ([0])?','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Möchten Sie das ausgewählte Formular ([0]) wirklich löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_form_message','default'=>'Are you sure you want to delete the selected form ([0])?','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', record.get('name'));

        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_form_title','default'=>'Delete form','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_form_title','default'=>'Delete form','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formular Löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_form_title','default'=>'Delete form','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response) {
            if (response !== 'yes') {
                return false;
            }

            record.destroy({
                callback: function() {
                    store.load();
                }
            });

        });
    },

    /**
     * Event listener method which will be fired when the user
     * insert a value in the search field on the right hand of the module,
     * to search forms by their name.
     *
     * @event change
     * @param [object] field - Ext.form.field.Text
     * @param [string] value - inserted search value
     * @return void
     */
    onSearch: function(field, value) {
        var store = this.getStore('Form'),
            searchString = Ext.String.trim(value);

        //scroll the store to first page
        store.currentPage = 1;

        //If the search-value is empty, reset the filter
        if ( searchString.length === 0 ) {
            store.clearFilter();
        } else {
            //This won't reload the store
            store.filters.clear();
            //Loads the store with a special filter
            store.filter('name', "%" + searchString + "%");
        }
    },

    /**
     * Opens the Ext.window.window to modify an existing form
     *
     * @param [Ext.grid.View] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - On which row position has been clicked
     * @param [integer] colIndex - On which coulmn position has been clicked
     * @param [object] item - The item that has been clicked
     */
    onOpenEditWindow: function (grid, rowIndex, colIndex, item) {
        var me = this,
            store = grid.getStore(),
            newStore = Ext.create('Shopware.apps.Form.store.Form'),
            record = store.getAt(rowIndex),
            fieldgridStore = me.getStore('Field');

        newStore.load({
            id: record.getId(),
            scope: this,
            callback: function(records, operation, success) {
                if (success) {
                    var newRecord = records[0];

                    var view = me.getView('main.Editwindow').create({
                        formRecord: newRecord,
                        fieldStore: this.getStore('Field')
                    });

                    view.down('form-main-fieldgrid').setDisabled(false);

                    fieldgridStore.getProxy().extraParams.formId = newRecord.data.id;
                    fieldgridStore.load();

                    view.show();
                }
            }
        });
    },


    /**
     * Opens the Ext.window.window to add a new form
     *
     * @event click
     * @return void
     */
    onOpenAddWindow: function() {
        this.getView('main.Editwindow').create({
            fieldStore: this.getStore('Field')
        }).show();
    },

    /**
     * Event listener method which fires when the user
     * clicks the delete button in the top toolbar
     *
     * Deletes the currently selected forms.
     *
     * @event click
     * @return void
     */
    onDeleteMultipleForms: function() {
        var grid = this.getFormgrid(),
            sm = grid.getSelectionModel(),
            selected = sm.selected.items,
            store = grid.getStore();


        var message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_forms_message','default'=>'Are you sure you want to delete the selected forms?','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_forms_message','default'=>'Are you sure you want to delete the selected forms?','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Möchten Sie die ausgewählten Formulare wirklich löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_forms_message','default'=>'Are you sure you want to delete the selected forms?','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');

        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_forms_title','default'=>'Delete forms','namespace'=>'backend/form/controller/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_forms_title','default'=>'Delete forms','namespace'=>'backend/form/controller/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Formulare löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_forms_title','default'=>'Delete forms','namespace'=>'backend/form/controller/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response) {
            if (response !== 'yes') {
                return false;
            }

            grid.setLoading(true);

            Ext.each(selected, function(record) {
                record.destroy();
            });

            store.load({
                callback: function() {
                    grid.setLoading(false);
                }
            });
        });
    },

    /**
     * Function to save a form
     *
     * @event click
     * @param [object] btn Contains the clicked button
     * @return void
     */
    onSaveForm: function(btn) {
        var me         = this,
            win        = btn.up('window'),
            formPanel  = win.down('form'),
            form       = formPanel.getForm(),
            record     = form.getRecord(),
            attributes,
            fieldStore = me.getStore('Field');

        if (!form.isValid()) {
            return;
        }

        if (record === undefined) {
            record = Ext.create('Shopware.apps.Form.model.Form');
            attributes = Ext.create('Shopware.apps.Form.model.Attribute');
            record.getAttributes().add(attributes);
        }

        form.updateRecord(record);

        formPanel.setLoading(true);
        record.save({
            callback: function() {
                formPanel.setLoading(false);
                formPanel.loadRecord(record);
                // set extraparams
                win.down('form-main-fieldgrid').setDisabled(false);

                fieldStore.getProxy().extraParams.formId = record.data.id;
                fieldStore.load();
            }
        });
    }
});
//
<?php }} ?>