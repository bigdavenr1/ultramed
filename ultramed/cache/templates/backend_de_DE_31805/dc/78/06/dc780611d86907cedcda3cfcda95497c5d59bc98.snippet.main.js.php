<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:212451832555447e75ac9f65-58950414%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc780611d86907cedcda3cfcda95497c5d59bc98' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/controller/main.js',
      1 => 1430113166,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212451832555447e75ac9f65-58950414',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75ad2e01_76632673',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75ad2e01_76632673')) {function content_55447e75ad2e01_76632673($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - feed main backend module
 *
 * The feed module main controller handles the initialisation of the product feed backend list.
 */
//
Ext.define('Shopware.apps.ProductFeed.controller.Main', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
	extend: 'Ext.app.Controller',

    mainWindow: null,

    /**
	 * Creates the necessary event listener for this
	 * specific controller and opens a new Ext.window.Window
	 * to display the subapplication
     *
     * @return void
	 */
	init: function() {
        var me = this;
        me.subApplication.listStore = me.getStore('List');
        me.subApplication.detailStore = me.getStore('Detail');
        me.subApplication.supplierStore = me.getStore('Supplier');
        me.subApplication.shopStore = me.getStore('Shop');
        me.subApplication.articleStore = me.getStore('Article');
        me.subApplication.availableCategoriesTree = me.getStore('Category');
        me.subApplication.comboTreeCategoryStore = me.getStore('CategoryForComboTree');

        me.mainWindow = me.getView('main.Window').create({
            listStore: me.subApplication.listStore
        });
        me.subApplication.listStore.load();

        me.callParent(arguments);
    }
});
//
<?php }} ?>