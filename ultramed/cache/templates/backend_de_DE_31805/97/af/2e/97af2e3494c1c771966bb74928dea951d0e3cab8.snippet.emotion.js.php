<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/emotion.js" */ ?>
<?php /*%%SmartyHeaderCode:84297316655434610b297e2-82303676%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '97af2e3494c1c771966bb74928dea951d0e3cab8' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/emotion.js',
      1 => 1430112888,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '84297316655434610b297e2-82303676',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610b6f0b1_02049619',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610b6f0b1_02049619')) {function content_55434610b6f0b1_02049619($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Emotion backend module.
 *
 * The emotion model contains all data for one shopware emotion.
 * The data used for the backend listing and the detail page.
 */
//
Ext.define('Shopware.apps.Emotion.model.Emotion', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'gridId', type: 'int', useNull: true },
        { name: 'templateId', type: 'int', useNull: true, defaultValue: 1 },
        { name: 'active', type: 'boolean' },
        { name: 'showListing', type: 'boolean' },
        { name: 'name', type: 'string' },

        { name: 'containerWidth', type: 'int' },
        { name: 'categoryId', type: 'int', useNull: true },
        { name: 'validFrom', type: 'date', dateFormat: 'd.m.Y', useNull: true },
        { name: 'validTo', type: 'date', dateFormat: 'd.m.Y', useNull: true },
        { name: 'validToTime', type: 'date', dateFormat: 'H:i', useNull: true },
        { name: 'validFromTime', type: 'date', dateFormat: 'H:i', useNull: true },
        { name: 'userId', type: 'int' },
        { name: 'createDate', type: 'date', dateFormat: 'd.m.Y', useNull: true  },
        { name: 'modified', type: 'date', dateFormat: 'd.m.Y', useNull: true },
        { name: 'template', type: 'string' },

        { name: 'isLandingPage', type: 'boolean' },
        { name: 'link', type: 'string' },
        { name: 'landingPageTeaser', type: 'string' },
        { name: 'seoKeywords', type: 'string' },
        { name: 'seoDescription', type: 'string' },
        { name: 'categoriesNames', type: 'string' },
        { name: 'categories', type: 'array' },
        { name: 'landingPageBlock', type: 'string' }
    ],
    associations: [
        { type: 'hasMany', model: 'Shopware.apps.Emotion.model.EmotionElement', name: 'getElements', associationKey: 'elements'},
        { type: 'hasMany', model: 'Shopware.apps.Emotion.model.Attribute', name: 'getAttributes', associationKey: 'attribute'},
        { type: 'hasMany', model: 'Shopware.apps.Emotion.model.Grid', name: 'getGrid', associationKey: 'grid' }
        //{  type: 'hasMany', model: 'Shopware.apps.Base.model.Category', name: 'getCategory', associationKey: 'category'}
    ],
    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            create: '<?php echo '/backend/Emotion/save';?>',
            update: '<?php echo '/backend/Emotion/save';?>',
            destroy: '<?php echo '/backend/Emotion/delete';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'data',
            totalProperty:'total'
        }
    }

});
//
<?php }} ?>