<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:13
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/store/subshops.js" */ ?>
<?php /*%%SmartyHeaderCode:157075819855447e6d6c2722-82713473%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e2f33b33d12c2e57fd2fa12e0e5cb0d419d54b70' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/premium/store/subshops.js',
      1 => 1430113165,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '157075819855447e6d6c2722-82713473',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e6d6d1802_63359450',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e6d6d1802_63359450')) {function content_55447e6d6d1802_63359450($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Premium
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Premium.store.Subshops', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',
    /**
    * Auto load the store after the component
    * is initialized
    * @boolean
    */
    autoLoad: false,
    /**
    * Amount of data loaded at once
    * @integer
    */
    pageSize: 20,
    /**
     * True to defer any filtering operation to the server
     * @boolean
     */
    remoteFilter: true,
    /**
    * Define the used model for this store
    * @string
    */
    model : 'Shopware.apps.Premium.model.Subshop',

	/**
	 * A config object containing one or more event handlers to be added to this object during initialization
	 * @object
	 */
	listeners: {
		/**
		 * Fires whenever records have been prefetched
		 * used to add some default values to the combobox
		 *
		 * @event load
		 * @param [object] store - Ext.data.Store
		 * @return void
		 */
		load: function(store) {
			var defaultSubShop = Ext.create('Shopware.apps.Premium.model.Subshop',{
				id : 0,
				name : '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'premium'/'subShop'/'comboBox_general','default'=>'Universally valid','namespace'=>'backend/premium/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'premium'/'subShop'/'comboBox_general','default'=>'Universally valid','namespace'=>'backend/premium/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Allgemein gültig<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'premium'/'subShop'/'comboBox_general','default'=>'Universally valid','namespace'=>'backend/premium/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
			});

			store.insert(0,defaultSubShop);
		}
	}
});
//<?php }} ?>