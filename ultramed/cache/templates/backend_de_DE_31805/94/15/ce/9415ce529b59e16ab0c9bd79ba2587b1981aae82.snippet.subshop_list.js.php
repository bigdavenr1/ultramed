<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/subshop_list.js" */ ?>
<?php /*%%SmartyHeaderCode:19877368525541e663d2f738-93562090%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9415ce529b59e16ab0c9bd79ba2587b1981aae82' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/subshop_list.js',
      1 => 1430113391,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19877368525541e663d2f738-93562090',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663d3ee46_59069096',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663d3ee46_59069096')) {function content_5541e663d3ee46_59069096($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Grid for Subshop-Selection
 *
 * todo@all: Documentation
 *
 */
//
Ext.define('Shopware.apps.Payment.view.payment.SubshopList', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.grid.Panel',

    ui: 'shopware-ui',

    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('payment-main-subshoplist')
    * @string
    */
    alias: 'widget.payment-main-subshoplist',
    /**
    * The window uses a border layout, so we need to set
    * a region for the grid panel
    * @string
    */
    region: 'center',

    border: 0,
    /**
    * The view needs to be scrollable
    * @string
    */
    autoScroll: true,

    overflowX: 'hidden',

    /**
     * This function is called, when the component is initiated
     * It creates the columns and the selection-model for the grid and sets the store
     */
    initComponent: function(){
        var me = this;
        me.columns = me.getColumns();
        me.store = Ext.create('Shopware.apps.Base.store.Shop');
        me.store.clearFilter();
        me.selModel = me.getGridSelModel();
        me.callParent(arguments);
    },

    /**
     * This function creates the columns of the grid
     * @return Array
     */
    getColumns: function(){
        var columns =  [{
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_subShopSelection_name','default'=>'Name','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_subShopSelection_name','default'=>'Name','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_subShopSelection_name','default'=>'Name','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            flex: 1,
            dataIndex: 'name'
        }];
        return columns;
    },

    /**
     * This function creates the selection-model for the grid
     * @return Ext.selection.CheckboxModel
     */
    getGridSelModel: function(){
        return Ext.create('Ext.selection.CheckboxModel');
    }
});
//<?php }} ?>