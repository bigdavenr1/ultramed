<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/visitors.js" */ ?>
<?php /*%%SmartyHeaderCode:42440111555434786144213-50334853%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e50d9d798b0fa320e595a25972c6b854c50fa2cd' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/visitors.js',
      1 => 1430113328,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '42440111555434786144213-50334853',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347861505b6_27204887',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347861505b6_27204887')) {function content_554347861505b6_27204887($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Visitors Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.navigation.Visitors', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-navigation-visitors',
    remoteSort: true,
    fields: [
        'name',
        { name: 'datum', type: 'date' },
        { name: 'totalImpressions', type: 'int' },
        { name: 'totalVisits', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics/getVisitors';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    },

    constructor: function (config) {
        var me = this;
        config.fields = me.fields;

        if (config.shopStore) {
            config.shopStore.each(function (shop) {
                config.fields.push('totalVisits' + shop.data.id);
                config.fields.push('totalImpressions' + shop.data.id);
            });
        }

        me.callParent(arguments);
    }
});
<?php }} ?>