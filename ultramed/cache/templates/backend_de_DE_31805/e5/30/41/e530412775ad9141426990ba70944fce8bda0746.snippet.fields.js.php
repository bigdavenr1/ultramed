<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/controller/fields.js" */ ?>
<?php /*%%SmartyHeaderCode:183568983655447b77478585-90609617%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e530412775ad9141426990ba70944fce8bda0746' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/controller/fields.js',
      1 => 1430112890,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '183568983655447b77478585-90609617',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b774cdfc7_29584801',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b774cdfc7_29584801')) {function content_55447b774cdfc7_29584801($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.controller.Fields', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
    extend: 'Ext.app.Controller',

    /**
     * Define references for the different parts of our application. The
     * references are parsed by ExtJS and Getter methods are automatically created.
     *
     * Example: { ref : 'grid', selector : 'grid' } transforms to this.getGrid();
     *          { ref : 'addBtn', selector : 'button[action=add]' } transforms to this.getAddBtn()
     *
     * @array
     */
    refs: [
        { ref: 'fieldgrid', selector: 'form-main-fieldgrid' }
    ],

    /**
     * Creates the necessary event listener for this
     * specific controller
     *
     * @return void
     */
    init: function() {
        var me = this;

        me.control({
            'form-main-fieldgrid dataview': {
                drop: me.onDropField
            },
            'form-main-fieldgrid': {
                beforeedit: me.onBeforeEditField,
                edit: me.onAfterEditField,
                canceledit: me.onCancelEditField,
                validateedit: me.onValidateEditField

            },
            'form-main-fieldgrid actioncolumn': {
                /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
                render: function (view) {
                    view.scope = this;
                    view.handler = this.onDeleteSingleField;
                }
                /*<?php }?>*/
            },

            'form-main-fieldgrid button[action=add]': {
                click: me.onAddField
            }
        });

        me.callParent(arguments);
    },

    /**
     * Event listener which deletes a single field based on the passed
     * grid (e.g. the grid store) and the row index
     *
     * @event render
     * @param [Ext.grid.View] grid - The grid on which the event has been fired
     * @param [integer] rowIndex - On which row position has been clicked
     * @param [integer] colIndex - On which coulmn position has been clicked
     * @param [object] item - The item that has been clicked
     * @return void
     */
    onDeleteSingleField: function(grid, rowIndex, colIndex, item) {
        var store  = grid.getStore(),
            record = store.getAt(rowIndex);

        var message = Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_field_message','default'=>'Are you sure you want to delete the selected field ([0])?','namespace'=>'backend/form/controller/fields')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_field_message','default'=>'Are you sure you want to delete the selected field ([0])?','namespace'=>'backend/form/controller/fields'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Möchten Sie das ausgewählte Feld ([0]) wirklich löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_field_message','default'=>'Are you sure you want to delete the selected field ([0])?','namespace'=>'backend/form/controller/fields'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', record.get('name'));

        Ext.MessageBox.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'dialog_delete_field_title','default'=>'Delete field','namespace'=>'backend/form/controller/fields')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_field_title','default'=>'Delete field','namespace'=>'backend/form/controller/fields'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Feld löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'dialog_delete_field_title','default'=>'Delete field','namespace'=>'backend/form/controller/fields'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', message, function (response) {
            if (response !== 'yes') {
                return false;
            }

            grid.setLoading(true);
            record.destroy({
                callback: function() {
                    store.load();
                    grid.setLoading(false);
                }
            });
        });
    },

    /**
     * Function to add a new field
     *
     * @event click
     * @return void
     */
    onAddField: function() {
        var me = this,
            grid = me.getFieldgrid(),
            editor = grid.editor,
            count = grid.store.count() + 1,
            newField = Ext.create('Shopware.apps.Form.model.Field', {
                position: count
            });

        grid.store.add(newField);
        editor.startEdit(newField, 1);
    },

    /**
     * Saves current positions in the grid to the backend
     *
     * @event drop
     * @param [HTMLElement ] The GridView node if any over which the mouse was positioned.
     * @param [Object] The data object gathered at mousedown time
     * @param [Ext.data.Model]
     * @param [String] "before" or "after" depending on whether the mouse is above or below the midline of the node.
     * @return void
     */
    onDropField: function (node, data, overModel, dropPosition) {
        var store = this.getStore('Field'),
            orderedItems = new Array(),
            index = 0;

        store.each(function(item) {
            orderedItems[index++] = item.data.id;
        });

        // Send current positions to backend
        Ext.Ajax.request({
            url: '<?php echo '/backend/form/changeFieldPosition';?>',
            method: 'POST',
            params: {
                data : JSON.stringify(orderedItems)
            }
        });
    },

    /**
     * Fired after a row is edited and passes validation. This event is fired
     * after the store's update event is fired with this edit.
     *
     * @event edit
     * @param [Ext.grid.plugin.Editing]
     * @param [object] An edit event with the following properties:
     *                 grid - The grid
     *                 record - The record that was edited
     *                 field - The field name that was edited
     *                 value - The value being set
     *                 row - The grid table row
     *                 column - The grid Column defining the column that was edited.
     *                 rowIdx - The row index that was edited
     *                 colIdx - The column index that was edited
     *                 originalValue - The original value for the field, before the edit (only when using CellEditing)
     *                 originalValues - The original values for the field, before the edit (only when using RowEditing)
     *                 newValues - The new values being set (only when using RowEditing)
     *                 view - The grid view (only when using RowEditing)
     *                 store - The grid store (only when using RowEditing)
     * @return void
     */
    onAfterEditField: function(editor, event) {
        var record = event.record,
            view   = editor.grid.getView();

        record.save();

        // enable add button
        view.ownerCt.down('button[action=add]').enable();

        // enable drag&drop when editing is over
        view.getPlugin('my-gridviewdragdrop').enable();
    },

    /**
     * Fires when the user has started editing a row but then cancelled the edit
     *
     * @event canceledit Fires when the user started editing but then cancelled the edit.
     * @param [Ext.grid.plugin.Editing]
     * @param [object] An edit event with the following properties:
     *                 grid - The grid
     *                 record - The record that was edited
     *                 field - The field name that was edited
     *                 value - The value being set
     *                 row - The grid table row
     *                 column - The grid Column defining the column that was edited.
     *                 rowIdx - The row index that was edited
     *                 colIdx - The column index that was edited
     *                 view - The grid view
     *                 store - The grid store
     * @return void
     */
    onCancelEditField: function(editor, event) {
        var grid   = editor.grid,
            record = event.record,
            store  = grid.getStore(),
            view   = grid.getView();

        if (record.phantom) {
            store.remove(record);
        }

        // enable add button
        view.ownerCt.down('button[action=add]').enable();

        // enable drag&drop when editing is canceled
        view.getPlugin('my-gridviewdragdrop').enable();
    },

    /**
     * Disables the add button and the drag and drop handler
     *
     * @event beforeedit
     * @param [Ext.grid.plugin.Editing]
     * @return void
     */
    onBeforeEditField: function(editor) {
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createupdate'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if (!$_tmp2){?>*/
        return false;
        /*<?php }?>*/

        var view = editor.grid.getView();

        // disable add button
        view.ownerCt.down('button[action=add]').disable();

        // disable drag&drop when row editing is started
        view.getPlugin('my-gridviewdragdrop').disable();
    },


    /**
     * Validates the record
     *
     * @event validateedit
     * @param [Ext.grid.plugin.Editing]
     * @param [object] An edit event
     * @return void
     */
    onValidateEditField: function(editor, event) {
        var record = event.record,
            store = event.store,
            newName = event.newValues.name,
            isValid = true;

        store.each(function(item) {
            if (item.internalId === record.internalId) {
                return true;
            }

            if ((newName === item.get('name'))) {
                record.markDirty();
                isValid = false;
            }
        });

        return isValid;
    }
});
//
<?php }} ?>