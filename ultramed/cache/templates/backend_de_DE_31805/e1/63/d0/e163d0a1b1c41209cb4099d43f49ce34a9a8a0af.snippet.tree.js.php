<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/tree.js" */ ?>
<?php /*%%SmartyHeaderCode:117138253355447a968825c9-58333623%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e163d0a1b1c41209cb4099d43f49ce34a9a8a0af' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/tree.js',
      1 => 1430113398,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '117138253355447a968825c9-58333623',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a968ac130_81507905',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a968ac130_81507905')) {function content_55447a968ac130_81507905($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site site Tree View
 *
 * This file contains the layout of the navigation tree.
 */

//

//
Ext.define('Shopware.apps.Site.view.site.Tree', {
	extend: 'Ext.tree.Panel',
	alias : 'widget.site-tree',
    rootVisible: false,
    animate: false,

    initComponent: function() {
        var me = this;
        me.bbar = me.getBottomToolbar();
        me.callParent(arguments);
    },

    getBottomToolbar: function() {
        var buttons = [];
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createGroup'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        buttons.push(Ext.create("Ext.button.Button",{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'treeCreateGroupButton','default'=>'Add group','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'treeCreateGroupButton','default'=>'Add group','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neue Gruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'treeCreateGroupButton','default'=>'Add group','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-blue-folder--plus',
            cls: 'small secondary',
            action: 'onCreateGroup'
        }));
        /*<?php }?>*/
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'deleteGroup'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        buttons.push('->');
        buttons.push(Ext.create("Ext.button.Button",{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'treeDeleteGroupButton','default'=>'Delete group','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'treeDeleteGroupButton','default'=>'Delete group','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gruppe löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'treeDeleteGroupButton','default'=>'Delete group','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'onDeleteGroup',
            cls: 'small secondary',
            iconCls: 'sprite-blue-folder--minus',
            disabled: true
        }));
        /*<?php }?>*/

        return buttons
    }
});
//<?php }} ?>