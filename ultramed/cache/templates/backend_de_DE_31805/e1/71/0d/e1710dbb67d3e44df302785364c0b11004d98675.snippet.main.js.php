<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:43805948355447ea4816b39-90805509%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e1710dbb67d3e44df302785364c0b11004d98675' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/controller/main.js',
      1 => 1430113158,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '43805948355447ea4816b39-90805509',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea4827886_68877594',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea4827886_68877594')) {function content_55447ea4827886_68877594($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - partner main backend module
 *
 * The partner module main controller handles the initialisation of the backend list.
 */
//
Ext.define('Shopware.apps.Partner.controller.Main', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
	extend: 'Ext.app.Controller',

    /**
     * Required sub-controller for this controller
     */
    requires: [
        'Shopware.apps.Partner.controller.Partner',
        'Shopware.apps.Partner.controller.Statistic'
    ],

    mainWindow: null,

    /**
     * Required stores for sub-application
     * @array
     */
    stores:[ 'List', 'Detail'],


    /**
	 * Creates the necessary event listener for this
	 * specific controller and opens a new Ext.window.Window
	 * to display the subapplication
     *
     * @return void
	 */
	init: function() {
        var me = this;
        /** me.subApplication.listStore stores the list data*/
        me.subApplication.listStore =  me.subApplication.getStore('List');
        /** me.subApplication.detailStore stores the detail data*/
        me.subApplication.detailStore =  me.subApplication.getStore('Detail');
        /** me.subApplication.statisticListStore stores the statistic list data for the statistic page*/
        me.subApplication.statisticListStore =  me.subApplication.getStore('StatisticList');
        /** me.subApplication.statisticChartStore stores the statistic chart data for the statistic page*/
        me.subApplication.statisticChartStore =  me.subApplication.getStore('StatisticChart');

        me.subApplication.listStore.load();
        me.mainWindow = me.getView('main.Window').create({
            listStore: me.subApplication.listStore
        });

        me.callParent(arguments);
    }
});
//
<?php }} ?>