<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/view/risk_management/panel.js" */ ?>
<?php /*%%SmartyHeaderCode:163163741555447cd7958938-97375129%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3a7a94f5806fec56780473dbae736927e2a6cc1f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/view/risk_management/panel.js',
      1 => 1430113396,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '163163741555447cd7958938-97375129',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd7b402c2_94146583',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd7b402c2_94146583')) {function content_55447cd7b402c2_94146583($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - RiskManagement view panel
 *
 * This is the main panel, which contains the different fieldsets.
 * Payment-FieldSet: Contains the combobox to choose the payment
 * Risk-FieldSet: Contains all rules and risks
 * Example-FieldSet: Contains example-usages
 */
//
Ext.define('Shopware.apps.RiskManagement.view.risk_management.Panel', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.panel.Panel',
	bodyPadding: 10,


    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('risk_management-main-panel')
    * @string
    */
    alias: 'widget.risk_management-main-panel',
    /**
    * The window uses a border layout, so we need to set
    * a region for the grid panel
    * @string
    */
    region: 'center',
    /**
    * The view needs to be scrollable
    * @string
    */
    autoScroll: true,

    /**
    * Sets up the ui component
    * @return void
    */
    initComponent: function() {
        var me = this;

		me.addEvents('onChangePayment');
		me.items = me.createFieldSets();

        me.callParent(arguments);
    },

	/**
	 * Creates the different fieldSets
	 * @return Array
	 */
	createFieldSets: function(){
		var me = this,
		    fieldSets = [],
			data = me.createData();
		me.paymentFieldSet = Ext.create('Ext.form.FieldSet', {
			title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'paymentFieldSet'/'title','default'=>'Choose payment method','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'paymentFieldSet'/'title','default'=>'Choose payment method','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart wählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'paymentFieldSet'/'title','default'=>'Choose payment method','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			items: [
				{
					xtype: 'combo',
					store: me.paymentStore,
					displayField: "description",
					valueField: 'id',
					emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'paymentFieldSet'/'comboBox'/'emptyText','default'=>'Please select','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'paymentFieldSet'/'comboBox'/'emptyText','default'=>'Please select','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'paymentFieldSet'/'comboBox'/'emptyText','default'=>'Please select','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '<tpl if="this.doHighlight(id)">',
                                '<div class="x-boundlist-item" style="background-color:#f08080;">{description}</div>',
                            '<tpl else>',
                                '<div class="x-boundlist-item">{description}</div>',
                            '</tpl>',
                        '</tpl>',
                        {
                            doHighlight: function(id) {
                                var record = me.paymentStore.findRecord('id', id);
                                return record.getRuleSets().count() > 0;
                            }
                        }
                    ),

					listeners: {
						change: function(comboBox, newValue){
							me.fireEvent('onChangePayment', me, newValue);
						}
					}
				}
			]
		});
		me.riskFieldSet = Ext.create('Ext.form.FieldSet',{
			hidden: true,
			title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'riskFieldSet'/'title','default'=>'Disable payment if','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'riskFieldSet'/'title','default'=>'Disable payment if','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sperre Zahlungsart WENN<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'riskFieldSet'/'title','default'=>'Disable payment if','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
		});

		me.exampleFieldSet = Ext.create('Ext.form.FieldSet',{
			hidden: true,
            cls: Ext.baseCSSPrefix + 'example-table',
			title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'title','default'=>'Examples','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'title','default'=>'Examples','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beispiele<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'title','default'=>'Examples','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			items: [{
				xtype: 'container',
				html: me.createHtml(data)
			}]
		});

		fieldSets.push(me.paymentFieldSet);
		fieldSets.push(me.riskFieldSet);
		fieldSets.push(me.exampleFieldSet);

		return fieldSets;
	},

	/**
	 * This function creates the dynamic HTML-code
	 * @param datas - Contains the array with all values for the example-fieldSet
	 * @return String
	 */
	createHtml: function(datas){
		var html = "<table>";
		Ext.each(datas, function(data){
			html = html + "<tr>";
			Ext.each(data, function(item){
				html = html + "<td>" + item + "</td>"
			});
			html = html + "</tr>";
		});
		html = html + "</table>";

		return html;
	},

	/**
	 * This function is used to create some arrays, which contain all data for the example-fieldSet
	 * @return Array
	 */
	createData: function(){
		var rules = [],
			syntax = [],
			example = [],
			data = [];

		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'rule','default'=>'Rule','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'rule','default'=>'Rule','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Regel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'rule','default'=>'Rule','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'orderValueGt','default'=>'Order value >=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderValueGt','default'=>'Order value >=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellwert >=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderValueGt','default'=>'Order value >=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'orderValueLt','default'=>'Order value <=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderValueLt','default'=>'Order value <=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellwert <=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderValueLt','default'=>'Order value <=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'customerGroupIs','default'=>'Customer group IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerGroupIs','default'=>'Customer group IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppe IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerGroupIs','default'=>'Customer group IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'customerGroupIsNot','default'=>'Customer group IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerGroupIsNot','default'=>'Customer group IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppe IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerGroupIsNot','default'=>'Customer group IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunde IST NEU<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zone IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zone IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'orderPositionsGt','default'=>'Order positions >=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderPositionsGt','default'=>'Order positions >=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestell-Positionen >=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'orderPositionsGt','default'=>'Order positions >=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'dunninglevelone','default'=>'Dunning level one IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunninglevelone','default'=>'Dunning level one IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 1 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunninglevelone','default'=>'Dunning level one IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'dunningleveltwo','default'=>'Dunning level two IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunningleveltwo','default'=>'Dunning level two IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 2 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunningleveltwo','default'=>'Dunning level two IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'dunninglevelthree','default'=>'Dunning level three IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunninglevelthree','default'=>'Dunning level three IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 3 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'dunninglevelthree','default'=>'Dunning level three IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inkasso IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine Bestellung vor mind. X Tagen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzahl Bestellungen <=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel aus Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'zipCodeIs','default'=>'Zip code IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zipCodeIs','default'=>'Zip code IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Postleitzahl IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'zipCodeIs','default'=>'Zip code IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'streetNameContains','default'=>'Street name CONTAINS X','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'streetNameContains','default'=>'Street name CONTAINS X','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Straßenname ENTHÄLT X<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'streetNameContains','default'=>'Street name CONTAINS X','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'customerNumberIs','default'=>'Customer number IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerNumberIs','default'=>'Customer number IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundennummer IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'customerNumberIs','default'=>'Customer number IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'lastNameContains','default'=>'Last name CONTAINS X','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'lastNameContains','default'=>'Last name CONTAINS X','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nachname ENTHÄLT X<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'lastNameContains','default'=>'Last name CONTAINS X','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Lieferadresse != Rechnungsadresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Attribut IST (1>5)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		rules.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Attribut IST NICHT (1>5)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'rules'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');


		syntax.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'syntax','default'=>'Syntax','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'syntax','default'=>'Syntax','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Syntax<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'syntax','default'=>'Syntax','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
ID der Kundengruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
ID der Kundengruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'customerGroupId','default'=>'ID of the customer group','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'oneOrZero','default'=>'1 or 0','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'oneOrZero','default'=>'1 or 0','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 oder 0<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'oneOrZero','default'=>'1 or 0','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
deutschland, europa, welt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
deutschland, europa, welt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Länder-Iso<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Länder-Iso<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'countryIso','default'=>'Country-Iso','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('');
		syntax.push('');
		syntax.push('');
		syntax.push('');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'categoryId','default'=>'ID of the category','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'categoryId','default'=>'ID of the category','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
ID der Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'categoryId','default'=>'ID of the category','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'streetName','default'=>'Street name or a part of the name','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'streetName','default'=>'Street name or a part of the name','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Begriff oder Teilbegriff<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'streetName','default'=>'Street name or a part of the name','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wert numerisch<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'numericalValue','default'=>'Value numerical','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'lastName','default'=>'Last name','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'lastName','default'=>'Last name','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nachname<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'lastName','default'=>'Last name','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name des Shops<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name des Shops<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'subShopId','default'=>'Name of the shop','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Lieferadresse != Rechnungsadresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'currencyIso','default'=>'Currency-Iso','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
attr*1-20*|5<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		syntax.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
attr*1-20*|5<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'syntax'/'attributes','default'=>'attr*1-20*|5','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');


		example.push('<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'example','default'=>'Example','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'example','default'=>'Example','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beispiel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'example','default'=>'Example','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 </b>');
		example.push('500.50');
		example.push('500.50');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
EK für Shopkunden, H für Händler<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
EK für Shopkunden, H für Händler<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'customerGroupId','default'=>'EK for customers, H for traders','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('1');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
deutschland, europa, welt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
deutschland, europa, welt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'zone','default'=>'germany, europe, world','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
z.B. DE für Deutschland<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
z.B. DE für Deutschland<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'countryIso','default'=>'e.g. DE for germany','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('5');
		example.push('');
		example.push('');
		example.push('');
		example.push('');
		example.push('30');
		example.push('30');
		example.push('3');
		example.push('48624');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'streetName','default'=>'e.g. \\\'delivery\\\', disables the payment for every address, which contains \\\'delivery\\\'','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'streetName','default'=>'e.g. \\\'delivery\\\', disables the payment for every address, which contains \\\'delivery\\\'','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
z.B. \'Packstation\', sperrt die Zahlungsarten für alle Adressen in denen \'Packstation\' vorkommt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'streetName','default'=>'e.g. \\\'delivery\\\', disables the payment for every address, which contains \\\'delivery\\\'','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('12345');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'lastName','default'=>'Smith','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'lastName','default'=>'Smith','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mustermann<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'lastName','default'=>'Smith','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push("<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
\'Deutsch\', falls in den Grundeinstellungen ein Shop mit diesem Namen definiert wurde.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
");
		example.push("<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
\'Deutsch\', falls in den Grundeinstellungen ein Shop mit diesem Namen definiert wurde.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'subShopId','default'=>'\'English\' if there was defined such a shop in the basic settings','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
");
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Lieferadresse != Rechnungsadresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'EUR','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'EUR','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
EUR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'EUR','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'USD','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'USD','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
EUR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'currencyIso','default'=>'USD','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
attr5|2 Zahlart sperren wenn Artikel im Warenkorb mit attr5 = 2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
		example.push('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Do not disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Do not disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
attr5|2 Zahlart sperren wenn Artikel im Warenkorb mit attr5 = 2<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'exampleFieldSet'/'example'/'attribute','default'=>'attr5|2 Do not disable payment if there are any articles in the basket with attr5 = 2','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');

		//Creates an array like this:
		//[0] => One item of each array rules/syntax/example
		for(var i=0; i<example.length; i++){
			data[i] = [rules[i], syntax[i], example[i]]
		}
		return data;
	}
});
//<?php }} ?>