<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/grid.js" */ ?>
<?php /*%%SmartyHeaderCode:88195907655447ce390ffa9-22700036%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c0cff4e33de475f329b55ce7fb2b34e1007e387' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/grid.js',
      1 => 1430111764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88195907655447ce390ffa9-22700036',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce39ef498_47416654',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce39ef498_47416654')) {function content_55447ce39ef498_47416654($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.manager.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.plugin-manager-manager-grid',
    border: 0,
    cls: Ext.baseCSSPrefix + 'plugin-manager-manager-grid',

    /**
     * Snippets for the component.
     * @object
     */
    snippets: {
		plugin_name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'plugin_name','default'=>'Plugin name','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'plugin_name','default'=>'Plugin name','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin-Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'plugin_name','default'=>'Plugin name','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		supplier: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'supplier','default'=>'Supplier','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'supplier','default'=>'Supplier','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hersteller<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'supplier','default'=>'Supplier','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		license: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'license','default'=>'License','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'license','default'=>'License','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Lizenz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'license','default'=>'License','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		version: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Version<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'version','default'=>'Version','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		added: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinzugefügt am<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'added','default'=>'Added on','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		active: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'active','default'=>'Active','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		inactive: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'inactive','default'=>'Inactive','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'inactive','default'=>'Inactive','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inaktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'inactive','default'=>'Inactive','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		actions: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'actions','default'=>'Action(s)','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'actions','default'=>'Action(s)','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktion(en)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'actions','default'=>'Action(s)','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		edit_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'edit_plugin','default'=>'Edit plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'edit_plugin','default'=>'Edit plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin editieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'edit_plugin','default'=>'Edit plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		install_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin installieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'install_plugin','default'=>'Install plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		install_uninstall_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'install_uninstall_plugin','default'=>'Install / uninstall plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'install_uninstall_plugin','default'=>'Install / uninstall plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin installieren / deinstallieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'install_uninstall_plugin','default'=>'Install / uninstall plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		delete_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'delete_plugin','default'=>'Delete plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'delete_plugin','default'=>'Delete plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'delete_plugin','default'=>'Delete plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		update_plugin_info: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'update_plugin_info','default'=>'Update plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'update_plugin_info','default'=>'Update plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin aktualisieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'update_plugin_info','default'=>'Update plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		reinstall_info: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'reinstall_info','default'=>'Reinstall plugin (Uninstall -> Install)','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'reinstall_info','default'=>'Reinstall plugin (Uninstall -> Install)','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erneut installieren (Deinstallieren -> Installieren)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'reinstall_info','default'=>'Reinstall plugin (Uninstall -> Install)','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		manual_add_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'manual_add','default'=>'Add plugin manually','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'manual_add','default'=>'Add plugin manually','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin manuell hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'manual_add','default'=>'Add plugin manually','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		search: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'search','default'=>'Search...','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'search','default'=>'Search...','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'search','default'=>'Search...','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		bought: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'bought','default'=>'Bought','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'bought','default'=>'Bought','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bought<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'bought','default'=>'Bought','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		rented: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'rented','default'=>'Rented','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'rented','default'=>'Rented','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gemietet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'rented','default'=>'Rented','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		tested: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'tested','default'=>'Tested','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'tested','default'=>'Tested','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Getestet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'tested','default'=>'Tested','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		days_left: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'days_left','default'=>'([0] days left)','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'days_left','default'=>'([0] days left)','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
(noch [0] Tage)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'days_left','default'=>'([0] days left)','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		active_plugins: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'active_plugins','default'=>'Active plugins','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'active_plugins','default'=>'Active plugins','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktivierte Plugins<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'active_plugins','default'=>'Active plugins','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		inactive_plugins: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'grid'/'inactive_plugins','default'=>'Inactive plugins','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'inactive_plugins','default'=>'Inactive plugins','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inaktive Plugins<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'grid'/'inactive_plugins','default'=>'Inactive plugins','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addAdditionalEvents();
        me.groupingPlugin = me.createGrouping();

        me.features = [ me.groupingPlugin ];
        me.store = me.pluginStore;
        me.columns = me.createColumns();
        me.tbar = me.createActionToolbar();
        me.bbar = me.createPagingToolbar();
        me.plugins = [
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1
            })
        ];

        me.callParent(arguments);
    },

    /**
     * Adds additional events for the component.
     *
     * @public
     * @return void
     */
    addAdditionalEvents: function() {
        var me = this;

        me.addEvents(
            'search',
            'uninstallInstall',
            'reinstallPlugin',
            'editPlugin',
            'manualInstall',
            'selectionChange',
            'updatePluginInfo',
            'deleteplugin',
            'updateDummyPlugin'
        );
    },

    /**
     * Creates the grid column model for the grid panel.
     *
     * @public
     * @return Array - computed columns
     */
    createColumns: function() {
        var me = this;

        return [{
            dataIndex: 'label',
            header: me.snippets.plugin_name,
            flex: 2,
            renderer: me.pluginNameRenderer
        }, {
            dataIndex: 'author',
            header: me.snippets.supplier,
            flex: 1,
            renderer: me.merchantRenderer
        }, {
            dataIndex: 'license',
            header: me.snippets.license,
            flex: 1,
            renderer: me.licenseRenderer
        }, {
            dataIndex: 'version',
            header: me.snippets.version,
            width: 50,
            renderer: me.versionRenderer
        }, {
            dataIndex: 'added',
            xtype: 'datecolumn',
            header: me.snippets.added,
            flex: 1
        }, {
            dataIndex: 'active',
            header: me.snippets.active,
            width: 70,
            xtype: 'booleancolumn',
            trueText: me.snippets.active,
            falseText: me.snippets.inactive,
            editor: {
                xtype: 'checkbox',
                allowBlank: false
            }
        }, {
            xtype: 'actioncolumn',
            header: me.snippets.actions,
            width: 90,
            items: [
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
            {
                iconCls: 'sprite-plus-circle',
                tooltip: me.snippets.install_plugin,

                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    me.fireEvent('updateDummyPlugin', grid, rowIndex, colIndex, item, eOpts, record);
                },

                getClass: function(value, metadata, record, rowIdx) {
                    if (!record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            },
        /*<?php }?>*/


        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
			{
                iconCls: 'sprite-pencil',
                tooltip: me.snippets.edit_plugin,
                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    me.fireEvent('editPlugin', grid, rowIndex, colIndex, item, eOpts, record);
                },

                getClass: function(value, metaData, record) {
                    if (record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                    if(record.get('installed') == null) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            },
        /*<?php }?>*/
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?>*/
			{
                iconCls: 'sprite-minus-circle',
                tooltip: me.snippets.install_uninstall_plugin,
                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    if (record.get('updateVersion')) {
                        record.set('version', record.get('updateVersion'));
                    }
                    me.fireEvent('uninstallInstall', grid, item, eOpts, record);
                },

                getClass: function(value, metadata, record, rowIdx) {
                    if (record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                    if (!record.get('capabilityInstall')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                    if (record.get('installed') == null)  {
                        return 'sprite-plus-circle';
                    }
                }
            },
        /*<?php }?>*/
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?>*/
			{
                iconCls: 'sprite-bin-metal-full',
                tooltip: me.snippets.delete_plugin,
                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    me.fireEvent('deleteplugin', grid, rowIndex, colIndex, item, eOpts, record);
                },

                getClass: function(value, metadata, record, rowIdx) {
                    if (record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                   if (record.get('installed') != null || record.get('source') == 'Default')  {
                       return Ext.baseCSSPrefix + 'hidden';
                   }
               }
            },
            {
                iconCls: 'sprite-arrow-circle-135',
                tooltip: me.snippets.update_plugin_info,
                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    record.set('version', record.get('updateVersion'));
                    me.fireEvent('updatePluginInfo', record, me.pluginStore);
                },
                getClass: function(value, metadata, record, rowIdx) {
                    if (record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                    if (record.get('updateVersion') == null) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            },
            {
                iconCls: 'sprite-arrow-continue',
                tooltip: me.snippets.reinstall_info,
                handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                    me.fireEvent('reinstallPlugin', record, me);
                },
                getClass: function(value, metadata, record, rowIdx) {
                    if (record.get('capabilityDummy')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                    if (!record.get('capabilityInstall')) {
                        return Ext.baseCSSPrefix + 'hidden';
                    }

                    if (!record.get('installed'))  {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            },
        /*<?php }?>*/
        ]
        }];
    },

    /**
     * Creates the action toolbar which is located above the
     * grid panel.
     *
     * @public
     * @return [object] Ext.toolbar.Toolbar
     */
    createActionToolbar: function() {
        var me = this;

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp5=ob_get_clean();?><?php if ($_tmp5){?>*/
        me.manualInstallBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.manual_add_plugin,
            iconCls: 'sprite-plus-circle',
            handler: function(button) {
                me.fireEvent('manualInstall', button);
            }
        });
        /*<?php }?>*/

        me.searchField = Ext.create('Ext.form.field.Text', {
            cls: 'searchfield',
            emptyText: me.snippets.search,
            enableKeyEvents:true,
            checkChangeBuffer: 500,
            width: 170,
            listeners: {
                scope: me,
                change: function(field, value, oldValue, event, eOpts) {
                    me.fireEvent('search', field, value, event, eOpts);
                }
            }
        });

        return Ext.create('Ext.toolbar.Toolbar', {
            ui: 'shopware-ui',
            items: [
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'install'),$_smarty_tpl);?>
<?php $_tmp6=ob_get_clean();?><?php if ($_tmp6){?>*/
				me.manualInstallBtn,
        /*<?php }?>*/
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'read'),$_smarty_tpl);?>
<?php $_tmp7=ob_get_clean();?><?php if ($_tmp7){?>*/
				'->', me.searchField, ' ' ]
        /*<?php }?>*/
		});
    },

    /**
     * Creates the paging toolbar which is located under the
     * grid panel.
     *
     * @public
     * @return [object] Ext.toolbar.Paging
     */
    createPagingToolbar: function() {
        var me = this;

        return Ext.create('Ext.toolbar.Paging', {
            store: me.store
        });
    },

    /**
     * Creates the grouping feature of the grid panel.
     *
     * @oublic
     * @return [object] Ext.grid.feature.Grouping
     */
    createGrouping: function() {
        var me = this;

        return Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: new Ext.XTemplate(
                '{name:this.formatName} ({rows.length} Plugins)',
                {
                    formatName: function(name) {
                        if(name == me.snippets.active) {
                            name = me.snippets.active_plugins;
                        } else {
                            name = me.snippets.inactive_plugins;
                        }
                        return name;
                    }
                }
            )
        });
    },

    /**
     * Render method which formats / converts the name of the plugin name
     * and adds the icon to the row.
     *
     * @public
     * @param [string] value - value of the current row
     * @param [string] meta - additional meta data of the row
     * @param [object] record - Shopware.apps.PluginManager.model.Plugin of the current row
     * @return [string] formatted / converted plugin name
     */
    pluginNameRenderer: function(value, meta, record) {
        var iconPath = record.get('icon'), iconEl, license = '', me = this;

        if(record.get('active') === false) {
            meta.style = 'opacity: 0.65';
        } else {
            meta.style = 'opacity: 1';
        }
        if (record && record.getLicense() instanceof Ext.data.Store && record.getLicense().first() instanceof Ext.data.Model) {
            var licenseModel = record.getLicense().first();
            switch(licenseModel.get('type')) {
                case 2:
                case 3:
                    var expiration = licenseModel.get('expiration');
                    var today = new Date();
                    if (Ext.isDate(expiration)) {
                        var days = Math.ceil((expiration.getTime()-today.getTime())/(86400000))
                        license = '<span style="color: #999; font-weight: bold;"> '+Ext.String.format(me.snippets.days_left, days)+'</span>'
                    }
                    break;
            }
        }

        if(iconPath) {
            iconEl = '<div style="display: inline-block;margin-right: 4px;width:16px;height:16px;background:url('+ iconPath +') no-repeat"></div>';
        } else {
            iconEl = '<div class="sprite-puzzle" style="display: inline-block;margin-right: 4px;"></div>';
        }

        return iconEl + '<strong>' + value + '</strong>' + license;
    },

    /**
     * Render method which formats / converts the merchant of the plugin.
     *
     * @public
     * @param [string] value - value of the current row
     * @param [string] meta - additional meta data of the row
     * @param [object] record - Shopware.apps.PluginManager.model.Plugin of the current row
     * @return [string] formatted / converted plugin merchant
     */
    merchantRenderer: function(value, meta, record) {
        var link = record.get('link')

        if(link.length) {
            return '<a href="'+link+'" target="_blank">' + value + '</a>';
        } else {
            return value;
        }
    },

    /**
     * Render method which formats / converts the merchant of the license.
     *
     * @public
     * @param [string] value - value of the current row
     * @param [string] meta - additional meta data of the row
     * @param [object] record - Shopware.apps.PluginManager.model.Plugin of the current row
     * @return [string] formatted / converted plugin license
     */
    licenseRenderer: function(value, meta, record) {
		var me = this;
        if (record && record.getLicense() instanceof Ext.data.Store && record.getLicense().first() instanceof Ext.data.Model) {
            var licenseModel = record.getLicense().first();
            switch (licenseModel.get('type')) {
                case 1:
                    return me.snippets.bought;
                    break;
                case 2:
                    return me.snippets.rented;
                    break;
                case 3:
                    return me.snippets.tested;
                    break;
            }
        } else {
            return '';
        }
    },

    /**
     * Renderer function for the version column.
     * @param value
     * @param meta
     * @param record
     */
    versionRenderer: function(value, meta, record) {
        var me = this, fragments, i;
        value += '';

        fragments = value.split('.');
        for(i = 0; i < 3; i++) {
            if (fragments.length < 3) {
                fragments.push('0');
            }
        }
        return fragments.join('.');
    }

});
//
<?php }} ?>