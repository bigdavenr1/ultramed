<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:25:22
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/article_list/store/list.js" */ ?>
<?php /*%%SmartyHeaderCode:4368211375541e6f2d23b71-17488540%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9193baee19f8ab8aecd894d2ebdaed8bda3fa38' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/article_list/store/list.js',
      1 => 1430112853,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4368211375541e6f2d23b71-17488540',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e6f2d2a010_56728653',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e6f2d2a010_56728653')) {function content_5541e6f2d2a010_56728653($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ArticleList
 * @subpackage Article
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * shopware AG (c) 2012. All rights reserved.
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.ArticleList.store.List', {
    extend: 'Ext.data.Store',
    autoLoad: false,
    model : 'Shopware.apps.ArticleList.model.List',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 40
});
//
<?php }} ?>