<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/manufacturer_type.js" */ ?>
<?php /*%%SmartyHeaderCode:1695660863554346113ebb18-40655832%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a9dde3f724c2777551b699d760dbd2912bf944fa' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/fields/manufacturer_type.js',
      1 => 1430113599,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1695660863554346113ebb18-40655832',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461141bb75_40301193',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461141bb75_40301193')) {function content_5543461141bb75_40301193($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
Ext.define('Shopware.apps.Emotion.view.components.fields.ManufacturerType', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.emotion-components-fields-manufacturer-type',
    name: 'manufacturer_type',

    /**
     * Snippets for the component
     * @object
     */
    snippets: {
        fields: {
            'manufacturer_type': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manufacturer_type'/'fields'/'manufacturer_type','default'=>'Manufacturer type','namespace'=>'backend/emotion/view/components/manufacturer_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'fields'/'manufacturer_type','default'=>'Manufacturer type','namespace'=>'backend/emotion/view/components/manufacturer_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Herstellertyp<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'fields'/'manufacturer_type','default'=>'Manufacturer type','namespace'=>'backend/emotion/view/components/manufacturer_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'empty_text': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manufacturer_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/manufacturer_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/manufacturer_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte wählen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'fields'/'empty_text','default'=>'Please select...','namespace'=>'backend/emotion/view/components/manufacturer_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        store: {
            'manufacturers_by_cat': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manufacturer_type'/'store'/'manufacturer_by_cat','default'=>'Manufacturer by category','namespace'=>'backend/emotion/view/components/manufacturer_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'store'/'manufacturer_by_cat','default'=>'Manufacturer by category','namespace'=>'backend/emotion/view/components/manufacturer_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hersteller einer Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'store'/'manufacturer_by_cat','default'=>'Manufacturer by category','namespace'=>'backend/emotion/view/components/manufacturer_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            'selected_manufacturers': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manufacturer_type'/'store'/'selected_manufacturers','default'=>'Selected manufacturers','namespace'=>'backend/emotion/view/components/manufacturer_type')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'store'/'selected_manufacturers','default'=>'Selected manufacturers','namespace'=>'backend/emotion/view/components/manufacturer_type'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgewählte Hersteller<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manufacturer_type'/'store'/'selected_manufacturers','default'=>'Selected manufacturers','namespace'=>'backend/emotion/view/components/manufacturer_type'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        Ext.apply(me, {
            emptyText: me.snippets.fields.empty_text,
            fieldLabel: me.snippets.fields.manufacturer_type,
            displayField: 'display',
            valueField: 'value',
            queryMode: 'local',
            triggerAction: 'all',
            store: me.createStore()
        });

        me.callParent(arguments);
    },

    /**
     * Creates a local store which will be used
     * for the combo box. We don't need that data.
     *
     * @public
     * @return [object] Ext.data.Store
     */
    createStore: function() {
        var me = this, snippets = me.snippets.store;

        return Ext.create('Ext.data.JsonStore', {
            fields: [ 'value', 'display' ],
            data: [{
                value: 'manufacturers_by_cat',
                display: snippets.manufacturers_by_cat
            }, {
                value: 'selected_manufacturers',
                display: snippets.selected_manufacturers
            }]
        });
    }
});<?php }} ?>