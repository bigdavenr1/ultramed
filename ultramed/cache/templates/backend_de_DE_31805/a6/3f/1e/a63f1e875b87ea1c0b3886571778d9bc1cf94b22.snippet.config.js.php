<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/model/config.js" */ ?>
<?php /*%%SmartyHeaderCode:186806743455447cb10cb621-97287401%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a63f1e875b87ea1c0b3886571778d9bc1cf94b22' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/model/config.js',
      1 => 1430113179,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '186806743455447cb10cb621-97287401',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb10ff535_90723850',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb10ff535_90723850')) {function content_55447cb10ff535_90723850($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Config list systeminfo backend module.
 *
 * This model represents a single shopware-config
 */

//
Ext.define('Shopware.apps.Systeminfo.model.Config', {

    /**
    * Extends the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Model',

    fields: [
		//
		'name', 'required', 'version', 'result', 'group'],
    /**
    * Configure the data communication
    * @object
    */
    proxy: {
        type: 'ajax',
        api: {
            //read out all configs
            read: '<?php echo '/backend/systeminfo/getConfigList';?>'
        },
        /**
        * Configure the data reader
        * @object
        */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//<?php }} ?>