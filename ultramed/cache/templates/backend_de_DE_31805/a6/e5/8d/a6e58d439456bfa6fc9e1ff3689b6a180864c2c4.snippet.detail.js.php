<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:126082039155447e756f2490-31570970%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6e58d439456bfa6fc9e1ff3689b6a180864c2c4' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/detail.js',
      1 => 1430113393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '126082039155447e756f2490-31570970',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75810cc6_55864533',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75810cc6_55864533')) {function content_55447e75810cc6_55864533($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
/**
 * Shopware UI - product feed detail main window.
 *
 * Displays all Detail product feed Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.Detail', {
    extend:'Ext.container.Container',
    alias:'widget.product_feed-feed-detail',
    border: 0,
    bodyPadding: 10,
    layout: 'column',
    autoScroll:true,
    defaults: {
        columnWidth: 0.5
    },
    //Text for the ModusCombobox
    variantExportData:[
        [1, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'variant_export_data'/'no','default'=>'No','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'variant_export_data'/'no','default'=>'No','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nein<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'variant_export_data'/'no','default'=>'No','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [2, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'variant_export_data'/'variant','default'=>'Variants','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'variant_export_data'/'variant','default'=>'Variants','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Varianten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'variant_export_data'/'variant','default'=>'Variants','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
']
    ],

    /**
     * Initialize the Shopware.apps.ProductFeed.view.feed.detail and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.items = [ me.createGeneralFormLeft(), me.createGeneralFormRight() ];

        me.callParent(arguments);
    },
    /**
     * creates all fields for the general form on the left side
     */
    createGeneralFormLeft:function () {
        var me = this;

        return Ext.create('Ext.container.Container', {
            layout: 'anchor',
            style: 'padding: 0 10px 0 0',
            defaults:{
                anchor:'100%',
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items:[
                {
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'title','default'=>'Title','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'title','default'=>'Title','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Titel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'title','default'=>'Title','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name:'name',
                    allowBlank:false,
                    required:true,
                    enableKeyEvents:true
                },
                {
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'file_name','default'=>'File name','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'file_name','default'=>'File name','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Dateiname<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'file_name','default'=>'File name','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name:'fileName',
                    allowBlank:false,
                    required:true,
                    enableKeyEvents:true
                },
                {
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'partner_id','default'=>'Partner ID','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'partner_id','default'=>'Partner ID','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner ID<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'partner_id','default'=>'Partner ID','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name:'partnerId',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'partner_id'/'help','default'=>'The partner ID will be attached to the corresponding link. So when a customer will buy an article the direct connection to the partner is saved. ','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'partner_id'/'help','default'=>'The partner ID will be attached to the corresponding link. So when a customer will buy an article the direct connection to the partner is saved. ','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Partner-ID wird an den jeweiligen Artikellink zum Shop angehängt. Somit entsteht beim Kauf des Artikels die Zuordnung zu einem Partner. Dies wird ebenfalls in den Statistiken berücksichtigt und macht Verkäufe zudem transparenter. <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'partner_id'/'help','default'=>'The partner ID will be attached to the corresponding link. So when a customer will buy an article the direct connection to the partner is saved. ','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    enableKeyEvents:true
                },
                {
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'hash','default'=>'Hash','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'hash','default'=>'Hash','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zugriffsschlüssel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'hash','default'=>'Hash','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    name:'hash',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'hash'/'help','default'=>'The Hash will be generated automatically. This value is shown in the URL of the generated product feed file. If you change this value the price portal is not able to access the feed.','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'hash'/'help','default'=>'The Hash will be generated automatically. This value is shown in the URL of the generated product feed file. If you change this value the price portal is not able to access the feed.','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der Zugriffschlüssel wird beim Anlegen eines neuen Produkt-Exports automatisch generiert. Dieser Schlüssel wird in die URL zur XML/CSV Datei eingebunden. Durch Ändern dieses Zugriffsschlüssels kann das Preisportal nicht mehr auf die Datei zugreifen. <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'hash'/'help','default'=>'The Hash will be generated automatically. This value is shown in the URL of the generated product feed file. If you change this value the price portal is not able to access the feed.','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    enableKeyEvents:true
                },
                {
                    xtype:'checkbox',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    inputValue:1,
                    uncheckedValue:0,
                    name:'active'
                },
                {
                    xtype: 'base-element-interval',
                    name: 'interval',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'help','default'=>'When to refresh feed cache. <br>- Only cron: cache is only refreshed by cron<br>- None: new feed is generated with each request','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'help','default'=>'When to refresh feed cache. <br>- Only cron: cache is only refreshed by cron<br>- None: new feed is generated with each request','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Caching kann auf 3 Arten erfolgen. <br>- Nur Cronjob: Der Feed wird nur vom Cronjob erstellt / aktualisiert<br>- Live: Der Feed wird mit jedem Aufruf neu erzeugt<br>- Intervall: Der Feed wird nach dem eingestellten Intervall aktualisiert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'help','default'=>'When to refresh feed cache. <br>- Only cron: cache is only refreshed by cron<br>- None: new feed is generated with each request','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    allowBlank: false,
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval','default'=>'Caching interval','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval','default'=>'Caching interval','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cache-Zeit / Methode<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval','default'=>'Caching interval','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: [
                        [-1, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'onlyCron','default'=>'Only cron','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'onlyCron','default'=>'Only cron','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nur cron<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'onlyCron','default'=>'Only cron','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [0, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'empty_value','default'=>'Live','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'empty_value','default'=>'Live','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Live<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'empty_value','default'=>'Live','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [120, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'2_minutes','default'=>'2 Minutes (120 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_minutes','default'=>'2 Minutes (120 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Minuten (120 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_minutes','default'=>'2 Minutes (120 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [300, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'5_minutes','default'=>'5 Minutes (300 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'5_minutes','default'=>'5 Minutes (300 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
5 Minuten (300 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'5_minutes','default'=>'5 Minutes (300 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [600, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'10_minutes','default'=>'10 Minutes (600 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'10_minutes','default'=>'10 Minutes (600 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
10 Minuten (600 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'10_minutes','default'=>'10 Minutes (600 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [900, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'15_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'15_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
15 Minuten (900 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'15_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [1800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'30_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'30_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
30 Minuten (1800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'30_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [3600, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'1_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Stunde (3600 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [7200, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'2_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Stunden (7200 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [14400, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'4_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'4_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
4 Stunden (14400 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'4_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [28800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'12_hours','default'=>'12 Hours (28800 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'12_hours','default'=>'12 Hours (28800 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
12 Stunden (28800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'12_hours','default'=>'12 Hours (28800 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [86400, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'1_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Tag (86400 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [172800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'2_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Tage (172800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'2_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
                        [604800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'interval'/'1_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Woche (604800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'interval'/'1_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
']
                    ]
                },
                {
                    name:'lastExport',
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'lastExport','default'=>'Last export','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'lastExport','default'=>'Last export','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Letzter Export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'lastExport','default'=>'Last export','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    xtype: 'displayfield',
                    renderer: function(value) {
                        if ( value === Ext.undefined ) {
                            return value;
                        }

                        return Ext.util.Format.date(value) + ' ' + Ext.util.Format.date(value, timeFormat);
                    }
                }
            ]
        });
    },

    /**
     * creates all fields for the general form on the right side
     */
    createGeneralFormRight: function () {
        var me = this;

        var currencyStore = Ext.create('Shopware.apps.Base.store.Currency').load();
        return Ext.create('Ext.container.Container', {
            layout: 'anchor',
            style: 'padding: 0 0 0 10px',
            defaults:{
                anchor:'100%',
                labelStyle:'font-weight: 700;',
                xtype:'combobox'
            },
            items:[
                {
                    name:'languageId',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'shop','default'=>'Shop','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'shop','default'=>'Shop','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'shop','default'=>'Shop','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: me.shopStore.load(),
                    valueField: 'id',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'language_id'/'help','default'=>'The export language','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'language_id'/'help','default'=>'The export language','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bei Subshop/Mehrsprachfähigkeit zu exportierende Sprache/Bezeichung. Hat ebenfalls Auswirkungen auf die Linkgenerierung.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'language_id'/'help','default'=>'The export language','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    displayField: 'name',
                    editable:false
                },
                {
                    name:'customerGroupId',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'customer_group','default'=>'Customer group','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customer_group','default'=>'Customer group','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customer_group','default'=>'Customer group','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: Ext.create('Shopware.store.CustomerGroup').load(),
                    valueField:'id',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'customergroup'/'help','default'=>'Defines the customer group the prices are taken out of','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customergroup'/'help','default'=>'Defines the customer group the prices are taken out of','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Definiert die Kundengruppe, aus der die Preise entnommen werden (default: Shopkunden)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customergroup'/'help','default'=>'Defines the customer group the prices are taken out of','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    displayField:'name'
                },
                {
                    name:'currencyId',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'currency','default'=>'Currency','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'currency','default'=>'Currency','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'currency','default'=>'Currency','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: currencyStore,
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'currency'/'help','default'=>'The export is based on the selected currency','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'currency'/'help','default'=>'The export is based on the selected currency','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Export in den verfügbaren Währungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'currency'/'help','default'=>'The export is based on the selected currency','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    valueField: 'id',
                    displayField: 'name'
                },
                {
                    xtype:'combotree',
                    name:'categoryId',
                    valueField: 'id',
                    forceSelection: false,
                    editable: true,
                    displayField: 'name',
                    treeField: 'categoryId',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'category','default'=>'Category','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'category','default'=>'Category','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'category','default'=>'Category','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'category'/'help','default'=>'This will execute the export for the selected category only','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'category'/'help','default'=>'This will execute the export for the selected category only','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hiermit ist ein Export für eine bestimmte Kategorie möglich. Bei Subshop wird hier für einen gesamten Export die Hauptkategorie des jeweiligen Shops ausgewählt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'category'/'help','default'=>'This will execute the export for the selected category only','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store: me.comboTreeCategoryStore,
                    selectedRecord : me.record
                },
                {
                    xtype:'combobox',
                    name:'variantExport',
                    fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'variantExport','default'=>'Export variants','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'variantExport','default'=>'Export variants','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Varianten exportieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'variantExport','default'=>'Export variants','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    store:new Ext.data.SimpleStore({
                        fields:['id', 'text'], data:this.variantExportData
                    }),
                    valueField:'id',
                    displayField:'text',
                    mode:'local',
                    allowBlank:false,
                    required:true,
                    editable:false
                },
                {
                    name: 'cacheRefreshed',
                    xtype: 'displayfield',
                    fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'cacheRefresh','default'=>'Last cache refresh','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'cacheRefresh','default'=>'Last cache refresh','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Letzte Cache Aktualisierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'cacheRefresh','default'=>'Last cache refresh','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    renderer: function(value) {
                        if ( value === Ext.undefined ) {
                            return value;
                        }

                        return Ext.util.Format.date(value) + ' ' + Ext.util.Format.date(value, timeFormat);
                    }
                }
            ]
        });
    }
});
//
<?php }} ?>