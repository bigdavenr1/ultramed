<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:25:16
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/ExtJs/overrides/Ext.view.BoundList.js" */ ?>
<?php /*%%SmartyHeaderCode:10761091325541e6ecd4bcd5-87338793%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '727a2aac232e6a1a44251ea6c50db7e9c9f5ad92' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Library/ExtJs/overrides/Ext.view.BoundList.js',
      1 => 1430110298,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10761091325541e6ecd4bcd5-87338793',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e6ecd4f031_02998267',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e6ecd4f031_02998267')) {function content_5541e6ecd4f031_02998267($_smarty_tpl) {?>/**
 * Shopware UI - BoundList Override
 *
 * This files provides an override to address the
 * boundlist easier in our Selenium Tests.
 *
 * The override adds an addtional HTML5 "data"-attrbute
 * to the element. The value of the attribute is
 * either the "name" property of the ComboBox or
 * the action
 *
 * shopware AG (c) 2012. All rights reserved.
 *
 * @link http://www.shopware.de/
 * @author st.pohl
 * @date 2012-05-02
 * @license http://www.shopware.de/license
 * @package overrides
 */
Ext.override(Ext.view.BoundList,
/** @lends Ext.view.BoundList# */
{
    /**
     * Suffix for the added data-attribute.
     * @string
     */
    dataSuffix: 'action',

    /**
     * Adds an additional HTML5 "data"-attribute
     * to easier address the element in our
     * selenium tests.
     *
     * @public
     * @return void
     */
    afterRender: function() {
        var me = this;
        me.callOverridden(arguments);

        if(me.el.dom && me.pickerField) {
            var dom = me.el.dom,
                value = me.action || me.pickerField.name;

            dom.setAttribute('data-' + me.dataSuffix, value);
        }
    }
});<?php }} ?>