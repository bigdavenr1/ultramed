<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/transaction.js" */ ?>
<?php /*%%SmartyHeaderCode:171239651155447c3d51aa15-91047287%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0018ab91b55955bdf0444e420acce42ca4c00e76' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/transaction.js',
      1 => 1430111760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171239651155447c3d51aa15-91047287',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d524418_01154688',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d524418_01154688')) {function content_55447c3d524418_01154688($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.model.main.Transaction', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'string' },
        { name: 'date', type: 'date' },
        { name: 'name', type: 'string' },
        { name: 'email', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'status', type: 'string' },
        { name: 'amount', type: 'float' },
        { name: 'currency', type: 'string' },
        { name: 'amountFormat', type: 'string' }
    ]
});
<?php }} ?>