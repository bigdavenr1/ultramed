<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/category_teaser.js" */ ?>
<?php /*%%SmartyHeaderCode:77276985855434611218f95-10317797%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00f23268ff3bebcdeff52196f1d0623fbba914a6' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/category_teaser.js',
      1 => 1430113366,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '77276985855434611218f95-10317797',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434611243d51_31692395',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434611243d51_31692395')) {function content_55434611243d51_31692395($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
//
Ext.define('Shopware.apps.Emotion.view.components.CategoryTeaser', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.emotion-components-category-teaser',

    snippets: {
        blog_category: {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog_category','default'=>'Blog category','namespace'=>'backend/emotion/view/components/category_teaser')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_category','default'=>'Blog category','namespace'=>'backend/emotion/view/components/category_teaser'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blog-Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_category','default'=>'Blog category','namespace'=>'backend/emotion/view/components/category_teaser'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            supportText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog_category_support','default'=>'The selected category is a blog Category','namespace'=>'backend/emotion/view/components/category_teaser')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_category_support','default'=>'The selected category is a blog Category','namespace'=>'backend/emotion/view/components/category_teaser'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bei der ausgewählten Kategorie handelt es sich um eine Blog-Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog_category_support','default'=>'The selected category is a blog Category','namespace'=>'backend/emotion/view/components/category_teaser'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        image: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'image','default'=>'Image','namespace'=>'backend/emotion/view/components/category_teaser')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'image','default'=>'Image','namespace'=>'backend/emotion/view/components/category_teaser'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bild<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'image','default'=>'Image','namespace'=>'backend/emotion/view/components/category_teaser'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Base path which will be used from the component.
     * @string
     */
    basePath: '<?php $_smarty_tpl->smarty->loadPlugin("smarty_function_flink"); echo smarty_function_flink(array("file" => '', "fullPath" => false), $_smarty_tpl); ?>',

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.mediaSelection = me.down('mediaselectionfield');

        var value = '';
        Ext.each(me.getSettings('record').get('data'), function(item) {
            if(item.key == 'image_type') {
                value = item.value;
                return false;
            }
        });

        if(!value || value !== 'selected_image') {
            me.mediaSelection.hide();
        }
    }
});
//<?php }} ?>