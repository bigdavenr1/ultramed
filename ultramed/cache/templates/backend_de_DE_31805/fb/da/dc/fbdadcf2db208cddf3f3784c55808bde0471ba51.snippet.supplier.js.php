<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/supplier.js" */ ?>
<?php /*%%SmartyHeaderCode:128114994855447e75973e59-84034704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fbdadcf2db208cddf3f3784c55808bde0471ba51' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/supplier.js',
      1 => 1430113602,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '128114994855447e75973e59-84034704',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e759a7ac5_92674565',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e759a7ac5_92674565')) {function content_55447e759a7ac5_92674565($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Tab View.
 *
 * Displays all tab supplier Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.tab.Supplier', {
    extend:'Ext.container.Container',
    alias:'widget.product_feed-feed-tab-supplier',
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'supplier','default'=>'Supplier filter','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier','default'=>'Supplier filter','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hersteller-Filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier','default'=>'Supplier filter','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    border: 0,
    padding: 10,
    cls: 'shopware-toolbar',
    layout: 'anchor',

    /**
     * Initialize the Shopware.apps.ProductFeed.view.feed.tab.Footer and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;
        me.items = me.getItems();
        me.callParent(arguments);
    },
    /**
     * creates all fields for the tab
     */
    getItems:function () {
        var me = this;
        return [{
            xtype:'ddselector',
            fromTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'supplier_available','default'=>'Available suppliers','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier_available','default'=>'Available suppliers','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verfügbare Hersteller<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier_available','default'=>'Available suppliers','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            toTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'supplier_chosen','default'=>'Blocked suppliers','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier_chosen','default'=>'Blocked suppliers','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ausgeschlossene Hersteller<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'supplier_chosen','default'=>'Blocked suppliers','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            fromStore: me.supplierStore,
            buttons:[ 'add','remove' ],
            gridHeight: 270,
            selectedItems: me.record.getSuppliers(),
            fromFieldDockedItems: [ me.getToolbar() ],
            buttonsText: {
                add: "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'supplier'/'button_add','default'=>'Add','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'button_add','default'=>'Add','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'button_add','default'=>'Add','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
",
                remove: "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'supplier'/'button_remove','default'=>'Remove','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'button_remove','default'=>'Remove','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Entfernen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'button_remove','default'=>'Remove','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
"
            }
        }];
    },
    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar:function () {
        return Ext.create('Ext.toolbar.Toolbar', {
                    dock:'top',
                    ui:'shopware-ui',
                    items:[
                        '->',
                        {
                            xtype:'textfield',
                            name:'searchfield',
                            action:'searchSupplier',
                            width:170,
                            cls:'searchfield',
                            enableKeyEvents:true,
                            checkChangeBuffer:500,
                            emptyText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'supplier'/'search','default'=>'Search...','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'search','default'=>'Search...','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suche...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'supplier'/'search','default'=>'Search...','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                        },
                        { xtype:'tbspacer', width:6 }
                    ]
                }
        );
    }
});
//
<?php }} ?>