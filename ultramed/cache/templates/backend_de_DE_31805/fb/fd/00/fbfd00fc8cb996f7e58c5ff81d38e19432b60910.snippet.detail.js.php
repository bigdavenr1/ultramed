<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:56606474355447c3d526c04-33392054%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fbfd00fc8cb996f7e58c5ff81d38e19432b60910' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/detail.js',
      1 => 1430111761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '56606474355447c3d526c04-33392054',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d639c89_95352185',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d639c89_95352185')) {function content_55447c3d639c89_95352185($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//

//
Ext.define('Shopware.apps.PaymentPaypal.view.main.Detail', {
    extend: 'Ext.form.Panel',
    alias: 'widget.paypal-main-detail',

    layout: 'anchor',
    border: false,
    width: 500,

    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'title','default'=>'Details','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'title','default'=>'Details','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Details<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'title','default'=>'Details','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    autoScroll: true,
    bodyPadding: 5,
    collapsible: true,
    disabled: true,

    defaults: {
        xtype: 'fieldset',
        layout: 'form',
        defaults: {
            anchor: '100%',
            labelWidth: 135,
            xtype: 'textfield',
            readOnly: true,
            hideEmptyLabel: false
        }
    },

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: me.getItems(),
            buttons: me.getButtons()
        });

        me.callParent(arguments);
    },

    /**
     * @return Array
     */
    getButtons: function() {
        var me = this;
        return [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'void_text','default'=>'Cancel booking','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'void_text','default'=>'Cancel booking','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autorisierung abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'void_text','default'=>'Cancel booking','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'secondary', //DoVoid
            hidden: true,
            action: 'void'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'auth_text','default'=>'Extends booking','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'auth_text','default'=>'Extends booking','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autorisierung verlängern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'auth_text','default'=>'Extends booking','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            hidden: true,
            action: 'auth' // DoReauthorization
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zurückerstatten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'secondary',
            action: 'refund' // Refund
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'capture_text','default'=>'Capture','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'capture_text','default'=>'Capture','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einziehen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'capture_text','default'=>'Capture','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary', //DoCapture
            hidden: true,
            action: 'capture'
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'book_text','default'=>'Authorize','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'book_text','default'=>'Authorize','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autorisieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'book_text','default'=>'Authorize','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            action: 'book' // DoAuthorization
        }];
    },

    /**
     * @return Array
     */
    getItems: function() {
        var me = this;
        return [{
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'title','default'=>'Order data','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'title','default'=>'Order data','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestelldaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'title','default'=>'Order data','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [{
                name: 'orderNumber',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnumer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_number','default'=>'Order number','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-datetime',
                name: 'orderDate',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'order_date','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_date','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestelldatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_date','default'=>'Order date','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'statusDescription',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'order_status','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_status','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_status','default'=>'Order status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'customer',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'customer','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'customer','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunde<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'customer','default'=>'Customer','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'currency',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'amountFormat',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Betrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'express',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'order_data'/'order_type','default'=>'Order type','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_type','default'=>'Order type','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Order-Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'order_data'/'order_type','default'=>'Order type','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                readOnly: true,
                store: [[0, 'Normal'], [1, 'Express'], [2, 'Plus']],
                xtype: 'combobox'
            }]
        },{
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'title','default'=>'Payment data','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'title','default'=>'Payment data','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsdaten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'title','default'=>'Payment data','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [{
                name: 'transactionId',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Transaktionscode<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'transaction_id','default'=>'Transaction ID','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-datetime',
                name: 'clearedDate',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'cleared_date','default'=>'Book date','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'cleared_date','default'=>'Book date','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Buchungsdatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'cleared_date','default'=>'Book date','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'base-element-datetime',
                name: 'paymentDate',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'date','default'=>'Payment date','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'date','default'=>'Payment date','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autorisierungsdatum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'date','default'=>'Payment date','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'paymentStatus',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'status','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'status','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlstatus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'status','default'=>'Payment status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'pendingReason',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'pending_reason','default'=>'Pending reason','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'pending_reason','default'=>'Pending reason','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Grund für Verzögerung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'pending_reason','default'=>'Pending reason','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'accountName',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'name','default'=>'Sender name','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'name','default'=>'Sender name','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abender-Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'name','default'=>'Sender name','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'accountEmail',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'mail','default'=>'Sender mail','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'mail','default'=>'Sender mail','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abender-Email<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'mail','default'=>'Sender mail','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'paymentCurrency',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'paymentAmountFormat',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'payment_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Betrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'payment_data'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                xtype: 'hidden',
                hidden: true,
                name: 'paymentAmount'
            }]
        }, {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'title','default'=>'Seller Protection Address','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'title','default'=>'Seller Protection Address','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verkäuferschutzadresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'title','default'=>'Seller Protection Address','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [{
                name: 'addressStatus',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'addressName',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'name','default'=>'Name','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'name','default'=>'Name','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'name','default'=>'Name','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'addressStreet',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'street','default'=>'Street','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'street','default'=>'Street','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Straße<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'street','default'=>'Street','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'addressCity',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'city','default'=>'Zip / City','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'city','default'=>'Zip / City','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PLZ / Stadt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'city','default'=>'Zip / City','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'addressCountry',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'country','default'=>'Country','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'country','default'=>'Country','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'country','default'=>'Country','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }, {
                name: 'addressPhone',
                fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'address_data'/'phone','default'=>'Phone number','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'phone','default'=>'Phone number','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Telefon-Nr.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'address_data'/'phone','default'=>'Phone number','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }]
        }, {
            xtype: 'grid',
            title: 'Transactions',
            margin: '10 0 0 0',
            bodyPadding: 0,
            border: false,
            columns: [{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'id','default'=>'Id','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'id','default'=>'Id','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Id<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'id','default'=>'Id','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'id',
                hidden: true,
                flex: 2
            },{
                xtype: 'datecolumn',
                format: Ext.Date.defaultFormat + ' H:i:s',
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'date','default'=>'Date','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'date','default'=>'Date','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'date','default'=>'Date','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'date',
                flex: 2
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'type','default'=>'Type','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'type','default'=>'Type','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Typ<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'type','default'=>'Type','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'type',
                flex: 1
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'status','default'=>'Status','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'status',
                flex: 1
            }, {
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'currency','default'=>'Currency','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'currency',
                flex: 1
            }, {
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'transactions'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Betrag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'transactions'/'amount','default'=>'Amount','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'amountFormat',
                align: 'right',
                flex: 1
            }, {
                xtype:'actioncolumn',
                width: 35,
                sortable: false,
                items: [{
                    iconCls: 'sprite-minus-circle',
                    tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zurückerstatten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'button'/'refund_text','default'=>'Refund','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    getClass: function(value, metadata, record) {
                        if(record.get('type') != 'Payment' || record.get('status') == 'Refunded') {
                            return 'x-hidden';
                        }
                    },
                    handler: function (view, rowIndex, colIndex, item, opts, record) {
                        me.fireEvent('refund', {
                            action: 'refund',
                            text: this.items[0].tooltip,
                            transactionId: record.get('id'),
                            paymentAmount: record.get('amount')
                        });
                    }
                }]
            }]
        }];
    }
});
//
<?php }} ?>