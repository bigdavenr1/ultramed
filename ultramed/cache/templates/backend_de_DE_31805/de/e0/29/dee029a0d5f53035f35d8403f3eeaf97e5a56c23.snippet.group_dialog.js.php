<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/group_dialog.js" */ ?>
<?php /*%%SmartyHeaderCode:71351126155447a96961d93-50480757%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dee029a0d5f53035f35d8403f3eeaf97e5a56c23' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/view/site/group_dialog.js',
      1 => 1430113398,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71351126155447a96961d93-50480757',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a9699e010_81820034',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a9699e010_81820034')) {function content_55447a9699e010_81820034($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site group dialog View
 */

//

//
Ext.define('Shopware.apps.Site.view.site.GroupDialog', {
    extend: 'Enlight.app.Window',
    alias: 'widget.site-group-dialog',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'createGroupWindowTitle','default'=>'New group','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowTitle','default'=>'New group','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neue Gruppe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowTitle','default'=>'New group','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    width: 400,
    height: 200,
    modal: true,
    resizable: false,
    bodyPadding: 10,
    dockedItems: [{
        xtype: 'toolbar',
        ui: 'shopware-ui',
        cls: 'shopware-toolbar',
        dock: 'bottom',
        items: ['->', {
            xtype: 'button',
            cls: 'secondary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'createGroupWindowCancelButton','default'=>'Cancel','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowCancelButton','default'=>'Cancel','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowCancelButton','default'=>'Cancel','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'onCreateGroupWindowClose'
        }, {
            xtype: 'button',
            cls: 'primary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'createGroupWindowSubmitButton','default'=>'Create','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowSubmitButton','default'=>'Create','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erstellen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowSubmitButton','default'=>'Create','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            action: 'onCreateGroupSubmit'
        }]
    }],
    items: [{
        xtype: 'fieldset',
        layout: 'anchor',
        defaults: {
            labelWidth: 155,
            anchor: '100%',
            xtype: 'textfield'
        },
        items: [{
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'createGroupWindowDescription','default'=>'Description','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowDescription','default'=>'Description','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowDescription','default'=>'Description','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name:  'description'
        }, {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'createGroupWindowTemplateVar','default'=>'Template variable','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowTemplateVar','default'=>'Template variable','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template-Variable<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'createGroupWindowTemplateVar','default'=>'Template variable','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'templateVar'
        }]
    }]
});
//<?php }} ?>