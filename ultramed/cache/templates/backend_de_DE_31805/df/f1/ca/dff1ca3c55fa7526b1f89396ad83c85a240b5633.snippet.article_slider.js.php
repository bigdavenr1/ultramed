<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/article_slider.js" */ ?>
<?php /*%%SmartyHeaderCode:1203512665554346114b4255-28896712%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dff1ca3c55fa7526b1f89396ad83c85a240b5633' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/article_slider.js',
      1 => 1430113365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1203512665554346114b4255-28896712',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461151d1e7_51144440',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461151d1e7_51144440')) {function content_5543461151d1e7_51144440($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
//
Ext.define('Shopware.apps.Emotion.view.components.ArticleSlider', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.emotion-components-article-slider',

    /**
     * Snippets for the component.
     * @object
     */
    snippets: {
        'select_article': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'select_article','default'=>'Select article(s)','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'select_article','default'=>'Select article(s)','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'select_article','default'=>'Select article(s)','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'article_administration': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_administration','default'=>'Article administration','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_administration','default'=>'Article administration','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel Verwaltung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_administration','default'=>'Article administration','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'name': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'name','default'=>'Article name','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'name','default'=>'Article name','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikelname<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'name','default'=>'Article name','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'ordernumber': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'ordernumber','default'=>'Ordernumber','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ordernumber','default'=>'Ordernumber','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'ordernumber','default'=>'Ordernumber','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        'actions': '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'actions','default'=>'Action(s)','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'actions','default'=>'Action(s)','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktion(en)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'actions','default'=>'Action(s)','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

        article_slider_max_number: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_max_number','default'=>'Maximum number of articles','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_max_number','default'=>'Maximum number of articles','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Max Anzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_max_number','default'=>'Maximum number of articles','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        article_slider_title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_title','default'=>'Title','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_title','default'=>'Title','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Überschrift<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_title','default'=>'Title','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        article_slider_arrows: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_arrows','default'=>'Display arrows','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_arrows','default'=>'Display arrows','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Pfeile anzeigen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_arrows','default'=>'Display arrows','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        article_slider_numbers: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_numbers','default'=>'Display numbers','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_numbers','default'=>'Display numbers','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nummern anzeigen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_numbers','default'=>'Display numbers','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        article_slider_scrollspeed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_scrollspeed','default'=>'Scroll speed','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_scrollspeed','default'=>'Scroll speed','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Scroll-Geschwindigkeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_scrollspeed','default'=>'Scroll speed','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

        article_slider_rotation: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_rotation','default'=>'Rotate automatically','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_rotation','default'=>'Rotate automatically','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Automatisch rotieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_rotation','default'=>'Rotate automatically','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        article_slider_rotatespeed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'article_slider_rotatespeed','default'=>'Rotation speed','namespace'=>'backend/emotion/view/components/article_slider')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_rotatespeed','default'=>'Rotation speed','namespace'=>'backend/emotion/view/components/article_slider'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Rotations-Geschwindigkeit<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'article_slider_rotatespeed','default'=>'Rotation speed','namespace'=>'backend/emotion/view/components/article_slider'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.add(me.createArticleFieldset());
        me.setDefaultValues();
        me.getGridData();

        me.articleType = me.down('emotion-components-fields-article-slider-type');
        if(!me.articleType.getValue()) {
            me.maxCountField.hide();
            me.articleFieldset.hide();
        }
        if(me.articleType.getValue() === 'selected_article') {
            me.maxCountField.hide();
            me.articleFieldset.show();
            me.rotateSpeed.show().enable();
            me.rotation.show().enable();
        } else {
            me.maxCountField.show();
            me.articleFieldset.hide();
            me.rotateSpeed.hide().disable();
            me.rotation.hide().disable();
        }
        me.articleType.on('change', me.onChange, me);

        me.refreshHiddenValue();
    },

    onChange: function(field, newValue) {
        var me = this;

        if(newValue !== 'selected_article') {
            me.maxCountField.show();
            me.articleFieldset.hide();
            me.rotateSpeed.hide().disable();
            me.rotation.hide().disable();
        } else {
            me.maxCountField.hide();
            me.articleFieldset.show();
            me.rotateSpeed.show().enable();
            me.rotation.show().enable();
        }
    },

    /**
     * Sets default values if the article slider
     * wasn't saved previously.
     *
     * @public
     * @return void
     */
    setDefaultValues: function() {
        var me = this,
            numberfields =  me.query('numberfield'),
            checkboxes = me.query('checkbox');

        Ext.each(numberfields, function(field) {
            if(field.getName() === 'article_slider_max_number') {
                me.maxCountField = field;
                if(!field.getValue()) {
                    field.setValue(25);
                }
            }

            if(field.getName() === 'article_slider_rotatespeed') {
                me.rotateSpeed = field;
            }

            if(!field.getValue()) {
                field.setValue(500);
            }
        });

        Ext.each(checkboxes, function(field) {
            if(field.getName() === 'article_slider_rotation') {
                me.rotation = field;
            }
        });
    },

    /**
     * Creates the fieldset which holds the article administration. The method
     * also creates the article store and registers the drag and drop plugin
     * for the grid.
     *
     * @public
     * @return [object] Ext.form.FieldSet
     */
    createArticleFieldset: function() {
        var me = this;

        me.articleSearch = Ext.create('Shopware.form.field.ArticleSearch', {
            layout: 'anchor',
            anchor: '100%',
            multiSelect: false,
            returnValue: 'name',
            hiddenReturnValue: 'number',
            listeners: {
                scope: me,
                valueselect: me.onAddArticleToGrid
            }
        });

        me.articleStore = Ext.create('Ext.data.Store', {
            fields: [ 'position', 'name', 'ordernumber', 'articleId' ]
        });

        me.ddGridPlugin = Ext.create('Ext.grid.plugin.DragDrop');

        me.articleGrid = Ext.create('Ext.grid.Panel', {
            columns: me.createColumns(),
            autoScroll: true,
            store: me.articleStore,
            height: 300,
            viewConfig: {
                plugins: [ me.ddGridPlugin ],
                listeners: {
                    scope: me,
                    drop: me.onRepositionArticle
                }
            }
        });

        return me.articleFieldset = Ext.create('Ext.form.FieldSet', {
            title: me.snippets.article_administration,
            layout:  'anchor',
            defaults: { anchor: '100%' },
            items: [ me.articleSearch, me.articleGrid ]
        });
    },

    /**
     * Helper method which creates the column model
     * for the article administration grid panel.
     *
     * @public
     * @return [array] computed columns
     */
    createColumns: function() {
        var me = this, snippets = me.snippets;

        return [{
            header: '&#009868;',
            width: 24,
            hideable: false,
            renderer : me.renderSorthandleColumn
        }, {
            dataIndex: 'name',
            header: snippets.name,
            flex: 1
        }, {
            dataIndex: 'ordernumber',
            header: snippets.ordernumber,
            flex: 1
        }, {
            xtype: 'actioncolumn',
            header: snippets.actions,
            width: 60,
            items: [{
                iconCls: 'sprite-minus-circle',
                action: 'delete-article',
                scope: me,
                handler: me.onDeleteArticle
            }]
        }];
    },

    /**
     * Event listener method which will be triggered when one (or more)
     * article are added to the article slider.
     *
     * Creates new models based on the selected articles and
     * assigns them to the article store.
     *
     * @public
     * @event selectMedia
     * @param [object] field - Shopware.MediaManager.MediaSelection
     * @param [array] records - array of the selected media
     */
    onAddArticleToGrid: function(field, returnVal, hiddenVal, record) {
        var me = this, store = me.articleStore;

        var model = Ext.create('Shopware.apps.Emotion.model.ArticleSlider', {
            position: store.getCount(),
            name: returnVal,
            ordernumber: hiddenVal,
            articleId: record.get('id')
        });
        store.add(model);

        field.searchField.setValue();
        me.refreshHiddenValue();

    },

    /**
     * Event listener method which will be triggered when the user
     * deletes a article from article administration grid panel.
     *
     * Removes the article from the article store.
     *
     * @event click#actioncolumn
     * @param [object] grid - Ext.grid.Panel
     * @param [integer] rowIndex - Index of the clicked row
     * @param [integer] colIndex - Index of the clicked column
     * @param [object] item - DOM node of the clicked row
     * @param [object] eOpts - additional event parameters
     * @param [object] record - Associated model of the clicked row
     */
    onDeleteArticle: function(grid, rowIndex, colIndex, item, eOpts, record) {
        var me = this;
        var store = grid.getStore();
        store.remove(record);
        me.refreshHiddenValue();
    },

    /**
     * Event listener method which will be fired when the user
     * repositions a article through drag and drop.
     *
     * Sets the new position of the article in the article store
     * and saves the data to an hidden field.
     *
     * @public
     * @event drop
     * @return void
     */
    onRepositionArticle: function() {
        var me = this;

        var i = 0;
        me.articleStore.each(function(item) {
            item.set('position', i);
            i++;
        });
        me.refreshHiddenValue();
    },

    /**
     * Refreshes the mapping field in the model
     * which contains all articles in the grid.
     *
     * @public
     * @return void
     */
    refreshHiddenValue: function() {
        var me = this,
            store = me.articleStore,
            cache = [];

        store.each(function(item) {
            cache.push(item.data);
        });
        var record = me.getSettings('record');
        record.set('mapping', cache);
    },

    /**
     * Refactor sthe mapping field in the global record
     * which contains all article in the grid.
     *
     * Adds all articles to the article administration grid
     * when the user opens the component.
     *
     * @return void
     */
    getGridData: function() {
        var me = this,
            elementStore = me.getSettings('record').get('data'), articleSlider;

        Ext.each(elementStore, function(element) {
            if(element.key === 'selected_articles') {
                articleSlider = element;
                return false;
            }
        });

        if(articleSlider && articleSlider.value) {
            Ext.each(articleSlider.value, function(item) {
                me.articleStore.add(Ext.create('Shopware.apps.Emotion.model.ArticleSlider', item));
            });
        }
    },

    /**
     * Renderer for sorthandle-column
     *
     * @param [string] value
     */
    renderSorthandleColumn: function() {
        return '<div style="cursor: move;">&#009868;</div>';
    }
});
//<?php }} ?>