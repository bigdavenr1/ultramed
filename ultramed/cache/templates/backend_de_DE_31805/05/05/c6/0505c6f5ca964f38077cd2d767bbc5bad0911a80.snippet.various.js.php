<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:09
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/model/various.js" */ ?>
<?php /*%%SmartyHeaderCode:188704745855447c4d6ab667-35711728%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0505c6f5ca964f38077cd2d767bbc5bad0911a80' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/model/various.js',
      1 => 1430113163,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '188704745855447c4d6ab667-35711728',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4d6ba446_46168876',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4d6ba446_46168876')) {function content_55447c4d6ba446_46168876($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Performance
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Base config model which holds references to the config items
 */
//
Ext.define('Shopware.apps.Performance.model.Various', {

    /**
     * Extends the standard Ext Model
     * @string
     */
    extend:'Ext.data.Model',

    /**
     * Contains the model fields
     * @array
     */
    fields:[
		//
        { name:'id', type:'int' },
        { name: 'disableShopwareStatistics', type: 'bool'},
        { name: 'TagCloud:show', type: 'bool'},
        { name: 'LastArticles:show', type: 'bool'},
        { name: 'LastArticles:lastarticlestoshow', type: 'int'},
        { name: 'disableArticleNavigation', type: 'bool'},
    ]

});
//
<?php }} ?>