<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/body.js" */ ?>
<?php /*%%SmartyHeaderCode:141116224955447e75926f42-57795571%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '14283de3fd116d726aefa181b23bbe56d65bcb7b' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/body.js',
      1 => 1430113601,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141116224955447e75926f42-57795571',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e7595bf99_36370180',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e7595bf99_36370180')) {function content_55447e7595bf99_36370180($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Tab View.
 *
 * Displays all ProductFeed body Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.tab.Body', {
    extend:'Ext.container.Container',
    alias:'widget.product_feed-feed-tab-body',
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'body','default'=>'Body','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'body','default'=>'Body','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'body','default'=>'Body','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    border: 0,
    padding: 10,
    cls: 'shopware-toolbar',
    layout: 'anchor',
    fieldName: 'body',
    /**
     * Initialize the Shopware.apps.ProductFeed.view.feed.tab.Footer and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.items = me.getItems();

        me.callParent(arguments);
    },
    /**
     * creates all fields for the tab
     */
    getItems:function () {
        var me = this;
        return [
            {
                xtype: 'codemirrorfield',
                mode: 'smarty',
                anchor:'100%',
                height: '240px',
                name: me.fieldName
            }
        ];
    }

});
//
<?php }} ?>