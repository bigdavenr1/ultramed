<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/rule.js" */ ?>
<?php /*%%SmartyHeaderCode:189672092655447cd78db4c6-04452542%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7838f3a148abdaf4f4c46670f4c474dd572046a4' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/rule.js',
      1 => 1430113171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189672092655447cd78db4c6-04452542',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd78f86a9_96366061',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd78f86a9_96366061')) {function content_55447cd78f86a9_96366061($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Rule model
 *
 * This model contains a single rule, which is made of the different risks.
 */
//
Ext.define('Shopware.apps.RiskManagement.model.Rule', {
    /**
    * Extends the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Model',
	idProperty: 'id',

	proxy: {
		type: 'ajax',
		/**
		* Configure the url mapping for the different
		* @object
		*/
		api: {
			//create articles
            create: '<?php echo Enlight_Application::Instance()->Front()->Router()->assemble(array('controller' => "risk_management", 'action' => "createRule", )); ?>',
            //edit articles
            update: '<?php echo Enlight_Application::Instance()->Front()->Router()->assemble(array('controller' => "risk_management", 'action' => "editRule", )); ?>',
	       	//function to delete articles
          	destroy: '<?php echo Enlight_Application::Instance()->Front()->Router()->assemble(array('controller' => "risk_management", 'action' => "deleteRule", )); ?>'
		},

		/**
		* Configure the data reader
		* @object
		*/
		reader: {
			type: 'json',
			root: 'data',
			totalProperty: 'total'
		}
	},

    /**
    * The fields used for this model
    * @array
    */
    fields: [
		//
		{ name: 'id', type: 'int' },
		{ name: 'paymentId', type: 'string' },
		{ name: 'rule1', type: 'string' },
		{ name: 'value1', type: 'string' },
		{ name: 'rule2', type: 'string' },
		{ name: 'value2', type: 'string' }
    ]
});
//<?php }} ?>