<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/grids/list.js" */ ?>
<?php /*%%SmartyHeaderCode:18760438445543461157b8a3-39935073%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a29d739ec1399ad1ed91fda7cc5f6308073dc650' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/grids/list.js',
      1 => 1430113369,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18760438445543461157b8a3-39935073',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346115f8f04_45034017',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346115f8f04_45034017')) {function content_554346115f8f04_45034017($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Toolbar
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.grids.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.emotion-grids-list',

    /**
     * Snippets which are used by this component.
     * @Object
     */
    snippets: {
        columns: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'name','default'=>'Name','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cols: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'cols','default'=>'Column(s)','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'cols','default'=>'Column(s)','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Spalte(n)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'cols','default'=>'Column(s)','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            rows: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'rows','default'=>'Row(s)','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'rows','default'=>'Row(s)','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zeile(n)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'rows','default'=>'Row(s)','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cellHeight: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'cellHeight','default'=>'Cell height (in px)','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'cellHeight','default'=>'Cell height (in px)','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zellenhöhe (in px)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'cellHeight','default'=>'Cell height (in px)','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            articleHeight: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'articleHeight','default'=>'Article element height','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'articleHeight','default'=>'Article element height','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Element Höhe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'articleHeight','default'=>'Article element height','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            gutter: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abstand<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'gutter','default'=>'Gutter','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            actions: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktion(en)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'columns'/'actions','default'=>'Action(s)','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        tooltips: {
            edit: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'edit','default'=>'Edit','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            duplicate: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Duplizieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'duplicate','default'=>'Duplicate','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            remove: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'tooltip'/'remove','default'=>'Delete','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        renderer: {
            articleHeight: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grids'/'list'/'renderer'/'articleHeight','default'=>'cell(s)','namespace'=>'backend/emotion/grids/list')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'renderer'/'articleHeight','default'=>'cell(s)','namespace'=>'backend/emotion/grids/list'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zelle(n)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grids'/'list'/'renderer'/'articleHeight','default'=>'cell(s)','namespace'=>'backend/emotion/grids/list'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @returns { Void }
     */
    initComponent: function() {
        var me = this;

        me.addEvents('selectionChange', 'editEntry', 'duplicate', 'remove');

        me.columns = me.createColumns();
        me.selModel = me.createSelectionModel();
        me.plugins = [ me.createEditor() ];
        me.bbar = me.createPagingToolbar();

        me.callParent(arguments);
    },

    /**
     * Creates the column model for the grid panel
     *
     * @returns { Array } columns
     */
    createColumns: function() {
        var me = this;

        return [{
            dataIndex: 'name',
            header: me.snippets.columns.name,
            flex: 1,
            renderer: me.nameRenderer,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'cols',
            header: me.snippets.columns.cols,
            flex: 1,
            editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'rows',
            header: me.snippets.columns.rows,
            flex: 1,
            editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'cellHeight',
            header: me.snippets.columns.cellHeight,
            flex: 1,
            renderer: me.cellHeightRenderer,
            editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'articleHeight',
            header: me.snippets.columns.articleHeight,
            flex: 1,
            renderer: me.articleHeightRenderer,
            editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
        }, {
            dataIndex: 'gutter',
            header: me.snippets.columns.gutter,
            flex: 1,
            renderer: me.cellHeightRenderer,
            editor: {
                xtype: 'numberfield',
                allowBlank: false
            }
        }, {
            xtype: 'actioncolumn',
            header: me.snippets.columns.actions,
            width: 85,
            items: [{
                iconCls: 'sprite-pencil',
                tooltip: me.snippets.tooltips.edit,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('editEntry', grid, rec, row, col);
                }
            }, {
                iconCls: 'sprite-blue-folder--plus',
                tooltip: me.snippets.tooltips.duplicate,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('duplicate', grid, rec, row, col);
                }
            }, {
                iconCls: 'sprite-minus-circle',
                tooltip: me.snippets.tooltips.remove,
                handler: function(grid, row, col) {
                    var rec = grid.getStore().getAt(row);
                    me.fireEvent('remove', grid, rec, row, col);
                },
                getClass: function(value, metadata, record) {
                    if (record.get('id') < 3)  {
                        return Ext.baseCSSPrefix + 'hidden';
                    }
                }
            }]
        }];
    },

    /**
     * Creates the selection model.
     *
     * @returns { Ext.selection.CheckboxModel }
     */
    createSelectionModel: function() {
        var me = this;

        return Ext.create('Ext.selection.CheckboxModel', {
            listeners:{
                selectionchange:function (sm, selections) {
                    me.fireEvent('selectionChange', selections);
                }
            }
        });
    },

    /**
     * Creates the row editor
     *
     * @returns { Ext.grid.plugin.RowEditing }
     */
    createEditor: function() {
        return Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
    },

    /**
     * Creates the paging toolbar at the bottom of the list.
     *
     * @returns { Ext.toolbar.Paging }
     */
    createPagingToolbar: function() {
        var me = this,
            toolbar = Ext.create('Ext.toolbar.Paging', {
            store: me.store,
            pageSize: 20
        });

        return toolbar;
    },

    /**
     * Column renderer for the `name` column.
     *
     * The method wraps the value in `strong`-tags.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    nameRenderer: function(value) {
        return Ext.String.format('<strong>[0]</strong>', value);
    },

    /**
     * Column renderer for the `cellHeight` column.
     *
     * The method appends an `px` to the incoming value.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    cellHeightRenderer: function(value) {
        return Ext.String.format('[0]px', value);
    },

    /**
     * Column renderer for the `articleHeight` column.
     *
     * The method appends a localized `cell` string to the incoming value.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    articleHeightRenderer: function(value) {
        return Ext.String.format('[0] [1]', value, this.snippets.renderer.articleHeight);
    },

    /**
     * Creates the row editor
     *
     * @returns { Ext.grid.plugin.RowEditing }
     */
    createEditor: function() {
        return Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2
        });
    },

    /**
     * Column renderer for the `name` column.
     *
     * The method wraps the value in `strong`-tags.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    nameRenderer: function(value) {
        return Ext.String.format('<strong>[0]</strong>', value);
    },

    /**
     * Column renderer for the `cellHeight` column.
     *
     * The method appends an `px` to the incoming value.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    cellHeightRenderer: function(value) {
        return Ext.String.format('[0]px', value);
    },

    /**
     * Column renderer for the `articleHeight` column.
     *
     * The method appends a localized `cell` string to the incoming value.
     *
     * @param { String } value - The column content
     * @returns { String } formatted output
     */
    articleHeightRenderer: function(value) {
        return Ext.String.format('[0] [1]', value, this.snippets.renderer.articleHeight);
    }
});
//<?php }} ?>