<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:10
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/store/shop.js" */ ?>
<?php /*%%SmartyHeaderCode:192297350855447c4e0d6d91-50812106%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a21b79910514091bc07f10e0188771fa2c08f575' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/store/shop.js',
      1 => 1430113164,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192297350855447c4e0d6d91-50812106',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4e0e3282_16071712',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4e0e3282_16071712')) {function content_55447c4e0e3282_16071712($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2013 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//
Ext.define('Shopware.apps.Performance.store.Shop', {
    extend: 'Ext.data.Store',

    fields: [ 'id', 'name' ],

    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Performance/getActiveShops';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>