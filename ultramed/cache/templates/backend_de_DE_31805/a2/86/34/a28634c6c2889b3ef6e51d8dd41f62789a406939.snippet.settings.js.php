<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/settings.js" */ ?>
<?php /*%%SmartyHeaderCode:14355145915543461173a176-31316627%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a28634c6c2889b3ef6e51d8dd41f62789a406939' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/templates/settings.js',
      1 => 1430113370,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14355145915543461173a176-31316627',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543461178e739_12393539',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543461178e739_12393539')) {function content_5543461178e739_12393539($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Main Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.Emotion.view.templates.Settings', {
    extend: 'Enlight.app.Window',
    alias: 'widget.emotion-view-templates-settings',
    width: 600,
    height: 250,
    autoShow: true,
    layout: 'fit',

    /**
     * Snippets which are used by this component.
     * @Object
     */
    snippets: {
        invalid_template: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die zur Verfügung gestellte Datei scheint keine valide Template-Datei zu sein.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'error'/'invalid_template','default'=>'The provided file seems to be not a valid template file','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        title_new: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'settings'/'title_new','default'=>'Create new template','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'title_new','default'=>'Create new template','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neues Template anlegen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'title_new','default'=>'Create new template','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        title_edit: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'settings'/'title_edit','default'=>'Edit existing template','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'title_edit','default'=>'Edit existing template','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestehendes Template bearbeiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'title_edit','default'=>'Edit existing template','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        fieldset_title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'settings'/'fieldset','default'=>'Define template','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'fieldset','default'=>'Define template','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template definieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'fieldset','default'=>'Define template','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        fields: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'name','default'=>'Name','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            file: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'settings'/'file','default'=>'Template file','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'file','default'=>'Template file','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template-Datei<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'settings'/'file','default'=>'Template file','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        support: {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'support'/'name','default'=>'Initial description of the template.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'support'/'name','default'=>'Initial description of the template.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Interne Bezeichnung des Templates<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'support'/'name','default'=>'Initial description of the template.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            file: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'support'/'file','default'=>'Template file name in the file system. The template must be under widgets/emotion.','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'support'/'file','default'=>'Template file name in the file system. The template must be under widgets/emotion.','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template Dateiname im Dateisystem. Das Template muss unterhalb von widgets/emotion liegen.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'support'/'file','default'=>'Template file name in the file system. The template must be under widgets/emotion.','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },
        buttons: {
            cancel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            save: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'templates'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'templates'/'button'/'save','default'=>'Save','namespace'=>'backend/emotion/view/detail'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.title = me.snippets[(me.hasOwnProperty('record') ? 'title_edit' : 'title_new' )];

        me.formPanel = Ext.create('Ext.form.Panel', {
            bodyPadding: 20,
            border: 0,
            bodyBorder: 0,
            items: [{
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '155',
                    anchor: '100%'
                },
                title: me.snippets.fieldset_title,
                items: me.createFormItems()
            }]
        });
        me.items = [ me.formPanel ];
        me.bbar = me.createActionButtons();

        if(me.hasOwnProperty('record')) {
            me.formPanel.loadRecord(me.record);
        }

        me.callParent(arguments);
    },

    /**
     * Creates the toolbar which contains the action buttons.
     *
     * @returns { Ext.toolbar.Toolbar }
     */
    createActionButtons: function() {
        var me = this;

        return Ext.create('Ext.toolbar.Toolbar', {
            docked: 'bottom',
            items: [ '->', {
                xtype: 'button',
                text: me.snippets.buttons.cancel,
                cls: 'secondary',
                handler: function() {
                    me.destroy();
                }
            }, {
                xtype: 'button',
                text: me.snippets.buttons.save,
                cls: 'primary',
                action: 'emotion-save-grid'
            }]
        });
    },

    /**
     * Creates the form items for the settings window.
     *
     * @returns { Array }
     */
    createFormItems: function() {
        var me = this, label = me.snippets.fields,
            support = me.snippets.support;

        var name = Ext.create('Ext.form.field.Text', {
            name: 'name',
            fieldLabel: label.name,
            allowBlank: false,
            supportText: support.name
        });

        var template = Ext.create('Ext.form.field.Text', {
            name: 'file',
            fieldLabel: label.file,
            allowBlank: false,
            validator: function(value) {
                return (/^((.*)\.tpl)$/.test(value)) ? true : me.snippets.invalid_template;
            },
            supportText: support.file
        });

        return [ name, template ];
    }
});
//<?php }} ?>