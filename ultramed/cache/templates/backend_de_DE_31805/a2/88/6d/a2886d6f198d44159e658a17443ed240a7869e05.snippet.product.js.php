<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/product.js" */ ?>
<?php /*%%SmartyHeaderCode:27034486355447ce37c9b32-77043381%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2886d6f198d44159e658a17443ed240a7869e05' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/product.js',
      1 => 1430111756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27034486355447ce37c9b32-77043381',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce37deba8_25177534',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce37deba8_25177534')) {function content_55447ce37deba8_25177534($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Main
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 * Shopware Plugin Manager - Product Model
 * The product model of the plugin manager represents a single product of the community store.
 * The associated data like details are available over the different associations.
 */
//
Ext.define('Shopware.apps.PluginManager.model.Product', {

    /**
    * Extends the standard Ext Model
    * @string
    */
    extend: 'Ext.data.Model',

    /**
    * @array
    */
    fields: [
        //
        { name: 'id', type: 'int' },
        { name: 'supplierID', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'supplierName', type: 'string' },
        { name: 'plugin_names', type: 'array' },
        { name: 'licence', type: 'array' },
        { name: 'datum', type: 'date', dateFormat: 'Y-m-d' },
        { name: 'vote_average', type: 'float' }
    ],

    /**
     * @array
     */
    associations:[
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Detail', name:'getDetail', associationKey:'details' },
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Media', name:'getMedia', associationKey:'images' },
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Category', name:'getCategory', associationKey:'categories' },
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Addon', name:'getAddon', associationKey:'addons' },
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Attribute', name:'getAttribute', associationKey:'attributes' },
    ]
});
//

<?php }} ?>