<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/configlist.js" */ ?>
<?php /*%%SmartyHeaderCode:125637876355447cb11bbc32-75614747%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6455d3f2c87af2503ea0dba34f006d666fe2359e' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/systeminfo/configlist.js',
      1 => 1430113401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '125637876355447cb11bbc32-75614747',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb1209762_44847607',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb1209762_44847607')) {function content_55447cb1209762_44847607($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Grid for the shopware-configs
 *
 * todo@all: Documentation
 *
 */
//
Ext.define('Shopware.apps.Systeminfo.view.systeminfo.Configlist', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.grid.Panel',

    ui: 'shopware-ui',

    /**
     * ID to access the component out of other components
     */
    id: 'systeminfo-main-configlist',

    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('systeminfo-main-configlist')
    * @string
    */
    alias: 'widget.systeminfo-main-configlist',
    /**
    * The window uses a border layout, so we need to set
    * a region for the grid panel
    * @string
    */
    region: 'center',
	autoScroll: true,
    /**
    * Set the used store. You just need to set the store name
    * due to the fact that the store is defined in the same
    * namespace
    * @string
    */
    store: 'Configs',

    initComponent: function(){
        this.columns = this.getColumns();

		var translations = [];
		translations['config'] = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'systeminfo'/'groupingFeature_config','default'=>'Settings','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_config','default'=>'Settings','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_config','default'=>'Settings','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
		translations['core'] = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'systeminfo'/'groupingFeature_core','default'=>'General','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_core','default'=>'General','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Allgemein<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_core','default'=>'General','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
		translations['extension'] = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'systeminfo'/'groupingFeature_extension','default'=>'Extensions','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_extension','default'=>'Extensions','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erweiterungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_extension','default'=>'Extensions','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
		translations['other'] = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'systeminfo'/'groupingFeature_other','default'=>'Other','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_other','default'=>'Other','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sonstiges<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'systeminfo'/'groupingFeature_other','default'=>'Other','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';

//        Row grouping
        this.groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
            groupHeaderTpl: Ext.create('Ext.XTemplate',
                '<div>{name:this.formatName}</div>',
                {
                    formatName: function(name) {
                        return Ext.String.trim(translations[name]);
                    }
                })
        });
        this.features = [ this.groupingFeature ];

        this.callParent(arguments);
    },

    /**
     * Creates the columns
     * @return array columns Contains the columns
     */
    getColumns: function(){
        var me = this;

        var columns = [
            {
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'config_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'name','default'=>'Name','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'name',
                flex: 1
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'config_grid'/'column'/'required','default'=>'Required','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'required','default'=>'Required','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Benötigt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'required','default'=>'Required','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'required',
                align: 'right',
                flex: 1
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'config_grid'/'column'/'version','default'=>'Version','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'version','default'=>'Version','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Version<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'version','default'=>'Version','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'version',
                align: 'right',
                flex: 1,
                renderer: me.renderVersion
            },{
                header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'config_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'config_grid'/'column'/'status','default'=>'Status','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex: 'result',
                flex: 1,
                renderer: me.renderStatus
            }
        ];
        return columns;
    },

    /**
     * Function to render the status. 1 = a green tick, everything else = a red cross
     * @param value The value of the field
     */
    renderStatus: function(value){
        if(value==1){
            return Ext.String.format('<div style="height: 16px; width: 16px" class="sprite-tick"></div>')
        }else{
            return Ext.String.format('<div style="height: 16px; width: 16px" class="sprite-cross"></div>')
        }
    },

    /**
     * Function to render the version. "True" is displayed as 1 and "false" is displayed as 0
     * @param value The value of the field
     */
    renderVersion: function(value){
        if(value==true){
            return 1
        }else if(value==false){
            return 0
        }else{
            return value;
        }
    }
});
//<?php }} ?>