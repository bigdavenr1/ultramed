<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/sidebar/seo.js" */ ?>
<?php /*%%SmartyHeaderCode:1731771654554476fc8126b3-19220957%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '17918fc3b5704c5cd0a6d6f2d2a27301b15cbb30' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/sidebar/seo.js',
      1 => 1430113618,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1731771654554476fc8126b3-19220957',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc873992_45637217',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc873992_45637217')) {function content_554476fc873992_45637217($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Detail
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Blog detail page - Sidebar
 * The assigned_articles component contains the configuration elements for the assgined blog articles relations.
 */
//
//
Ext.define('Shopware.apps.Blog.view.blog.detail.sidebar.Seo', {
    /**
     * Define that the billing field set is an extension of the Ext.form.FieldSet
     * @string
     */
    extend:'Ext.form.Panel',
    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.blog-blog-detail-sidebar-seo',

    bodyPadding: 10,
    autoScroll: true,
    border:false,
    /**
     * Helper property which contains the name of the add event which fired when the user
     * clicks the button of the form panel
     */
    addEvent: 'addAssignedArticle',

    /**
     * Helper property which contains the name of the remove event which fired when the user
     * clicks the action column of the grid panel
     */
    removeEvent: 'removeAssignedArticle',

    /**
	 * The initComponent template method is an important initialization step for a Component.
     * It is intended to be implemented by each subclass of Ext.Component to provide any needed constructor logic.
     * The initComponent method of the class being created is called first,
     * with each initComponent method up the hierarchy to Ext.Component being called thereafter.
     * This makes it easy to implement and, if needed, override the constructor logic of the Component at any step in the hierarchy.
     * The initComponent method must contain a call to callParent in order to ensure that the parent class' initComponent method is also called.
	 *
	 * @return void
	 */
    initComponent:function () {
        var me = this;
        me.registerEvents();
        me.title = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'title','default'=>'Search Engine Optimization','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'title','default'=>'Search Engine Optimization','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Suchmaschinen Optimierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'title','default'=>'Search Engine Optimization','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
        me.items = me.createElements();
        me.callParent(arguments);
    },

    /**
     * Registers additional component events.
     */
    registerEvents: function() {
    	this.addEvents(
    		/**
    		 * Event will be fired when the user want to add a similar article
    		 *
    		 * @event
    		 */
    		'metaDescriptionChanged'
    	);
    },

    /**
     * Creates the elements for the similar article panel.
     * @return array
     */
    createElements: function() {
        var me = this;

        me.noticeContainer = me.createNoticeContainer();
        me.formPanel = me.createFormPanel();
        me.previewFieldSet = me.createPreviewFieldSet();

        return [
            me.noticeContainer, me.formPanel, me.previewFieldSet
        ];
    },

    /**
     * Creates the notice container for the similar articles panel.
     * @return Ext.container.Container
     */
    createNoticeContainer: function() {
        var me = this;

        return Ext.create('Ext.container.Container', {
            style: 'font-style: italic; color: #999; font-size: x-small; margin: 0 0 8px 0;',
            html: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'notice','default'=>'At this point you have the option to perform an on-page optimization of your blog entry, which helps you increasing its relevance within search engines. <br /> <br /> All entries made here will be output in the HTML source code.','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'notice','default'=>'At this point you have the option to perform an on-page optimization of your blog entry, which helps you increasing its relevance within search engines. <br /> <br /> All entries made here will be output in the HTML source code.','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hier haben Sie die Möglichkeit, eine On-Page Optimierung ihres Blog-Eintrags durchzuführen, was Ihnen hilft die Relevanz des Eintrags für Suchmaschinen zu steigern. <br /><br /> Alle hier getätigten Angaben werden im HTML-Quellcode ausgegeben.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'notice','default'=>'At this point you have the option to perform an on-page optimization of your blog entry, which helps you increasing its relevance within search engines. <br /> <br /> All entries made here will be output in the HTML source code.','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });
    },

    /**
     * Creates the form field set for the similar article panel. The form panel is used to
     * edit or add new similar articles to the article on the detail page.
     * @return Ext.form.FieldSet
     */
    createFormPanel: function() {
        var me = this;

        return Ext.create('Ext.form.FieldSet', {
            layout: 'anchor',
            padding: 10,
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'on_page'/'title','default'=>'On-Page Optimization','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'on_page'/'title','default'=>'On-Page Optimization','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
On-Page Optimierung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'on_page'/'title','default'=>'On-Page Optimization','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            defaults: {
                labelWidth:100,
                anchor: '100%',
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items: me.createFormItems()
        });
    },

    /**
     * Creates the form items.
     * @return
     */
    createFormItems: function() {
        var me = this;
        me.metaDescription = Ext.create('Ext.form.field.TextArea', {
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field'/'description','default'=>'Description','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'description','default'=>'Description','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'description','default'=>'Description','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            enableKeyEvents: true,
            maxLength:150,
            supportText: ' ',
            name:'metaDescription',
            listeners: {
                keyup: function() {
                    me.fireEvent('metaDescriptionChanged', this);
                }
            }
        });

        me.metaTitle = Ext.create('Ext.form.field.Text', {
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Titel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'title','default'=>'Title','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name:'metaTitle'
        });

        return [
            me.metaTitle,
            {
                xtype:'textareafield',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field'/'keywords','default'=>'Keywords','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'keywords','default'=>'Keywords','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keywords<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field'/'keywords','default'=>'Keywords','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'metaKeyWords'
            },
            me.metaDescription
        ];
    },

    /**
     * Creates the form field for the preview
     * @return Ext.form.FieldSet
     */
    createPreviewFieldSet: function () {
        var me = this;
        return {
            xtype:'googlepreview',
            fieldSetTitle: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'title','default'=>'Preview','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'title','default'=>'Preview','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vorschau<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'title','default'=>'Preview','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            viewData: me.detailRecord,
            titleField: me.metaTitle,
            fallBackTitleField: me.mainTitleField,
            descriptionField: me.metaDescription,
            supportText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'supportText','default'=>'This preview displayed can differ from the version shown in the search engine.','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'supportText','default'=>'This preview displayed can differ from the version shown in the search engine.','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die angezeigte Vorschau muss nicht mit der Abbildung in der Suchmaschine übereinstimmen.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'supportText','default'=>'This preview displayed can differ from the version shown in the search engine.','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            refreshButtonText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'generate','default'=>'Generate Preview','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'generate','default'=>'Generate Preview','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vorschau erstellen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail'/'sidebar'/'seo'/'field_set'/'preview'/'generate','default'=>'Generate Preview','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        };
    }
});
//
<?php }} ?>