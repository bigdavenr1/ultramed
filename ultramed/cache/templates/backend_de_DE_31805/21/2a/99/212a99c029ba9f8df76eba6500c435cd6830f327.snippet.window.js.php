<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:12915785345541e663be9765-36044600%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '212a99c029ba9f8df76eba6500c435cd6830f327' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/main/window.js',
      1 => 1430113390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12915785345541e663be9765-36044600',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663c4d5d1_99592183',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663c4d5d1_99592183')) {function content_5541e663c4d5d1_99592183($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Main payment window
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Payment.view.main.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_title','default'=>'Payments','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','default'=>'Payments','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','default'=>'Payments','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    cls: Ext.baseCSSPrefix + 'payment-window',
    alias: 'widget.payment-main-window',
    border: false,
    autoShow: true,
    layout: 'border',
    height: '90%',
    width: 925,

    stateful:true,
    stateId:'shopware-payment-window',

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.registerEvents();
        me.tabPanel = me.createTabPanel();
        me.tree = Ext.create('Shopware.apps.Payment.view.payment.Tree');
        me.items = [{
            xtype: 'container',
            region: 'center',
            layout: 'border',
            items: [ me.createToolbar(), me.tabPanel ]
        }, me.tree ];

        me.callParent(arguments);
    },

    /**
     * This function registers the special events
     */
    registerEvents: function() {
        this.addEvents(
            /**
             * This event is fired, when the user presses the "save"-button
             * @param generalForm Contains the general form-panel
             * @param countrySelection Contains the countries-grid with its selections
             * @param subShopSelection Contains the subShop-grid with its selections
             * @param subShopSelection Contains the surcharge-grid
             * @event savePayment
             */
            'savePayment',

            /**
             * This event is fired, when the user changes the active tab
             * @param tabPanel Contains the tabPanel
             * @param newTab Contains the new active tab
             * @param oldTab Contains the old tab, which was active before
             * @param generalForm Contains the general form-panel
             */
            'changeTab'
        );
    },

    /**
     * This function creates the toolbar
     * @return [Ext.toolbar.Toolbar]
     */
    createToolbar: function() {
        var me = this;

        var buttons = ['->'];
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        buttons.push(Ext.create('Ext.button.Button',{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_save','default'=>'Save','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_save','default'=>'Save','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_save','default'=>'Save','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            action: 'save',
            name: 'save',
            disabled: true,
            handler: function() {
                me.fireEvent('savePayment', me.generalForm, me.countrySelection, me.subshopSelection, me.surcharge)
            }
        }));
        /*<?php }?>*/
        return Ext.create('Ext.toolbar.Toolbar',{
            name: 'gridToolBar',
            region: 'south',
            ui: 'shopware-ui',
            cls: 'shopware-toolbar',
            items: buttons
        });
    },

    /**
     * This function creates the tabPanel with its items
     * @return [Ext.tab.Panel]
     */
    createTabPanel: function() {
        var me = this;

        me.generalForm = Ext.create('Shopware.apps.Payment.view.payment.FormPanel');

        me.countrySelection = Ext.create('Shopware.apps.Payment.view.payment.CountryList', {
            record: me.record,
            paymentStore: me.paymentStore
        });

        me.subshopSelection = Ext.create('Shopware.apps.Payment.view.payment.SubshopList',{
            record: me.record,
            paymentStore: me.paymentStore
        });

        me.surcharge = Ext.create('Shopware.apps.Payment.view.payment.Surcharge',{
            record: me.record,
            paymentStore: me.paymentStore
        });


        return Ext.create('Ext.tab.Panel', {
            autoShow: false,
            //disabled to enable it, when the tree is clicked at least once
            disabled: true,
            layout: 'fit',
            region: 'center',
            autoScroll: true,
            border: 0,
            bodyBorder: false,
            defaults: {
                layout: 'fit'
            },
            items: [{
                xtype: 'container',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_generalForm','default'=>'General','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_generalForm','default'=>'General','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Generell<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_generalForm','default'=>'General','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [ me.generalForm ]
            }, {
                xtype: 'container',
                autoRender: true,
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_countrySelection','default'=>'Country selection','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_countrySelection','default'=>'Country selection','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Länder-Auswahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_countrySelection','default'=>'Country selection','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [ me.countrySelection ]
            }, {
                xtype: 'container',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_countrySurcharge','default'=>'Country surcharge','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_countrySurcharge','default'=>'Country surcharge','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Länder-Aufschlag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_countrySurcharge','default'=>'Country surcharge','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                autoRender: true,
                items: [ me.surcharge ]
            }, {
                xtype: 'container',
                autoRender: true,
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title_subshopSelection','default'=>'Subshop selection','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_subshopSelection','default'=>'Subshop selection','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Subshop-Auswahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title_subshopSelection','default'=>'Subshop selection','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [ me.subshopSelection ]
            }],

            listeners: {
                tabchange: function(tabPanel, newTab, oldTab) {
                    me.fireEvent('changeTab', tabPanel, newTab, oldTab, me.generalForm);
                }
            }
        });

    }
});
//<?php }} ?>