<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:44:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/view/main/selection.js" */ ?>
<?php /*%%SmartyHeaderCode:15228026945544725f004802-13575651%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2187d935f38ee65593c149c77678fe7c60a32279' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/view/main/selection.js',
      1 => 1430113378,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15228026945544725f004802-13575651',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5544725f0385c2_68303616',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5544725f0385c2_68303616')) {function content_5544725f0385c2_68303616($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
// Auswahl &uuml;bernehmen
/**
 * Shopware UI - Media Manager Selection Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */

//
Ext.define('Shopware.apps.MediaManager.view.main.Selection', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"selectionWindowTitle",'default'=>'Mediaselection','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selectionWindowTitle",'default'=>'Mediaselection','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Medienauswahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selectionWindowTitle",'default'=>'Mediaselection','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    cls: Ext.baseCSSPrefix + 'media-manager-window ' + Ext.baseCSSPrefix + 'media-manager-selection',
    alias: 'widget.mediamanager-selection-window',
    border: false,
    autoShow: true,
    layout: 'border',
    height: 500,
    width: 730,

    /**
     * Forces the window to be on front
     * @boolean
     * @default false
     */
    forceToFront: false,

    /**
     * Collection of used snippets.
     * @object
     */
	snippets:{
		album: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"selection/album",'default'=>'Album','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/album",'default'=>'Album','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Album<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/album",'default'=>'Album','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		searchText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"selection/search_text",'default'=>'Search...','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/search_text",'default'=>'Search...','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Durchsuchen...<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/search_text",'default'=>'Search...','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		applySelection: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"selection/apply_selection",'default'=>'Apply Selection','namespace'=>'backend/media_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/apply_selection",'default'=>'Apply Selection','namespace'=>'backend/media_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Auswahl übernehmen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"selection/apply_selection",'default'=>'Apply Selection','namespace'=>'backend/media_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.mediaView = Ext.create('Shopware.apps.MediaManager.view.media.View', {
            mediaStore: me.mediaStore,
            validTypes: me.validTypes,
            createInfoPanel: false,
            createDeleteButton: false,
            createMediaQuantitySelection: false,
            selectionMode: me.selectionMode
        });

        me.items = [{
            xtype: 'mediamanager-album-tree',
            store: me.albumStore,
            width: 155,

            // We don't need toolbars here
            createToolbars: false,

            // Deactive the drag and drop reordering of the albums
            viewConfig: {  },

            // Customize the column model
            createColumns: function() {
                return [{
                    xtype: 'treecolumn',
                    text: me.snippets.album,
                    flex: 2,
                    sortable: true,
                    dataIndex: 'text'
                }];
            }
        }, me.mediaView
        ];

        me.bbar = me.createFooterToolbar();
        me.callParent(arguments);
    },

    /**
     * Creates the footer toolbar which features the
     * search field and the "apply selection" button
     *
     * @return [object] generated Ext.toolbar.Toolbar
     */
    createFooterToolbar: function() {
        var me = this;

        var searchField = Ext.create('Ext.form.field.Text', {
            name: 'searchfield',
            cls: 'searchfield',
            emptyText: me.snippets.searchText,
            enableKeyEvents: true,
            checkChangeBuffer: 500,
            action: 'mediamanager-selection-window-searchfield'
        });

        var addBtn = Ext.create('Ext.button.Button', {
            text: me.snippets.applySelection,
            cls: 'primary',
            action: 'mediamanager-selection-window-apply-selection',
            handler: function(btn) {
                if (Ext.isFunction(me.selectionHandler)) {
                    me.selectionHandler.call(
                        me.eventScope,
                        btn,
                        me,
                        me.mediaView.dataView.getSelectionModel().getSelection()
                    );
                }
            }
        });

        return Ext.create('Ext.toolbar.Toolbar', {
            padding: '15 0',
            items: [
                { xtype: 'tbspacer', width: 26 },
                searchField,
                '->',
                addBtn,
                { xtype: 'tbspacer', width: 6 }
            ]
        })
    }
});
//
<?php }} ?>