<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:03
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/model/privilege.js" */ ?>
<?php /*%%SmartyHeaderCode:148217059755447cbf585cc4-97110933%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad2fdd4bc17f56bb535cfa7b20903d4959e92c85' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/model/privilege.js',
      1 => 1430113185,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148217059755447cbf585cc4-97110933',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cbf59c556_67507610',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cbf59c556_67507610')) {function content_55447cbf59c556_67507610($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Backend - User Manager Privilege Model
 *
 * The user manager privilege model represents a row the s_core_acl_privileges.
 * It is used to create or delete a single resource privilege.
 */
//
Ext.define('Shopware.apps.UserManager.model.Privilege', {

    /**
     * Define that the privilege model is an extension of the Ext.data.Model
     */
    extend: 'Ext.data.Model',

    /**
     * The field property contains all model fields.
     * @array
     */
	fields: [
		//
        { name: 'id',     type: 'int'},
        { name: 'name',     type: 'string'},
        { name: 'resourceId',     type: 'int'}
    ],

    /**
    * Configure the data communication
    * @object
    */
    proxy: {
        /**
         * Set proxy type to ajax
         * @string
         */
        type: 'ajax',

        /**
         * Specific urls to call on CRUD action methods "create", "read", "update" and "destroy".
         * @object
         */
        api: {
            create: '<?php echo '/backend/UserManager/savePrivilege';?>',
            destroy: '<?php echo '/backend/UserManager/deletePrivilege';?>'
        },

        /**
         * The Ext.data.reader.Reader to use to decode the server's
         * response or data read from client. This can either be a Reader instance,
         * a config object or just a valid Reader type name (e.g. 'json', 'xml').
         * @object
         */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//

<?php }} ?>