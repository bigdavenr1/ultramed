<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/store/category_for_combo_tree.js" */ ?>
<?php /*%%SmartyHeaderCode:44771178255447e75ab67b0-05003577%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54391bdeb65d7da6075b0b7ebd829718442d2ea9' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/store/category_for_combo_tree.js',
      1 => 1430113168,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '44771178255447e75ab67b0-05003577',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75abe098_66780065',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75abe098_66780065')) {function content_55447e75abe098_66780065($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - for ProductFeed backend module.
 *
 * The category store loads and store the category model
 */
//
Ext.define('Shopware.apps.ProductFeed.store.CategoryForComboTree', {
    /**
     * Extend for the standard ExtJS 4
     * @string
     */
    extend : 'Ext.data.TreeStore',

    remoteFilter: true,
    /**
     * Auto load the store after the component
     * is initialized
     * @boolean
     */
    autoLoad : false,
    /**
     * Define the used model for this store
     * @string
     */
    model : 'Shopware.apps.ProductFeed.model.CategoryForComboTree'
});
//
<?php }} ?>