<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:26
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/overview/view/main/grid.js" */ ?>
<?php /*%%SmartyHeaderCode:5536078465543477622f4a9-40976738%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54abaf9871875df0c299b78f3ab89ab23c6b2af9' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/overview/view/main/grid.js',
      1 => 1430113388,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5536078465543477622f4a9-40976738',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434776291b53_69088484',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434776291b53_69088484')) {function content_55434776291b53_69088484($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Overview
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Overview.view.main.Grid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.overview-main-grid',
    sortableColumns: false,
    features: [{
        ftype: 'summary'
    }],

    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents: function () {
        this.addEvents(
            /**
             * Event will be fired when the date-range changes
             *
             * @event dateChange
             * @param [Date] fromDate
             * @param [Date] toDate
             */
            'dateChange'
        );
    },

    /**
     * Sets up the ui component
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.registerEvents();

        me.columns = me.getColumns();
        me.tbar    = me.getToolbar();

        me.callParent(arguments);
    },

    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns: function() {
        var me = this;

        var columns = [{
            xtype: 'datecolumn',
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_date','default'=>'Date','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_date','default'=>'Date','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_date','default'=>'Date','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'date',
            flex: 1
        }, {
            xtype: 'numbercolumn',
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_amount','default'=>'Turnover','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_amount','default'=>'Turnover','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_amount','default'=>'Turnover','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'amount',
            align: 'right',
            flex: 1,
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_countOrders','default'=>'Orders','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countOrders','default'=>'Orders','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countOrders','default'=>'Orders','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'countOrders',
            align: 'right',
            flex: 1,
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            xtype: 'numbercolumn',
            header: '&#216; <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_averageOrders','default'=>'Order value','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_averageOrders','default'=>'Order value','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellwert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_averageOrders','default'=>'Order value','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'averageOrders',
            align: 'right',
            flex: 1,
            summaryType: 'average',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            xtype: 'numbercolumn',
            header: '&#216; <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_averageUsers','default'=>'Visits/orders','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_averageUsers','default'=>'Visits/orders','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Besucher/Bestellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_averageUsers','default'=>'Visits/orders','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'averageUsers',
            align: 'right',
            flex: 1,
            summaryType: 'average',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_countUsers','default'=>'New users','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countUsers','default'=>'New users','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neue Benutzer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countUsers','default'=>'New users','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'countUsers',
            align: 'right',
            flex: 1,
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_countCustomers','default'=>'New customers','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countCustomers','default'=>'New customers','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neue Kunden<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_countCustomers','default'=>'New customers','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'countCustomers',
            align: 'right',
            flex: 1,
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_visits','default'=>'Visits','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_visits','default'=>'Visits','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Besucher<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_visits','default'=>'Visits','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'visits',
            flex: 1,
            align: 'right',
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }, {
            header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'column_hits','default'=>'Page impressions','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_hits','default'=>'Page impressions','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Seitenzugriffe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'column_hits','default'=>'Page impressions','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            dataIndex: 'hits',
            flex: 1,
            align: 'right',
            summaryType: 'sum',
            summaryRenderer: me.summaryRenderer,
            renderer: me.trendRenderer
        }];

        return columns;
    },

    /**
     * Renders Trendicons
     *
     * @param [object] - value
     * @param [object] - metaData
     * @param [object] - record
     * @param [number] - rowIndex
     * @param [number] - colIndex
     * @param [object] - store
     * @param [object] - view
     * @return [string]
     */
    trendRenderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
        var header = view.getHeaderCt().getHeaderAtIndex(colIndex);
        var colName = header.dataIndex;
        var lastRecord = store.getAt(rowIndex - 1);
        var icon;

        if (header.summaryType === 'average') {
            value = Ext.util.Format.number(value, '0.00');
        }

        if (lastRecord) {
            if (lastRecord.get(colName) < record.get(colName)) {
                icon = 'sprite-arrow-045-small';
            } else if (lastRecord.get(colName) > record.get(colName)) {
                icon = 'sprite-arrow-225-small';
            }

            if (icon) {
                value = '<span class="' + icon +'" style="padding-right: 25px"></span>' + value;
            }
        }

        return value;
    },

    /**
     * Normalizes numbers
     *
     * @param [Object] value - The calculated value.
     * @return [string]
     */
    summaryRenderer: function(value) {
        if (value !== parseInt(value, 10)) {
            value = Ext.util.Format.number(value, '0.00');
        }

        return '<b>' + value + '</b>';
    },

    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar: function() {
        var me       = this,
            today    = new Date();

        var fromDate = Ext.create('Ext.form.field.Date', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldLabel_from','default'=>'From','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldLabel_from','default'=>'From','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Von<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldLabel_from','default'=>'From','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'from_date',
            labelWidth: 50,
            width: 150,
            maxValue: today,
            value: new Date(today.getFullYear(), today.getMonth() - 1, today.getDate())
        });

        var toDate = Ext.create('Ext.form.field.Date', {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'fieldLabel_to','default'=>'To','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldLabel_to','default'=>'To','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'fieldLabel_to','default'=>'To','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'to_date',
            labelWidth: 50,
            width: 150,
            maxValue: today,
            value: today
        });

        var filterButton = Ext.create('Ext.button.Button', {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'buttonText_filter','default'=>'Filter','namespace'=>'backend/overview/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'buttonText_filter','default'=>'Filter','namespace'=>'backend/overview/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filtern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'buttonText_filter','default'=>'Filter','namespace'=>'backend/overview/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-arrow-circle-135',
            scope : this,
            handler: function() {
                me.fireEvent('dateChange', fromDate.getValue(), toDate.getValue());
            }
        });

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            ui: 'shopware-ui',
            items: [ fromDate, toDate, { xtype: 'tbspacer' }, filterButton]
        });

        return toolbar;
    }
});
//
<?php }} ?>