<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/category.js" */ ?>
<?php /*%%SmartyHeaderCode:177209873355447e755acd77-10752306%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b73a94cd6232facf887931a748f7867a174ec3be' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/model/category.js',
      1 => 1430113166,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177209873355447e755acd77-10752306',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e755bd949_58570790',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e755bd949_58570790')) {function content_55447e755bd949_58570790($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - ProductFeed backend module.
 *
 * The category model is used for the category filter tree.
 */
//
Ext.define('Shopware.apps.ProductFeed.model.Category', {
    /**
     * Extends the default extjs 4 model
     * @string
     */
    extend : 'Shopware.apps.Base.model.Category',
    /**
     * Defined items used by that model
     * 
     * We use a reduces feature set here - just necessary fields are selected
     * 
     * @array
     */
    fields : [
		//
        { name : 'checked',  type: 'boolean' }
    ],
    proxy : {
        type : 'ajax',
        api : {
            read : '<?php echo '/backend/category/getList';?>'
        },
        reader : {
            type : 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>