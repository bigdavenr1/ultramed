<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/statistic/list.js" */ ?>
<?php /*%%SmartyHeaderCode:187164401055447ea46c7977-37579002%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7fb5dd13badf698614ea3f9a63fc12631aa0d46c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/statistic/list.js',
      1 => 1430113390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '187164401055447ea46c7977-37579002',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea4736ef3_46798202',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea4736ef3_46798202')) {function content_55447ea4736ef3_46798202($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - partner statistic list window.
 *
 * Displays the partner static list
 */
//
Ext.define('Shopware.apps.Partner.view.statistic.List', {
    extend:'Ext.grid.Panel',
    border: false,
    alias:'widget.partner-statistic-list',
    region:'center',
    autoScroll:true,
    store:'List',
    ui:'shopware-ui',
    /**
     * Initialize the Shopware.apps.Customer.view.main.List and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;
        me.store = me.listStore;
        me.columns = me.getColumns();
        me.pagingbar = me.getPagingBar();
        me.features = me.getFeatures();

        me.dockedItems = [me.pagingbar ];
        me.callParent(arguments);
    },

    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns:function () {
        var me = this;

        var columnsData = [
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'list'/'column'/'ordertime','default'=>'Order time','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'ordertime','default'=>'Order time','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestell-Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'ordertime','default'=>'Order time','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'orderTime',
                xtype: 'datecolumn',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'list'/'column'/'ordernumber','default'=>'Order number','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'ordernumber','default'=>'Order number','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'ordernumber','default'=>'Order number','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'number',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'list'/'column'/'net_turnover','default'=>'Net turnover','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'net_turnover','default'=>'Net turnover','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Netto-Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'net_turnover','default'=>'Net turnover','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'netTurnOver',
                xtype: 'numbercolumn',
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return me.summaryRenderer(me, value, summaryData, dataIndex);
                } ,
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'list'/'column'/'provision','default'=>'Provision','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'provision','default'=>'Provision','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Provision<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'column'/'provision','default'=>'Provision','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'provision',
                xtype: 'numbercolumn',
                summaryType: 'sum',
                summaryRenderer: function(value, summaryData, dataIndex) {
                    return me.summaryRenderer(me, value, summaryData, dataIndex);
                } ,
                flex:1
            }
        ];
        return columnsData;
    },

    /**
     * Normalizes numbers
     *
     * @param [Object] value - The calculated value.
     * @return [string]
     */
    summaryRenderer: function(scope, value, summaryData, dataIndex) {
        var store = scope.getStore(),
            proxy = store.getProxy(),
            reader = proxy.getReader(),
            rawData = reader.rawData,
            summaryValue = (dataIndex === 'netTurnOver') ? rawData.totalNetTurnOver : rawData.totalProvision;

        if (summaryValue !== parseInt(summaryValue, 10)) {
            summaryValue = Ext.util.Format.number(summaryValue, '0.00');
        }
        return '<b> <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'statistic'/'list'/'label'/'total','default'=>'Total:','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'label'/'total','default'=>'Total:','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gesamt:<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'statistic'/'list'/'label'/'total','default'=>'Total:','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
 ' + summaryValue + '</b>';
    },

    /**
     * Creates the items of the action column
     *
     * @return [array] action column itesm
     */
    getActionColumnItems: function () {
        var me = this,
                actionColumnData = [];

        actionColumnData.push({
            iconCls:'sprite-pencil',
            cls:'editBtn',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner editieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('editColumn', view, rowIndex, colIndex, item);
            }
        });

        actionColumnData.push({
            iconCls:'sprite-minus-circle-frame',
            action:'delete',
            cls:'delete',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('deleteColumn', view, rowIndex, colIndex, item);
            }
        });

        actionColumnData.push({
            iconCls:'sprite-chart-up-color',
            cls:'chart-up-color',
            tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Statistik<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler:function (view, rowIndex, colIndex, item) {
                me.fireEvent('statistic', view, rowIndex, colIndex, item);
            }
        });
        return actionColumnData;
    },
    /**
     * Creates the paging toolbar for the grid to allow
     * and store paging. The paging toolbar uses the same store as the Grid
     *
     * @return Ext.toolbar.Paging The paging toolbar for the customer grid
     */
    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store:me.listStore,
            dock:'bottom',
            displayInfo:true
        });

    },

    /**
     * Creates the summary feature for the grid
     *
     * @return Ext.toolbar.Paging The paging toolbar for the customer grid
     */
    getFeatures: function () {
       return [{
            ftype: 'summary'
        }];
    }
});
//
<?php }} ?>