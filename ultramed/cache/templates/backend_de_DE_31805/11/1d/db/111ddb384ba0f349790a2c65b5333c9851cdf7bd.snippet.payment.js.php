<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/controller/payment.js" */ ?>
<?php /*%%SmartyHeaderCode:18201597055541e663d6ef56-10666490%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '111ddb384ba0f349790a2c65b5333c9851cdf7bd' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/controller/payment.js',
      1 => 1430113160,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18201597055541e663d6ef56-10666490',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663dbbaa0_06237863',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663dbbaa0_06237863')) {function content_5541e663dbbaa0_06237863($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware Controller - Payment list backend module
 *
 * Payment controller of the payment module.
 * It handles all actions made in the module.
 * Listeners:
 *  - Create button  => Creates a new payment.
 *  - Tab click => Changes the active tab and automatically selects the countries/subshops.
 *  - Save button => Saves the payment with the edited information.
 *  - Delete button => Deletes the selected payment.
 */

//
Ext.define('Shopware.apps.Payment.controller.Payment', {

    /**
     * Extend from the standard ExtJS 4
     * @string
     */
    extend:'Ext.app.Controller',

    refs: [
        { ref: 'mainWindow', selector: 'payment-main-window' },
        { ref: 'surchargeGrid', selector: 'payment-main-surcharge' }
    ],

    /**
     * Creates the necessary event listener for this
     * specific controller and opens a new Ext.window.Window
     * @return void
     */
    init:function(){
        var me = this;

        me.control({
            'payment-main-window payment-main-tree':{
                itemclick:me.onItemClick
            },
            'payment-main-window':{
                savePayment:me.onSavePayment,
                changeTab:me.onChangeTab
            },

            'payment-main-tree':{
                deletePayment: me.onDeletePayment,
                createPayment:me.onCreatePayment
            }

        });
        me.callParent(arguments);
    },

    /**
     * This function deletes the selected payment, if the payment is not a default-payment
     * @param tree Contains the tree
     */
    onDeletePayment: function(tree, btn){
        var selection = tree.getSelectionModel().getSelection(),
            win = tree.up('window'),
            saveButton = win.down('button[name=save]'),
            countryGrid = win.down('payment-main-countrylist'),
            paymentStore = this.subApplication.paymentStore,
            tabPanel = win.down('tabpanel');


        if(selection[0].data.source == 1){
            selection[0].destroy({
                callback:function(data, operation){
                    var records = operation.getRecords(),
                        record = records[0],
                        rawData = record.getProxy().getReader().rawData;

                    if(operation.success){
                        Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'delete_growlMessage_content','default'=>'The payment has been successfully deleted.','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_content','default'=>'The payment has been successfully deleted.','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Zahlungsart wurde erfolgreich gelöscht.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_content','default'=>'The payment has been successfully deleted.','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_title','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    }else{
                        Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'delete_growlMessage_subject','default'=>'Delete payment','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', rawData.errorMsg.errorInfo[2], '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_title','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    }

                    paymentStore.load();
                    tabPanel.disable(true);
                    btn.disable(true);
                    saveButton.disable(true);
                }
            });
        }
    },


    /**
     * Is fired, when the tab is changed
     * Automatically selects the countries/shops and sets the surcharge
     * @param tabPanel Contains the tabpanel
     * @param newTab Contains the new tab, which was clicked now
     * @param oldTab Contains the old tab, which was opened before the new tab
     * @param formPanel Contains the general formpanel
     */
    onChangeTab:function(tabPanel, newTab, oldTab, formPanel){
        var grid = newTab.items.items[0],
            record = formPanel.getRecord(),
            recordStore;

        switch (grid.xtype) {
            //The formpanel and the surcharge-grid mustn't be affected
            case 'payment-main-formpanel':
                return;
                break;
            case 'payment-main-surcharge':
                return;
                break;
            case 'payment-main-countrylist':
                recordStore = record.getCountriesStore;
                break;
            case 'payment-main-subshoplist':
                recordStore = record.getShopsStore;
                break;
        }

        var store = grid.getStore().load({
            callback: function(){
                var matches = [];
                //Selects each country and sets the surcharge
				if(recordStore){
					Ext.each(recordStore.data.items, function(item){
						var tmpRecord = store.getById(item.get('id'));
						matches.push(tmpRecord);
						tmpRecord.data.surcharge = item.get('surcharge');
					});
					grid.getSelectionModel().select(matches);
				}
            }
        });
    },

    /**
     * Is fired, when the "create"-button is pressed
     * @param btn Contains the create-button
     */
    onCreatePayment:function(btn){
        var win = btn.up('window'),
			tabPanel = win.down('tabpanel'),
			formPanel = win.down('form'),
            paymentModel = Ext.create('Shopware.apps.Payment.model.Payment'),
			gridToolBar = win.down('toolbar[name=gridToolBar]'),
			btnSave = gridToolBar.down('button[name=save]');

		paymentModel.set('source', 1);

		tabPanel.setDisabled(false);
		tabPanel.setActiveTab(0);
		formPanel.loadRecord(paymentModel);
		btnSave.enable(true);
    },

    /**
     * Is fired, when the user wants to update a payment
     * Sets the surcharges and the selections and saves them
     * @param generalForm Contains the general form-panel
     * @param countryGrid Contains the grid with all countries
     * @param subShopGrid Contains the grid with all subShops
     * @param surchargeGrid Contains the grid with all surcharges
     */
    onSavePayment:function(generalForm, countryGrid, subShopGrid, surchargeGrid){
        var record = generalForm.getRecord(),
			win = generalForm.up('window'),
			tree = win.down('treepanel'),
			paymentStore = tree.getStore(),
			tabPanel = win.down('tabpanel'),
			me = this;

		generalForm.getForm().updateRecord(record);

        var surchargeStore = Ext.clone(record['getCountriesStore']),
            surchargeString = "";

        //Creates a string with the surcharges and the iso of the countries
        Ext.each(surchargeStore.data.items, function(item){
            if(item.data.surcharge) {
                surchargeString = surchargeString + item.data.iso + ":" + item.data.surcharge + ";";
            }
        });
        surchargeString = surchargeString.slice(0, surchargeString.length - 1);
        record.data.surchargeString = surchargeString;

        var countryStore = record['getCountriesStore'];

        var subshops = subShopGrid.getSelectionModel().getSelection(),
            subshopStore = record['getShopsStore'];

        //If the tab is activated at least once, so changes could be made
        if(subShopGrid.rendered) {
            subshopStore.removeAll();
            subshopStore.add(subshops);
        }

        //If the tab is activated at least once, so changes could be made
        if(countryGrid.rendered) {
            var updated = surchargeGrid.getStore().getUpdatedRecords(),
                selection = countryGrid.getSelectionModel().getSelection();

            // Merge updated records to the main countyStore
            if(updated.length) {
                Ext.each(updated, function(record) {
                    var id = record.get('id');

                    Ext.each(selection, function(tmpRecord) {
                        if(tmpRecord.get('id') === id) {
                            tmpRecord.set('surcharge', record.get('surcharge'));

                            // Clear dirty state
                            tmpRecord.dirty = false;
                            tmpRecord.modified = false;
                        }
                    });

                });
            }
            countryStore.removeAll();
            countryStore.add(selection);
        }

        record.save({
            callback: function(data, operation){
                var records = operation.getRecords(),
                    record = records[0],
                    rawData = record.getProxy().getReader().rawData;

                if(operation.success){
					paymentStore.load();

					//tabPanel, newTab, oldTab, formPanel
					me.onChangeTab(tabPanel, tabPanel.getActiveTab(), '', generalForm);
                    Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart gespeichert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'update_growl_message_content','default'=>'The payment was successfully updated.','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_content','default'=>'The payment was successfully updated.','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Zahlungsart wurde erfolgreich gespeichert.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_content','default'=>'The payment was successfully updated.','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
", '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_title','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                } else {
                    Shopware.Notification.createGrowlMessage('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsart gespeichert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update_growl_message_subject','default'=>'Update payment','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', rawData.errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment_title','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment_title','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                }
            }
        });
    },

    /**
     * Function to load all data into the grids and formpanels
     *
     * @param [Ext.view.View] view
     * @param [Ext.data.Model] record The clicked record
     */
    onItemClick:function(view, record) {
        var win = view.up('window'),
            tabPanel = win.tabPanel,
            form = win.generalForm,
            treeToolBar = win.down('toolbar[name=treeToolBar]'),
            gridToolBar = win.down('toolbar[name=gridToolBar]'),
            btnSave = gridToolBar.down('button[name=save]'),
            btnDelete = treeToolBar.down('button[name=delete]'),
            surchargeGrid = win.down('payment-main-surcharge');

        surchargeGrid.reconfigure(Ext.clone(record.getCountriesStore));

		if(record.get('source') == 1){
			btnDelete.enable();
		}else{
			btnDelete.disable();
		}
        tabPanel.setDisabled(false);
        btnSave.enable(true);
        tabPanel.setActiveTab(0);

        form.loadRecord(record);
    }
});
//<?php }} ?>