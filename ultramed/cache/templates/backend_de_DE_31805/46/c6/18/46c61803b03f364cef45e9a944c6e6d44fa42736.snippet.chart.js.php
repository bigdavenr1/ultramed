<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/chart.js" */ ?>
<?php /*%%SmartyHeaderCode:1759543713554347857b5080-54059036%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46c61803b03f364cef45e9a944c6e6d44fa42736' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/main/chart.js',
      1 => 1430113330,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1759543713554347857b5080-54059036',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347857ea627_04647392',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347857ea627_04647392')) {function content_554347857ea627_04647392($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Chart Base Class
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.main.Chart', {
    extend: 'Ext.chart.Chart',
    alias: 'widget.analytics-chart',
    multipleShopTips: false,
    animate: true,
    theme: 'Category3',

    initComponent: function () {
        var me = this;

        me.callParent(arguments);
    },


    initMultipleShopTipsStores: function () {
        var me = this;

        me.tipStore = Ext.create('Ext.data.JsonStore', {
            fields: ['name', 'data']
        });

        me.tipStoreTable = Ext.create('Ext.data.JsonStore', {
            fields: ['name', 'data']
        });

        me.tipChart = {
            xtype: 'chart',
            width: 100,
            height: 100,
            animate: false,
            store: me.tipStore,
            shadow: false,
            insetPadding: 5,
            theme: 'Base:gradients',
            series: [
                {
                    type: 'pie',
                    field: 'data',
                    showInLegend: false,
                    label: {
                        field: 'name',
                        display: 'rotate',
                        contrast: true,
                        font: '9px Arial'
                    }
                }
            ]
        };

        me.tipGrid = {
            xtype: 'grid',
            store: me.tipStoreTable,
            height: 130,
            flex: 1,
            columns: [
                {
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"main/chart/name",'default'=>'Name','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"main/chart/name",'default'=>'Name','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"main/chart/name",'default'=>'Name','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    dataIndex: 'name',
                    flex: 1
                },
                {
                    xtype: 'numbercolumn',
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    dataIndex: 'data',
                    align: 'right',
                    flex: 1
                }
            ]
        };
    },

    initMultipleShopTipsData: function (item, tipObj, dateFormatString, defaultTitle) {

        var storeItem = item.storeItem, me = this,
            dataChart = [], dataTable = [];

        if (!dateFormatString) {
            dateFormatString = 'F, Y';
        }

        if (!defaultTitle) {
            defaultTitle = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
        }

        me.shopStore.each(function (shop) {
            var value = storeItem.get('turnover' + shop.data.id);

            if (!value) {
                return;
            }

            if (shop.data.currencyChar == "&euro;") {
                shop.data.currencyChar = "€";
            }

            // Data for chart
            dataChart[dataChart.length] = { name: shop.data.name, data: value};
            // Data for table
            dataTable[dataTable.length] = { name: shop.data.name, data: value};

        });
        // Load data with plain values into pie chart
        me.tipStore.loadData(dataChart);

        // Add total sum to table
        dataTable[dataTable.length] = {
            name: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            data: storeItem.get('turnover')
        };

        // Load formatted data with sum row into table
        me.tipStoreTable.loadData(dataTable);

        if (!dataChart.length) {
            tipObj.hide();
        } else {
            tipObj.show();
        }
        tipObj.setTitle(defaultTitle + " " + Ext.Date.format(storeItem.get('date'), dateFormatString));
    },

    createLineSeries: function(config, tips) {
        var defaultConfig = {
            type: 'line',
            axis: [ 'left', 'bottom' ],
            highlight: true,
            fill: true,
            smooth: true
        },
        tipsConfig = {
            trackMouse: true,
            height: 60,
            width: 120,
            layout: 'fit',
            highlight: {
                size: 7,
                radius: 7
            }
        };

        if (Ext.isObject(config)) {
            defaultConfig = Ext.apply({ }, config, defaultConfig);
        }

        if (Ext.isObject(tips)) {
            tipsConfig = Ext.apply({ }, tips, tipsConfig);
        }

        defaultConfig.tips = tipsConfig;

        return defaultConfig;
    },

    getAxesFields: function(fieldPrefix) {
        var me = this,
            fields = [];

        if (me.shopSelection == Ext.undefined || me.shopSelection.length <= 0) {
            return [fieldPrefix];
        }

        Ext.each(me.shopSelection, function (shopId) {
            fields.push(fieldPrefix + shopId);
        });
    
        return fields;
    },

    getAxesTitles: function(defaultName) {
        var me = this,
            titles = [];

        if (me.shopSelection == Ext.undefined || me.shopSelection.length <= 0) {
            return defaultName;
        }

        Ext.each(me.shopSelection, function (shopId) {
            if (shopId) {
                var shop = me.shopStore.getById(shopId);
                titles.push(shop.get('name'));
            }
        });

        return titles;
    }


});
//
<?php }} ?>