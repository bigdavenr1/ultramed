<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/comment.js" */ ?>
<?php /*%%SmartyHeaderCode:398505867554476fc458e60-05553423%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4698cd61b8c30ad0cca0ecbff8953f928048890b' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/comment.js',
      1 => 1430112875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '398505867554476fc458e60-05553423',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc475088_30978153',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc475088_30978153')) {function content_554476fc475088_30978153($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model -  Blog backend module.
 *
 * The blog comment model of the blog module represent a data row of the s_blog_comments or the
 * Shopware\Models\Blog\Comment doctrine model, with some additional data for the additional information panel.
 */
//
Ext.define('Shopware.apps.Blog.model.Comment', {
	/**
	* Extends the standard ExtJS 4
	* @string
	*/
    extend : 'Ext.data.Model',
	/**
	* The fields used for this model
	* @array
	*/
    fields : [
		//
        { name : 'id', type : 'int' },
        { name : 'name', type : 'string' },
        { name : 'headline', type : 'string' },
        { name : 'content', type : 'string' },
        { name : 'creationDate', type : 'date' },
        { name : 'active', type : 'boolean' },
        { name : 'points', type : 'float' },
        { name : 'eMail', type : 'string' }
    ],
	/**
	* If the name of the field is 'id' extjs assumes autmagical that
	* this field is an unique identifier. 
	*/
    idProperty : 'id',
	/**
	* Configure the data communication
	* @object
	*/
    proxy : {
        type : 'ajax',
        api:{
            read:   '<?php echo '/backend/Blog/getBlogComments';?>',
            update: '<?php echo '/backend/Blog/acceptBlogComment/targetField/blogComments';?>',
            destroy:'<?php echo '/backend/Blog/deleteBlogComment/targetField/blogComments';?>'
        },
        reader : {
            type : 'json',
            root : 'data',
            totalProperty: 'totalCount'
        }
    }
});
//
<?php }} ?>