<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/store/encoder.js" */ ?>
<?php /*%%SmartyHeaderCode:178491109455447cb12eb592-85619746%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a28cd14b63730211984b13fbaf770e7dd224748' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/store/encoder.js',
      1 => 1430113180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '178491109455447cb12eb592-85619746',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb12f2a92_03472497',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb12f2a92_03472497')) {function content_55447cb12f2a92_03472497($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Encoder Systeminfo backend module.
 *
 * The encoder store contains the used encoder.
 */
//
Ext.define('Shopware.apps.Systeminfo.store.Encoder', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',
    /**
    * Auto load the store after the component
    * is initialized
    * @boolean
    */
    autoLoad: true,

    /**
    * Define the used model for this store
    * @string
    */
    model : 'Shopware.apps.Systeminfo.model.Encoder'
});
//<?php }} ?>