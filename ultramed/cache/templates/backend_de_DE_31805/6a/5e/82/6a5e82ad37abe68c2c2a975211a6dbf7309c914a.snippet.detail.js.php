<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/model/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:155154859055447ea43dcb21-32657183%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a5e82ad37abe68c2c2a975211a6dbf7309c914a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/model/detail.js',
      1 => 1430113159,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '155154859055447ea43dcb21-32657183',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea440eae8_04547365',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea440eae8_04547365')) {function content_55447ea440eae8_04547365($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model -  partner detail
 * The partner detail model of the partner module represent a data row of the s_emarketing_partner or the
 * Shopware\Models\Partner\Partner doctrine model, with some additional data for the additional information panel.
 */
//
Ext.define('Shopware.apps.Partner.model.Detail', {
	/**
	* Extends the standard ExtJS 4
	* @string
	*/
    extend : 'Ext.data.Model',
	/**
	* The fields used for this model
	* @array
	*/
    fields : [
		//
        { name : 'id', type : 'int' },
        { name : 'idCode', type : 'string' },
        { name : 'date', type : 'date' },
        { name : 'customerId', type : 'int' },
        { name : 'company', type : 'string' },
        { name : 'contact', type : 'string' },
        { name : 'street', type : 'string' },
        { name : 'streetNumber', type : 'string' },
        { name : 'zipCode', type : 'string' },
        { name : 'city', type : 'string' },
        { name : 'phone', type : 'string' },
        { name : 'fax', type : 'string' },
        { name : 'countryName', type : 'string' },
        { name : 'email', type : 'string' },
        { name : 'web', type : 'string' },
        { name : 'profile', type : 'string' },
        { name : 'fix', type : 'float' },
        { name : 'percent', type : 'float' },
        { name : 'cookieLifeTime', type : 'int' },
        { name : 'active', type : 'int' }
    ],
	/**
	* If the name of the field is 'id' extjs assumes autmagical that
	* this field is an unique identifier. 
	*/
    idProperty : 'id',
	/**
	* Configure the data communication
	* @object
	*/
    proxy : {
        type : 'ajax',
        api:{
            read:   '<?php echo '/backend/Partner/getDetail';?>',
            create: '<?php echo '/backend/Partner/savePartner';?>',
            update: '<?php echo '/backend/Partner/savePartner';?>',
            destroy:'<?php echo '/backend/Partner/deletePartner';?>'
        },
        reader : {
            type : 'json',
            root : 'data'
        }
    }
});
//
<?php }} ?>