<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/window.js" */ ?>
<?php /*%%SmartyHeaderCode:174902191455447e756b9f15-98124295%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3963bd78b2de45759047f323c1ba6eeb92d6946d' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/window.js',
      1 => 1430113393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '174902191455447e756b9f15-98124295',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e756ee434_24585835',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e756ee434_24585835')) {function content_55447e756ee434_24585835($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - ProductFeed main window.
 *
 * Displays all Detail Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'detail_title','default'=>'Feed - Configuration','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'detail_title','default'=>'Feed - Configuration','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Feed - Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'detail_title','default'=>'Feed - Configuration','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.product_feed-feed-window',
    border: false,
    autoShow: true,
    layout: 'fit',
    height: '90%',
    /**
     * Display no footer button for the detail window
     * @boolean
     */
    footerButton:false,
    width: 925,

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        // Create our form panel and assign it to the namespace for later usage
        me.formPanel = me.createFormPanel();
        me.items = me.formPanel;

        me.dockedItems = [{
            dock: 'bottom',
            xtype: 'toolbar',
            ui: 'shopware-ui',
            cls: 'shopware-toolbar',
            items: me.createFormButtons()
        }];
        me.callParent(arguments);

        // Load form data if we're having a record
        if(me.record){
            //set the default value
            if(me.record.data.encodingId == 0) me.record.data.encodingId = 1;
            if(me.record.data.formatId == 0) me.record.data.formatId = 1;

            me.formPanel.loadRecord(me.record);
        }
    },

    /**
     * creates the form panel
     */
    createFormPanel: function(){
        var me = this;
        return Ext.create('Ext.form.Panel', {
            layout: {
                type: 'vbox',
                align : 'stretch'
            },
            defaults: { flex: 1 },
            unstyled: true,
            items: [
                {
                    xtype:'product_feed-feed-detail',
                    record: me.record,
                    comboTreeCategoryStore: me.comboTreeCategoryStore,
                    shopStore: me.shopStore,
                    availableCategoriesTree: me.availableCategoriesTree,
                    style:'padding: 10px'
                },
                {
                    xtype: 'tabpanel',
                    minHeight: 320,
                    items: [
                        {
                            xtype:'product_feed-feed-tab-format'
                        },
                        {
                            xtype:'product_feed-feed-tab-header'
                        },
                        {
                            xtype:'product_feed-feed-tab-body'
                        },
                        {
                            xtype:'product_feed-feed-tab-footer'
                        },
                        {
                            xtype:'product_feed-feed-tab-category',
                            availableCategoriesTree: me.availableCategoriesTree,
                            record: me.record
                        },
                        {
                            xtype:'product_feed-feed-tab-supplier',
                            supplierStore: me.supplierStore,
                            record: me.record
                        },
                        {
                            xtype:'product_feed-feed-tab-article',
                            articleStore: me.articleStore,
                            record: me.record
                        },
                        {
                            xtype:'product_feed-feed-tab-filter'
                        }
                    ]
                }
            ]
        });
    },
    /**
     * creates the form buttons cancel and save
     */
    createFormButtons: function(){
        var me = this;
        return ['->',
            {
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                scope:me,
                cls: 'secondary',
                handler:function () {
                    this.destroy();
                }
            },
            {
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action:'save',
                disabled:true,
                cls:'primary'
            }
        ];
    }
});
//
<?php }} ?>