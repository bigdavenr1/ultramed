<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/partner/list.js" */ ?>
<?php /*%%SmartyHeaderCode:108094712655447ea448ca47-30518299%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b32dc3d5099597af1d699c8848b01059a6503bd1' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/partner/list.js',
      1 => 1430113389,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '108094712655447ea448ca47-30518299',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea45235e0_50667839',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea45235e0_50667839')) {function content_55447ea45235e0_50667839($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - partner list window.
 *
 * Displays the partner list
 */
//
Ext.define('Shopware.apps.Partner.view.partner.List', {
    extend:'Ext.grid.Panel',
    border: false,
    alias:'widget.partner-partner-list',
    region:'center',
    autoScroll:true,
    store:'List',
    ui:'shopware-ui',
    /**
     * Initialize the Shopware.apps.Customer.view.main.List and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.registerEvents();
        me.columns = me.getColumns();
        me.toolbar = me.getToolbar();
        me.pagingbar = me.getPagingBar();
        me.store = me.listStore;
        me.dockedItems = [ me.toolbar, me.pagingbar ];
        me.callParent(arguments);
    },
    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(

                /**
                 * Event will be fired when the user clicks the delete icon in the
                 * action column
                 *
                 * @event deleteColumn
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'deleteColumn',

                /**
                 * Event will be fired when the user clicks the delete icon in the
                 * action column
                 *
                 * @event editColumn
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'editColumn',

                /**
                 * Event will be fired when the user clicks the exectue icon in the
                 * action column
                 *
                 * @event statistic
                 * @param [object] View - Associated Ext.view.Table
                 * @param [integer] rowIndex - Row index
                 * @param [integer] colIndex - Column index
                 * @param [object] item - Associated HTML DOM node
                 */
                'statistic'
        );

        return true;
    },
    /**
     * Creates the grid columns
     *
     * @return [array] grid columns
     */
    getColumns:function () {
        var me = this;

        var columnsData = [
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Firma<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'company',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'registered','default'=>'Registered','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'registered','default'=>'Registered','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Eingetragen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'registered','default'=>'Registered','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'date',
                xtype: 'datecolumn',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'active',
                renderer: me.activeRenderer,
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'monthly_amount','default'=>'Monthly turnover','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'monthly_amount','default'=>'Monthly turnover','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Monatsumsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'monthly_amount','default'=>'Monthly turnover','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'monthlyAmount',
                xtype: 'numbercolumn',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'yearly_amount','default'=>'Yearly turnover','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'yearly_amount','default'=>'Yearly turnover','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Jahresumsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'yearly_amount','default'=>'Yearly turnover','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'yearlyAmount',
                xtype: 'numbercolumn',
                flex:1
            },
            {
                header:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'column'/'partner_link','default'=>'Partner link','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'partner_link','default'=>'Partner link','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner Link<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'column'/'partner_link','default'=>'Partner link','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                dataIndex:'idCode',
                renderer: me.partnerLinkRenderer,
                flex:1
            },
            {
                xtype:'actioncolumn',
                width:130,
                items:me.getActionColumnItems()
            }
        ];
        return columnsData;
    },

    /**
     * renders the active field of the grid
     *
     * @param value
     * @return { String }
     */
    activeRenderer : function(value) {
        if(value) {
            return "<span style='font-weight: 700; color:green;'><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'active_value'/'yes','default'=>'Yes','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'active_value'/'yes','default'=>'Yes','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
ja<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'active_value'/'yes','default'=>'Yes','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>";
        }
        return "<span style='font-weight: 700; color:red;'><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'active_value'/'no','default'=>'No','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'active_value'/'no','default'=>'No','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
nein<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'active_value'/'no','default'=>'No','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>";
    },

    /**
     * renders the partner link of the grid
     *
     * @param value
     * @return { String }
     */
    partnerLinkRenderer : function(value) {
        return '<a href="<?php echo '/backend/partner/redirectToPartnerLink';?>' + '?sPartner='+ value + '" target="_blank">' + 'link' + '</a>';
    },
    /**
     * Creates the items of the action column
     *
     * @return [array] action column itesm
     */
    getActionColumnItems: function () {
        var me = this,
            actionColumnData = [];

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'update'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
            actionColumnData.push({
                iconCls:'sprite-pencil',
                cls:'editBtn',
                tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner editieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'edit','default'=>'Edit partner','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                handler:function (view, rowIndex, colIndex, item) {
                    me.fireEvent('editColumn', view, rowIndex, colIndex, item);
                }
            });
            /*<?php }?>*/

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
            actionColumnData.push({
               iconCls:'sprite-minus-circle-frame',
               action:'delete',
               cls:'delete',
               tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'delete','default'=>'Delete partner','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
               handler:function (view, rowIndex, colIndex, item) {
                   me.fireEvent('deleteColumn', view, rowIndex, colIndex, item);
               }
            });
            /*<?php }?>*/

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'statistic'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp3){?>*/
            actionColumnData.push({
                iconCls:'sprite-chart-up-color',
                cls:'chart-up-color',
                tooltip:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Statistik<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'action_column'/'statistic','default'=>'Statistics','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                handler:function (view, rowIndex, colIndex, item) {
                    me.fireEvent('statistic', view, rowIndex, colIndex, item);
                }
            });
            /*<?php }?>*/
        return actionColumnData;
    },
    /**
     * Creates the grid toolbar with the add and delete button
     *
     * @return [Ext.toolbar.Toolbar] grid toolbar
     */
    getToolbar:function () {
        return Ext.create('Ext.toolbar.Toolbar',
            {
                dock:'top',
                ui:'shopware-ui',
                items:[
                    /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp4=ob_get_clean();?><?php if ($_tmp4){?>*/
                    {
                        iconCls:'sprite-plus-circle',
                        text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'list'/'button'/'add','default'=>'Add','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'add','default'=>'Add','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'list'/'button'/'add','default'=>'Add','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                        action:'add'
                    }
                    /*<?php }?>*/
                ]
            });
    },
    /**
     * Creates the paging toolbar for the grid to allow
     * and store paging. The paging toolbar uses the same store as the Grid
     *
     * @return Ext.toolbar.Paging The paging toolbar for the customer grid
     */
    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store:me.listStore,
            dock:'bottom',
            displayInfo:true
        });

    }
});
//
<?php }} ?>