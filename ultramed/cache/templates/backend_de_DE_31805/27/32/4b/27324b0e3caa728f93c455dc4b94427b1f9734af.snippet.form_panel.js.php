<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/form_panel.js" */ ?>
<?php /*%%SmartyHeaderCode:11684012735541e663ccf820-98314776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '27324b0e3caa728f93c455dc4b94427b1f9734af' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/form_panel.js',
      1 => 1430113391,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11684012735541e663ccf820-98314776',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663d29fe9_74676775',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663d29fe9_74676775')) {function content_5541e663d29fe9_74676775($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - The general-formpanel to edit general payment-information
 *
 * todo@all: Documentation
 *
 */
//
Ext.define('Shopware.apps.Payment.view.payment.FormPanel', {
    extend : 'Ext.form.Panel',
    autoShow: true,
    alias : 'widget.payment-main-formpanel',
    region: 'center',
    layout: 'anchor',
    autoScroll: true,
    bodyPadding: '10px',
    name:  'formpanel',
    preventHeader: true,
    border: 0,
    defaults:{
        labelStyle:'font-weight: 700; text-align: right;',
        labelWidth:130,
        anchor:'100%'
    },
	plugins: [{
		pluginId: 'translation',
		ptype: 'translation',
		translationType: 'config_payment',
		translationMerge: true
	}],

    /**
     * This function is called, when the component is initiated
     * It creates the columns of the grid
     */
    initComponent: function(){
        var me = this;
        me.items = me.getItems();
        me.callParent(arguments);
    },

    /**
     * This function creates the columns of the grid
     * @return Array
     */
    getItems: function(){
        var items = [{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_description_label','default'=>'Description','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_description_label','default'=>'Description','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bezeichnung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_description_label','default'=>'Description','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'description',
			translatable: true
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_name_label','default'=>'Name','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_name_label','default'=>'Name','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_name_label','default'=>'Name','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'name'
        },{
            xtype: 'textfield',
            hidden: true,
            name: 'id'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_template_label','default'=>'Template','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_template_label','default'=>'Template','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Template<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_template_label','default'=>'Template','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'template'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_class_label','default'=>'Class','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_class_label','default'=>'Class','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Systemklasse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_class_label','default'=>'Class','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'class'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_table_label','default'=>'General surcharge','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_table_label','default'=>'General surcharge','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datenbank-Tabelle<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_table_label','default'=>'General surcharge','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'table'
        },{
            xtype: 'textarea',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_additional'-'description_label','default'=>'Additional description','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_additional'-'description_label','default'=>'Additional description','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zusätzliche Beschreibung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_additional'-'description_label','default'=>'Additional description','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'additionalDescription',
			translatable: true
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_surcharge_label','default'=>'Surcharge in %','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_surcharge_label','default'=>'Surcharge in %','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aufschlag/Abschlag (in %)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_surcharge_label','default'=>'Surcharge in %','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'debitPercent'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_generalSurcharge_label','default'=>'General Surcharge','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_generalSurcharge_label','default'=>'General Surcharge','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Pauschaler Aufschlag<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_generalSurcharge_label','default'=>'General Surcharge','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'surcharge',
			supportText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'payment'/'surcharge'/'supportText','default'=>'Use \\\',\\\' or \\\'.\\\' for decimal numbers','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment'/'surcharge'/'supportText','default'=>'Use \\\',\\\' or \\\'.\\\' for decimal numbers','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Benutze \',\' oder \'.\' für Dezimalzahlen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'payment'/'surcharge'/'supportText','default'=>'Use \\\',\\\' or \\\'.\\\' for decimal numbers','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_position_surcharge','default'=>'Position','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_position_surcharge','default'=>'Position','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Position<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_position_surcharge','default'=>'Position','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'position'
        },{
            xtype: 'checkbox',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_active_label','default'=>'Active','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_active_label','default'=>'Active','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_active_label','default'=>'Active','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            inputValue: 1,
            uncheckedValue: 0,
            name: 'active'
        },{
            xtype: 'checkbox',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_esdActive_label','default'=>'Active for ESD products','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_esdActive_label','default'=>'Active for ESD products','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv für ESD-Produkte<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_esdActive_label','default'=>'Active for ESD products','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            inputValue: 1,
            uncheckedValue: 0,
            name: 'esdActive'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_urlIFrame_label','default'=>'URL for iFrame','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_urlIFrame_label','default'=>'URL for iFrame','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
URL für iFrame<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_urlIFrame_label','default'=>'URL for iFrame','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'embedIFrame'
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_action_label','default'=>'Action','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_action_label','default'=>'Action','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Action<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_action_label','default'=>'Action','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'action',
            disabled: true
        },{
            xtype: 'textfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'formpanel_pluginID_label','default'=>'PluginID','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_pluginID_label','default'=>'PluginID','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PluginID<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'formpanel_pluginID_label','default'=>'PluginID','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'pluginId',
            disabled: true
        }];

        return items;
    }
});
//<?php }} ?>