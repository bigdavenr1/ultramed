<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/store/risks.js" */ ?>
<?php /*%%SmartyHeaderCode:92599084655447cd7b52a70-82498798%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '274c6c04e5f0e958bf41fd2aeca8d6f651499775' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/store/risks.js',
      1 => 1430113171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '92599084655447cd7b52a70-82498798',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd7c1f592_60463076',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd7c1f592_60463076')) {function content_55447cd7c1f592_60463076($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Risks store
 *
 * This store contains all risks.
 */
//
Ext.define('Shopware.apps.RiskManagement.store.Risks', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',

	/**
	* The fields used for this store
	* @array
	*/
	fields: [
		{ name: 'description', type: 'string' },
		{ name: 'value', type: 'string' }
	],

	data: [
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'startValue','default'=>'Please choose','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'startValue','default'=>'Please choose','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bitte auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'startValue','default'=>'Please choose','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: "" },
        //
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'orderValueGt','default'=>'Ordervalue >=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderValueGt','default'=>'Ordervalue >=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellwert >=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderValueGt','default'=>'Ordervalue >=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ORDERVALUEMORE' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'orderValueLt','default'=>'Ordervalue <=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderValueLt','default'=>'Ordervalue <=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellwert <=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderValueLt','default'=>'Ordervalue <=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ORDERVALUELESS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'customerGroupIs','default'=>'Customergroup IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerGroupIs','default'=>'Customergroup IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppe IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerGroupIs','default'=>'Customergroup IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'CUSTOMERGROUPIS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'customerGroupIsNot','default'=>'Customergroup IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerGroupIsNot','default'=>'Customergroup IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundengruppe IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerGroupIsNot','default'=>'Customergroup IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'CUSTOMERGROUPISNOT' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kunde IST NEU<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'newCustomer','default'=>'Customer IS NEW','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'NEWCUSTOMER' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zone IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zoneIs','default'=>'Zone IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ZONEIS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Zone IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zoneIsNot','default'=>'Zone IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ZONEISNOT' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'countryIs','default'=>'Country IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'LANDIS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'countryIsNot','default'=>'Country IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'LANDISNOT' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'orderPositionsGt','default'=>'Orderpositions >=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderPositionsGt','default'=>'Orderpositions >=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Bestellpositionen >=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'orderPositionsGt','default'=>'Orderpositions >=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ORDERPOSITIONSMORE' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'dunninglevelone','default'=>'Dunning level 1 IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunninglevelone','default'=>'Dunning level 1 IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 1 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunninglevelone','default'=>'Dunning level 1 IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'DUNNINGLEVELONE' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'dunningleveltwo','default'=>'Dunning level 2 IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunningleveltwo','default'=>'Dunning level 2 IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 2 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunningleveltwo','default'=>'Dunning level 2 IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'DUNNINGLEVELTWO' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'dunninglevelthree','default'=>'Dunning level 3 IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunninglevelthree','default'=>'Dunning level 3 IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mahnstufe 3 IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'dunninglevelthree','default'=>'Dunning level 3 IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'DUNNINGLEVELTHREE' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Inkasso IST WAHR<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'encashment','default'=>'Encashment IS TRUE','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'INKASSO' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine Bestellung vor mind. X Tagen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'lastOrderLess','default'=>'No order before at least X days','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'LASTORDERLESS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzahl Bestellungen <=<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'ordersLess','default'=>'Quantity orders <=','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'LASTORDERSLESS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel aus Kategorie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleFromCategory','default'=>'Article from category','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ARTICLESFROM' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'zipCodeIs','default'=>'Zipcode IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zipCodeIs','default'=>'Zipcode IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Postleitzahl IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'zipCodeIs','default'=>'Zipcode IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ZIPCODE' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'streetNameContains','default'=>'Streetname CONTAINS X','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'streetNameContains','default'=>'Streetname CONTAINS X','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Straßenname ENTHÄLT X<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'streetNameContains','default'=>'Streetname CONTAINS X','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'PREGSTREET' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'customerNumberIs','default'=>'Customernumber IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerNumberIs','default'=>'Customernumber IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundennummer IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'customerNumberIs','default'=>'Customernumber IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'CUSTOMERNR' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'lastNameContains','default'=>'Lastname CONTAINS X','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'lastNameContains','default'=>'Lastname CONTAINS X','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Nachname ENTHÄLT X<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'lastNameContains','default'=>'Lastname CONTAINS X','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'LASTNAME' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'subShopIs','default'=>'Shop IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'SUBSHOP' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shop IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'subShopIsNot','default'=>'Shop IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'SUBSHOPNOT' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Lieferadresse != Rechnungsadresse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'shippingAddressDifferBillingAddress','default'=>'Shipping-Address != Billing-Address','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'DIFFER' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel IST<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'currencyIsoIs','default'=>'Currency Iso IS','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'CURRENCIESISOIS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Währungs-Iso-Kürzel IST NICHT<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'currencyIsoIsNot','default'=>'Currency Iso IS NOT','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'CURRENCIESISOISNOT' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Attribut IST (1>5)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleAttributeIs','default'=>'Article attribute IS (1>5)','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ATTRIS' },
		{ description: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'risks_store'/'comboBox'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Artikel-Attribut IST NICHT (1>5)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'risks_store'/'comboBox'/'articleAttributeIsNot','default'=>'Article attribute IS NOT (1>5)','namespace'=>'backend/risk_management/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', value: 'ATTRISNOT' }
	]
});
//<?php }} ?>