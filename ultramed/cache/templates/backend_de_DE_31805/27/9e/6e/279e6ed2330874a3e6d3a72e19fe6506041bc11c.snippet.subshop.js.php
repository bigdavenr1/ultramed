<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/subshop.js" */ ?>
<?php /*%%SmartyHeaderCode:141815726855447cd7911742-91633724%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '279e6ed2330874a3e6d3a72e19fe6506041bc11c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/model/subshop.js',
      1 => 1430113171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '141815726855447cd7911742-91633724',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd7924c32_13972066',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd7924c32_13972066')) {function content_55447cd7924c32_13972066($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Premium
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.RiskManagement.model.Subshop', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend:'Ext.data.Model',
	/**
	* Configure the data communication
	* @object
	*/
	proxy: {
		type: 'ajax',
		/**
		* Configure the url mapping for the different
		* @object
		*/
		api: {
			//read out all articles
			read: '<?php echo '/backend/premium/getSubShops';?>'
		},
		/**
		* Configure the data reader
		* @object
		*/
		reader: {
			type: 'json',
			root: 'data'
		}
	},
    /**
     * The fields used for this model
     * @array
     */
    fields:[
		//
        { name:'id', type:'int' },
        { name:'localeId', type:'int' },
        { name:'categoryId', type:'int' },
        { name:'name', type:'string' }
    ]
});

//<?php }} ?>