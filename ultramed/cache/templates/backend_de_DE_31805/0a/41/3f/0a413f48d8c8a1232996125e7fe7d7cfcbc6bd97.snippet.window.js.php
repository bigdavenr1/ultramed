<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:58027829755447ce3802bb9-04929697%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0a413f48d8c8a1232996125e7fe7d7cfcbc6bd97' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/main/window.js',
      1 => 1430111764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58027829755447ce3802bb9-04929697',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'storeApiAvailable' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce384f203_85448453',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce384f203_85448453')) {function content_55447ce384f203_85448453($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.main.Window', {
    /**
     * Define that the plugin manager main window is an extension of the enlight application window
     * @string
     */
    extend:'Enlight.app.Window',
    /**
     * Set base css class prefix and module individual css class for css styling
     * @string
     */
    cls:Ext.baseCSSPrefix + 'plugin-manager-main-window',
    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.plugin-manager-main-window',
    /**
     * Set no border for the window
     * @boolean
     */
    border:false,
    /**
     * True to automatically show the component upon creation.
     * @boolean
     */
    autoShow:true,
    /**
     * Set border layout for the window
     * @string
     */
    layout:'fit',
    /**
     * Define window width
     * @integer
     */
    width:1000,
    /**
     * Define window height
     * @integer
     */
    height:'90%',
    /**
     * True to display the 'maximize' tool button and allow the user to maximize the window, false to hide the button and disallow maximizing the window.
     * @boolean
     */
    maximizable:true,
    /**
     * True to display the 'minimize' tool button and allow the user to minimize the window, false to hide the button and disallow minimizing the window.
     * @boolean
     */
    minimizable:true,
    /**
     * A flag which causes the object to attempt to restore the state of internal properties from a saved state on startup.
     */
    stateful:true,
    /**
     * The unique id for this object to use for state management purposes.
     */
    stateId:'shopware-plugin-manager-main-window',

    /**
     * Title of the window.
     * @string
     */
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','default'=>'Plugin Manager 2.0','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'Plugin Manager 2.0','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin-Manager 2.0<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'Plugin Manager 2.0','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.tabPanel = me.createTabPanel();
        me.items = me.tabPanel;
        me.callParent(arguments);
    },

    /**
     * Creates a tab panel which holds off the different sections
     * of the detail page.
     *
     * @public
     * @return [object] Ext.tab.Panel
     */
    createTabPanel: function() {
        var me = this;

        me.managerContainer = Ext.create('Ext.container.Container', {
            layout: 'card',
            autoScroll: true,
            region: 'center',
            items: [{
                xtype: 'plugin-manager-manager-grid',
                border: 0,
                pluginStore: me.pluginStore
            }]
        });

        /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
        me.storeContainer = Ext.create('Ext.container.Container', {
            layout: 'card',
            autoScroll: true,
            region: 'center',
            name: 'store-card',
            items: [{
                xtype: 'plugin-manager-store-view',
                topSellerStore: me.topSellerStore,
                communityStore: me.communityStore,
                categoryStore: me.categoryStore
            }]
        });
        /** <?php }?> */

        return Ext.create('Ext.tab.Panel', {
            xtype: 'tabpanel',
            plain: true,
            name: 'main-tab',
            items: [{
                xtype: 'panel',
                layout: 'border',
                name: 'manager',
                initialTitle: 'manager',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tabs'/'manager','default'=>'Extensions / Purchases','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'manager','default'=>'Extensions / Purchases','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkäufe / Erweiterungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'manager','default'=>'Extensions / Purchases','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [{
                    xtype: 'plugin-manager-manager-navigation',
                    region: 'west',
                    width: 220,
                    updatesStore: me.updatesStore
                }, me.managerContainer ]
            },
            /** <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?> */
            {
                xtype: 'panel',
                layout: 'border',
                name: 'store',
                initialTitle: 'store',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tabs'/'store','default'=>'Community Store','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'store','default'=>'Community Store','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Community Store<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tabs'/'store','default'=>'Community Store','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                items: [{
                    xtype: 'plugin-manager-store-navigation',
                    region: 'west',
                    width: 220,
                    categoryStore: me.categoryStore,
                    updatesStore: me.updatesStore
                }, me.storeContainer ]
            }/** <?php }?> */]
        });
    }
});
//
<?php }} ?>