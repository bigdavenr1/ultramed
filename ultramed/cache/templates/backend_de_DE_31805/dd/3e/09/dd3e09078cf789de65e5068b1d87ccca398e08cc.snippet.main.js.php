<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:332647678554476fc941a96-75336177%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dd3e09078cf789de65e5068b1d87ccca398e08cc' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/controller/main.js',
      1 => 1430112874,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '332647678554476fc941a96-75336177',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc962c89_15162130',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc962c89_15162130')) {function content_554476fc962c89_15162130($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - blog main backend module
 *
 * The blog module main controller handles the initialisation of the blog backend list.
 */
//
Ext.define('Shopware.apps.Blog.controller.Main', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
	extend: 'Ext.app.Controller',

    mainWindow: null,

	/**
	 * Creates the necessary event listener for this
	 * specific controller and opens a new Ext.window.Window
	 * to display the subapplication
     *
     * @return void
	 */
	init: function() {
        var me = this;
        me.subApplication.treeStore =  me.subApplication.getStore('Tree').load();
        me.subApplication.listStore =  me.subApplication.getStore('List').load();

        me.subApplication.detailStore =  me.subApplication.getStore('Detail');
        me.subApplication.categoryPathStore =  me.subApplication.getStore('CategoryPath');
        me.subApplication.templateStore =  me.subApplication.getStore('Template');
        me.subApplication.commentStore =  me.subApplication.getStore('Comment');

        me.mainWindow = me.getView('main.Window').create({
            listStore: me.subApplication.listStore,
            treeStore: me.subApplication.treeStore,
            detailStore: me.subApplication.detailStore,
            categoryPathStore: me.subApplication.categoryPathStore,
            templateStore: me.subApplication.templateStore,
            commentStore: me.subApplication.commentStore

    });
        me.callParent(arguments);
    }
});
//
<?php }} ?>