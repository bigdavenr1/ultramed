<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:52
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/list.js" */ ?>
<?php /*%%SmartyHeaderCode:65407872555447c3c579ce8-53798273%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca2542753c183cbd98f81e22e0ce60005d1a6590' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/store/main/list.js',
      1 => 1430111761,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '65407872555447c3c579ce8-53798273',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3c587217_47287920',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3c587217_47287920')) {function content_55447c3c587217_47287920($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.store.main.List', {
	extend: 'Ext.data.Store',
	model: 'Shopware.apps.PaymentPaypal.model.main.List',
	proxy: {
        type: 'ajax',
        url : '<?php echo '/backend/PaymentPaypal/getList';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    },
    remoteSort: true,
    remoteFilter: true
});
<?php }} ?>