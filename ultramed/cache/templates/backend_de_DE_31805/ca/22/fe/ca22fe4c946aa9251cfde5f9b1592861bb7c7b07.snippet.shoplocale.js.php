<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:46:51
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/store/shoplocale.js" */ ?>
<?php /*%%SmartyHeaderCode:301680779554472dbc121d0-24565160%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca22fe4c946aa9251cfde5f9b1592861bb7c7b07' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/store/shoplocale.js',
      1 => 1430113177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '301680779554472dbc121d0-24565160',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554472dbc1a3a8_08481325',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554472dbc1a3a8_08481325')) {function content_554472dbc1a3a8_08481325($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Snippet
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Snippet.store.Shoplocale', {
    extend: 'Ext.data.Store',
    batch: true,
    autoLoad: false,
    model : 'Shopware.apps.Snippet.model.Shoplocale'
});
//
<?php }} ?>