<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:23:35
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/model/field.js" */ ?>
<?php /*%%SmartyHeaderCode:153844488455447b771faf47-42854269%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '50e5b4759ea10113b9327defc3bde598a5ea1c34' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/form/model/field.js',
      1 => 1430112891,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '153844488455447b771faf47-42854269',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447b77231b98_16845306',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447b77231b98_16845306')) {function content_55447b77231b98_16845306($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Form
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Form.model.Field', {

    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields : [
		//
        { name : 'id',       type : 'int' },
        { name : 'name',     type : 'string' },
        { name : 'label',    type : 'string' },
        { name : 'typ',      type : 'string' },
        { name : 'class',    type : 'string' },
        { name : 'value',    type : 'string' },
        { name : 'note',     type : 'string' },
        { name : 'errorMsg', type : 'string' },
        { name : 'required', type : 'bool' },
        { name : 'position', type : 'int' }
    ],

    validations: [
        { field: 'name',  type: 'presence'},
        { field: 'label', type: 'presence'},
        { field: 'name',  type: 'presence'}
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy: {
        type: 'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '<?php echo '/backend/form/getFields';?>',
            create: '<?php echo '/backend/form/createField';?>',
            update: '<?php echo '/backend/form/updateField';?>',
            destroy: '<?php echo '/backend/form/removeField';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>