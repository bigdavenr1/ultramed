<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/store/versions.js" */ ?>
<?php /*%%SmartyHeaderCode:40322999155447cb12e1be4-48763367%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '861fd5992a098505a8855e51afe6b9b264ea3df7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/store/versions.js',
      1 => 1430113180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '40322999155447cb12e1be4-48763367',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb12e91f1_30809293',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb12e91f1_30809293')) {function content_55447cb12e91f1_30809293($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Versionslist systeminfo module
 *
 * The versions store contains all plugin-versions
 */
//
Ext.define('Shopware.apps.Systeminfo.store.Versions', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',
    /**
    * Auto load the store after the component
    * is initialized
    * @boolean
    */
    autoLoad: true,

    /**
    * Define the used model for this store
    * @string
    */
    model : 'Shopware.apps.Systeminfo.model.Version'
});
//<?php }} ?>