<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:44:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/model/attribute.js" */ ?>
<?php /*%%SmartyHeaderCode:11640742785544725edcc5b5-06901407%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '860cab3cb6f4c410223a42b789a4e25b874c52ef' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/model/attribute.js',
      1 => 1430113147,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11640742785544725edcc5b5-06901407',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5544725ede58b3_62226033',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5544725ede58b3_62226033')) {function content_5544725ede58b3_62226033($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.MediaManager.model.Attribute', {
	extend: 'Ext.data.Model',
	fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'mediaId', type: 'int', useNull: true }
    ]
});
//

<?php }} ?>