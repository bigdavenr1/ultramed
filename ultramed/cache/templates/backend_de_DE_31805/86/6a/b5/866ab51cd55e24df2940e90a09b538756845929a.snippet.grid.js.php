<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/grid.js" */ ?>
<?php /*%%SmartyHeaderCode:148577138555434610c5a648-89960507%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '866ab51cd55e24df2940e90a09b538756845929a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/grid.js',
      1 => 1430112889,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148577138555434610c5a648-89960507',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610c760c8_73747772',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610c760c8_73747772')) {function content_55434610c760c8_73747772($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Emotion backend module.
 */
//
Ext.define('Shopware.apps.Emotion.model.Grid', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'cols', type: 'int', defaultValue: 4 },
        { name: 'rows', type: 'int', defaultValue: 20 },
        { name: 'cellHeight', type: 'int', defaultValue: 185 },
        { name: 'articleHeight', type: 'int', defaultValue: 2 },
        { name: 'gutter', type: 'int', defaultValue: 10 }
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            create: '<?php echo '/backend/Emotion/createGrid';?>',
            update: '<?php echo '/backend/Emotion/updateGrid';?>',
            destroy: '<?php echo '/backend/Emotion/deleteGrid';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader: {
            type:'json',
            root:'data'
        }
    }
});
//
<?php }} ?>