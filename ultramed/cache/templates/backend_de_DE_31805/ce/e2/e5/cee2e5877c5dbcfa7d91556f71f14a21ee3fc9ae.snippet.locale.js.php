<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 11:31:51
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/login/store/locale.js" */ ?>
<?php /*%%SmartyHeaderCode:19317575265541f687dcb010-07321852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cee2e5877c5dbcfa7d91556f71f14a21ee3fc9ae' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/login/store/locale.js',
      1 => 1430113144,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19317575265541f687dcb010-07321852',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541f687dcd242_77980544',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541f687dcd242_77980544')) {function content_5541f687dcd242_77980544($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Login
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Login.store.Locale', {
	extend: 'Ext.data.Store',
	autoLoad: true,
	model : 'Shopware.apps.Login.model.Locale'
});<?php }} ?>