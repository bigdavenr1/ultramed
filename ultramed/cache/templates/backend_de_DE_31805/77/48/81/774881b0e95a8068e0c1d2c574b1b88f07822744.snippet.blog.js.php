<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/blog.js" */ ?>
<?php /*%%SmartyHeaderCode:9305926885543461133cbb1-37153351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '774881b0e95a8068e0c1d2c574b1b88f07822744' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/view/components/blog.js',
      1 => 1430113366,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9305926885543461133cbb1-37153351',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434611356f67_83274471',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434611356f67_83274471')) {function content_55434611356f67_83274471($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
//
Ext.define('Shopware.apps.Emotion.view.components.Blog', {
    extend: 'Shopware.apps.Emotion.view.components.Base',
    alias: 'widget.emotion-components-blog',

    snippets: {
        entry_amount: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'entry_amount','default'=>'Number of entries','namespace'=>'backend/emotion/view/components/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'entry_amount','default'=>'Number of entries','namespace'=>'backend/emotion/view/components/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Anzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'entry_amount','default'=>'Number of entries','namespace'=>'backend/emotion/view/components/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        thumbnail_size: {
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'thumbnail_size','default'=>'Thumbnail size','namespace'=>'backend/emotion/view/components/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'thumbnail_size','default'=>'Thumbnail size','namespace'=>'backend/emotion/view/components/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Thumbnail Größe<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'thumbnail_size','default'=>'Thumbnail size','namespace'=>'backend/emotion/view/components/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            supportText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'thumbnail_size_support','default'=>'Thumbnail number that should be used. In the standard you have 0-3 available.','namespace'=>'backend/emotion/view/components/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'thumbnail_size_support','default'=>'Thumbnail number that should be used. In the standard you have 0-3 available.','namespace'=>'backend/emotion/view/components/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Thumbnail-Nummer, die verwendet werden soll. Im Standard stehen Ihnen 0 bis 3 zur Verfügung.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'thumbnail_size_support','default'=>'Thumbnail number that should be used. In the standard you have 0-3 available.','namespace'=>'backend/emotion/view/components/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * Initiliaze the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;
        me.callParent(arguments);

        me.numberField = me.down('numberfield');

        if(!me.numberField.getValue()) {
            me.numberField.setValue(1);
        }
    }
});
//<?php }} ?>