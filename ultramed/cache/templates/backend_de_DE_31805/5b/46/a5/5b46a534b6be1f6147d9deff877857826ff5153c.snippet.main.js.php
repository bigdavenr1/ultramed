<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:19:50
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:194503635955447a96aa4722-67282747%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b46a534b6be1f6147d9deff877857826ff5153c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/site/controller/main.js',
      1 => 1430113175,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '194503635955447a96aa4722-67282747',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447a96aef521_89894710',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447a96aef521_89894710')) {function content_55447a96aef521_89894710($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Site
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Site main Controller
 *
 * This file handles the main window.
 */

//

//
Ext.define('Shopware.apps.Site.controller.Main', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
    extend: 'Ext.app.Controller',

    /**
     * Define references for the different parts of the application. The
     * references are parsed by ExtJS and Getter methods are automatically created.
     *
     * Example: { ref : 'grid', selector : 'grid' } transforms to this.getGrid();
     *          { ref : 'addBtn', selector : 'button[action=add]' } transforms to this.getAddBtn()
     *
     * @object
     */
    refs:[
        { ref:'mainWindow', selector:'site-mainWindow' },
        { ref:'confirmationBox', selector:'site-confirmationBox' },
        { ref:'detailForm', selector:'site-form' },
        { ref:'parentIdField', selector:'site-form hidden[name=parentId]' },
        { ref:'navigationTree', selector:'site-tree' },
        { ref:'deleteSiteButton', selector:'site-mainWindow button[action=onDeleteSite]' },
        { ref:'saveSiteButton', selector:'site-form button[action=onSaveSite]' }
    ],

    /**
     * Creates the necessary event listeners for this
     * controller and the main window
     *
     * @return void
     */
    init: function() {
        var me = this;

        me.subApplication.nodeStore = me.subApplication.getStore('Nodes');

        me.groupStore = me.subApplication.getStore('Groups');

        me.selectedStore = me.subApplication.getStore('Selected');


        me.mainWindow = me.getView('main.Window').create({ nodeStore: me.subApplication.nodeStore, groupStore: me.groupStore, selectedStore: me.selectedStore  });

        me.control({
            //fires, when the user tries to create a new site
            'site-mainWindow button[action=onCreateSite]': {
                click: me.onCreateSite
            },
            //fires, when the user tries to delete a site
            'site-mainWindow button[action=onDeleteSite]': {
                click: me.onDeleteSite
            }
        });

        me.callParent(arguments);
    },

    /**
     * Event listener method which is called when the onCreateSite is fired.
     * It'll reset the whole form, enabling the user to create a new site from scratch.
     */
    onCreateSite: function() {
        var me = this,
            form = me.getDetailForm().getForm(),
			detailForm = me.getDetailForm(),
			ddselector = detailForm.down('ddselector'),
            tree = me.getNavigationTree(),
            saveSiteButton = me.getSaveSiteButton();

        var record = Ext.create('Shopware.apps.Site.model.Nodes');

        //if the current selection is not a root note (like gLeft)
        if (tree.getSelectionModel().hasSelection() && tree.getSelectionModel().getSelection()[0].data.parentId != 'root' ) {

            //get parentName and parentId
            var parentName = tree.getSelectionModel().getSelection()[0].data.description,
                parentId = tree.getSelectionModel().getSelection()[0].data.helperId;

            //ask if the user wants to create a subSite of the currently selected one
            Ext.Msg.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateNewSiteConfirmationBoxCaption','default'=>'Create subpage?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateNewSiteConfirmationBoxCaption','default'=>'Create subpage?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Unterseite erstellen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateNewSiteConfirmationBoxCaption','default'=>'Create subpage?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onCreateNewSiteConfirmationBoxText','default'=>'Are you sure you want to create a subpage of \\\'[0]\\\'?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateNewSiteConfirmationBoxText','default'=>'Are you sure you want to create a subpage of \\\'[0]\\\'?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wollen Sie eine Unterseite von \'[0]\' erstellen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onCreateNewSiteConfirmationBoxText','default'=>'Are you sure you want to create a subpage of \\\'[0]\\\'?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', parentName), function(btn){
                if (btn == 'yes'){
					ddselector.toStore.removeAll();
					ddselector.fromStore.load();
                    form.reset();
                    form.loadRecord(record);

                    //set the parentId so we know this will be a subSite
                    me.getParentIdField().setValue(parentId);
                } else {
					ddselector.toStore.removeAll();
					ddselector.fromStore.load();
                    form.reset();
                    form.loadRecord(record);
                }
                /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'createSite'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
                saveSiteButton.enable();
                /*<?php }else{ ?>*/
                saveSiteButton.disable();
                /*<?php }?>*/
            });
        } else {
			ddselector.toStore.removeAll();
			ddselector.fromStore.load();
            form.reset();
            form.loadRecord(record);
        }
    },

    /**
     * Event listener method which is called when the onDeleteSite is fired.
     * A confirmation dialog will open, which will in turn send an ajax request containing the sites id to the site php controller.
     */
    onDeleteSite: function() {
        var me = this,
            tree = me.getNavigationTree(),
            siteName = tree.getSelectionModel().getSelection()[0].data.description,
            siteId = tree.getSelectionModel().getSelection()[0].data.helperId;

        //open confirmation box, ask if the user really wants to delete the selected site
        //if yes, delete site with id and reload store
        Ext.Msg.confirm('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteSiteConfirmationBoxCaption','default'=>'Delete site?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteConfirmationBoxCaption','default'=>'Delete site?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Seite löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteConfirmationBoxCaption','default'=>'Delete site?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', Ext.String.format('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteSiteConfirmationBoxText','default'=>'Are you sure you want to delete \\\'[0]\\\'?','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteConfirmationBoxText','default'=>'Are you sure you want to delete \\\'[0]\\\'?','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Wollen Sie die Seite \'[0]\' wirklich löschen?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteConfirmationBoxText','default'=>'Are you sure you want to delete \\\'[0]\\\'?','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', siteName), function(btn){
            if (btn == 'yes'){
                Ext.Ajax.request({
                    url : '<?php echo '/backend/Site/deleteSite';?>',
                    scope: me,
                    params: {
                        siteId: siteId
                    },
                    success: function(response){
                        me.getStore('Nodes').load();
                        tree.getSelectionModel().deselectAll();
                        me.getDetailForm().getForm().reset();
                        Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteSiteSuccess','default'=>'The Site has been deleted successfully.','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteSuccess','default'=>'The Site has been deleted successfully.','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Seite wurde erfolgreich gelöscht.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteSuccess','default'=>'The Site has been deleted successfully.','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    },
                    failure: function(response) {
                        var errorMsg = response.proxy.reader.jsonData.message;
                        Shopware.Notification.createGrowlMessage('','<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'onDeleteSiteError','default'=>'An error has occurred while trying to delete the site: ','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteError','default'=>'An error has occurred while trying to delete the site: ','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim Löschen der Seite ist ein Fehler aufgetreten: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'onDeleteSiteError','default'=>'An error has occurred while trying to delete the site: ','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
' + errorMsg, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'mainWindowTitle','namespace'=>'backend/site/site')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopseiten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'mainWindowTitle','namespace'=>'backend/site/site'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    }
                });
            }
        });
    }
});
//<?php }} ?>