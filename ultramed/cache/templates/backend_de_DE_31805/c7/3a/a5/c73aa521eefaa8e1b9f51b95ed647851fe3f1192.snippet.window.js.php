<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:89703624655447cb1151eb4-34813457%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c73aa521eefaa8e1b9f51b95ed647851fe3f1192' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/view/main/window.js',
      1 => 1430113401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89703624655447cb1151eb4-34813457',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb11b77b6_43603554',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb11b77b6_43603554')) {function content_55447cb11b77b6_43603554($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - General window systeminfo
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Systeminfo.view.main.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','default'=>'System-Information','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'System-Information','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
System-Informationen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'System-Information','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    cls: Ext.baseCSSPrefix + 'systeminfo-window',
    alias: 'widget.systeminfo-main-window',
    autoShow: true,
    layout: 'fit',
    stateful:true,
    stateId:'shopware-systeminfo-window',
	height: '90%',
 	width: 925,
	overflow: 'hidden',

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;
        var tabPanel = me.createTabPanel();

        me.items = [tabPanel];
        me.callParent(arguments);
    },

    /**
     * Creates the tabPanel
     * @return [Ext.tab.Panel]
     */
    createTabPanel: function(){
        var me = this;
        var tabPanel = Ext.create('Ext.tab.Panel', {
            items: [
                {
                    xtype: 'container',
					overflowY: 'scroll',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tabpanel'/'config_tab'/'title','default'=>'Server-Configs','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'config_tab'/'title','default'=>'Server-Configs','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Server-Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'config_tab'/'title','default'=>'Server-Configs','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    items: [{
                        xtype: 'systeminfo-main-encoder',
                        encoderStore: me.encoderStore
                    },{
                        xtype: 'systeminfo-main-configlist'
                    }]
                },{
                    xtype: 'container',
					overflowY: 'scroll',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tabpanel'/'path_tab'/'title','default'=>'Shopware-Paths','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'path_tab'/'title','default'=>'Shopware-Paths','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopware-Verzeichnisse<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'path_tab'/'title','default'=>'Shopware-Paths','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    items:[{
                        xtype: 'systeminfo-main-pathlist'
                    }]
                },{
                    xtype: 'container',
                    overflowY: 'scroll',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tabpanel'/'file_tab'/'title','default'=>'Shopware-Files','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'file_tab'/'title','default'=>'Shopware-Files','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Shopware-Dateien<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'file_tab'/'title','default'=>'Shopware-Files','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    items:[{
                        xtype: 'systeminfo-main-filelist'
                    }]
                },{
                    xtype: 'container',
					overflowY: 'scroll',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tabpanel'/'version_tab'/'title','default'=>'Version-info','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'version_tab'/'title','default'=>'Version-info','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Versions-Info<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'version_tab'/'title','default'=>'Version-info','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    items:[{
                        xtype: 'systeminfo-main-versionlist'
                    }]
                },{
                    xtype: 'container',
					layout: 'fit',
					overflowY: 'hidden',
                    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'tabpanel'/'info_tab'/'title','default'=>'PHP-Info','namespace'=>'backend/systeminfo/view')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'info_tab'/'title','default'=>'PHP-Info','namespace'=>'backend/systeminfo/view'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PHP-Info<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'tabpanel'/'info_tab'/'title','default'=>'PHP-Info','namespace'=>'backend/systeminfo/view'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    items:[{
                        xtype: 'systeminfo-main-phpinfo'
                    }]
                }
            ]
        });

        return tabPanel;
    }
});
//
<?php }} ?>