<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:44:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/model/media.js" */ ?>
<?php /*%%SmartyHeaderCode:13404123515544725eda0fc2-72832154%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c7d8e0dc9b16028e5dc7056da5f782ac346aa3b4' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/media_manager/model/media.js',
      1 => 1430113148,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13404123515544725eda0fc2-72832154',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5544725edca039_42328039',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5544725edca039_42328039')) {function content_5544725edca039_42328039($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.MediaManager.model.Media', {
	extend: 'Ext.data.Model',
	fields: [
		//
		'created', 'description', 'extension', 'id', { name: 'name', sortType: 'asUCText' }, 'type', 'path', 'userId', 'thumbnail', 'width', 'height', 'albumId', 'newAlbumID' ],
	proxy: {
		type: 'ajax',
        api: {
            read: '<?php echo '/backend/MediaManager/getAlbumMedia';?>',
            create: '<?php echo '/backend/MediaManager/saveMedia';?>',
            update: '<?php echo '/backend/MediaManager/saveMedia/targetField/media';?>',
            destroy: '<?php echo '/backend/MediaManager/removeMedia';?>'
        },
		reader: {
			type: 'json',
			root: 'data',
            totalProperty: 'total'
		}
	},
    associations: [
        { type: 'hasMany', model: 'Shopware.apps.MediaManager.model.Attribute', name: 'getAttributes', associationKey: 'attribute'}
    ]
});
//

<?php }} ?>