<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:20:55
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/store/form/search_field.js" */ ?>
<?php /*%%SmartyHeaderCode:110868855755447ad76b3464-72924097%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c7180b55964414eb66fef7a10ab850ea6c388d23' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/store/form/search_field.js',
      1 => 1430113352,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '110868855755447ad76b3464-72924097',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ad76c93c1_33381865',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ad76c93c1_33381865')) {function content_55447ad76c93c1_33381865($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.SearchField', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.Config.model.form.SearchField',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/Config/getTableList/name/searchField';?>',
        api: {
            create: '<?php echo '/backend/Config/saveTableValues/name/searchField';?>',
            update: '<?php echo '/backend/Config/saveTableValues/name/searchField';?>',
            destroy: '<?php echo '/backend/Config/deleteTableValues/name/searchField';?>'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//<?php }} ?>