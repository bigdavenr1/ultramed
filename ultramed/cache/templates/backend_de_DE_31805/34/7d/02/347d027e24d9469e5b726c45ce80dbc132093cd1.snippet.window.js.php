<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/window.js" */ ?>
<?php /*%%SmartyHeaderCode:1163803724554476fc52fd73-16649508%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '347d027e24d9469e5b726c45ce80dbc132093cd1' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/window.js',
      1 => 1430113345,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1163803724554476fc52fd73-16649508',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc566393_86572413',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc566393_86572413')) {function content_554476fc566393_86572413($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Blog detail main window.
 *
 * Displays all Detail Blog Information
 */
//
Ext.define('Shopware.apps.Blog.view.blog.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'detail_title','default'=>'Blog article configuration','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'detail_title','default'=>'Blog article configuration','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blogartikel Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'detail_title','default'=>'Blog article configuration','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.blog-blog-window',
    border: false,
    autoShow: true,
    /**
     * Define window height
     * @integer
     */
    height:'90%',
    width: 1024,
    modal:false,
    layout: {
        type: 'fit'
    },

    /**
     * Display no footer button for the detail window
     * @boolean
     */
    footerButton:false,

    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        // Create our form panel and assign it to the namespace for later usage
        me.formPanel = me.createFormPanel();

        me.items = [
            {
                xtype:'tabpanel',
                region:'center',
                items:me.getTabs()
            }
        ];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            cls: 'shopware-toolbar',
            ui: 'shopware-ui',
            items: me.createFormButtons()
        }];
        me.callParent(arguments);

        //have to load it here because in some cases the view is not totally created before the record starts loading
        if(me.record) {
            me.formPanel.loadRecord(me.record);
        }
    },

    /**
     * Creates the tabs for the tab panel of the window.
     * Contains the detail form which is used to display the blog data for an existing blog entry
     * or to create a new blog.
     *
     * The second tab contains a list of all blog comments
     */
    getTabs:function () {
        var me = this;
        return [
            me.formPanel,
            {
                xtype: 'blog-blog-detail-comments',
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'comment'/'title','default'=>'Blog article comments','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'comment'/'title','default'=>'Blog article comments','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Blogartikel Kommentare<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'comment'/'title','default'=>'Blog article comments','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                commentStore: me.commentStore
            }

        ];
    },


    /**
     * creates the form panel
     */
    createFormPanel: function() {
        var me = this;
        me.mainView = Ext.create('Shopware.apps.Blog.view.blog.detail.Main',{
            flex: 4,
            record: me.record
        });

        return Ext.create('Ext.form.Panel', {
            layout: {
                align: 'stretch',
                type: 'hbox'
            },
            border: 0,
            bodyPadding: 10,
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'title','default'=>'General Settings','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'title','default'=>'General Settings','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einstellungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'title','default'=>'General Settings','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [
                me.mainView,
                {
                    xtype: 'panel',
                    layout: {
                        type: 'accordion',
                        animate: Ext.isChrome
                    },
                    flex: 2,
                    margin: '0 0 0 10',
                    items: [
                        {
                            xtype: 'blog-blog-detail-sidebar-options',
                            detailRecord: me.record,
                            categoryPathStore: me.categoryPathStore,
                            templateStore: me.templateStore
                        },
                        {
                            xtype: 'blog-blog-detail-sidebar-assigned_articles',
                            gridStore: me.record.getAssignedArticles()
                        },
                        {
                            xtype: 'blog-blog-detail-sidebar-seo',
                            detailRecord: me.record,
                            mainTitleField: me.mainView.mainTitle
                        },
                        {
                            xtype: 'blog-blog-detail-sidebar-attributes',
                            detailRecord: me.record
                        }
                    ]
                }
            ]
        });
    },
    /**
     * creates the form buttons cancel and save
     */
    createFormButtons: function(){
        var me = this;
        return ['->',
            {
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'cancel','default'=>'Cancel','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                cls: 'secondary',
                scope:me,
                handler:function () {
                    this.destroy();
                }
            }
		/* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?> */
			,{
                text:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Speichern<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'button'/'save','default'=>'Save','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                action:'save',
                cls:'primary'
            }
		/* <?php }?> */
        ];
    }
});
//
<?php }} ?>