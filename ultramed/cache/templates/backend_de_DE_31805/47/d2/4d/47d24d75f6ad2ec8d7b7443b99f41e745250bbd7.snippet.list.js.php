<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:52
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/list.js" */ ?>
<?php /*%%SmartyHeaderCode:76479978055447c3c4925e8-06951953%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47d24d75f6ad2ec8d7b7443b99f41e745250bbd7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/list.js',
      1 => 1430111760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76479978055447c3c4925e8-06951953',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3c4a80b8_87975536',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3c4a80b8_87975536')) {function content_55447c3c4a80b8_87975536($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.model.main.List', {
	extend: 'Ext.data.Model',
	fields: [
		{ name: 'id', type: 'int' },
		{ name: 'userId',  type: 'string' },
		{ name: 'transactionId', type: 'string' },

		{ name: 'clearedId', type: 'int' },
		{ name: 'statusId', type: 'int' },
		{ name: 'clearedDescription', type: 'string' },
		{ name: 'statusDescription', type: 'string' },

		{ name: 'currency', type: 'string' },
		{ name: 'amount', type: 'float' },
		{ name: 'amountFormat', type: 'string' },
		{ name: 'customer', type: 'string' },
        { name: 'customerId', type: 'string' },
		{ name: 'orderDate', type: 'date' },
		{ name: 'clearedDate', type: 'date' },
		{ name: 'orderNumber', type: 'string' },
		{ name: 'shopId', type: 'int' },
		{ name: 'shopName', type: 'string' },
		{ name: 'paymentDescription', type: 'string' },
		{ name: 'paymentKey', type: 'string' },
		{ name: 'comment', type: 'string' },

		{ name: 'invoiceId', type: 'string' },
		{ name: 'invoiceHash', type: 'string' },
		{ name: 'trackingId', type: 'string' },
		{ name: 'dispatchId', type: 'int' },
		{ name: 'dispatchDescription', type: 'string' },
        { name: 'express', type: 'int'}
	]
});
<?php }} ?>