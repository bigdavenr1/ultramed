<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:53
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:4162597555447c3d506d38-41233272%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '316fd9bb21aaaaa6d2269595bfd67275d016d085' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/model/main/detail.js',
      1 => 1430111760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4162597555447c3d506d38-41233272',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3d5181d8_38739731',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3d5181d8_38739731')) {function content_55447c3d5181d8_38739731($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

Ext.define('Shopware.apps.PaymentPaypal.model.main.Detail', {
    extend: 'Ext.data.Model',
	fields: [
        { name: 'transactionId', type: 'string' },
        //{ name: 'orderNumber', type: 'string' },

		{ name: 'addressStatus',  type: 'string' },
        { name: 'addressName',  type: 'string' },
        { name: 'addressStreet',  type: 'string' },
        { name: 'addressCity',  type: 'string' },
        { name: 'addressCountry',  type: 'string' },
        { name: 'addressPhone',  type: 'string' },

        { name: 'accountEmail',  type: 'string' },
        { name: 'accountName',  type: 'string' },
        { name: 'accountStatus',  type: 'string' },

        { name: 'protectionStatus',  type: 'string' },
        { name: 'paymentStatus',  type: 'string' },
        { name: 'pendingReason',  type: 'string' },

        { name: 'paymentDate',  type: 'date' },
        { name: 'paymentType',  type: 'string' },
        { name: 'paymentAmount',  type: 'string' },
        { name: 'paymentCurrency',  type: 'string' },
        { name: 'paymentAmountFormat', type: 'string' }
	],

    associations: [{
        type: 'hasMany', model: 'Shopware.apps.PaymentPaypal.model.main.Transaction',
        name: 'getTransactions', associationKey: 'transactions'
    }]
});
<?php }} ?>