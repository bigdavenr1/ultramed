<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:25:18
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/index/model/customers.js" */ ?>
<?php /*%%SmartyHeaderCode:18339253085541e6eecdb179-94204844%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '318fe848be144ea54fd302d34a8e0932b4f8c2eb' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/index/model/customers.js',
      1 => 1430113141,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18339253085541e6eecdb179-94204844',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e6eece4771_12249801',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e6eece4771_12249801')) {function content_5541e6eece4771_12249801($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Index.model.Customers', {
	extend: 'Ext.data.Model',
    fields: [
		//
		'customer', 'amount']
});
//
<?php }} ?>