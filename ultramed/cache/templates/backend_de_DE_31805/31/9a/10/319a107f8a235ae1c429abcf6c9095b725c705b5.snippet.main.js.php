<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:52
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:194923424855447c3c59cfe9-33767595%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '319a107f8a235ae1c429abcf6c9095b725c705b5' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/controller/main.js',
      1 => 1430111747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '194923424855447c3c59cfe9-33767595',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3c5f8725_33640065',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3c5f8725_33640065')) {function content_55447c3c5f8725_33640065($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//
//
Ext.define('Shopware.apps.PaymentPaypal.controller.Main', {
    extend: 'Enlight.app.Controller',

    refs: [
        { ref: 'window', selector: 'paypal-main-window' },
        { ref: 'detail', selector: 'paypal-main-detail' },
        { ref: 'list', selector: 'paypal-main-list' },
        { ref: 'shopCombo', selector: 'paypal-main-list [name=shopId]' },
        { ref: 'balance', selector: 'paypal-main-list field[name=balance]' },
        { ref: 'transactions', selector: 'paypal-main-detail grid' }
    ],

    stores: [
        'main.List', 'main.Balance', 'main.Detail'
    ],
    models: [
        'main.List', 'main.Balance', 'main.Detail', 'main.Transaction'
    ],
    views: [
        'main.Window', 'main.List', 'main.Detail', 'main.Action'
    ],

    snippets:  {
        error: {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"balanceErrorTitle",'default'=>'Error','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"balanceErrorTitle",'default'=>'Error','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Fehler<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"balanceErrorTitle",'default'=>'Error','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            general: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"errorMessageGeneral",'default'=>'<b>Possible cause:</b><br>[0]<br><br><b>Actual error message:</b><br>[1]','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"errorMessageGeneral",'default'=>'<b>Possible cause:</b><br>[0]<br><br><b>Actual error message:</b><br>[1]','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<b>Möglicher Grund:</b><br>[0]<br><br><b>Fehlermeldung:</b><br>[1]<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"errorMessageGeneral",'default'=>'<b>Possible cause:</b><br>[0]<br><br><b>Actual error message:</b><br>[1]','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            10011: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"invalidTransaction",'default'=>'The transactionId you passed is not valid or not known to PayPal','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"invalidTransaction",'default'=>'The transactionId you passed is not valid or not known to PayPal','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die Transaktions-ID ist nicht gültig oder PayPal nicht bekannt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"invalidTransaction",'default'=>'The transactionId you passed is not valid or not known to PayPal','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            10007: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"permissionDenied",'default'=>'The PayPal credentials you configured are not valid for this Transaction','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"permissionDenied",'default'=>'The PayPal credentials you configured are not valid for this Transaction','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ihre PayPal-Zugangsdaten sind für diese Transaktions-ID nicht gültig.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"permissionDenied",'default'=>'The PayPal credentials you configured are not valid for this Transaction','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            10002: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"securityError",'default'=>'Your PayPal credentials are not valid.','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"securityError",'default'=>'Your PayPal credentials are not valid.','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Ihre PayPal-Zugangsdaten sind nicht gültig.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"securityError",'default'=>'Your PayPal credentials are not valid.','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }
    },

    /**
     * The main window instance
     * @object
     */
    mainWindow: null,

    init: function () {
        var me = this;

        // Init main window
        me.mainWindow = me.getView('main.Window').create({
            autoShow: true,
            scope: me
        });

        me.detailStore = me.getStore('main.Detail');

        me.getStore('main.Balance').load({
            callback: me.onLoadBalance,
            scope: this
        });

        // Register events
        me.control({
            'paypal-main-list': {
                selectionchange: me.onSelectionChange,
                shopSelectionChanged: me.onShopSelectionChanged
            },
            'paypal-main-detail button': {
                click: me.onClickDetailButton
            },
            'paypal-main-detail': {
                refund: me.onClickDetailButton
            },
            'paypal-main-list [name=searchfield]': {
                change: me.onSearchForm
            }
        });
    },

    /**
     * Returns the currently selected shop.
     *
     * If there is no vaild shop selected, the first shop is returned, if that fails, 0 is returned.
     * In the later case, the controller should select the shop via getActiveDefault()
     *
     * @returns int
     */
    getSelectedShop: function() {
        var me = this,
            shopCombo = me.getShopCombo(),
            shopId = shopCombo.getValue(),
            first = shopCombo.store.first();

        if (typeof(shopId) != "number") {
            if (first && first.get('id')) {
                return first.get('id');
            }
            return 0;
        }

        return shopId;
    },

    /**
     * Callback function called when the user changed the shop selection combo
     *
     * @param shopId
     */
    onShopSelectionChanged: function(shopId) {
        var me = this,
            grid = me.getList(),
            store = grid.store;

        if (typeof(shopId) != "number" && shopId != '' && shopId != null) {
            return;
        }
        store.clearFilter(true);
        store.filter('shopId', shopId);

        me.getStore('main.Balance').getProxy().extraParams['shopId'] = shopId;
        me.getStore('main.Balance').load({
            callback: me.onLoadBalance,
            scope: this
        });
    },

    /**
     * Callback function triggered when the user enters something into the search field
     *
     * @param field
     * @param value
     */
    onSearchForm: function(field, value) {
        var me = this;
        var store = me.getStore('main.List');
        if (value.length === 0 ) {
            store.load();
        } else {
            store.load({
                filters : [{
                    property: 'search',
                    value: '%' + value + '%'
                }]
            });
        }
    },

    /**
     * Callback function triggered when the user clicks one of the action buttons in the detail form
     *
     * @param button
     */
    onClickDetailButton: function(button) {
        var me = this,
            detail = me.getDetail(),
            detailData = detail.getForm().getFieldValues(),
            action;
        detailData.transactionId = button.transactionId || detailData.transactionId;
        detailData.paymentAmount = button.paymentAmount || detailData.paymentAmount;
        action = me.getView('main.Action').create({
            paymentAction: button.action,
            paymentActionName: button.text,
            detailData: detailData
        });

        action.on('destroy', function() {
            me.getList().getStore().load();
        })
    },

    /**
     * Callback function triggered when the user clicks on an entry in the list
     *
     * @param table
     * @param records
     */
    onSelectionChange: function(table, records) {
        var me = this,
            formPanel = me.getDetail(),
            record = records.length ? records[0] : null;

        var shopId = me.getSelectedShop();


        if(record) {
            formPanel.setLoading(true);
            formPanel.loadRecord(record);
            me.detailStore.load({
                extraParams: {
                    'shopId': shopId
                },
                filters : [{
                    property: 'transactionId',
                    value: record.get('transactionId')
                }],
                callback: me.onLoadDetail,
                scope: me
            });
            formPanel.enable();
        } else {
            formPanel.disable();
        }
    },

    /**
     * Displays a sticky notification if available. Else the default growlmessage is shown
     *
     * @param title
     * @param message
     */
    showGrowlMessage: function(title, message) {
        if (typeof Shopware.Notification.createStickyGrowlMessage == 'function') {
            Shopware.Notification.createStickyGrowlMessage({
                title: title,
                text:  message
            });
        } else {
            Shopware.Notification.createGrowlMessage(title, message);
        }
    },

    /**
     * Convenience function which will look up any given error code in order to give the user some more
     * info about what happened and what he can do about it.
     *
     * @param title
     * @param error
     * @param code
     */
    showPayPalErrorMessage: function(title, error, code) {
        var me = this,
            message;

        if (!code || !me.snippets.error[code]) {
            message = error;
        } else {
            message = Ext.String.format(me.snippets.error.general, me.snippets.error[code], error);
        }

        me.showGrowlMessage(title, message);
    },

    /**
     * Callback function for the "load paypal balance" ajax request
     *
     * @param records
     * @param operation
     * @param success
     */
    onLoadBalance: function(records, operation, success) {
        var me = this,
            error, errorCode;

        if (!success) {
            error = operation.request.proxy.reader.rawData.error;
            errorCode = operation.request.proxy.reader.rawData.errorCode;

            me.showPayPalErrorMessage(me.snippets.error.title, error, errorCode);
        }else if(records.length) {
            var record = records[0];
            me.getBalance().setValue(record.get('balanceFormat'));
        }
    },

    /**
     * Callback function for the "load details" ajax request
     *
     * @param records
     * @param operation
     * @param success
     */
    onLoadDetail: function(records, operation, success) {
        var me = this,
            formPanel = me.getDetail(),
            detail = (records && records.length) ? records[0] : null,
            status, pending, fields,
            error, errorCode;


        if(!detail) {
            formPanel.disable();
            formPanel.setLoading(false);
            if (!success) {
                error = operation.request.proxy.reader.rawData.message;
                errorCode = operation.request.proxy.reader.rawData.errorCode;
                me.showPayPalErrorMessage(me.snippets.error.title, error, errorCode);
            }
            return;
        }
        formPanel.loadRecord(detail);
        me.getTransactions().reconfigure(
            detail.getTransactions()
        );
        status = detail.get('paymentStatus');
        pending = detail.get('pendingReason');
        fields = formPanel.query('button');
        Ext.each(fields, function(field) {
            field.hide();
        });
        switch(status) {
            case 'Expired':
                formPanel.down('[action=auth]').show();
                break;
            case 'Completed':
            case 'PartiallyRefunded':
            case 'Canceled-Reversal ':
                formPanel.down('[action=refund]').show();
                break;
            case 'Pending':
            case 'In-Progress':
                if(pending == 'order') {
                    formPanel.down('[action=book]').show();
                } else {
                    formPanel.down('[action=capture]').show();
                }
                formPanel.down('[action=void]').show();
                break;
        }
        formPanel.setLoading(false);
    }
});
//
<?php }} ?>