<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:28:49
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/controller/main.js" */ ?>
<?php /*%%SmartyHeaderCode:189915731255447cb1305ab2-52041237%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '312550f5d226e6099f21021edd4168b6fd29c260' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/systeminfo/controller/main.js',
      1 => 1430113179,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189915731255447cb1305ab2-52041237',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cb1315bf4_48469919',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cb1315bf4_48469919')) {function content_55447cb1315bf4_48469919($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Systeminfo
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Controller - Systeminfo backend module
 *
 * Main controller of the systeminfo module.
 * It only creates the main-window.
 * shopware AG (c) 2012. All rights reserved.
 */
// 
Ext.define('Shopware.apps.Systeminfo.controller.Main', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.app.Controller',

    /**
    * Required views for controller
    * @array
    */
    views: [ 'main.Window' ],

    requires: [ 'Shopware.apps.Systeminfo.controller.Systeminfo' ],

    init: function() {
        var me = this,
            //Contains the necessary configs
            configStore = me.subApplication.getStore('Configs'),
            //Contains the necessary paths
            pathStore = me.subApplication.getStore('Paths'),
            //Contains the necessary files
            fileStore = me.subApplication.getStore('Files'),
            //Contains the necessary versions
            versionStore = me.subApplication.getStore('Versions');
        me.mainWindow = me.getView('main.Window').create({
            configStore: configStore,
            pathsStore: pathStore,
            fileStore: fileStore,
            versionStore: versionStore
        });
        this.callParent(arguments);
    }
});
//
<?php }} ?>