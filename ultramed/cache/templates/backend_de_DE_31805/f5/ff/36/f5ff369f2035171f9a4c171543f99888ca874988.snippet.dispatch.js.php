<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/dispatch.js" */ ?>
<?php /*%%SmartyHeaderCode:83182490855434785984cd8-44651239%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5ff369f2035171f9a4c171543f99888ca874988' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/dispatch.js',
      1 => 1430113329,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '83182490855434785984cd8-44651239',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347859b5cf6_52844996',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347859b5cf6_52844996')) {function content_554347859b5cf6_52844996($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Payment Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Dispatch', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-dispatch',
    animate: true,
    shadows: true,

    legend: {
        position: 'right'
    },

    initComponent: function () {
        var me = this;

        me.series = [];

        me.axes = [
            {
                type: 'Numeric',
                position: 'bottom',
                fields: me.getAxesFields('turnover'),
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                grid: true,
                minimum: 0
            },
            {
                type: 'Category',
                position: 'left',
                fields: ['name'],
                title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'chart'/'dispatch'/'title','default'=>'Shipping','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'dispatch'/'title','default'=>'Shipping','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Versandart<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'chart'/'dispatch'/'title','default'=>'Shipping','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }
        ];

        this.series = [
            {
                type: 'bar',
                axis: 'bottom',
                gutter: 80,
                xField: 'name',
                yField: me.getAxesFields('turnover'),
                title: me.getAxesTitles('<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'),
                stacked: true,
                label: {
                    display: 'insideEnd',
                    field: 'turnover',
                    renderer: Ext.util.Format.numberRenderer('0.00'),
                    orientation: 'horizontal',
                    'text-anchor': 'middle'
                },
                tips: {
                    trackMouse: true,
                    width: 300,
                    height: 60,
                    renderer: function (storeItem, barItem) {
                        var name = storeItem.get('name'),
                            field = barItem.yField,
                            shopId = field.replace('turnover', ''),
                            shop;

                        if (shopId) {
                            shop = me.shopStore.getById(shopId);
                            name = shop.get('name') + '<br><br>&nbsp;' + name;
                        }

                        var turnover = Ext.util.Format.currency(
                            storeItem.get(field),
                            me.subApp.currencySign,
                            2,
                            (me.subApp.currencyAtEnd == 1)
                        );
                        this.setTitle(name + ' : ' + turnover);
                    }
                }
            }
        ];

        me.callParent(arguments);
    }

});
//
<?php }} ?>