<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:26
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/overview/model/overview.js" */ ?>
<?php /*%%SmartyHeaderCode:2118756262554347761c3720-44595968%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '36725e162253868e916dbd20340611af3d431fe2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/overview/model/overview.js',
      1 => 1430113158,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2118756262554347761c3720-44595968',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347761f37e8_74997584',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347761f37e8_74997584')) {function content_554347761f37e8_74997584($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Overview
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Overview.model.Overview', {

    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     *
     * @array
     */
    fields : [
		//
        { name: 'visits',           type: 'int' },
        { name: 'averageUsers',     type: 'float' },
        { name: 'hits',             type: 'int' },
        { name: 'countOrders',      type: 'int' },
        { name: 'countUsers',       type: 'int' },
        { name: 'countCustomers',   type: 'int' },
        { name: 'amount',           type: 'float' },
        { name: 'date',             type: 'date' },
        { name: 'averageOrders',    type: 'float' }
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy: {
        type: 'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '<?php echo '/backend/overview/getOrderSummary';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>