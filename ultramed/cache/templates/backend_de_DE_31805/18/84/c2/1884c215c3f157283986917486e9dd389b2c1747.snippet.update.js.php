<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:40
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/update.js" */ ?>
<?php /*%%SmartyHeaderCode:127691987355447ce4120634-85086695%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1884c215c3f157283986917486e9dd389b2c1747' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/controller/update.js',
      1 => 1430111755,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '127691987355447ce4120634-85086695',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'storeApiAvailable' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce417a921_45463842',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce417a921_45463842')) {function content_55447ce417a921_45463842($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Controller
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.controller.Update', {

    /**
     * The parent class that this class extends.
     * @string
     */
    extend:'Ext.app.Controller',

    /**
     * References for the controller for easier accessing.
     * @array
     */
    refs: [
        { ref: 'mainWindow', selector: 'plugin-manager-main-window' },
        { ref: 'managerNavigation', selector: 'plugin-manager-manager-navigation' },
        { ref: 'storeNavigation', selector: 'plugin-manager-store-navigation' },
        { ref: 'pluginGrid', selector: 'plugin-manager-manager-grid' },
    ],

    snippets: {
   		update:{
   			title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin manager<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'title','default'=>'Plugin manager','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
   			downloadsuccessful: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin wurde erfolgreich heruntergeladen.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadsuccessful','default'=>'Plugin was downloaded successfully','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
   			downloadfailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim download des Plugins ist ein Fehler aufgetreten[0]Bitte prüfen Sie zuerst Ihre Dateirechte auf den Ordnern /files/downloads und engine/Shopware/Plugins/Community<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailed','default'=>'An error occurred while downloading the plugin. Please check the file permissions of the directories /files/downloads and engine/Shopware/Plugins/Community','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
   			downloadfailedlicense: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin konnte nicht heruntergeladen werden, bitte überprüfen Sie ihre Dateirechte und ob Sie das gewünschte Plugin lizensiert haben.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'downloadfailedlicense','default'=>'An error occurred while downloading the plugin. Please check your directory-rights and license for this plugin.','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
   			updatesuccessful: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin [0] wurde erfolgreich aktualisiert<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatesuccessful','default'=>'Plugin [0] have been updated successfully','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
   			updatefailed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Beim update des Plugins ist ein Fehler aufgetreten. <br>Soll das Backup wiederhergestellt werden?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'account'/'updatefailed','default'=>'An error occurred while updating the plugin. Do you want to load the backup?','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            wantToStartUpdate: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'update'/'wantToStartUpdate','default'=>'You are about to update the plugin [0]. Do you want to proceed?','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update'/'wantToStartUpdate','default'=>'You are about to update the plugin [0]. Do you want to proceed?','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sie werden das Plugin [0] aktualisieren. Möchten Sie fortfahren?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'update'/'wantToStartUpdate','default'=>'You are about to update the plugin [0]. Do you want to proceed?','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
   		}
   	},

    /**
     * A template method that is called when your application boots.
     * It is called before the Application's launch function is executed
     * so gives a hook point to run any code before your Viewport is created.
     *
     * @return void
     */
    init: function () {
        var me = this,
            updatePlugin;

        me.callParent(arguments);

        /**
         * Check if the plugin manager was invoked in order to update a given plugin
         *
         * <?php if ($_smarty_tpl->tpl_vars['storeApiAvailable']->value){?>
         */
        if (me.subApplication.params && me.subApplication.params.updatePlugin) {
            updatePlugin = me.subApplication.params.updatePlugin;

            Ext.MessageBox.confirm(me.snippets.update.title, Ext.String.format(me.snippets.update.wantToStartUpdate, updatePlugin), function(btn) {
                if(btn == 'yes') {
                    me.startPluginUpdate(updatePlugin);
                } else {
                    return false;
                }
            });
        }
        /** <?php }?> */
    },

    /**
     * Inits an update for a given plugin.
     *
     * @param updatePlugin
     */
    startPluginUpdate: function(updatePlugin) {
        var me = this,
            accountController = me.subApplication.getController('Account');

        // Check if user is logged in into the store and then triggers the first update step
        if(!accountController.checkLogin()) {
            accountController.onOpenLogin({
               controller: 'Update',
               action: 'doAutoUpdateFirstStep',
               params: updatePlugin
           });
        } else {
            me.doAutoUpdateFirstStep(updatePlugin);
        }
    },

    /**
     * Performs the actual plugin download
     *
     * In order to update a plugin, two steps are required
     *
     * 1. The new version of the plugin needs to be downloaded
     * 2. The new version of the plugin needs to be installed
     *
     * Currently this cannot be done in one request, as we need to create a new instance of the new plugin
     *
     * @param updatePlugin
     */
    doAutoUpdateFirstStep: function(updatePlugin) {
        var me = this;

        var window = me.getMainWindow();

        if (window) {
            window.setLoading(true);
        }

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/downloadPluginByName';?>',
            method: 'POST',
            params: {
                name: updatePlugin
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);

                if (window) {
                    window.setLoading(false);
                }

                if (response.success === true) {
                    Shopware.Notification.createGrowlMessage(
                       me.snippets.update.title,
                       me.snippets.update.downloadsuccessful
                    );

                    me.onAutoUpdateSecondStep(updatePlugin, response.articleId, response.activated, response.installed, response.availableVersion);

                } else {
                    var message = response.message + '';
                    if (message.length === 0) {
                        message = me.snippets.update.downloadfailedlicense
                    }
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.update.title,
                       text: message,
                       log: true
                    });
                }
            }
        });
    },

    /**
     * Performs the update of the new plugin
     *
     * In order to update a plugin, two steps are required
     *
     * 1. The new version of the plugin needs to be downloaded
     * 2. The new version of the plugin needs to be installed
     *
     * Currently this cannot be done in one request, as we need to create a new instance of the new plugin
     *
     * @param updatePlugin      The name of the plugin to update
     * @param articleId         The store's article id of the plugin
     * @param activated         Was the old plugin version activated?
     * @param installed         Was the olf plugin version installed?
     * @param availableVersion  Version to update to
     */
    onAutoUpdateSecondStep: function(updatePlugin, articleId, activated, installed, availableVersion) {
        var me = this;

        var window = me.getMainWindow();

        if (window) {
            window.setLoading(true);
        }

        Ext.Ajax.request({
            url:'<?php echo '/backend/PluginManager/updatePlugin';?>',
            method: 'POST',
            params: {
                name: updatePlugin,
                articleId: articleId,
                activated: activated,
                installed: installed,
                availableVersion: availableVersion
            },
            callback: function(request, opts, operation) {
                var response = Ext.decode(operation.responseText);

                if (window) {
                    window.setLoading(false);
                }

                if (response.success === true) {
                    Shopware.Notification.createGrowlMessage(
                       me.snippets.update.title,
                       Ext.String.format(me.snippets.update.updatesuccessful, updatePlugin)
                    );
                    me.filterForPlugin(updatePlugin);
                } else {
                    var message = response.message + '';
                    if (message.length === 0) {
                        message = me.snippets.update.downloadfailedlicense
                    }
                    Shopware.Notification.createStickyGrowlMessage({
                       title: me.snippets.update.title,
                       text: message,
                       log: true
                    });
                }
            }
        });
    },


    /**
     * Activates the "installed plugin" tab and searches for the updated plugin
     *
     * @param plugin
     */
    filterForPlugin: function(plugin) {
        var me = this, category = null, pluginStore = me.subApplication.pluginStore,
            mainWindow = me.getMainWindow(),
            navigation = me.getManagerNavigation(),
            grid       = me.getPluginGrid(),
            store      = navigation.extensionCategoryStore;

        // Set record active
        store.each(function(item) {
            if (item.raw.requestParam == null) {
                item.set('selected', true);
            } else {
                item.set('selected', false);
            }
        });

        if (navigation && navigation.accountCategoryStore instanceof Ext.data.Store) {
            var accountCategoryStore = navigation.accountCategoryStore;
            accountCategoryStore.each(function(item) {
               item.set('selected', false);
            });
        }

        var items = mainWindow.managerContainer.items,
            length = items.length;

        mainWindow.managerContainer.getLayout().setActiveItem(0);
        if(length > 1) {
            items.getAt(length-1).destroy();
        }


        if (grid && grid.searchField) {
            grid.searchField.setValue(plugin);
        }
    }

});
//
<?php }} ?>