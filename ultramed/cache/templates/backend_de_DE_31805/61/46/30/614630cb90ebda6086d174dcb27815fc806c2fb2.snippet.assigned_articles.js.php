<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/assigned_articles.js" */ ?>
<?php /*%%SmartyHeaderCode:302163327554476fc41e5b4-64961845%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '614630cb90ebda6086d174dcb27815fc806c2fb2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/model/assigned_articles.js',
      1 => 1430112875,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '302163327554476fc41e5b4-64961845',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc4318f9_61532879',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc4318f9_61532879')) {function content_554476fc4318f9_61532879($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model -  blog backend module.
 *
 * The blog assigned_articles model of the blog module holds the blog assigned articles data for the detail page
 */
//
Ext.define('Shopware.apps.Blog.model.AssignedArticles', {

    /**
    * Extends the standard Ext Model
    * @string
    */
    extend: 'Ext.data.Model',

    /**
     * Fields array which contains the model fields
     * @array
     */
    fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'number', type: 'string', mapping: 'mainDetail.number' }
    ]

});
//

<?php }} ?>