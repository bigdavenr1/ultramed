<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/field.js" */ ?>
<?php /*%%SmartyHeaderCode:23672562455434610bf8225-31991499%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b95d7ea6e69b8a99d0d1c3e7f36cccd3cbd6236' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/model/field.js',
      1 => 1430112889,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23672562455434610bf8225-31991499',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434610c19646_46107809',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434610c19646_46107809')) {function content_55434610c19646_46107809($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage Main
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Emotion backend module.
 */
//
Ext.define('Shopware.apps.Emotion.model.Field', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
		//
        { name: 'id', type: 'int' },
        { name: 'componentId', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'xType', type: 'string' },
        { name: 'valueType', type: 'string' },
        { name: 'fieldLabel', type: 'string' },
        { name: 'supportText', type: 'string' },
        { name: 'helpTitle', type: 'string' },
        { name: 'helpText', type: 'string' },
        { name: 'store', type: 'string', useNull: true, defaultValue: null },
        { name: 'displayField', type: 'string', useNull: true, defaultValue: null },
        { name: 'valueField', type: 'string', useNull: true, defaultValue: null },
        { name: 'allowBlank', type: 'int' },
        { name: 'defaultValue', type: 'string' }
    ]

});
//
<?php }} ?>