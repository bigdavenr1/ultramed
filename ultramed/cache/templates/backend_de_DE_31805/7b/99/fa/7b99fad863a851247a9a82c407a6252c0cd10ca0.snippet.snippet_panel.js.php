<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:46:51
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/view/main/snippet_panel.js" */ ?>
<?php /*%%SmartyHeaderCode:677963369554472dbab24b3-83568922%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b99fad863a851247a9a82c407a6252c0cd10ca0' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/view/main/snippet_panel.js',
      1 => 1430113399,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '677963369554472dbab24b3-83568922',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554472dbac1c73_12292207',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554472dbac1c73_12292207')) {function content_554472dbac1c73_12292207($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Snippet
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Snippet.view.main.SnippetPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.snippet-main-snippetPanel',
    plain: true,

    /**
     * Initializes the component
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me    = this,
            items = [];

        me.shoplocaleStore.each(function(record) {
            items.push({
                title: record.get('displayName'),
                xtype: 'snippet-main-grid',
                store: me.snippetStore,
                shoplocale: record
            });
        });

        me.items = items;

        me.callParent(arguments);
    },

    /**
     * @param boolean - enabled
     * @return void
     */
    enableExpertMode: function(enabled) {
        var me = this;

        me.items.each(function(item) {
            item.enableExpertMode(enabled);
        });
    }
});
//
<?php }} ?>