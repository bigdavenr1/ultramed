<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:03
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:210159810255447cbf5f5516-52799360%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '98aff908aec359e2fdc432762900d10982870d62' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/user_manager/view/main/window.js',
      1 => 1430113404,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210159810255447cbf5f5516-52799360',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cbf6b16e7_00653100',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cbf6b16e7_00653100')) {function content_55447cbf6b16e7_00653100($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    UserManager
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Media Manager Main Window
 *
 * This file contains the business logic for the User Manager module. The module
 * handles the whole administration of the backend users.
 */
//
Ext.define('Shopware.apps.UserManager.view.main.Window', {
	extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'title','default'=>'Backend user administration','namespace'=>'backend/user_manager/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'Backend user administration','namespace'=>'backend/user_manager/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Backend Benutzer Administration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'Backend user administration','namespace'=>'backend/user_manager/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.usermanager-main-window',
    border: false,
    autoShow: true,
    layout: 'border',
    height: '90%',
    width: 1200,
    stateful: true,
    stateId: 'user-manager-main-window',


    /**
     * Initializes the component and builds up the main interface
     *
     * @return void
     */
    initComponent: function() {
        var me = this;

        /* <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'read'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?> */
        me.appContent = Ext.create('Ext.container.Container', {
            unstyled: true,
            region: 'center',
            border: false,
            layout: 'border',
            autoScroll: true,
            listeners: {
                afterrender: function(con) {
                    con.getEl().setStyle('overflow', 'hidden');

                    window.setTimeout(function() {
                        con.getEl().setStyle('overflow', 'auto');
                    }, 10);
                }
            },
            items: [{
                xtype: 'usermanager-user-list',
                userStore: me.userStore
            }]
        });
        /* <?php }?> */
        me.items = [{
            xtype: 'usermanager-main-navigation',
            region: 'west'
        }, me.appContent];

        me.callParent(arguments);
    }
});
//<?php }} ?>