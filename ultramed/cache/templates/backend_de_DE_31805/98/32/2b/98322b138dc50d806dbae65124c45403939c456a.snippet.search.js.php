<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/search.js" */ ?>
<?php /*%%SmartyHeaderCode:1406537539554347860e48d2-56078725%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '98322b138dc50d806dbae65124c45403939c456a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/search.js',
      1 => 1430113327,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1406537539554347860e48d2-56078725',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554347860f9b20_92584159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554347860f9b20_92584159')) {function content_554347860f9b20_92584159($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Search Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.navigation.Search', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-navigation-search',
    fields: [
        'searchterm',
        'countRequests',
        'countResults'
    ],
    remoteSort: true,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics/getSearchTerms';?>',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
<?php }} ?>