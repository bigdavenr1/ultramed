<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/customer_group.js" */ ?>
<?php /*%%SmartyHeaderCode:139358568455434785a51449-10629603%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d4ade2b296386c21d9f8686c02e9782d413b163' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/customer_group.js',
      1 => 1430113329,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '139358568455434785a51449-10629603',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785a62454_26068071',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785a62454_26068071')) {function content_55434785a62454_26068071($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics CustomerGroup Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.CustomerGroup', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-customer-group',

    legend: {
        position: 'right'
    },
    mask: 'horizontal',

    initComponent: function () {
        var me = this;

        me.series = [
            {
                type: 'pie',
                field: 'turnover',
                showInLegend: true,
                label: {
                    field: 'customerGroup',
                    display: 'rotate',
                    contrast: true,
                    font: '18px Arial'
                },
                tips: {
                    trackMouse: true,
                    width: 180,
                    height: 30,
                    renderer: function (storeItem) {
                        var value = Ext.util.Format.currency(
                            storeItem.get('turnover'),
                            me.subApp.currencySign,
                            2,
                            (me.subApp.currencyAtEnd == 1)
                        );

                        var title = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Umsatz<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'general'/'turnover','default'=>'Turnover','namespace'=>'backend/analytics/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: ' + value;
                        this.setTitle(title);
                    }
                }
            }
        ];

        me.callParent(arguments);
    }
});
//
<?php }} ?>