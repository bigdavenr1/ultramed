<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:41:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/model/form/template.js" */ ?>
<?php /*%%SmartyHeaderCode:1579773071554471aa2324a3-85552310%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a496e2ee4e61d30bdce87429735fab9a16f78c54' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/model/form/template.js',
      1 => 1430113349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1579773071554471aa2324a3-85552310',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554471aa278250_28847447',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554471aa278250_28847447')) {function content_554471aa278250_28847447($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Config
 * @subpackage Config
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Config.model.form.Template', {
    extend: 'Shopware.apps.Base.model.Template',

    fields: [
		//
        { name: 'description', type : 'string', useNull: true },
        { name: 'author', type : 'string', useNull: true },
        { name: 'license', type : 'string', useNull: true },
        { name: 'esi', type : 'boolean' },
        { name: 'styleSupport', type : 'boolean' },
        { name: 'emotion', type : 'boolean' },
        { name: 'enabled', type : 'boolean' },
        { name: 'preview', type : 'boolean' },
        { name: 'previewThumb', type : 'string', useNull: true },
        { name: 'previewFull', type : 'string', useNull: true }
    ]
});
//<?php }} ?>