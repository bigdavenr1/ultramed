<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:04:28
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments/info_panel.js" */ ?>
<?php /*%%SmartyHeaderCode:1487090732554476fc6783d3-02387965%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a475188911be36e9a93ed230043ac3e756149e62' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/blog/view/blog/detail/comments/info_panel.js',
      1 => 1430113617,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1487090732554476fc6783d3-02387965',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554476fc707a56_00997008',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554476fc707a56_00997008')) {function content_554476fc707a56_00997008($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Blog
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//
/**
 * Shopware UI - Blog view infopanel
 *
 * This infopanel contains some information about the chosen comment.
 */
//
Ext.define('Shopware.apps.Blog.view.blog.detail.comments.InfoPanel', {
    extend : 'Ext.form.Panel',
    alias : 'widget.blog-blog-detail-comments-info_panel',
    autoShow : true,
    name:  'infopanel',
    cls: 'detail-view',
    region: 'east',
    style:'background: #FFFFFF !important',
    split: true,
    bodyPadding: 10,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'title','default'=>'More information','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'title','default'=>'More information','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Mehr Informationen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'title','default'=>'More information','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    width: 300,
    collapsible: true,
    autoScroll: true,


    initComponent: function(){
        var me = this;

        me.infoView = me.createInfoView();
        me.items = [ me.infoView ];

        me.callParent(arguments);
    },

    createInfoView: function(){
        var me = this;
        var infoView = Ext.create('Ext.view.View', {
            name: 'infoView',
            emptyText: 'No additional information found',
            tpl: me.createInfoPanelTemplate(),
            region: 'center',
            itemSelector: 'div.info-view',
            height: '100%',
            renderData: []
        });

        return infoView;
    },

    /**
     * The template for the infopanel
     */
    createInfoPanelTemplate: function(){
        return new Ext.XTemplate(
            '<tpl for=".">',
                '<div class="info-view">',
                    '<div class="base-info">',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'headline','default'=>'Headline: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'headline','default'=>'Headline: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Überschrift: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'headline','default'=>'Headline: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<span>{headline}</span>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'author','default'=>'Author: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'author','default'=>'Author: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Autor: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'author','default'=>'Author: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<span>{name}</span>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'email','default'=>'eMail: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'email','default'=>'eMail: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
eMail: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'email','default'=>'eMail: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<span>{eMail}</span>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'creation_date','default'=>'Creation Date: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'creation_date','default'=>'Creation Date: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Erstellungsdatum: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'creation_date','default'=>'Creation Date: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<span>{[this.formatDate(values.creationDate)]}</span>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'status','default'=>'Status: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'status','default'=>'Status: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Status: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'status','default'=>'Status: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<tpl if="active==1"><span style="color: green"><b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusAccepted','default'=>'Accepted','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusAccepted','default'=>'Accepted','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Freigeschaltet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusAccepted','default'=>'Accepted','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b></span></tpl>',
                            '<tpl if="active==0"><span style="color: red"><b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusNotAccepted','default'=>'Not accepted yet','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusNotAccepted','default'=>'Not accepted yet','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Noch nicht freigeschaltet<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'statusNotAccepted','default'=>'Not accepted yet','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b></span></tpl>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'points','default'=>'Points: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'points','default'=>'Points: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Punkte: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'points','default'=>'Points: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            //function to create a star-rating
                            '<span>{[this.formatPoints(values.points)]}</span>',
                        '</p>',
                        '<p style="margin-top: 10px;">',
                            '<b><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'blog'/'detail'/'comments'/'info_panel'/'comment','default'=>'Comment: ','namespace'=>'backend/blog/view/blog')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'comment','default'=>'Comment: ','namespace'=>'backend/blog/view/blog'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kommentar: <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'blog'/'detail'/'comments'/'info_panel'/'comment','default'=>'Comment: ','namespace'=>'backend/blog/view/blog'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</b>',
                            '<br />',
                            '<span>{content}</span>',
                        '</p>',
                    '</div>',
                '</div>',
            '</tpl>',
            {
                /**
                 * Member function of the template which formats a date string
                 *
                 * @param [string] value - Date string in the following format: Y-m-d H:i:s
                 * @return [string] formatted date string
                 */
                formatDate: function(value) {
                    return Ext.util.Format.date(value) + ' ' + Ext.util.Format.date(value, timeFormat);
                },

                /**
                 * Function to format the points as a stars-rating
                 * @param points Contains the points
                 */
                formatPoints: function(points) {
                    var html = '';
                    var count = 0;
                    points = points/2;
                    for(var i=0; i<points; i++){
                        if((i-points) == -0.5) {
                            //create half-star
                            html = html + '<div style="height: 16px; width: 16px; display: inline-block;" class="sprite-star-half"></div>';
                        }else{
                            //create full stars
                            html = html + '<div style="height: 16px; width: 16px; display: inline-block;" class="sprite-star"></div>';
                        }
                        count++;
                    }

                    //add empty stars, so 5 stars are displayed
                    for(var i=0; i<(5-count); i++){
                        html = html + '<div style="height: 16px; width: 16px; display: inline-block;" class="sprite-star-empty"></div>';
                    }
                    return html;
                }
            }
        )
    }
});
//<?php }} ?>