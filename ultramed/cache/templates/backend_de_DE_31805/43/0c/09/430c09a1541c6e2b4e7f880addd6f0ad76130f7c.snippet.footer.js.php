<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:36:21
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/footer.js" */ ?>
<?php /*%%SmartyHeaderCode:5160272155447e7595eac3-90178890%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '430c09a1541c6e2b4e7f880addd6f0ad76130f7c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/product_feed/view/feed/tab/footer.js',
      1 => 1430113601,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5160272155447e7595eac3-90178890',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e75971214_97319007',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e75971214_97319007')) {function content_55447e75971214_97319007($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    ProductFeed
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Tab View.
 *
 * Displays all tab footer Information
 */
//
Ext.define('Shopware.apps.ProductFeed.view.feed.tab.Footer', {
    extend:  'Shopware.apps.ProductFeed.view.feed.tab.Body',
    alias:'widget.product_feed-feed-tab-footer',
    title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tab'/'title'/'footer','default'=>'Footer','namespace'=>'backend/product_feed/view/feed')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'footer','default'=>'Footer','namespace'=>'backend/product_feed/view/feed'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Fusszeile<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tab'/'title'/'footer','default'=>'Footer','namespace'=>'backend/product_feed/view/feed'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    fieldName: 'footer'
});
//
<?php }} ?>