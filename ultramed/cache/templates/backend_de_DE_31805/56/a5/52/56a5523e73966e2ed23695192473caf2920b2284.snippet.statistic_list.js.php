<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/model/statistic_list.js" */ ?>
<?php /*%%SmartyHeaderCode:4396403155447ea4411649-23896046%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56a5523e73966e2ed23695192473caf2920b2284' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/model/statistic_list.js',
      1 => 1430113159,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4396403155447ea4411649-23896046',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea4424555_58659660',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea4424555_58659660')) {function content_55447ea4424555_58659660($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model -  partner list backend module.
 *
 * The partner list model of the partner module holds the partner statistic data for the statistic list
 */
//
Ext.define('Shopware.apps.Partner.model.StatisticList', {
	/**
	* Extends the standard ExtJS 4
	* @string
	*/
    extend : 'Ext.data.Model',
	/**
	* The fields used for this model
	* @array
	*/
    fields : [
		//
        { name : 'id', type : 'int' },
        { name : 'number', type : 'string' },
        { name : 'netTurnOver', type : 'float' },
        { name : 'provision', type : 'float' },
        { name : 'orderTime', type : 'date' }
    ],
	/**
	* If the name of the field is 'id' extjs assumes autmagical that
	* this field is an unique identifier. 
	*/
    idProperty : 'id',
	/**
	* Configure the data communication
	* @object
	*/
    proxy : {
        type : 'ajax',
        api:{
            read:   '<?php echo '/backend/Partner/getStatisticList';?>'
        },
        reader : {
            type : 'json',
            root : 'data',
            totalProperty: 'totalCount'
        }
    }
});
//
<?php }} ?>