<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:27:10
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/settings/elements/multi_request_button.js" */ ?>
<?php /*%%SmartyHeaderCode:22965669955447c4e0a5e23-53434387%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccf11a62300d73f11fce01553126f7b43304bcbe' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/performance/view/tabs/settings/elements/multi_request_button.js',
      1 => 1430113619,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22965669955447c4e0a5e23-53434387',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c4e0c4021_98039082',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c4e0c4021_98039082')) {function content_55447c4e0c4021_98039082($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Customer
 * @subpackage Order
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Button which triggers the multi request dialog
 */
//
Ext.define('Shopware.apps.Performance.view.tabs.settings.elements.MultiRequestButton', {
    /**
     * Extend from our base grid
     * @string
     */
    extend:'Ext.container.Container',

    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.performance-multi-request-button',

    /**
     * Event and title needs to be passed as config params
     */
    event: '',
    title: '',
    showEvent: 'showMultiRequestDialog',

    /**
     * Initialize the button
     * @return void
     */
    initComponent:function () {
        var me = this;

        me.items = [ me.createButton() ];

        me.callParent(arguments);
    },

    createButton: function() {
        var me = this;

        return {
            xtype: 'button',
            cls: 'primary',
            margin: '0 0 10 0',
            text: me.title,
            handler: function() {
                me.fireEvent(me.showEvent, me.event, me.up('fieldset'));
            }
        };
    }

});
//
<?php }} ?>