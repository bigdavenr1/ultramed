<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:27
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/store/payments.js" */ ?>
<?php /*%%SmartyHeaderCode:210880963155447cd7c279d9-06738794%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0240ac4bd247e7c89bf85be1e3a6a0abbefb3663' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/risk_management/store/payments.js',
      1 => 1430113171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210880963155447cd7c279d9-06738794',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447cd7c34a16_02996636',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447cd7c34a16_02996636')) {function content_55447cd7c34a16_02996636($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    RiskManagement
 * @subpackage Store
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Payment store
 *
 * This store contains all payments.
 */
//
Ext.define('Shopware.apps.RiskManagement.store.Payments', {

    /**
    * Extend for the standard ExtJS 4
    * @string
    */
    extend: 'Ext.data.Store',
    /**
    * Auto load the store after the component
    * is initialized
    * @boolean
    */
    autoLoad: false,
    /**
    * Amount of data loaded at once
    * @integer
    */
    pageSize: 20,
    remoteFilter: true,
    /**
    * Define the used model for this store
    * @string
    */
    model : 'Shopware.apps.RiskManagement.model.Payment',

	/**
	 * Configure the data communication
	 * @object
	 */
	proxy:{
		type:'ajax',
		/**
		 * Configure the url mapping for the different
		 * @object
		 */
		api:{
			//read out all articles
			read:'<?php echo Enlight_Application::Instance()->Front()->Router()->assemble(array('controller' => "risk_management", 'action' => "getPayments", )); ?>'
		},
		/**
		 * Configure the data reader
		 * @object
		 */
		reader:{
			type:'json',
			root:'data',
			//total values, used for paging
			totalProperty:'total'
		}
	}

});
//<?php }} ?>