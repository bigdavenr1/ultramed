<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/manual_install.js" */ ?>
<?php /*%%SmartyHeaderCode:202231143155447ce3a47f03-99726266%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '02ba5c3c3a2faabcd6fbdebe7bece2bd0da9349b' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/view/manager/manual_install.js',
      1 => 1430111765,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '202231143155447ce3a47f03-99726266',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce3a85e32_06490722',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce3a85e32_06490722')) {function content_55447ce3a85e32_06490722($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Order
 * @subpackage View
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Stephan Pohl
 * @author     $Author$
 */

//
//
Ext.define('Shopware.apps.PluginManager.view.manager.ManualInstall', {
    /**
     * Define that the plugin manager main window is an extension of the enlight application window
     * @string
     */
    extend:'Enlight.app.Window',
    /**
     * Set base css class prefix and module individual css class for css styling
     * @string
     */
    cls:Ext.baseCSSPrefix + 'plugin-manager-main-window',
    /**
     * List of short aliases for class names. Most useful for defining xtypes for widgets.
     * @string
     */
    alias:'widget.plugin-manager-manager-manual-install',
    /**
     * Set no border for the window
     * @boolean
     */
    border:false,
    /**
     * True to automatically show the component upon creation.
     * @boolean
     */
    autoShow:true,
    /**
     * Set border layout for the window
     * @string
     */
    layout:'fit',
    /**
     * Define window width
     * @integer
     */
    width:400,
    /**
     * Define window height
     * @integer
     */
    height:200,
    /**
     * True to display the 'maximize' tool button and allow the user to maximize the window, false to hide the button and disallow maximizing the window.
     * @boolean
     */
    maximizable:true,
    /**
     * True to display the 'minimize' tool button and allow the user to minimize the window, false to hide the button and disallow minimizing the window.
     * @boolean
     */
    minimizable:true,

    /**
     * Don't create a footer button.
     * @boolean
     */
    footerBtn: false,

    /**
     * A flag which causes the object to attempt to restore the state of internal properties from a saved state on startup.
     */
    stateful:true,
    /**
     * The unique id for this object to use for state management purposes.
     */
    stateId:'shopware-plugin-manager-manager-manual-install-window',

    /**
     * Title of the window.
     * @string
     */
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'title','default'=>'Plugin manager - Install plugin manually','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'title','default'=>'Plugin manager - Install plugin manually','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin-Manager - Plugin manuell installieren<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'title','default'=>'Plugin manager - Install plugin manually','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

	snippets:{
		cancel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Abbrechen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'cancel','default'=>'Cancel','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		upload_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'upload_plugin','default'=>'Upload plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'upload_plugin','default'=>'Upload plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin hochladen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'upload_plugin','default'=>'Upload plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		note: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'note','default'=>'Note','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'note','default'=>'Note','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hinweis<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'note','default'=>'Note','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		fieldset_text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'fieldset_text','default'=>'Here you have the possibility to upload and install your plugins manually. Please consider that they have to be in a ZIP archive and check if the file size don´t exceed the upload size limit.','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'fieldset_text','default'=>'Here you have the possibility to upload and install your plugins manually. Please consider that they have to be in a ZIP archive and check if the file size don´t exceed the upload size limit.','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hier haben Sie die Möglichkeit Plugins manuell hochzuladen und installieren. Bitte beachten Sie beim Hochladen von Plugins, dass diese als ZIP-Archiv vorliegen müssen und Prüfen Sie vor dem Hochladen, ob die Dateigröße nicht Ihre maximalen Upload-Größe überschreitet.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'fieldset_text','default'=>'Here you have the possibility to upload and install your plugins manually. Please consider that they have to be in a ZIP archive and check if the file size don´t exceed the upload size limit.','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
		select_plugin: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'manager'/'manual_install'/'select_plugin','default'=>'Select plugin','namespace'=>'backend/plugin_manager/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'select_plugin','default'=>'Select plugin','namespace'=>'backend/plugin_manager/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Plugin auswählen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'manager'/'manual_install'/'select_plugin','default'=>'Select plugin','namespace'=>'backend/plugin_manager/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
	},

    /**
     * Initializes the component.
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.addEvents('uploadPlugin');

        me.formPanel = me.createFormPanel();
        me.items = me.formPanel;
        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: ['->', {
                text: me.snippets.cancel,
                cls: 'secondary',
                handler: function() {
                    me.destroy();
                }
            },
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'upload'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/{
                text: me.snippets.upload_plugin,
                cls: 'primary',
                handler: function(btn) {
                    me.fireEvent('uploadPlugin', me, me.formPanel, btn);
                }
            }
        /*<?php }?>*/]
        }];

        me.callParent(arguments);
    },

    /**
     * Creates the form panel which contains the file upload
     * component where the user could select a file for it's
     * local data system.
     *
     * @public
     * @return [object] Ext.form.Panel
     */
    createFormPanel: function() {
        var me = this;

        me.noticeFieldset = Ext.create('Ext.form.FieldSet', {
            title: me.snippets.note,
            html: me.snippets.fieldset_text
        });

        me.fileUpload = Ext.create('Ext.form.field.File', {
            fieldLabel: me.snippets.upload_plugin,
            name: 'plugin',
            labelWidth: 125,
            allowBlank: false,
            buttonConfig: {
                cls: 'primary small',
                text: me.snippets.select_plugin
            }
        });

        return Ext.create('Ext.form.Panel', {
            border: 0,
            layout: 'anchor',
            url: '<?php echo '/backend/PluginManager/upload';?>',
            bodyPadding: 10,
            defaults: { anchor: '100%' },
            cls: 'shopware-form',
            items: [ me.noticeFieldset, me.fileUpload ]
        });
    }
});
//
<?php }} ?>