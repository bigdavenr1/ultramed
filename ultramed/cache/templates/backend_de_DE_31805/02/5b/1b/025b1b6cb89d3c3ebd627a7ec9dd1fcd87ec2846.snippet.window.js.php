<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:46:51
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:963919402554472dba3e4e8-04965210%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '025b1b6cb89d3c3ebd627a7ec9dd1fcd87ec2846' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/snippet/view/main/window.js',
      1 => 1430113399,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '963919402554472dba3e4e8-04965210',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554472dba8d822_41412729',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554472dba8d822_41412729')) {function content_554472dba8d822_41412729($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Snippet
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Snippet.view.main.Window', {
    extend: 'Enlight.app.Window',
    alias: 'widget.snippet-main-window',

    layout: 'border',
    width: 980,
    height: '90%',
    stateful: true,
    stateId: 'shopware-snippet-main-window',

    /**
     * Contains all snippets for this view
     * @object
     */
    snippets: {
        title:                  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'title','default'=>'Snippet administration','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title','default'=>'Snippet administration','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Textbaustein Verwaltung<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'title','default'=>'Snippet administration','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        buttonInstallLanguage:  '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_install_language','default'=>'Install new Language','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_install_language','default'=>'Install new Language','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Install new Language<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_install_language','default'=>'Install new Language','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        buttonRemoveLanguage:   '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_remove_language','default'=>'Remove Language','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_remove_language','default'=>'Remove Language','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Remove Language<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_remove_language','default'=>'Remove Language','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        buttonLanguages:        '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_languages','default'=>'Languages','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_languages','default'=>'Languages','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Languages<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_languages','default'=>'Languages','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        buttonImportExport:     '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_import_export','default'=>'Import / Export','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_import_export','default'=>'Import / Export','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Import / Export<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_import_export','default'=>'Import / Export','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
        buttonExpert:           '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_expert','default'=>'Expert-Mode','namespace'=>'backend/snippet/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_expert','default'=>'Expert-Mode','namespace'=>'backend/snippet/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Experten-Modus<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_expert','default'=>'Expert-Mode','namespace'=>'backend/snippet/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
    },

    /**
     * Initializes the component and builds up the main interface
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.title = me.snippets.title;

        me.items = [{
            xtype: 'snippet-main-navigation',
            region: 'west',
            width: 180,
            store: me.nSpaceStore
        }, {
            xtype: 'snippet-main-snippetPanel',
            region: 'center',
            nSpaceStore: me.nSpaceStore,
            snippetStore: me.snippetStore,
            shoplocaleStore: me.shoplocaleStore
        }];

        me.tbar = me.getToolbar();

        me.callParent(arguments);
    },

    /**
     * Creates the toolbar.
     *
     * @return [object] generated Ext.toolbar.Toolbar
     */
    getToolbar: function() {
        var me      = this,
            buttons = [];

        buttons.push({
            xtype: 'button',
            text: me.snippets.buttonImportExport,
            action: 'export',
            iconCls: 'sprite-arrow-circle-double-135'
        });

        buttons.push({
            xtype: 'tbseparator'
        });

        buttons.push({
            xtype: 'button',
            iconCls: 'sprite-construction',
            text: me.snippets.buttonExpert,
            action: 'expert',
            enableToggle: true
        });

        return {
            xtype: 'toolbar',
            ui: 'shopware-ui',
            items: buttons
        };
    }
});
//
<?php }} ?>