<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/plugin.js" */ ?>
<?php /*%%SmartyHeaderCode:168138566255447ce37a3672-26247875%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c7bf162c37ae713904baf3d2eedd89a1f3c4a97' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/plugin.js',
      1 => 1430111756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '168138566255447ce37a3672-26247875',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce37c6f74_00751744',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce37c6f74_00751744')) {function content_55447ce37c6f74_00751744($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Main
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 * Shopware Plugin Manager - Plugin Model
 * The plugin model contains the raw data of an plugin of the shopware shop.
 */
//
Ext.define('Shopware.apps.PluginManager.model.Plugin', {

    /**
    * Extends the standard Ext Model
    * @string
    */
    extend: 'Ext.data.Model',

   /**
    * The batch model is only a data container which contains all
    * data for the global stores in the model association data.
    * An Ext.data.Model needs one field.
    * @array
    */
    fields: [
        //
       { name: 'id', type: 'int' },
       { name: 'name', type: 'string' },
       { name: 'label', type: 'string' },
       { name: 'namespace', type: 'string' },
       { name: 'source', type: 'string' },
       { name: 'description', type: 'string' },
       { name: 'active', type: 'boolean' },
       { name: 'added', type: 'date' },
       { name: 'installed', type: 'date', useNull: true },
       { name: 'updated', type: 'date' },
       { name: 'author', type: 'string' },
       { name: 'copyright', type: 'string' },
       { name: 'license', type: 'string' },
       { name: 'version', type: 'string' },
       { name: 'support', type: 'string' },
       { name: 'changes', type: 'string' },
       { name: 'link', type: 'string' },
       { name: 'icon', type: 'string' },

       { name: 'updateVersion', type: 'string', useNull: true },

       { name: 'capabilityUpdate', type: 'boolean' },
       { name: 'capabilityEnable', type: 'boolean' },
       { name: 'capabilityInstall', type: 'boolean' },
       { name: 'capabilityDummy', type: 'boolean' },

       { name: 'configForms', type: 'array' },
       { name: 'licenses', type: 'array' }
    ],

    /**
     * @array
     */
    associations:[
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.License', name:'getLicense', associationKey:'licenses' },
        { type:'hasMany', model:'Shopware.apps.PluginManager.model.Product', name:'getProduct', associationKey:'product' }
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            update: '<?php echo '/backend/PluginManager/savePlugin';?>',
            destroy: '<?php echo '/backend/PluginManager/deletePlugin';?>'
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'data',
            totalProperty:'total'
        }
    }

});
//



<?php }} ?>