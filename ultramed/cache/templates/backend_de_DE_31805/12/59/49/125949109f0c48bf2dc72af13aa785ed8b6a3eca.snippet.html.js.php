<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:41:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/view/element/html.js" */ ?>
<?php /*%%SmartyHeaderCode:1777696265554471aa474345-11581241%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '125949109f0c48bf2dc72af13aa785ed8b6a3eca' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/view/element/html.js',
      1 => 1430113355,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1777696265554471aa474345-11581241',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554471aa477d53_67648444',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554471aa477d53_67648444')) {function content_554471aa477d53_67648444($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */
Ext.define('Shopware.apps.Config.view.element.Html', {
    extend: 'Ext.form.field.HtmlEditor',
    alias: [
        'widget.config-element-html',
        'widget.config-element-htmleditor'
    ]
});<?php }} ?>