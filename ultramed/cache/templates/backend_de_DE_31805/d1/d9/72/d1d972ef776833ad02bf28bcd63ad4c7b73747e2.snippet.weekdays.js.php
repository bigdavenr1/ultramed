<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/weekdays.js" */ ?>
<?php /*%%SmartyHeaderCode:198947810055434786153ce9-04223247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1d972ef776833ad02bf28bcd63ad4c7b73747e2' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/weekdays.js',
      1 => 1430113328,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '198947810055434786153ce9-04223247',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5543478615fab5_82994633',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543478615fab5_82994633')) {function content_5543478615fab5_82994633($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Weekdays Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.navigation.Weekdays', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-navigation-weekdays',
    remoteSort: true,
    fields: [
        { name: 'date', type: 'date', dateFormat: 'timestamp' },
        { name: 'turnover', type: 'float' },
        'displayDate'
    ],
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics/getWeekdays';?>',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    },

    constructor: function (config) {
        var me = this;
        config.fields = me.fields;

        if (config.shopStore) {
            config.shopStore.each(function (shop) {
                config.fields.push('turnover' + shop.data.id);
            });
        }

        me.callParent(arguments);
    }
});
<?php }} ?>