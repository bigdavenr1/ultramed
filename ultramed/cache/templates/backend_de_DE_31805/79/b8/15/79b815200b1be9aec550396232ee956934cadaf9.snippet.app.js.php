<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/app.js" */ ?>
<?php /*%%SmartyHeaderCode:19039454845541e663662eb4-88263243%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '79b815200b1be9aec550396232ee956934cadaf9' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/app.js',
      1 => 1430112764,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19039454845541e663662eb4-88263243',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e6636bb579_75129588',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e6636bb579_75129588')) {function content_5541e6636bb579_75129588($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage App
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Payment', {
	/**
	* Extends from our special controller, which handles the
	* sub-application behavior and the event bus
	* @string
	*/
    extend : 'Enlight.app.SubApplication',
	/**
	* The name of the module. Used for internal purpose
	* @string
	*/
	name: 'Shopware.apps.Payment',
	/**
	* Sets the loading path for the sub-application.
	*
	* Note that you'll need a "loadAction" in your
	* controller (server-side)
	* @string
	*/
    loadPath : '<?php echo '/backend/payment/load';?>',

    /**
     * Enable bulk loading
     * @boolean
     */
    bulkLoad: true,

    /**
    * Required views for controller
    * @array
    */
    views: [ 'main.Window', 'payment.Tree', 'payment.CountryList', 'payment.Surcharge', 'payment.FormPanel', 'payment.SubshopList' ],
    /**
    * Required stores for controller
    * @array
    */
    stores: [ 'Countries', 'Payments' ],
    /**
    * Required models for controller
    * @array
    */
    models: [ 'Country', 'Payment', 'Attribute' ],

	/**
	* Requires controllers for sub-application
	* @array
	*/
    controllers : [ 'Payment', 'Main' ],
        /**
     * Returns the main application window for this is expected
     * by the Enlight.app.SubApplication class.
     * The class sets a new event listener on the "destroy" event of
     * the main application window to perform the destroying of the
     * whole sub application when the user closes the main application window.
     *
     * This method will be called when all dependencies are solved and
     * all member controllers, models, views and stores are initialized.
     *
     * @private
     * @return [object] mainWindow - the main application window based on Enlight.app.Window
     */
    launch:function () {
        var me = this,
            mainController = me.getController('Main');

        return mainController.mainWindow;
    }
});
//<?php }} ?>