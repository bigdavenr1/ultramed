<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:41
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/category.js" */ ?>
<?php /*%%SmartyHeaderCode:191528353455434785936550-29023260%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '854b38e136be981445aca033a51dd0b2d638974c' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/view/chart/category.js',
      1 => 1430113328,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '191528353455434785936550-29023260',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434785954a46_39714900',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434785954a46_39714900')) {function content_55434785954a46_39714900($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics Category Chart
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
//
//
Ext.define('Shopware.apps.Analytics.view.chart.Category', {
    extend: 'Shopware.apps.Analytics.view.main.Chart',
    alias: 'widget.analytics-chart-category',

    legend: {
        position: 'right'
    },
    mask: 'horizontal',
    listeners: {
        select: {
            fn: function (me, selection) {
                me.setZoom(selection);
                me.mask.hide();
            }
        }
    },
    initComponent: function () {
        var me = this;

        me.series = [
            {
                type: 'pie',
                field: 'turnover',
                showInLegend: true,
                listeners: {
                    itemmouseup: function (item) {
                        var node = item.storeItem.data.node;
                        if (!node) {
                            return;
                        }
                        me.setLoading(true);
                        me.store.getProxy().extraParams['node'] = node;
                        me.store.load({
                            callback: function() {
                                me.setLoading(false);
                            }
                        });
                    }
                },
                tips: {
                    trackMouse: true,
                    width: 180,
                    height: 45,
                    renderer: function (storeItem) {
                        var value = Ext.util.Format.currency(
                            storeItem.get('turnover'),
                            me.subApp.currencySign,
                            2,
                            (me.subApp.currencyAtEnd == 1)
                        );

                        this.setTitle(storeItem.get('name') + '<br><br>&nbsp;' +  value);
                    }
                },
                label: {
                    field: 'name',
                    display: 'rotate',
                    contrast: true,
                    font: '18px Arial'
                }
            }
        ];

        me.callParent(arguments);
    }
});
//
<?php }} ?>