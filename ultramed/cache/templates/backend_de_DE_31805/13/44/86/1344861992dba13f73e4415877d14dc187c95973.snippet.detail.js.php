<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:37:08
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/partner/detail.js" */ ?>
<?php /*%%SmartyHeaderCode:203644471855447ea454de90-54559955%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1344861992dba13f73e4415877d14dc187c95973' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/partner/view/partner/detail.js',
      1 => 1430113389,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '203644471855447ea454de90-54559955',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ea466e551_37177701',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ea466e551_37177701')) {function content_55447ea466e551_37177701($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Partner
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */
//
/**
 * Shopware UI - Partner detail main window.
 *
 * Displays all Detail Partner Information
 */
//
Ext.define('Shopware.apps.Partner.view.partner.Detail', {
    extend:'Ext.container.Container',
    alias:'widget.partner-partner-detail',
    border: 0,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'partner'/'configuration'/'title','default'=>'Partner configuration','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'partner'/'configuration'/'title','default'=>'Partner configuration','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'partner'/'configuration'/'title','default'=>'Partner configuration','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    partnerId:0,

    //Text for the ModusCombobox
    cookieLifeTimeGrading:[
        [0, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'life_time_grading'/'none','default'=>'None (0 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'life_time_grading'/'none','default'=>'None (0 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Keine (0 Sec.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'life_time_grading'/'none','default'=>'None (0 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [900, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'fifteen_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'fifteen_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
15 Minuten (900 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'fifteen_minutes','default'=>'15 Minutes (900 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [1800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'thirty_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'thirty_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
30 Minuten (1800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'thirty_minutes','default'=>'30 Minutes (1800 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [3600, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'one_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Stunde (3600 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_hour','default'=>'1 Hour (3600 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [7200, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'two_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Stunden (7200 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_hours','default'=>'2 Hours (7200 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [14400, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'four_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'four_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
4 Stunden (14400 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'four_hours','default'=>'4 Hours (14400 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [28800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'eight_hours','default'=>'8 Hours (28800 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'eight_hours','default'=>'8 Hours (28800 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
8 Stunden (28800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'eight_hours','default'=>'8 Hours (28800 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [86400, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'one_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Tag (86400 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_day','default'=>'1 Day (86400 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [172800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'two_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Tage (172800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_days','default'=>'2 Days (172800 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [259200, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'three_days','default'=>'3 Days (259200 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'three_days','default'=>'3 Days (259200 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
3 Tage (259200 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'three_days','default'=>'3 Days (259200 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [604800, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'one_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Woche (604800 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_week','default'=>'1 Week (604800 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [1209600, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'two_weeks','default'=>'2 Weeks (1209600 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_weeks','default'=>'2 Weeks (1209600 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Woche (1209600 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_weeks','default'=>'2 Weeks (1209600 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [2419200, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'one_month','default'=>'1 Month (2419200 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_month','default'=>'1 Month (2419200 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
1 Monat (2419200 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'one_month','default'=>'1 Month (2419200 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [4838400, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'two_months','default'=>'2 Months (4838400 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_months','default'=>'2 Months (4838400 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
2 Monate (4838400 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'two_months','default'=>'2 Months (4838400 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'],
        [7257600, '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'mode_combo_box'/'three_months','default'=>'3 Months (7257600 Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'three_months','default'=>'3 Months (7257600 Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
3 Monate (7257600 Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'mode_combo_box'/'three_months','default'=>'3 Months (7257600 Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
']
    ],
    /**
     * Initialize the Shopware.apps.Partner.view.partner.detail and defines the necessary
     * default configuration
     */
    initComponent:function () {
        var me = this;

        me.registerEvents();

        if(me.record){
            me.partnerId = me.record.data.id;
        }

        me.generalFieldset = Ext.create('Ext.form.FieldSet', {
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field_set'/'configuration','default'=>'General configuration','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field_set'/'configuration','default'=>'General configuration','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Konfiguration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field_set'/'configuration','default'=>'General configuration','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            bodyPadding: 10,
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items:me.createGeneralForm()
        });

        me.partnerFieldset = Ext.create('Ext.form.FieldSet', {
            title:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field_set'/'partner_information','default'=>'Partner information','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field_set'/'partner_information','default'=>'Partner information','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Partner Informationen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field_set'/'partner_information','default'=>'Partner information','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            bodyPadding: 10,
            layout: 'column',
            defaults: {
                columnWidth: 0.5
            },
            items:me.createPartnerForm()
        });

        me.items = [ me.generalFieldset, me.partnerFieldset ];

        me.callParent(arguments);
        if (me.record.get('customerId') > 0) {
            me.fireEvent('mapCustomerAccount', me.customerMapping, me.record.get('customerId'), 0,null);
        }
    },

    /**
     * Defines additional events which will be
     * fired from the component
     *
     * @return void
     */
    registerEvents:function () {
        this.addEvents(
                /**
                 * Event will be fired when the user changes the customer-account field
                 *
                 * @event mapCustomerAccount
                 * @param [Ext.form.field.Field] this
                 * @param [object] newValue
                 * @param [object] oldValue
                 * @param [object] eOpts
                 */
                'mapCustomerAccount'
        );

        return true;
    },


    /**
     * creates the general form and layout
     *
     * @return [Array] computed form
     */
    createGeneralForm:function () {
        var leftContainer, rightContainer, me = this;

        leftContainer = Ext.create('Ext.container.Container', {
            defaults:{
                labelWidth:180,
                minWidth:250,
                width: 400,
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items:me.createGeneralFormLeft()
        });

        rightContainer = Ext.create('Ext.container.Container', {
            defaults:{
                labelWidth:180,
                minWidth:250,
                width: 400,
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items:me.createGeneralFormRight()
        });

        return [ leftContainer, rightContainer ];
    },

    /**
     * creates the general form and layout
     *
     * @return [Array] computed form
     */
    createPartnerForm:function () {
        var leftContainer, rightContainer, me = this;

        leftContainer = Ext.create('Ext.container.Container', {
            defaults:{
                labelWidth:180,
                minWidth:250,
                width: 400,
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items:me.createPartnerFormLeft()
        });

        rightContainer = Ext.create('Ext.container.Container', {
            defaults:{
                labelWidth:180,
                minWidth:250,
                width: 400,
                labelStyle:'font-weight: 700;',
                xtype:'textfield'
            },
            items:me.createPartnerFormRight()
        });

        return [ leftContainer, rightContainer ];
    },


    /**
     * creates all fields for the general form on the left side
     */
    createGeneralFormLeft:function () {
        var me = this;
        me.customerMapping = Ext.create('Ext.form.field.Text',{
            fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'customer_account','default'=>'Customer account','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customer_account','default'=>'Customer account','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kundenkonto<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customer_account','default'=>'Customer account','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name:'customerId',
            checkChangeBuffer:800,
            labelWidth:180,
            minWidth:250,
            width: 400,
            labelStyle:'font-weight: 700;',
            supportText: "<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'supportText'/'noCustomerMapped','default'=>'No customer account has been linked','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'supportText'/'noCustomerMapped','default'=>'No customer account has been linked','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Es wurde keine Kundenkonto verknüpft<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'supportText'/'noCustomerMapped','default'=>'No customer account has been linked','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
",
            helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'customerMapping'/'help','default'=>'Link a customer account to enable a partner to have a look at the frontend statistics in the accoount section. You can link an account by entering a customer email or a customer number.','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customerMapping'/'help','default'=>'Link a customer account to enable a partner to have a look at the frontend statistics in the accoount section. You can link an account by entering a customer email or a customer number.','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verknüpft ein Kundenkonto, damit der Partner Zugriff zur Frontend Statistik im Accountbereich bekommt. Sie können zum Verknüpfen eine eMail-Adresse hinterlegen oder eine Kundennummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'customerMapping'/'help','default'=>'Link a customer account to enable a partner to have a look at the frontend statistics in the accoount section. You can link an account by entering a customer email or a customer number.','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            listeners: {
                change: function(field, newValue, oldValue, eOpts) {
                    me.fireEvent('mapCustomerAccount', field, newValue, oldValue, eOpts)
                }
            }
        });
        return [
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'tracking_code','default'=>'Tracking code','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'tracking_code','default'=>'Tracking code','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Tracking-Code<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'tracking_code','default'=>'Tracking code','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'idCode',
                allowBlank:false,
                required:true,
                enableKeyEvents:true,
                checkChangeBuffer:500,
                helpText:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'tracking_code'/'help','default'=>'This is the individual tracking code for each partner that will be appended to the URL.','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'tracking_code'/'help','default'=>'This is the individual tracking code for each partner that will be appended to the URL.','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Dies ist der individuelle Tracking-Code für jeden Partner. Dieser Code wird an die Shop URL gehängt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'tracking_code'/'help','default'=>'This is the individual tracking code for each partner that will be appended to the URL.','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                vtype:'remote',
                validationUrl:'<?php echo '/backend/partner/validateTrackingCode';?>',
                validationRequestParam:me.partnerId,
                validationErrorMsg:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'error_message'/'used_tracking_code','default'=>'The tracking code is already in use','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'error_message'/'used_tracking_code','default'=>'The tracking code is already in use','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Der eingegebene Tracking-Code wird bereits verwendet.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'error_message'/'used_tracking_code','default'=>'The tracking code is already in use','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                validateOnChange:true,
                validateOnBlur:false
            },
            me.customerMapping,
            {
                xtype:'checkbox',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Aktiv<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'active','default'=>'Active','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                inputValue:1,
                uncheckedValue:0,
                name:'active'
            }
        ];
    },

    /**
     * creates all fields for the general form on the left side
     */
    createGeneralFormRight:function () {
        var me = this;

        return [
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'commission','default'=>'Commission in %','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'commission','default'=>'Commission in %','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Provision in %<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'commission','default'=>'Commission in %','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                xtype:'numberfield',
                name:'percent',
                decimalPrecision: 2,
                maxValue:100,
                minValue:0,
                allowBlank:false,
                hideTrigger:true,
                keyNavEnabled:false,
                mouseWheelEnabled:false,
                required:true,
                allowDecimals: true,
                decimalSeparator: '.'
            },
            {
                xtype:'combobox',
                name:'cookieLifeTime',
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'cookieLifeTime','default'=>'Cookie lifetime(Sec.)','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'cookieLifeTime','default'=>'Cookie lifetime(Sec.)','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Gültigkeit Cookie (Sek.)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'cookieLifeTime','default'=>'Cookie lifetime(Sec.)','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                store:new Ext.data.SimpleStore({
                    fields:['id', 'text'], data:me.cookieLifeTimeGrading
                }),
                valueField:'id',
                displayField:'text',
                mode:'local'
            }
        ]
    },

    /**
     * creates all fields for the general form on the right side
     */
    createPartnerFormLeft:function () {
        var me = this;
        return [
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Firma<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'company','default'=>'Company','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                allowBlank:false,
                required:true,
                name:'company'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'street','default'=>'Street','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'street','default'=>'Street','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Straße<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'street','default'=>'Street','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'street'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'street_number','default'=>'Street number','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'street_number','default'=>'Street number','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hausnummer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'street_number','default'=>'Street number','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'streetNumber'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'zip_code','default'=>'Zip code','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'zip_code','default'=>'Zip code','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Postleitzahl<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'zip_code','default'=>'Zip code','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'zipCode'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'city','default'=>'City','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'city','default'=>'City','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stadt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'city','default'=>'City','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'city'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'country','default'=>'Country','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'country','default'=>'Country','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Land<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'country','default'=>'Country','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'countryName'
            }

        ]
    },

    /**
     * creates all fields for the general form on the right side
     */
    createPartnerFormRight:function () {
        var me = this;
        return [
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'phone','default'=>'Phone','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'phone','default'=>'Phone','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Telefon<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'phone','default'=>'Phone','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'phone'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'fax','default'=>'Fax','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'fax','default'=>'Fax','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Fax<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'fax','default'=>'Fax','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'fax'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'email','default'=>'Email','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'email','default'=>'Email','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
eMail<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'email','default'=>'Email','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'email'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'web','default'=>'Web','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'web','default'=>'Web','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Web<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'web','default'=>'Web','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'web'
            },
            {
                fieldLabel:'<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'detail_general'/'field'/'profile','default'=>'Profile','namespace'=>'backend/partner/view/partner')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'profile','default'=>'Profile','namespace'=>'backend/partner/view/partner'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Profil<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'detail_general'/'field'/'profile','default'=>'Profile','namespace'=>'backend/partner/view/partner'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                name:'profile'
            }
        ]
    }
});
//
<?php }} ?>