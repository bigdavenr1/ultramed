<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:29:42
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/referrer_revenue.js" */ ?>
<?php /*%%SmartyHeaderCode:10608832565543478616fb44-08570412%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4133c1f0e6001e592de83370120f40cca13e1a68' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/analytics/store/navigation/referrer_revenue.js',
      1 => 1430113327,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10608832565543478616fb44-08570412',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55434786178421_69586334',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55434786178421_69586334')) {function content_55434786178421_69586334($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Analytics ReferrerRevenue Store
 *
 * @category   Shopware
 * @package    Analytics
 * @copyright  Copyright (c) shopware AG (http://www.shopware.de)
 *
 */
Ext.define('Shopware.apps.Analytics.store.navigation.ReferrerRevenue', {
    extend: 'Ext.data.Store',
    alias: 'widget.analytics-store-navigation-referrer_revenue',
    remoteSort: true,
    fields: [
        'host',
        'turnover',
        'average',
        'turnoverNewCustomer',
        'turnoverRegularCustomer',
        'orderCount',
        'newCustomers',
        'regularCustomers',
        'averageNewCustomer',
        'averageRegularCustomer'
    ],
    proxy: {
        type: 'ajax',
        url: '<?php echo '/backend/analytics/getReferrerRevenue';?>',
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});
<?php }} ?>