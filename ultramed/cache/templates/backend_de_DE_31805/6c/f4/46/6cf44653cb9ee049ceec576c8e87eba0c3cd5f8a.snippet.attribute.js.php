<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:39
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/attribute.js" */ ?>
<?php /*%%SmartyHeaderCode:166177857055447ce37280a9-82733732%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6cf44653cb9ee049ceec576c8e87eba0c3cd5f8a' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Default/Core/PluginManager/Views/backend/plugin_manager/model/attribute.js',
      1 => 1430111756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '166177857055447ce37280a9-82733732',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ce373fd83_31139797',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ce373fd83_31139797')) {function content_55447ce373fd83_31139797($_smarty_tpl) {?>/**
 * Shopware 4
 * Copyright © shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    PluginManager
 * @subpackage Main
 * @copyright Copyright (c) shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author     Oliver Denter
 * @author     $Author$
 */

/**
 * Shopware Plugin Manager - Attribute Model
 * The attribute model contains some additional configuration for the community product.
 * For example the licence_key.
 */
//
Ext.define('Shopware.apps.PluginManager.model.Attribute', {

    /**
    * Extends the standard Ext Model
    * @string
    */
    extend: 'Ext.data.Model',

    /**
    * The batch model is only a data container which contains all
    * data for the global stores in the model association data.
    * An Ext.data.Model needs one field.
    * @array
    */
    fields: [
        //
        { name: 'changelog', type: 'string', useNull: true },
        { name: 'licence_key', type: 'string', useNull: true },
        { name: 'test_modus', type: 'int', useNull: true },
        { name: 'version', type: 'string', useNull: true },
        { name: 'certificate', type: 'int', useNull: true },
        { name: 'support_by', type: 'int', useNull: true },
        { name: 'forum', type: 'string', useNull: true },
        { name: 'install_description', type: 'string', useNull: true },
        { name: 'shopware_compatible', type: 'string', useNull: true },
        { name: 'forum_url', type: 'string', useNull: true },
        { name: 'store_url', type: 'string', useNull: true }

    ]
});
//

<?php }} ?>