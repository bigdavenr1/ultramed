<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/tree.js" */ ?>
<?php /*%%SmartyHeaderCode:9262531605541e663c7bcd4-92891463%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6c9b0a3fb94a6df120128b16da8476c5c98477b7' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/view/payment/tree.js',
      1 => 1430113391,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9262531605541e663c7bcd4-92891463',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663ca04f0_05397163',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663ca04f0_05397163')) {function content_5541e663ca04f0_05397163($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Tree to select the payment
 *
 * todo@all: Documentation
 *
 */
//
Ext.define('Shopware.apps.Payment.view.payment.Tree', {
    extend : 'Ext.tree.Panel',
    alias : 'widget.payment-main-tree',
    autoShow : true,
    region: 'west',
    name:  'tree',
    flex: 0.3,
    rootVisible: false,
    useArrows: false,
    lines: false,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'tree_panel_title','default'=>'Available payments','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree_panel_title','default'=>'Available payments','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Verfügbare Zahlungsarten<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'tree_panel_title','default'=>'Available payments','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    store: 'Payments',

    /**
     * This function is called, when the component is initiated
     */
    initComponent: function(){
        var me = this;
        me.registerEvents();

        var buttons = [];
        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'create'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
        buttons.push({
            xtype: 'tbspacer',
            width: 5
        });

        buttons.push(Ext.create('Ext.button.Button', {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_new','default'=>'New','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_new','default'=>'New','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Neu<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_new','default'=>'New','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-plus-circle',
            action: 'create',
            cls: 'small secondary',
            handler: function(){
                me.fireEvent('createPayment', this)
            }
        }));
        /*<?php }?>*/

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        buttons.push('->');
        buttons.push(Ext.create('Ext.button.Button', {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button_delete','default'=>'Delete','namespace'=>'backend/payment/payment')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_delete','default'=>'Delete','namespace'=>'backend/payment/payment'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button_delete','default'=>'Delete','namespace'=>'backend/payment/payment'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            iconCls: 'sprite-minus-circle',
            action: 'delete',
            name: 'delete',
            cls: 'small secondary',
            disabled: true,
            handler: function(){
                me.fireEvent('deletePayment', me, this)
            }
        }));
        buttons.push({
            xtype: 'tbspacer',
            width: 5
        });
        /*<?php }?>*/

        me.toolBar = Ext.create('Ext.toolbar.Toolbar', {
            name: 'treeToolBar',
            dock: 'bottom',
            items: buttons
        });

        me.dockedItems = me.toolBar;

        me.callParent(arguments);
    },

    /**
     * This function registers the special events
     */
    registerEvents: function() {
        this.addEvents(
            /**
             * This event is fired, when the user presses the "new"-button
             * @param this Contains the button
             * @event createPayment
            */
            'createPayment',
            /**
             * This event is fired, when the user wants to delete a payment
             * @param me Contains the tree
             * @param this Contains the button
             * @event deletePayment
             */
            'deletePayment'
        );
    }
});
//<?php }} ?>