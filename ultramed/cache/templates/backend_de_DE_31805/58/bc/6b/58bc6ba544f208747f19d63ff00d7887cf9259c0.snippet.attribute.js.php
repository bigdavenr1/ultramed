<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:35:58
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/model/attribute.js" */ ?>
<?php /*%%SmartyHeaderCode:205093262955447e5e5c91d9-78489865%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '58bc6ba544f208747f19d63ff00d7887cf9259c0' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/banner/model/attribute.js',
      1 => 1430112854,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205093262955447e5e5c91d9-78489865',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447e5e5f1093_13753815',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447e5e5f1093_13753815')) {function content_55447e5e5f1093_13753815($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Banner
 * @subpackage Banner
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Banner
 *
 * Backend - Management for Banner. Create | Modify | Delete.
 * Standard banner model
 */
//
Ext.define('Shopware.apps.Banner.model.Attribute', {
    /**
     * Extends the default extjs 4 model
     * @string
     */
    extend : 'Ext.data.Model',

    /**
     * Defined items used by that model
     * 
     * We have to have a splitted date time object here.
     * One part is used as date and the other part is used as time - this is because
     * the form has two separate fields - one for the date and one for the time.
     * 
     * @array
     */
    fields : [
		//
        { name : 'id', type: 'int' },
        { name : 'bannerId', type: 'int', useNull: true }
    ]
});
//
<?php }} ?>