<?php /* Smarty version Smarty-3.1.12, created on 2015-04-30 10:22:59
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/model/country.js" */ ?>
<?php /*%%SmartyHeaderCode:8183421745541e663b71754-34465986%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15aa3be6a177eb6af6f7cb183d9945ac8f08a80e' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/payment/model/country.js',
      1 => 1430113161,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8183421745541e663b71754-34465986',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5541e663ba2e78_62537755',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5541e663ba2e78_62537755')) {function content_5541e663ba2e78_62537755($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Payment
 * @subpackage Model
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Country list backend module.
 *
 * The Country-Model represents a single country.
 * It extends the base-country-model
 */
//
Ext.define('Shopware.apps.Payment.model.Country', {

    /**
     * Extends the standard Ext Model
     * @string
     */
    extend:'Shopware.apps.Base.model.Country',

    /**
     * The fields used for this model
     * @array
     */
     //todo@ps paymentId
    fields:[
		//
        { name: 'surcharge', type: 'double' },
        { name: 'payment', type: 'int' }
    ],

    proxy :
    {
        type : 'ajax',
        api : {
            read : '<?php echo '/backend/payment/getCountries';?>'
        },
        reader : {
            type : 'json',
            root: 'data'
        }
    }
});
//

<?php }} ?>