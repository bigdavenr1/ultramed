<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:26:52
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/window.js" */ ?>
<?php /*%%SmartyHeaderCode:40459845155447c3c4aacc6-74638302%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '67ce3b8c23f34a2edee3e6ab73d945b223dd76a1' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/engine/Shopware/Plugins/Community/Frontend/SwagPaymentPaypal/Views/backend/payment_paypal/view/main/window.js',
      1 => 1430111762,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '40459845155447c3c4aacc6-74638302',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447c3c4cd260_91100588',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447c3c4cd260_91100588')) {function content_55447c3c4cd260_91100588($_smarty_tpl) {?>/*
 * (c) shopware AG <info@shopware.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

//

//
Ext.define('Shopware.apps.PaymentPaypal.view.main.Window', {
    extend: 'Enlight.app.Window',
    alias: 'widget.paypal-main-window',

    width: 1200,
    height: 500,
    layout: 'border',

    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window'/'title','default'=>'PayPal Payments','namespace'=>'backend/payment_paypal/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'PayPal Payments','namespace'=>'backend/payment_paypal/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
PayPal Zahlungen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window'/'title','default'=>'PayPal Payments','namespace'=>'backend/payment_paypal/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    /**
     *
     */
    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: me.getItems()
        });

        me.callParent(arguments);
    },

    /**
     * @return array
     */
    getItems: function() {
        var me = this;
        return [{
            region: 'east',
            xtype: 'paypal-main-detail'
        }, {
            region: 'center',
            xtype: 'paypal-main-list'
        }];
    }
});
//
<?php }} ?>