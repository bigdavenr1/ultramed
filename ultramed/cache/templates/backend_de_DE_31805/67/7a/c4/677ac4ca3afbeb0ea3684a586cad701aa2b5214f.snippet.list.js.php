<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 09:29:14
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/view/log/list.js" */ ?>
<?php /*%%SmartyHeaderCode:37659316355447ccaee5919-18686959%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '677ac4ca3afbeb0ea3684a586cad701aa2b5214f' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/log/view/log/list.js',
      1 => 1430113375,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '37659316355447ccaee5919-18686959',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_55447ccaf338c4_21869799',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55447ccaf338c4_21869799')) {function content_55447ccaf338c4_21869799($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Log
 * @subpackage View
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Log view list
 *
 * This grid contains all logs and its information.
 */
//
Ext.define('Shopware.apps.Log.view.log.List', {

    /**
    * Extend from the standard ExtJS 4
    * @string
    */
    extend: 'Ext.grid.Panel',
    border: 0,

    ui: 'shopware-ui',

    /**
    * Alias name for the view. Could be used to get an instance
    * of the view through Ext.widget('log-main-list')
    * @string
    */
    alias: 'widget.log-main-list',
    /**
    * The window uses a border layout, so we need to set
    * a region for the grid panel
    * @string
    */
    region: 'center',
    /**
    * The view needs to be scrollable
    * @string
    */
    autoScroll: true,

    /**
    * Sets up the ui component
    * @return void
    */
    initComponent: function() {
        var me = this;
		me.registerEvents();
		me.selModel = me.getGridSelModel();
		me.store = me.logStore;
		me.toolbar = me.getToolbar();
		me.columns = me.getColumns();
		me.dockedItems = [];
		me.dockedItems.push(me.toolbar);

		// Add paging toolbar to the bottom of the grid panel
		me.dockedItems.push({
			dock: 'bottom',
			xtype: 'pagingtoolbar',
			displayInfo: true,
			store: me.store
		});
		me.callParent(arguments);
    },

	/**
	 * Creates the toolbar
	 *
	 * @return [object] Ext.toolbar.Toolbar
	 */
	getToolbar: function(){
		var items = [];
		/*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?>*/
		items.push(Ext.create('Ext.button.Button',{
			iconCls: 'sprite-minus-circle',
			text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'deleteMarkedEntries','default'=>'Delete marked entries','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'deleteMarkedEntries','default'=>'Delete marked entries','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Markierte Einträge löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'deleteMarkedEntries','default'=>'Delete marked entries','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			disabled: true,
			action: 'deleteMultipleLogs'
		}));
		/*<?php }?>*/
		items.push('->');
		items.push({
			xtype: 'combo',
			store: Ext.create('Shopware.apps.Log.store.Users'),
			valueField:'id',
			displayField:'name',
			emptyText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'toolbar'/'filterField','default'=>'Filter by','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'filterField','default'=>'Filter by','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filtern nach<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'toolbar'/'filterField','default'=>'Filter by','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
		});
		items.push({
			xtype: 'tbspacer',
			width: 6
		});

		return Ext.create('Ext.toolbar.Toolbar', {
			dock: 'top',
			ui: 'shopware-ui',
			items: items
		});
	},

    /**
     * Creates the selectionModel of the grid with a listener to enable the delete-button
     */
    getGridSelModel: function(){
		return Ext.create('Ext.selection.CheckboxModel',{
			listeners: {
				selectionchange: function(sm, selections) {
					var owner = this.view.ownerCt,
						btn = owner.down('button[action=deleteMultipleLogs]');

					//If no log is marked
					if(btn) {
						btn.setDisabled(selections.length == 0);
					}
				}
			}
		});
    },

    /**
     *  Creates the columns
	 *
	 *  @return array columns Contains all columns
     */
    getColumns: function(){
        var me = this;

        var columns = [{
			header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column_date','default'=>'Date','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_date','default'=>'Date','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Datum<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_date','default'=>'Date','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			dataIndex: 'date',
			flex: 1,
			xtype: 'datecolumn',
			renderer: me.renderDate
		},{
			header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column_user','default'=>'User','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_user','default'=>'User','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Benutzer<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_user','default'=>'User','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			dataIndex: 'user',
			flex: 1
		}, {
			header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column_module','default'=>'Module','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_module','default'=>'Module','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Modul<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_module','default'=>'Module','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			dataIndex: 'key',
			flex: 1
		},{
			header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'column_text','default'=>'Text','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_text','default'=>'Text','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Text<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'column_text','default'=>'Text','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			dataIndex: 'text',
			flex: 1
		}
		/*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if ($_tmp2){?>*/
        ,
		{
			header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'actioncolumn','default'=>'Options','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'actioncolumn','default'=>'Options','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Optionen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'actioncolumn','default'=>'Options','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			xtype: 'actioncolumn',
			renderer: me.renderActionColumn
		}
		/*<?php }?>*/
		];

        return columns;
    },

	/**
	 * Renders the date
	 *
	 * @param value
	 * @return [date] value Contains the date
	 */
	renderDate: function(value){
		return Ext.util.Format.date(value) + ' ' + Ext.util.Format.date(value, timeFormat);
	},

	/**
	 * Renders the action-column
	 *
	 * @param value Contains the clicked value
	 * @param metaData Contains the metaData
	 * @param model Contains the selected model
	 * @param rowIndex Contains the rowIndex of the selection
	 * @return [object] Ext.DomHelper
	 */
	renderActionColumn: function(value, metaData, model, rowIndex){
		var data = [];

		data.push(Ext.DomHelper.markup({
			tag:'img',
			'class': 'x-action-col-icon sprite-minus-circle',
			tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'grid'/'actioncolumn'/'buttonTooltip','default'=>'Delete log','namespace'=>'backend/log/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'actioncolumn'/'buttonTooltip','default'=>'Delete log','namespace'=>'backend/log/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Log löschen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'grid'/'actioncolumn'/'buttonTooltip','default'=>'Delete log','namespace'=>'backend/log/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
			cls:'sprite-minus-circle',
			onclick: "Ext.getCmp('" + this.id + "').fireEvent('deleteColumn', " + rowIndex + ");"
		}));

		return data;
	},

	/**
	 * Defines additional events which will be
	 * fired from the component
	 *
	 * @return void
	 */
	registerEvents:function () {
		this.addEvents(
			/**
			 * Event will be fired when the user clicks the delete icon in the
			 * action column
			 *
			 * @event deleteColumn
			 * @param [integer] rowIndex - Row index of the selection
			 */
			'deleteColumn'
		)
	}
});
//<?php }} ?>