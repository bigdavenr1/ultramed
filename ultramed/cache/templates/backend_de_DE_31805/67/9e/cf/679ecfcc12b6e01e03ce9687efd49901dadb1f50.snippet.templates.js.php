<?php /* Smarty version Smarty-3.1.12, created on 2015-05-01 11:23:29
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/controller/templates.js" */ ?>
<?php /*%%SmartyHeaderCode:4837740275543461195c3d6-63368347%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '679ecfcc12b6e01e03ce9687efd49901dadb1f50' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/emotion/controller/templates.js',
      1 => 1430112888,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4837740275543461195c3d6-63368347',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554346119f24c8_45089707',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554346119f24c8_45089707')) {function content_554346119f24c8_45089707($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Emotion
 * @subpackage Controller
 * @copyright  Copyright (c) 2012, shopware AG (http://www.shopware.de)
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Emotion Main Controller
 *
 * This file contains the business logic for the Emotion module.
 */
//
Ext.define('Shopware.apps.Emotion.controller.Templates', {

    /**
     * Extend from the standard ExtJS 4 controller
     * @string
     */
	extend: 'Ext.app.Controller',

    /**
     * References to components
     * @Array
     */
    refs: [
        { ref: 'list', selector: 'emotion-templates-list' },
        { ref: 'toolbar', selector: 'emotion-templates-toolbar' },
        { ref: 'settings', selector: 'emotion-view-templates-settings' }
    ],

    snippets: {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'title','default'=>'Emotions','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'title','default'=>'Emotions','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Einkaufswelt<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'title','default'=>'Emotions','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            copie: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'copie','default'=>'Copie','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'copie','default'=>'Copie','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Kopie<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'copie','default'=>'Copie','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            edited: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'edited','default'=>'The template [0] was successfully edited.','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'edited','default'=>'The template [0] was successfully edited.','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Template [0] wurde erfolgreich bearbeitet.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'edited','default'=>'The template [0] was successfully edited.','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            duplicated: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'duplicated','default'=>'The template [0] was successfully duplicated.','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'duplicated','default'=>'The template [0] was successfully duplicated.','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Template [0] wurde erfolgreich dupliziert.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'duplicated','default'=>'The template [0] was successfully duplicated.','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            removed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'removed','default'=>'The template [0] was successfully removed.','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'removed','default'=>'The template [0] was successfully removed.','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Das Template [0] wurde erfolgreich entfernt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'removed','default'=>'The template [0] was successfully removed.','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            marked_removed: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'marked_removed','default'=>'The selected template are successfully removed.','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'marked_removed','default'=>'The selected template are successfully removed.','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Die ausgewählten Templates wurden erfolgreich entfernt.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'marked_removed','default'=>'The selected template are successfully removed.','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            confirm: {
                remove: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'confirm'/'remove','default'=>'Are you sure you want to remove the template [0]?','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'confirm'/'remove','default'=>'Are you sure you want to remove the template [0]?','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sind Sie sicher, dass Sie das Template [0] entfernen möchten?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'confirm'/'remove','default'=>'Are you sure you want to remove the template [0]?','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                marked_remove: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'confirm'/'marked_remove','default'=>'Are you sure you want to remove the selected template(s)?','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'confirm'/'marked_remove','default'=>'Are you sure you want to remove the selected template(s)?','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sind Sie sicher, dass Sie die ausgewählten Templates entfernen möchten?<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'confirm'/'marked_remove','default'=>'Are you sure you want to remove the selected template(s)?','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            },
            alert: {
                default_remove: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'global'/'alert','default'=>'Default templates could not be removed.','namespace'=>'backend/emotion/templates/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'alert','default'=>'Default templates could not be removed.','namespace'=>'backend/emotion/templates/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Standard-Templates können nicht entfernt werden.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'global'/'alert','default'=>'Default templates could not be removed.','namespace'=>'backend/emotion/templates/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
            }
        },

    /**
     * Creates the necessary event listener for this
     * specific controller and opens a new Ext.window.Window
     * to display the subapplication
     *
     * @return void
     */
    init: function() {
        var me = this;

        me.control({
            'emotion-templates-list': {
                'edit': me.onInlineEdit,
                'selectionChange': me.onSelectionChange,
                'editEntry': me.onEdit,
                'duplicate': me.onDuplicate,
                'remove': me.onRemove
            },
            'emotion-templates-toolbar': {
                'searchGrids': me.onSearch
            },
            'emotion-templates-toolbar button[action=emotion-templates-new-template]': {
                click: me.onCreate
            },
            'emotion-templates-toolbar button[action=emotion-templates-delete-marked-templates]': {
                click: me.onMultipleRemove
            },
            'emotion-view-templates-settings button[action=emotion-save-grid]': {
                click: me.onSave
            }
        });
    },

    /**
     * Event listener method which will be triggered when the user wants to
     * create a new grid.
     *
     * @returns { Void }
     */
    onCreate: function() {
        var me = this;

        me.getView('templates.Settings').create();
    },

    /**
     * Event handler method which will be triggered when the user
     * clicks on the pencil icon in the list.
     *
     * The method opens the `settings` window to update the values.
     *
     * @param { Shopware.apps.Emotion.view.templates.List } grid
     * @param { Shopware.apps.Emotion.model.Template } rec
     * @returns { Void }
     */
    onEdit: function(grid, rec) {
        var me = this;

        me.getView('templates.Settings').create({
            record: rec
        });
    },

    /**
     * Event handler which will be triggered when the user updates an record
     * using the row editor plugin for the list.
     *
     * @param { Object } editor
     * @param { Object } values
     * @returns { Boolean }
     */
    onInlineEdit: function(editor, values) {
        var me = this,
            grid = editor.grid,
            record = values.record;

        if(!record) {
            return false;
        }
        grid.setLoading(true);
        record.save({
            callback: function() {
                grid.setLoading(false);
                Shopware.Notification.createGrowlMessage(me.snippets.title, Ext.String.format(me.snippets.edited, record.get('name')));
            }
        });
    },

    /**
     * Event listener method which will be triggered when the user
     * clicks on the duplicate icon in the list.
     *
     * The method sends an AJAX requests to the server side and transforms
     * the received JSON object to a record.
     *
     * @param { Shopware.apps.Emotion.view.grids.List } grid
     * @param { Shopware.apps.Emotion.model.Grid } rec
     * @returns { Boolean }
     */
    onDuplicate: function(grid, rec) {
        var me = this;

        if(!rec) {
            return false;
        }

        grid.setLoading(true);
        Ext.Ajax.request({
            url: '<?php echo '/backend/Emotion/duplicateTemplate';?>',
            params: { id: rec.get('id') },
            success: function(response) {
                var values = Ext.JSON.decode(response.responseText),
                    duplicateRecord;

                values.data.name += ' ' + me.snippets.copie;
                duplicateRecord = Ext.create('Shopware.apps.Emotion.model.Template', values.data);
                grid.getStore().add(duplicateRecord);
                duplicateRecord.save();
                grid.setLoading(false);
                Shopware.Notification.createGrowlMessage(me.snippets.title, Ext.String.format(me.snippets.duplicated, rec.get('name')));
            }
        });
    },

    /**
     * Event listener method which will be triggered when the user
     * clicks the delete icon in the list.
     *
     * @param { Shopware.apps.Emotion.view.grids.List } grid
     * @param { Shopware.apps.Emotion.model.Grid } rec
     * @returns { Void|Boolean }
     */
    onRemove: function(grid, rec) {
        var me = this,
            store = grid.getStore();

        if(rec.data.id < 2) {
            Ext.Msg.alert(me.snippets.title, me.snippets.alert.default_remove);
            return false;
        }

        Ext.Msg.confirm(me.snippets.title, Ext.String.format(me.snippets.confirm.remove, rec.get('name')), function(btn) {
            if(btn !== 'yes') {
                return false;
            }

            store.remove(rec);
                grid.setLoading(true);
                rec.destroy({
                    callback: function() {
                        Shopware.Notification.createGrowlMessage(me.snippets.title, Ext.String.format(me.snippets.removed, rec.get('name')));
                        grid.setLoading(false);
                    }
                });
        });
    },

    /**
     * Event listener method which will be triggered when the user
     * clicks the `remove selected grids` button.
     *
     * The method loops through the selection and destroyies the records.
     *
     * @returns { Void }
     */
    onMultipleRemove: function() {
        var me = this,
            grid = me.getList(),
            selModel = grid.getSelectionModel(),
            selected = selModel.getSelection();

        Ext.Msg.confirm(me.snippets.title, me.snippets.confirm.marked_remove, function(btn) {
            if(btn !== 'yes') {
                return false;
            }

            Ext.each(selected, function(item) {
                if(item.data.id > 1) {
                    item.destroy();
                }
            });

            grid.getStore().load({
                callback: function() {
                    Shopware.Notification.createGrowlMessage(me.snippets.title, me.snippets.marked_removed);
                }
            });
        });
    },

    /**
     * Event listener method which will be triggered when the user wants to
     * create a new grid.
     *
     * @returns { Void }
     */
    onCreate: function() {
        var me = this;

        me.getView('templates.Settings').create();
    },

    /**
     * Event listener method which will be triggered when the user clicks on the save
     * button in the `settings` window.
     *
     * @returns { Boolean }
     */
    onSave: function(btn) {
        var me = this,
            win = me.getSettings(),
            form = win.formPanel,
            rec = form.getRecord(),
            newRec = false;

        btn.setDisabled(true);
        if(!form.getForm().isValid()) {
            btn.setDisabled(false);
            return false;
        }

        if(rec) {
            form.getForm().updateRecord(rec);
        } else {
            rec = Ext.create('Shopware.apps.Emotion.model.Template', form.getForm().getValues());
            newRec = true;
        }

        rec.save({
            callback: function() {
                if(newRec) {
                    me.getList().getStore().add(rec);
                }
                Shopware.Notification.createGrowlMessage(me.snippets.title, Ext.String.format(me.snippets.edited, rec.get('name')));
                win.destroy();
            }
        });
    },

    /**
     * Event listener method which will be triggered when the user selects
     * one or more entries in the list.
     *
     * The method just unlocks the `delete`-button.
     *
     * @param { Array } selection - Array of the selected records
     * @returns { Void }
     */
    onSelectionChange: function(selection) {
        var me = this,
            toolbar = me.getToolbar(),
            btn = toolbar.deleteBtn,
            defaultSelected;

        Ext.each(selection, function(item) {
            if(item.data.id < 2) {
                defaultSelected = true;
                return false;
            }
        });

        btn.setDisabled(!(selection.length && !defaultSelected));
    },

    /**
     * Event listener method which will be (buffered) triggered
     * when the user inserts a search term.
     *
     * The method uses a custom `filterBy`-method to search for the
     * incoming value.
     *
     * @param { String } value - Search term
     * @returns { Void }
     */
    onSearch: function(value) {
        var me = this,
            grid = me.getList(),
            store = grid.getStore();

        if(!value.length) {
            store.clearFilter();
        } else {
            store.clearFilter(true);
            value = value.toLowerCase();
            store.filterBy(function(rec) {
                var name = rec.get('name').toLowerCase();
                return name.indexOf(value) !== -1;
            });
        }
    }
});
//<?php }} ?>