<?php /* Smarty version Smarty-3.1.12, created on 2015-05-02 08:41:46
         compiled from "/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/view/template/preview.js" */ ?>
<?php /*%%SmartyHeaderCode:950850867554471aa38e1b1-99464991%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '42ad26d9e82dd55e40689dc1cb0c700c3811e499' => 
    array (
      0 => '/home/wwwumed/www.ultra-med.de/htdocs/templates/_default/backend/config/view/template/preview.js',
      1 => 1430113360,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '950850867554471aa38e1b1-99464991',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_554471aa3a1c21_27693461',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_554471aa3a1c21_27693461')) {function content_554471aa3a1c21_27693461($_smarty_tpl) {?>/**
 * Shopware 4.0
 * Copyright © 2012 shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//

//
Ext.define('Shopware.apps.Config.view.template.Preview', {
    extend: 'Enlight.app.SubWindow',
    alias: 'widget.config-template-preview',

    height: 768,
    width: 1024,

    layout: 'fit',
    basePath: '/templates/',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Vorschau: [name]<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'template'/'preview_title','default'=>'Preview: [name]','namespace'=>'backend/config/view/form'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',

    initComponent: function() {
        var me = this;

        me.title = new Ext.Template(me.title).applyTemplate(me.template.data);

        Ext.applyIf(me, {
            items: {
                xtype: 'image',
                src: me.basePath + '/' + me.template.get('previewFull'),
                listeners : {
                    render : function(c) {
                        c.getEl().on('click', function(){ this.fireEvent('click', c); }, c);
                    }
                }
            }
        });

        me.callParent(arguments);
    }
});
//
<?php }} ?>