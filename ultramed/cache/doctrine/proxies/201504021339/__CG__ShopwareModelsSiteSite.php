<?php

namespace Shopware\Proxies\__CG__\Shopware\Models\Site;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Site extends \Shopware\Models\Site\Site implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'tpl1variable', 'tpl1path', 'tpl2variable', 'tpl2path', 'tpl3variable', 'tpl3path', 'description', 'pageTitle', 'metaKeywords', 'metaDescription', 'html', 'grouping', 'position', 'link', 'target', 'children', 'parent', 'parentId', 'attribute');
        }

        return array('__isInitialized__', 'id', 'tpl1variable', 'tpl1path', 'tpl2variable', 'tpl2path', 'tpl3variable', 'tpl3path', 'description', 'pageTitle', 'metaKeywords', 'metaDescription', 'html', 'grouping', 'position', 'link', 'target', 'children', 'parent', 'parentId', 'attribute');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Site $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl1Variable($tpl1variable)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl1Variable', array($tpl1variable));

        return parent::setTpl1Variable($tpl1variable);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl1Variable()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl1Variable', array());

        return parent::getTpl1Variable();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl2Variable($tpl2variable)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl2Variable', array($tpl2variable));

        return parent::setTpl2Variable($tpl2variable);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl2Variable()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl2Variable', array());

        return parent::getTpl2Variable();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl3Variable($tpl3variable)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl3Variable', array($tpl3variable));

        return parent::setTpl3Variable($tpl3variable);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl3Variable()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl3Variable', array());

        return parent::getTpl3Variable();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl1Path($tpl1path)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl1Path', array($tpl1path));

        return parent::setTpl1Path($tpl1path);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl1Path()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl1Path', array());

        return parent::getTpl1Path();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl2Path($tpl2path)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl2Path', array($tpl2path));

        return parent::setTpl2Path($tpl2path);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl2Path()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl2Path', array());

        return parent::getTpl2Path();
    }

    /**
     * {@inheritDoc}
     */
    public function setTpl3Path($tpl3path)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTpl3Path', array($tpl3path));

        return parent::setTpl3Path($tpl3path);
    }

    /**
     * {@inheritDoc}
     */
    public function getTpl3Path()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTpl3Path', array());

        return parent::getTpl3Path();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription($description)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescription', array($description));

        return parent::setDescription($description);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescription', array());

        return parent::getDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setHtml($html)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHtml', array($html));

        return parent::setHtml($html);
    }

    /**
     * {@inheritDoc}
     */
    public function getHtml()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHtml', array());

        return parent::getHtml();
    }

    /**
     * {@inheritDoc}
     */
    public function setGrouping($grouping)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setGrouping', array($grouping));

        return parent::setGrouping($grouping);
    }

    /**
     * {@inheritDoc}
     */
    public function getGrouping()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGrouping', array());

        return parent::getGrouping();
    }

    /**
     * {@inheritDoc}
     */
    public function setPosition($position)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPosition', array($position));

        return parent::setPosition($position);
    }

    /**
     * {@inheritDoc}
     */
    public function getPosition()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPosition', array());

        return parent::getPosition();
    }

    /**
     * {@inheritDoc}
     */
    public function setLink($link)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLink', array($link));

        return parent::setLink($link);
    }

    /**
     * {@inheritDoc}
     */
    public function getLink()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLink', array());

        return parent::getLink();
    }

    /**
     * {@inheritDoc}
     */
    public function setTarget($target)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTarget', array($target));

        return parent::setTarget($target);
    }

    /**
     * {@inheritDoc}
     */
    public function getTarget()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTarget', array());

        return parent::getTarget();
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParent', array());

        return parent::getParent();
    }

    /**
     * {@inheritDoc}
     */
    public function setParent($parent)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setParent', array($parent));

        return parent::setParent($parent);
    }

    /**
     * {@inheritDoc}
     */
    public function getChildren()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChildren', array());

        return parent::getChildren();
    }

    /**
     * {@inheritDoc}
     */
    public function setChildren($children)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChildren', array($children));

        return parent::setChildren($children);
    }

    /**
     * {@inheritDoc}
     */
    public function getParentId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParentId', array());

        return parent::getParentId();
    }

    /**
     * {@inheritDoc}
     */
    public function setParentId($parentId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setParentId', array($parentId));

        return parent::setParentId($parentId);
    }

    /**
     * {@inheritDoc}
     */
    public function getAttribute()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAttribute', array());

        return parent::getAttribute();
    }

    /**
     * {@inheritDoc}
     */
    public function setAttribute($attribute)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAttribute', array($attribute));

        return parent::setAttribute($attribute);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaDescription($metaDescription)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMetaDescription', array($metaDescription));

        return parent::setMetaDescription($metaDescription);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaDescription()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMetaDescription', array());

        return parent::getMetaDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaKeywords($metaKeywords)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMetaKeywords', array($metaKeywords));

        return parent::setMetaKeywords($metaKeywords);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaKeywords()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMetaKeywords', array());

        return parent::getMetaKeywords();
    }

    /**
     * {@inheritDoc}
     */
    public function setPageTitle($pageTitle)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPageTitle', array($pageTitle));

        return parent::setPageTitle($pageTitle);
    }

    /**
     * {@inheritDoc}
     */
    public function getPageTitle()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPageTitle', array());

        return parent::getPageTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function fromArray(array $array = array (
))
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'fromArray', array($array));

        return parent::fromArray($array);
    }

    /**
     * {@inheritDoc}
     */
    public function setOneToOne($data, $model, $property, $reference = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOneToOne', array($data, $model, $property, $reference));

        return parent::setOneToOne($data, $model, $property, $reference);
    }

    /**
     * {@inheritDoc}
     */
    public function setOneToMany($data, $model, $property, $reference = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOneToMany', array($data, $model, $property, $reference));

        return parent::setOneToMany($data, $model, $property, $reference);
    }

    /**
     * {@inheritDoc}
     */
    public function setManyToOne($data, $model, $property)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setManyToOne', array($data, $model, $property));

        return parent::setManyToOne($data, $model, $property);
    }

}
